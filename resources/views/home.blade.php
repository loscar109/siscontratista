@extends('layouts.admin')
<link rel="stylesheet" href="{{asset('css/preview.css')}}">


@section('titulo')
<div class="box-header" style="text-align: center">
    <a href ={{ url()->previous() }}>
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>    
    <h4 class = "box-title">Bienvenido <b> {{ Auth::user()->nameComplete() }}</b></h4>

    <div class="box-tools pull-right">     
        <p>Fecha de Hoy: <strong>{{Carbon\Carbon::parse(Carbon\Carbon::now())->formatLocalized('%A %d %B %Y')}}</strong></p>
    </div>
</div>
@endsection

@section('content')
@if (auth()->user()->nameRoleUser()=="EMPLEADO")
<div class="box-body">
  @include('works.mensaje')

    <!-- Pertenece al estado CREADO -->
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    @if (array_key_exists(1, $allTicket))
                        {{count($allTicket[1])}}
                    @else
                        0
                    @endif
                </h3>
                <p>TICKET NUEVOS</p>
            </div>
            <div class="icon">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            </div>
                <a class="small-box-footer" href="{{asset('/works/created')}}">
                    <i class="fa fa-arrow-circle-right">
                        Ver Detalle
                    </i>
                </a>
        </div>
    </div>

    

    <!-- pertenece a los estados  REVISADO, EN PROCESO Y REPROGRAMADO -->
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>

                    <!-- REVISADO + EN PROCESO + REPROGRAMADO -->
                    @if ( array_key_exists(2, $allTicket) && array_key_exists(3, $allTicket) && array_key_exists(6, $allTicket) )
                        {{ count($allTicket[2] + $allTicket[3] + $allTicket[6]) }}

                    <!-- REVISADO  -->
                    @elseif ( array_key_exists(2, $allTicket) )
                        {{ count($allTicket[2])}}

                    <!-- REPGROGRAMADO  -->
                    @elseif ( array_key_exists(6, $allTicket) )
                        {{ count($allTicket[6])}}

                    <!-- EN PROCESO  -->
                    @elseif ( array_key_exists(3, $allTicket) )
                        {{ count($allTicket[3])}}
                    
                    <!-- REVISADO + EN PROCESO -->
                    @elseif ( array_key_exists(2, $allTicket) && array_key_exists(3, $allTicket))
                        {{ count($allTicket[2] + $allTicket[3]) }}

                    <!-- REVISADO + REPROGRAMADO -->
                    @elseif ( array_key_exists(2, $allTicket) && array_key_exists(6, $allTicket))
                        {{ count($allTicket[2] + $allTicket[6]) }}


                    <!-- REPROGRAMADO + EN PROCESO -->
                    @elseif ( array_key_exists(3, $allTicket) && array_key_exists(6, $allTicket))
                        {{ count($allTicket[3] + $allTicket[6]) }}

                    @else
                        0
                    @endif
                </h3>
                <p>TICKET ABIERTOS</p>
            </div>
            <div class="icon">
                <i class="fas fa-bolt"></i>
            </div>
            <a class="small-box-footer" href="{{asset('/works/inProccess')}}">
                <i class="fa fa-arrow-circle-right">
                    Ver Detalle
                </i>
            </a>
        </div>
    </div>

  <!-- INICIO -->
    @if($resolved_tickets->isNotEmpty() || $no_resolved_tickets->isNotEmpty())

        <div class ="box-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-solid ">
                    <div class="box-header with-border" style="text-align: center">
                        <h4 class="box-title">
                            @if(now()->month == 1)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Enero de {{ now()->year }}
                            @endif
                            @if(now()->month == 2)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Febrero {{ now()->year }}
                            @endif
                            @if(now()->month == 3)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Marzo {{ now()->year }}
                            @endif      
                            @if(now()->month == 4)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Abril {{ now()->year }}
                            @endif
                            @if(now()->month == 5)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Mayo {{ now()->year }}
                            @endif
                            @if(now()->month == 6)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Junio {{ now()->year }}
                            @endif 
                            @if(now()->month == 7)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Julio {{ now()->year }}
                            @endif     
                            @if(now()->month == 8)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Agosto {{ now()->year }}
                            @endif     
                            @if(now()->month == 9)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Septiembre {{ now()->year }}
                            @endif              
                            @if(now()->month == 10)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Octubre {{ now()->year }}
                            @endif
                            @if(now()->month == 11)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Noviembre {{ now()->year }}
                            @endif
                            @if(now()->month == 12)
                                <i class="fa fa-tasks" aria-hidden="true"></i> Tickets del mes de Diciembre {{ now()->year }}
                            @endif                  
                        </h4>
                    </div>
                    <div class="box-body"> 
                        @if( $resolved_tickets->isNotEmpty() )
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                                <div class="box box-solid box-default">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <i class="fa fa-tasks" aria-hidden="true"></i> Tickets Resueltos
                                        </h4>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="divDetalle" class="table-responsive">
                                                <table id="tablaDetalle" class="table table-bordered table-condensed table-hover">
                                                    <thead style="background-color:#357CA5">
                                                        <tr>
                                                            <th width="10%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Codigo</p>
                                                            </th>
                                                            <th width="25%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Ticket</p>
                                                            </th> 
                                                            <th width="20%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Fecha de Creación</p>
                                                            </th> 
                                                            <th width="15%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Dias Transcurridos</p>
                                                            </th> 
                                                            <th width="30%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Fecha de Cierre (Resuelto)</p>
                                                            </th>                                       
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ( $resolved_tickets as $resolved_ticket )
                                                            <tr> 
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $resolved_ticket->number }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                        <i class="fa fa-check" style="color:green;"></i> {{ $resolved_ticket->description }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $resolved_ticket->created_at->format('d/m/Y') }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                        {{$resolved_ticket->time_resolved }}

                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $resolved_ticket->updated_at->format('d/m/Y') }}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        @endforeach 
                                                    </tbody>               
                                                </table>
                                            </div>                 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        @endif 
                        @if( $no_resolved_tickets->isNotEmpty() )
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-right">
                                <div class="box box-solid box-default">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <i class="fa fa-tasks" aria-hidden="true"></i> Tickets Cancelados
                                        </h4>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                                
                                            <div id="divDetalle" class="table-responsive">
                                                <table id="tablaDetalle2" class="table table-bordered table-condensed table-hover">
                                                    <thead style="background-color:#357CA5">
                                                        <tr>
                                                            <th width="10%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Codigo</p>
                                                            </th>
                                                            <th width="25%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Ticket</p>
                                                            </th> 
                                                            <th width="20%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Fecha de Creación</p>
                                                            </th> 
                                                            <th width="15%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Dias Transcurridos</p>
                                                            </th> 
                                                            <th width="30%" style="color:#FFFFFF" height="25px">
                                                                <p class="text-uppercase">Fecha de Cierre (Cancelado)</p>
                                                            </th>                 
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ( $no_resolved_tickets as $no_resolved_ticket )
                                                        

                                                            <tr> 
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $no_resolved_ticket->number }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                        <i class="fa fa-close" style="color:red;"></i> {{ $no_resolved_ticket->description }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $no_resolved_ticket->created_at->format('d/m/Y') }}
                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                        {{$no_resolved_ticket->time_not_resolved }}

                                                                    </p>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <p class="text-uppercase">
                                                                         {{ $no_resolved_ticket->updated_at->format('d/m/Y') }}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        @endforeach 
                                                    </tbody>               
                                                </table>
                                            </div>       
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        @endif 

                    </div>
                </div>
            </div>    
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align='center'>
            <div class="box box-solid" align='center'>
                <div class="box-header with-border" align='center'>
                    <h4 class="box-title" align='center'>
                        <i class="fa fa-pie-chart" aria-hidden="true"></i>  Gráfico
                    </h4>
                </div>
                <div class="box-body" align='center'>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div id="pie_today_div" align='right'></div>
                    </div> 
                    @if($resolved_tickets->isNotEmpty())
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div id="columna"align='left'></div>      
                        </div> 
                    @endif
                </div>
            </div>
        </div>
    @endif



   <!-- FIN --> 
    
</div>
@endif

  
@if (auth()->user()->nameRoleUser()=="ADMIN" || auth()->user()->nameRoleUser()=="JEFE")
    <div class="row">
        @if($tabla->isNotEmpty())
            <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
              <div id="columnchart_values" class="panel panel-primary"></div>
            </div>
            <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
                <h3> Stock Crítico </h3>
                <div class="panel panel-primary">
                    <table  class="table table-bordered table-condensed table-hover">
                    <thead style="background-color:#A9D0F5">
                        <th>Materiales</th>
                        <th>Stock Mínimo</th>
                        <th>Stock Actual</th>
                        <th>Stock Faltante</th>

                    </thead>
                    <tbody>
                        @foreach ($tabla as $t)
                        <tr> 
                            <td class="text-left" style="font-size:110%">{{ $t->description }} </td>
                            <td class="text-right" style="font-size:110%"><b>{{ $t->smin }}</b> </td>
                            <td class="text-right"><span class="label label-primary" style="font-size:100%">{{$t->stock}}</span></td>       
                            <td class="text-right"> 
                                <span class="label label-danger" style="font-size:100%">{{$t->smin - $t->stock}}</span>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                    </table>          
                </div>
            </div>
        @else

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Todo en orden</span>
                    <span class="info-box-number">No hay Stock Crítico</span>
                  </div>
                </div>
              </div>
        
        </div>
        @endif
    
            
          </div> 




    @endif
    


@push('scripts')   
<script src="{{asset('js/loader.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
$(document).ready(function() {

    $('#tablaDetalle').DataTable({
        "aaSorting":[],
        "language":{
            "info":"_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next":"Siguiente",
                "previous":"Anterior"
            },
            "lengthMenu":'Mostrar <select>'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<select> registros',
            "loadingRecords":"Cargando...",
            "processing":"Procesando...",
            "emptyTable":"No hay datos",
            "zeroRecords":"No hay coincidencias",
            "infoEmpty":"",
            "infoFiltered":""

        },
        "pageLength" : 5,
        "lengthMenu": "[[5, 10], [5, 10]]"
    });

    $('#tablaDetalle2').DataTable({
        "aaSorting":[],
        "language":{
            "info":"_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next":"Siguiente",
                "previous":"Anterior"
            },
            "lengthMenu":'Mostrar <select>'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<select> registros',
            "loadingRecords":"Cargando...",
            "processing":"Procesando...",
            "emptyTable":"No hay datos",
            "zeroRecords":"No hay coincidencias",
            "infoEmpty":"",
            "infoFiltered":""

        },
        "pageLength" : 5,
        "lengthMenu": "[[5, 10], [5, 10]]"
    });








});


</script>
  
    <script type="text/javascript">
            google.charts.load("current", {packages:['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
              var data = google.visualization.arrayToDataTable([
                ["Elemento", "Stock actual", "Faltante"],
                    @foreach ($grafico as $g)
                    ['{{ $g->description }}', {{ $g->stock }}, {{ ($g->smin-$g->stock) }}],
                    @endforeach                          
              ]);
        
              var view = new google.visualization.DataView(data);
              view.setColumns([0,
                               1,
                               { calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation",
                                },
                               2,
                               { calc: "stringify",
                                 sourceColumn: 2,
                                 type: "string",
                                 role: "annotation"
                                },
                                
                                 
                                 ]);
              var options = {
                title: "Top 5 de materiales en niveles criticos",
                width: 800, //400
                height: 800, //600
                bar: {groupWidth: "95%"},
                legend: false,
                colors: ["#3C8DBC",'#DD4B39'],
                vAxis: {title : 'Proporcion del Stock actual en relación al mínimo'},

                isStacked: 'percent',
              };
              var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
              chart.draw(view, options);
            } 
        </script>

        <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Resueltos', {{ $count_resolved_tickets }}],
                ['Cancelados', {{ $count_no_resolved_tickets }}],
            ]);
            

            var options = {
            title: 'Recuento de Tickets (Resueltos y No Resueltos) del último mes',
            width: 800, //400
            height: 600, //600
            legend: false,
            colors: ["#3C8DBC",'#DD4B39']
            };

            var chart = new google.visualization.PieChart(document.getElementById('pie_today_div'));
            chart.draw(data, options);
        }
        </script>


    <script type="text/javascript">

        google.charts.load("current", {packages:['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
          var data = google.visualization.arrayToDataTable([
            ["Dias", "Cantidad de Ticket"],
                @foreach ($days_agruped as $key=>$value)
                    [ {{ $key }}, {{ $value }} ],
                @endforeach  
            ]); 

            var view = new google.visualization.DataView(data);
            

            var options = {
                title: "Cantidad de Tickets Resueltos medido en días del último mes",
                width: 800,
                height: 600,
                legend: { position: "none" },
                vAxis: {title : 'Cantidad de Tickets',format: '0'},
                hAxis : {title : 'Dias',format: '0'},
                colors: ["#3C8DBC","#DD4B39"]
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columna"));
            chart.draw(view, options);
        }




   
   
   </script>
   @endpush
@endsection