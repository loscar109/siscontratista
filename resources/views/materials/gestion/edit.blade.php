@extends('layouts.admin')

@section('titulo')

    <h4 class="box-title">Editar Ticket: <strong>{{$tickets->description}}</strong></h4>

@endsection

@section('content')
    <div>     
        <a href="{{asset('/tickets/gestion')}}"><button title="atras" class="btn btn-default btn-responsive">
            <i class="fa fa-arrow-left"> Atras</i> </button>
        </a>   
    </div>
    <br>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @include('errors.request')
   
        </div>
    </div>

           @can('tickets.gestion.edit') 
            {!!Form::model($tickets, [
                'method'=>'PATCH',
                'route'=>['tickets.gestion.update',$tickets->id]
            ])!!}
            @endcan
            

            {{Form::token()}}

            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                        <label for="description" class="control-label">Descripción</label>
                            <input 
                                name="description"
                                type="text"
                                minlength="13"
                                maxlength="30"
                                class="form-control"
                                value="{{ $tickets->description }}"
                                required autofocus>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>
                                Categoria
                            </label>
                            <select
                                id="ticket_category_id"
                                name="ticket_category_id"
                                class="form-control">
                                    @foreach ($ticket_categories as $ticket_category)
                                        @if ($ticket_category->id==$tickets->ticket_category_id)
                                            <option value="{{$ticket_category->id}}" selected>{{$ticket_category->name}}</option> 
                                        @else
                                             <option value="{{$ticket_category->id}}">{{$ticket_category->name}}</option>                                                
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                
            

        
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <button class="btn btn-danger" type="reset">Cancelar</button>
                    </div>
                </div>
            </div>
        {!!Form::close()!!}

    @push('scripts')     
        <script type="text/javascript">    
            $(document).ready(function(){
                $("#ticket_category_id").select2({
                    placeholder:'-Seleccione una categoria de ticket-',
                    width: '100%',
                });
            });
        </script>
    @endpush


@endsection