
{!! Form::model(Request::only(
    ['comprobante', 'desde', 'hasta']),
    ['url' => 'materials/gestion/movimientos/'.$material->id, 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search'] 
    
    )!!}





    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
        <label for="current_stage_id">Comprobante</label>
        <div class="form-group">
                <select 
                    name="comprobante"
                    id="comprobante"
                    class="comprobante form-control"
                    >
                    <option 
                        value="0" 
                        disabled="true" 
                        selected="true"
                        title="-Seleccione un comprobante-"
                    >
                    -Seleccione un comprobante-
                    </option>
                        <option value='Ticket'@if($comprobante!=null && $comprobante=='Ticket')selected @endif>Ticket
                        <option value='Remito'@if($comprobante!=null && $comprobante=='Remito')selected @endif>Remito
                        <option value='Carga inicial'@if($comprobante!=null && $comprobante=='Carga inicial')selected @endif>Carga inicial
                        <option value=0>-- Todos los comprobantes --
                    </option>
                       
                </select>
        </div>
    </div>

    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
        <label for="desde">Fecha Desde</label>
        <input
            type="date"
            name="desde"
            id="desde"
            class="fecha form-control"
            value="{{$desde}}"
        >        
    </div>    
    
    
    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
        <label for="hasta">Fecha Hasta</label>
        <input
            type="date"
            name="hasta"
            id="hasta"
            class="fecha form-control"
            value="{{$hasta}}"
        >       
    </div>

    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
        <label for=""></label>
        <div class="form-group">
            <span class="input-group-btn">
                <button
                    title="buscar"
                    type="submit"
                    id="bt_add"
                    name="filtrar"
                    class="btn btn-primary btn-responsive">
                        <i class="fa fa-filter"></i> Filtrar
                </button>        
            </span>
        </div>
    </div>
    






{{Form::close()}}

@push('scripts')     
<script type="text/javascript">
    $(document).ready(function(){
        $("#comprobante").select2({
            width: '100%', 
            placeholder: '-Seleccione un comprobante-',
             
        });

 //si existe un cambio en Fecha Desde
    $('#desde').change(function() {
        
        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {
      
        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () { 
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);
       
 
      });


    });
        

    
</script>
@endpush
