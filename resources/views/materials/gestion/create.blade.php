@extends('layouts.admin')
<style>
        #preview {
        
        padding:5px;
        border-radius:2px;
        background:#fff;
        max-width:460px;
        max-height:460px;
    
        }
    
        #preview img {width:100%;display:block;   border:1px solid #ddd;;}
    </style>
@section('titulo')
    <div class="box-header" style="text-align: center">
        <a href="{{asset('/materials/gestion')}}">
            <button title="atras" class="btn btn-default btn-responsive pull-left">
                <i class="fa fa-arrow-left"></i> Atras
            </button>
        </a>     
    </div>
@endsection

@section('content')
<div class ="box-body">
    @include('errors.request')
    @include('materials.gestion.mensaje')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-box"></i>Crear Material
                </h4>
            </div>

            {!!Form::open(array(
                'url'=>'materials/gestion',
                'method'=>'POST',
                'autocomplete'=>'off',
                'files' => true,
            ))!!}

            {{Form::token()}}
            <div class="box-body">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="form-group">
                        <label for="description">Nombre</label>
                        <input 
                            type="string"
                            name="description"
                            minlength="6"
                            maxlength="30"
                            required value="{{old('description')}}"
                            class="form-control"
                            placeholder="nombre..."
                            title="Introduzca una descripcion para el material a crear"
                            >
                    </div>

                    <div class="form-group">   
                        <label>Categoría</label>
                        <select 
                            name="category_id"
                            id="category_id"
                            class="category_id form-control"
                            required
                            >
                            <option 
                                value="0" 
                                disabled="true" 
                                selected="true"
                                title="-Seleccione una categoria-"
                                >
                                -Seleccione una categoria-
                            </option>
                            @foreach ($categories as $category)
                                <option 
                                    value="{{$category->id }}">
                                        {{$category->description}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
                        <label for="stock" class="control-label">Stock</label>
                            <input id="stock"  type="number" class="form-control"  name="stock" value="{{ old('stock') }}" required autofocus>
                                @if ($errors->has('stock'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stock') }}</strong>
                                    </span>
                                @endif
                    </div>
        
                    <div class="form-group{{ $errors->has('smin') ? ' has-error' : '' }}">
                        <label for="smin" class="control-label">Stock Minimo</label>
                            <input id="smin"  type="number" class="form-control"  name="smin" value="{{ old('smin') }}" required autofocus>
                                @if ($errors->has('smin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('smin') }}</strong>
                                    </span>
                                @endif
                    </div>

                </div>

                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="form-group{{ $errors->has('smax') ? ' has-error' : '' }}">
                        <label for="smax" class="control-label">Stock Maximo</label>
                            <input id="smax" type="number" class="form-control"  name="smax" value="{{ old('smax') }}" required autofocus>
                                @if ($errors->has('smax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('smax') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('bundle_name') ? ' has-error' : '' }}">
                        <label for="bundle_name" class="control-label">Nombre del Paquete</label>
                            <input id="bundle_name" minlength="6" maxlength="30" type="text" class="form-control"  name="bundle_name" value="{{ old('bundle_name') }}" required autofocus>
                                @if ($errors->has('bundle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bundle_name') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('bundle_quantity') ? ' has-error' : '' }}">
                        <label for="bundle_quantity" class="control-label">Cantidad por Paquete</label>
                            <input id="bundle_quantity" type="number" class="form-control"  name="bundle_quantity" value="{{ old('bundle_quantity') }}" required autofocus>
                                @if ($errors->has('bundle_quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bundle_quantity') }}</strong>
                                    </span>
                                @endif
                    </div>
        
            
                    <label for="photo">Imagen</label>
                        <input
                            id="file"
                            type="file"
                            name="photo"
                            class="img-responsive"
                            >

                </div>

                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <div class="form-group" >
                        <hr>
                        <div id="preview"></div>
                    </div>
                </div>
            </div>


        </div>

            
    <div class="container">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
            <div class="form-group">
                <button title="Limpiar" class="btn btn-danger btn-lg btn-block" type="reset"><i class="fa fa-eraser">...</i> Limpiar</button>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
            <div class="form-group">
                <button title="Guardar" class="btn btn-success btn-lg btn-block" type="submit"> <i class="fa fa-check"></i> Guardar</button>
            </div>
        </div>
    </div>
          

    {!!Form::close()!!}
           
                


                







    @push('scripts')     
    <script type="text/javascript">
    document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }
     $(document).ready(function(){
        $("#category_id").select2({
            placeholder:'-Seleccione una categoria-',
            width: '100%'
        });


       
    
       

       


     });
    </script>
    @endpush

@endsection


