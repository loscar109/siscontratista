
{!! Form::model(Request::only(
    ['category_id']),
    ['url' => 'materials/gestion', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search'] 
    
    )!!}




<div class="row">



    <div class="col-lg-7 col-sm-9 col-md-8 col-xs-10 ">
        <div class="form-group">

                <select 
                    name="category_id"
                    id="category_id"
                    class="category_id form-control"
                    >
                    <option 
                        value="0" 
                        disabled="true" 
                        selected="true"
                        title="-Seleccione una categoria-"
                    >
                    -Seleccione una categoria-
                    </option>
                    @foreach ($categories as $category)
                        <option 
                            value="{{$category->id}}"
                            @if($category_id!=null && $category_id==$category->id)
                                selected
                            @endif
                            >
                            {{$category->description}}
                        </option>
                    @endforeach
                       
                </select>
        </div>
    </div>



    <div class="col">
        <div class="form-group">
            <span class="input-group-btn">
                <button
                    title="buscar"
                    type="submit"
                    class="btn btn-primary btn-responsive">
                        <i class="fa fa-filter"></i> Filtrar
                </button>
               
            </span>
        </div>
    </div>


</div>



{{Form::close()}}

@push('scripts')     
<script type="text/javascript">
    $(document).ready(function(){
        $("#category_id").select2({
            width: '100%', 
            placeholder: '-Seleccione una categoria-',
            

    
        });
    });
        

    
</script>
@endpush
