@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
   
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('materials.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"> </i> Indice de Materiales
                </h4>
                <div class="box-tools">
                    <a href="gestion/create">
                        <button class="btn btn-default">
                            <i class="fa fa-tasks"> <sup> <i class="fa fa-plus"></i></sup></i> Nuevo
                        </button>
                    </a>       
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">

                            @include('materials.gestion.search')

                           
                                
                        </div>
                        
                    </div>
                    @if($materials->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr class="text-uppercase">
                                    <th width="20%" style="color:#FFFFFF" height="25px">Material</th>
                                    <th width="5%" style="color:#FFFFFF" height="25px">Cantidad</th>
                                    <th width="5%" style="color:#FFFFFF" height="25px">Stock Minimo</th>
                                    <th width="5%" style="color:#FFFFFF" height="25px">Stock Maximo</th>
                                    <th width="20%" style="color:#FFFFFF" height="25px">Nombre del empaquetado</th>
                                    <th width="5%" style="color:#FFFFFF" height="25px">Cantidad por paquete</th>

                                    <th width="20%" style="color:#FFFFFF" height="25px">Imagen</th>
                                    <th width="20%" style="color:#FFFFFF" height="25px">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($materials as $material)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td><p class="text-uppercase">{{$material->description }}</p></td>
                                        <td class="text-right"><p class="text-uppercase">{{$material->stock }}</p></td>
                                        <td class="text-right"><p class="text-uppercase">{{$material->smin }}</p></td>
                                        <td class="text-right"><p class="text-uppercase">{{$material->smax }}</p></td>
                                        <td class="text-left"><p class="text-uppercase">{{$material->bundle_name }}</p></td>
                                        <td class="text-right"><p class="text-uppercase">{{$material->bundle_quantity }}</p></td>

                                        <td>
                                            <img src="{{asset('imagenes/materiales/'.$material->photo)}}"
                                            alt="Sin imagen"
                                            class="img-thumbnail"
                                            height="100px"
                                            width="100px"
                                            class="img-responsive"
                                            >
                                        </td>
                                        <td>
                                            <a 
                                            href= "{{URL::action('MaterialController@movimientos',$material->id)}}"
                                            class="btn btn-primary"
                                            >
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                            Ver Movimiento
                    
                                        </a>      


                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay Materiales registrados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row text-center">
    <div class="col">
        @if (count($materials)> 0)
            @if (isset($_GET['category_id']))   
                <a 
                    href="{{ route('material.pdf', ['category_id'=>$_GET['category_id']] ) }}"
                    class="btn btn-danger">
                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                    Descargar Todos los materiales filtrados

                </a>
            @else
                <a 
                    href="{{ route('material.pdf') }}"
                    class="btn btn-danger"
                    target="_blank">
                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                    Descargar Todos los materiales

                </a>
            @endif
        @endif
        
    </div>
</div>



@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            
            
            $('#tablaDetalle').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection


