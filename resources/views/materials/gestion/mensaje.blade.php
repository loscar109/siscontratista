@if (Session::has('delete_material_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_material_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_material'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_material') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_material'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_material') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_material'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_material') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif