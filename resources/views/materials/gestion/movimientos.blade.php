@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
   
@endsection
@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"> </i> Movimiento
                </h4>
            </div>
            <div class="box-body">
                <div class="text-center">
                    <img src="{{asset('imagenes/materiales/'.$material->photo)}}"
                        alt="Sin imagen"
                        class="img-thumbnail"
                        height="100px"
                        width="150px"
                        class="img-responsive"
                        >

                <p><b>{{ $material->description }}</b></p>
            </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">

                            @include('materials.gestion.search2') 

                           
                                
                        </div>
                        
                    </div>
                </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    @if($movimientos->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr class="text-uppercase">
                                    <th style="color:#FFFFFF" height="25px">Fecha</th>
                                    <th style="color:#FFFFFF" height="25px">Tipo de Comprobante</th>
                                    <th style="color:#FFFFFF" height="25px">Numero</th>
                                    <th style="color:#FFFFFF" height="25px">Cantidad</th>
                                    <th style="color:#FFFFFF" height="25px">Stock</th>

                                    <th style="color:#FFFFFF" height="25px">Usuario</th>

                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($movimientos as $m)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td>{{ Carbon\Carbon::parse($m->date)->format('d/m/Y') }}</td>
                                        <td><p class="text-uppercase">{{$m->comprobante }}</p></td>
                                        <td class="text-right"><p class="text-uppercase">{{$m->number }}</p></td>
                                        <td class="text-right">
                                            @if ($m->quantity<0 )
                                                <b style="color:red">{{$m->quantity }}</b>
                                            @else
                                                <b>{{$m->quantity }}</b>

                                            @endif
                                        </td>
                                        <td class="text-right"><p class="text-uppercase">{{$m->stock }}</p></td>

                                        <td><p class="text-uppercase">{{$m->user->name }}</p></td>

                                    </tr>
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay movimientos registrados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            
            
            $('#tablaDetalle').DataTable({
                "aaSorting":[],

                "language":{

                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection
