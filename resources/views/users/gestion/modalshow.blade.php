{{--ventanita modal cuando se haga clic en ver--}}
<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-show-{{$us->id}}">



    {{Form::Open(array(
        'action'=>array('UserController@show',$us->id),
        'method'=>'get'
        ))}}



        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">
                
                <!-- cabecera del modal -->
                <div class="modal-header" style="background-color:#357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Detalles del Usuario <b>{{$us->usname}}</b></h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                @if ($us->usimagen == NULL)
                                    <img 
                                        src="{{asset('imagenes/users/default.png')}}"
                                        alt="nada que mostrar"
                                        class="img-thumbnail"
                                        height="250px"
                                        width="250px"
                                        class="img-responsive"
                                    > 
                                @else
                                    <img 
                                        src="{{asset('imagenes/users/'.$us->usimagen)}}"
                                        alt="nada que mostrar"
                                        class="img-thumbnail"
                                        height="250px"
                                        width="250px"
                                        class="img-responsive"
                                >
                                @endif
                               
                            </div>
                            <div class="col-md-8 ml-auto">
                                    <p>Ficha Técnica</p>
                                    <ul class="list-group">
                                        <li>Nombre y Apellido : {{$us->usname }}</li>
                                        <li>E-mail : {{$us->usemail }}</li>
                                        <li>Rol : {{$us->rolname }}</li>
                                    </ul>                   
                            </div>
                        </div>                     
                    </div>
                </div>

                <!--pie del modal-->
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12 ml-auto">
                                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">
                                <i class="fa fa-close"> Cerrar</i>
                            </button>
                            @include('errors.request')
                        </div>
                    </div>
                </div>
               
            
            </div>    
        </div>

    {{Form::Close()}}

</div>

