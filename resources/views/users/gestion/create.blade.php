@extends('layouts.admin')
<style>
    #preview {
    
    padding:5px;
    border-radius:2px;
    background:#fff;
    max-width:640px;
    max-height:480px;

    }

    #preview img {width:100%;display:block;   border:1px solid #ddd;;}
</style>
@section('titulo')

<div class="box-header" style="text-align: center">
    <a href="{{asset('/home')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>    
    
</div>
@endsection

@section('content')
<div class ="box-body">
    @include('errors.request')
    @include('users.gestion.mensaje')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-user"></i>Crear Usuario
                </h4>
            </div>

        {!!Form::open(array(
            'url'=>'users/gestion',
            'method'=>'POST',
            'autocomplete'=>'off',
            'files' => true,
        ))!!}

        {{Form::token()}}
        <div class="box-body">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">        
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="control-label">Nombre</label>
                        <input id="name" minlength="6" maxlength="30" type="text" class="form-control"  name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label for="surname" class="control-label">Apellido</label>
                        <input id="surname" minlength="6" maxlength="30" type="text" class="form-control"  name="surname" value="{{ old('surname') }}" required autofocus>
                            @if ($errors->has('surname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label">E-Mail</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Constraseña</label>
                        <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif   
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="control-label">Confirmar Constraseña</label>         
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>    
                </div>


            </div>       

            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                <div class="form-group">
                    <label>       
                        Pais
                    </label>
                    <select 
                        name="country_id"
                        id="country_id"
                        class="country_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione un pais"
                            >
                            -Seleccione un pais-
                        </option>
                        @foreach ($countries as $country)
                            <option 
                                value="{{$country->id }}">
                                    {{$country->name}}
                            </option>
                        @endforeach
                    </select>
                    <br>
                    <br>
                    <label>
                        Provincia
                    </label>
                    <select
                        name="province_id"
                        id="province_id"
                        class="province_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione una provincia"
                            >
                            -Seleccione una provincia-
                        </option>             
                    </select>
                    <br>
                    <br>
                    <label>
                        Ciudad
                    </label>
                    <select
                        name="city_id"
                        id="city_id"
                        class="city_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione una ciudad"
                            >
                            -Seleccione una ciudad-
                        </option>              
                    </select>
                    <br>
                    <br>
                    <label>
                        Barrio
                    </label>
                    <select
                        name="district_id"
                        id="district_id"
                        class="district_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione un barrio"
                            >
                            -Seleccione un barrio-
                        </option>              
                    </select>
                    <br>
                </div>
                <div class="form-group">
                        <label for="address">
                            Direccion
                        </label>
                        <input 
                            type="text"
                            name="address"
                            required value="{{old('address')}}"
                            class="form-control"
                            placeholder="Calle/Av/Numero..."
                            title="Introduzca la direccion"
                            >
                    </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                <div class="form-group {{ $errors->has('imagen') ? 'has-error' : '' }} ">
                    <label for="imagen">Imagen</label>
                        <input
                            id="file"
                            type="file"
                            name="imagen"
                            class="img-responsive"
                            >
                </div>
                <hr>
                <div id="preview"></div>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                <h3>Lista de Roles</h3>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($roles as $role)
                                <li>
                                    <label>
                                        {{ Form::radio('roles[]', $role->id, null) }}
                                        {{ $role->name }}
                                        <em>({{ $role->description ?: 'Sin descripcion'}})</em>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
        <div class="form-group">
            <button title="Limpiar" class="btn btn-danger btn-lg btn-block" type="reset"><i class="fa fa-eraser">...</i> Limpiar</button>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
        <div class="form-group">
            <button title="Guardar" class="btn btn-success btn-lg btn-block" type="submit"> <i class="fa fa-check"></i> Guardar</button>
        </div>
    </div>
</div>
{!!Form::close()!!}

    @push('scripts')     
    <script type="text/javascript">

        

        document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }

        $(document).ready(function(){
                $("#country_id").select2({
                    placeholder:'-Seleccione un pais-',
                    width:'100%', 
                    height:'100%',           
                });

                $("#province_id").select2({
                    placeholder:'-Seleccione una provincia-',
                    width:'100%', 
                    height:'100%',  
                });

                $("#city_id").select2({
                    placeholder:'-Seleccione una localidad-',
                    width:'100%', 
                    height:'100%',  
                });

                $("#district_id").select2({
                    placeholder:'-Seleccione un barrio-',
                    width:'100%', 
                    height:'100%',  
                });

                $(document).on('change','.country_id',function(){
                    console.log("hmm its change");
                    var country_id=$(this).val();
                    console.log(country_id);
                    var div=$(this).parent();    
                    var op=" ";
                    


                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('users/gestion/create/findProvince')!!}',
                        data:{'id':country_id},
                        success:function(data){
                            console.log('success');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            div.find('.province_id').html(" ");
                            div.find('.province_id').append(op);
                        },
                        error:function(){    
                        }
                    });
                });


                $(document).on('change','.province_id',function(){
                    console.log("hmm its change");
                    var province_id=$(this).val();
                    console.log(province_id);
                    var div=$(this).parent();    
                    var op=" ";
                    


                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('users/gestion/create/findCity')!!}',
                        data:{'id':province_id},
                        success:function(data){
                            console.log('success');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            div.find('.city_id').html(" ");
                            div.find('.city_id').append(op);
                        },
                        error:function(){    
                        }
                    });
                });

                $(document).on('change','.city_id',function(){
                    console.log("hmm its change");
                    var city_id=$(this).val();
                    console.log(city_id);
                    var div=$(this).parent();    
                    var op=" ";
                    


                    $.ajax({
                        type:'get',
                        url:'{!!URL::to('users/gestion/create/findDistrict')!!}',
                        data:{'id':city_id},
                        success:function(data){
                            console.log('success');
                            console.log(data);
                            console.log(data.length);
                            op+='<option value="0" selected disabled>-Seleccione un barrio-</option>';
                            for(var i=0;i<data.length;i++){
                                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            }
                            div.find('.district_id').html(" ");
                            div.find('.district_id').append(op);
                        },
                        error:function(){    
                        }
                    });
                });



            });       

            
       

        

       

    </script>
@endpush

@endsection