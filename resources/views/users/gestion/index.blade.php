@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align: center">
    <a href="{{asset('/home')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>    
    
</div>
@endsection

@section('content')
<div class ="box-body">
    @include('users.gestion.mensaje')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-user"></i>Indice de Empleados
                </h4>
                <div class="box-tools">
                    <a href="gestion/create">
                        <button class="btn btn-default btn-responsive pull-right">
                            <i class="fas fa-user-plus"> </i> Nuevo
                        </button>    
                    </a>
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if( $users->isNotEmpty() )
                        <div id="divDetalle" class="table-responsive">
                            <table id="tablaDetalle" class="table table-bordered table-condensed table-hover">
                                <thead style="background-color:#357CA5">
                                    <tr>
                                        <th width="20%" style="color:#FFFFFF" height="25px">
                                            <p class="text-uppercase">Usuario</p>
                                        </th>
                                        <th width="40%" style="color:#FFFFFF" height="25px">
                                            <p class="text-uppercase">Correo Electronico</p>
                                        </th>
                                        <th width="20%" style="color:#FFFFFF" height="25px">
                                            <p class="text-uppercase">Rol</p>
                                        </th>
                                        <th width="20%" style="color:#FFFFFF" height="25px">
                                            <p class="text-uppercase">
                                                Opciones
                                            </p>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $users as $user )
                                        <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                            <td style="text-align: left">
                                                <p class="text-uppercase">
                                                    <i class="fa fa-user"></i> {{ $user->nameComplete() }}
                                                </p>
                                            </td>
                                            <td style="text-align: left">
                                                <p class="text-lowercase">
                                                    {{$user->email }}
                                                </p>
                                            </td>
                                            <td style="text-align: left">
                                                @foreach ($user->roleUsers as $rol)
                                                    <p class="text-lowercase"> {{ $rol->name }}</p>
                                                @endforeach
                                            </td>
                                            <td style="text-align: center" colspan="1">
                                                <a data-backdrop="static" data-keyboard="false" data-target="#modal-delete-{{ $user->id }}" data-toggle="modal">
                                                    <button title="eliminar" class="btn btn-celeste btn-responsive">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </a> 
                                            </td>
                                        </tr>
                                        @include('users.gestion.modaldelete')  
                                    @endforeach 
                                </tbody>               
                            </table>
                        </div>    
                    @else
                        <p class="p-3 mb-2 bg-warning text-dark">No hay usuarios registrados</p>
                    @endif
                </div> 
            </div>
        </div>      
    </div>
</div>

@push('scripts')     
    <script src="{{asset('js/tablaDetalle.js')}}"></script>
@endpush

@endsection