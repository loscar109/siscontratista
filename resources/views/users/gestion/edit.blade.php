@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Editar Usuario: <strong>{{$users->name}}</strong></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('users/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @include('errors.request')

           @can('users.gestion.edit') 
            {!!Form::model($users, [
                'method'=>'PATCH',
                'route'=>['users.gestion.update',$users->id]
            ])!!}
            @endcan
            

            {{Form::token()}}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="control-label">Nombre</label>
                        <input id="name" minlength="13" maxlength="30" type="text" class="form-control" name="name" value="{{ $users->name }}" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label">E-Mail</label>

                        <input id="email" type="email" class="form-control" name="email" value="{{ $users->email }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Constraseña</label>

                        <input id="password" type="password" class="form-control" name="password"  required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
                
                <div class="form-group">
                    <label for="password-confirm" class="control-label">Confirmar Constraseña</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
               

                <hr>
                <h3>Lista de Roles</h3>
                <div class="form-group">
                    <ul class="list-unstyled">
                        @foreach ($roles as $role)
                            <li>
                                <label>
                                    {{ Form::checkbox('roles[]', $role->id, null) }}
                                    {{ $role->name }}
                                    <em>({{ $role->description ?: 'Sin descripcion'}})</em>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            <div class="form-group">
                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i></button>
                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i></button>
            </div>
            {!!Form::close()!!}
           

        </div>
    </div>



@endsection