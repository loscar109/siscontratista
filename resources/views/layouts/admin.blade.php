<!DOCTYPE html>
<html lang="es">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>SisContratista</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <!-- Bootstrap 3.3.5 -->   
      <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/skin-blue.css')}}">
      <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonDefault.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonGris.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonDanger.css')}}">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
      <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('css/AdminLTE.css')}}">
      <link rel="stylesheet" href="{{asset('css/_all-skins.css')}}">
      <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
      <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
  </head>

  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">
      <header class="main-header">   
        <!-- Logo -->
        <a href="{{ route('login') }}" class="logo">    
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Si</b>C</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SisContratista</b></span>
        </a>  
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">  
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              @if (auth()->user()->nameRoleUser()=="JEFE" || auth()->user()->nameRoleUser()=="EMPLEADO"|| auth()->user()->nameRoleUser()=="ADMIN")
              <li class="dropdown messages-menu">
                <a
                  href="{{ asset('manual/ManualDeUsuario.pdf') }}"
                  target="_blank"
                  title="manual de usuario"
                  >
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                </a>
              </li>
              <li class="dropdown messages-menu">
                <a href="{{asset('messages/gestion')}}">
                  <i class="fa fa-envelope"></i>
                    @if ( count(Auth::user()->message_to->where('read','=','0')) == 0 )
                      <span style="font-size:100%"></span>
                    @else
                      <span style="font-size:100%" class="label label-success">{{count(Auth::user()->message_to->where('read','=','0'))}}</span>
                    @endif
                </a>
              </li>             
              @endif

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  @if (Auth::user()->photo == NULL)
                    <img 
                      src="{{asset('/imagenes/users/default.png')}}"
                      class="user-image"
                      height="250px"
                      width="250px"
                      > 
                  @else
                    <img 
                      src="{{asset(Auth::user()->photo)}}"
                      class="user-image"
                      height="200px"
                      width="200px"
                      >
                  @endif
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>

                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      @if (Auth::user()->photo == NULL)
                        <img 
                          src="{{asset('/imagenes/users/default.png')}}"
                          class="img-circle"
                          height="250px"
                          width="250px"
                        > 
                      @else
                        <img 
                          src="{{asset(Auth::user()->photo)}}"
                          class="img-circle"
           
                        >
                      @endif
                    <p>
                      <span class="hidden-xs">{{ Auth::user()->name }}</span> <br>
                      <span class="hidden-xs">{{ Auth::user()->email }}</span> 
                      <small>{{ Auth::user()->nameRoleUser() }}</small>
                    </p>
                  </li>     
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('logout') }}">
                          <button title="Cerrar sesion"class="btn btn-primary btn-responsive">
                              <i class="fas fa-power-off"></i>
                          </button>
                        </a>
                    </div>
                  </li>
                </ul>
              </li>      
            </ul>
          </div>
        </nav>
      </header>


      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar"> 
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">

            <li>
              <a href="{{asset('/home')}}">
                <i class="fa fa-home" ></i><span>Inicio</span>

              </a>
            </li>
 
            @can('users.gestion.index')
              <li>
                <a href="{{asset('users/gestion')}}">
                  <i class="fa fa-user"></i> <span>Usuarios</span>
                </a>
              </li>
            @endcan

            @can('roles.gestion.index')
              <li>
                <a href="{{asset('roles/gestion')}}">
                  <i class="fa fa-key"></i> <span>Roles</span>
                </a>
              </li>
            @endcan

            @can('tickets.gestion.index')
            <li>
              <a href="{{asset('tickets/gestion')}}">
                <i class="fa fa-tasks" aria-hidden="true"></i> <span>Tickets</span>
              </a>
            </li>
            @endcan

            @can('workgroups.gestion.index')
            <li>
              <a href="{{asset('workgroups/gestion')}}">
                <i class="fa fa-users" aria-hidden="true"></i> <span>Grupos de Trabajo</span>
              </a>
            </li>
            @endcan


            @can('configuracion.index')
            <li>
              <a href="{{asset('configuracion') }}">
                <i class="fa fa-cogs"></i>
                  <span>Configuracion</span>                   
              </a>
            </li>
            @endcan

           
            @can('materials.gestion.index')
            <li>
              <a href="{{asset('materials/gestion')}}">
                <i class="fa fa-box"></i>
                  <span>Materiales</span>                   
              </a>
            </li>
            @endcan

            @can('requisitions.gestion.index')
            <li>
              <a href="{{asset('requisitions/gestion')}}">
                <i class="fas fa-box-open"></i>
                  <span>Entradas</span>                   
              </a>
            </li>
            @endcan
           
        </section>
        <!-- /.sidebar -->
      </aside>





       <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">   
        <!-- Main content -->
        <section class="content">       
          <div class="row" >
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="box">
                  @yield('titulo')
                  @yield('content')		        
              </div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->     
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <!--Fin-Contenido-->

    
      
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>

    
    @stack('scripts')
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
   
 </body>
</html>
