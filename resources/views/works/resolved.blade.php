@extends('layouts.admin')
@section('titulo')

<h4 class="box-title" >Bienvenido</h4>
   
@endsection
@section('content')
<div>     
    <a href={{ url()->previous() }}><button title="atras" class="btn btn-celeste btn-responsive">
        <i class="fa fa-arrow-left"> Atras</i> </button>
    </a>     
</div>
<br>
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        @include('works.search')
    </div>

</div>

<br> 
<div id="divDetalle" class="table-responsive">
    <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
        <thead style="background-color:#357CA5">
            <tr>
                <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Numero de Ticket</p></th>        
                <th width="15%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Prioridad</p></th>        

                <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Ticket</p></th>        
                <th width="15%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Estado actual</p></th>        

                <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>        



            </tr>
        </thead>
        <tbody>
            
            @foreach ($tickets as $t)
                <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                    <td>{{$t->number}}</td>
                    <td>
                        @if ($t->priority  == 5)
                            <span class="label label-danger" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                        @if ($t->priority  == 4)
                            <span class="label label-warning" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                        @if ($t->priority  == 3)
                            <span class="label label-warning" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                        @if ($t->priority  == 2)
                            <span class="label label-success" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                        @if ($t->priority  == 1)
                            <span class="label label-success" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                        @if ($t->priority  != 1 && $t->priority  != 2 && $t->priority  != 3 && $t->priority  != 4 && $t->priority  != 5)
                            <span class="label label-default" style="font-size:150%">{{$t->priority }}</span>
                        @endif
                    </td>
                    <td><p class="text-uppercase">{{$t->description }}</p></td>
                    <td>
                        @if ($t->ticket_stage->name  == "Creado")
                            <span class="label label-default">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  == "Revisado")
                            <span class="label label-primary">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  == "En proceso")
                            <span class="label label-info">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  == "Resuelto")
                            <span class="label label-success">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  == "No Resuelto")
                            <span class="label label-danger">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  == "Reasignado")
                            <span class="label label-warning">{{$t->ticket_stage->name }}</span>
                        @endif
                        @if ($t->ticket_stage->name  != "Creado" && $t->ticket_stage->name  != "Revisado" && $t->ticket_stage->name  != "En proceso" && $t->ticket_stage->name  != "Resuelto" && $t->ticket_stage->name  != "No Resuelto" && $t->ticket_stage->name  != "Reasignado")
                            <span class="label label-default">{{$t->ticket_stage->name }}</span>
                        @endif
                    </td>

                    <td>
                        @if ($t->ticket_stage->name  != "Resuelto")
                            <a href="{{URL::action('WorkController@show',$t->id)}}">
                                <button title="ver" class="btn btn-primary btn-responsive btn-sm">
                                    <i class="fa fa-wrench" aria-hidden="true"></i> Realizar Ticket

                                </button> 
                            </a>      
                        @else
                        <a href="{{URL::action('WorkController@show',$t->id)}}">
                            <button title="ver" class="btn btn-success btn-responsive btn-sm">
                                <i class="fa fa-eye" aria-hidden="true"></i>  Ver el Detalle

                            </button> 
                        </a>      
                            
                        @endif
                       
                    </td>


                </tr>
            @endforeach
        </tbody>        
    </table>
</div>



@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tablaDetalle').DataTable({
                "aaSorting":[],
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""



                }

            });
            cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection




