@extends('layouts.admin')
<style>
    #preview {
    
    padding:5px;
    border-radius:2px;
    background:#fff;
    max-width:720px;
    max-height:480px;

    }

    #preview img {width:100%;display:block;   border:1px solid #ddd;;}

</style>

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href={{ asset('/works/inProccess') }}>
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
    <h4 class="box-title"> Detalles del Ticket: <b>{{ucwords($ticket->number)}}</b></h4>
</div>
@endsection



@section('content')
<div class="box-body">
    @include('works.mensaje')

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @include('errors.request')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"></i> {{"Ticket Nro:" . $ticket->number}}
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="divDetalle" class="table-responsive">
                        <table  style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Codigo de Ticket</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{ $ticket->number }}
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Estado Actual</p></th>
                                        <td style="background-color:#FFFFFF">
                                            @if ($ticket->current_stage->name  == "Creado")
                                                <span class="label label-default">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  == "Revisado")
                                                <span class="label label-primary">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  == "En proceso")
                                                <span class="label label-info">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  == "Resuelto")
                                                <span class="label label-success">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  == "No Resuelto")
                                                <span class="label label-danger">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  == "Reasignado")
                                                <span class="label label-warning">{{$ticket->current_stage->name }}</span>
                                            @endif
                                            @if ($ticket->current_stage->name  != "Creado" && $ticket->current_stage->name  != "Revisado" && $ticket->current_stage->name  != "En proceso" && $ticket->current_stage->name  != "Resuelto" && $ticket->current_stage->name  != "No Resuelto" && $ticket->current_stage->name  != "Reasignado")
                                                <span class="label label-default">{{$ticket->current_stage->name }}</span>
                                            @endif
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Descripción</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{ $ticket->description }}
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Detalle</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{ $ticket->detail }}
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Fecha de Creación</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{ $ticket->created_at->format('d/m/Y H:i:s') . " (" .$ticket->created_at->diffForhumans() . ") "}}
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Prioridad</p></th>
                                        <td style="background-color:#FFFFFF">
                                            @if ($ticket->priority  == 5)
                                                <span class="label label-danger" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                            @if ($ticket->priority  == 4)
                                                <span class="label label-warning" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                            @if ($ticket->priority  == 3)
                                                <span class="label label-warning" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                            @if ($ticket->priority  == 2)
                                                <span class="label label-success" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                            @if ($ticket->priority  == 1)
                                                <span class="label label-success" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                            @if ($ticket->priority  != 1 && $ticket->priority  != 2 && $ticket->priority  != 3 && $ticket->priority  != 4 && $ticket->priority  != 5)
                                                <span class="label label-default" style="font-size:120%">{{$ticket->priority }}</span>
                                            @endif
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Ubicación</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{$ticket->address() }}
                                        </td>
                                </tr>
                                <tr>
                                    <th style="background-color:#FFFFFF"><p class="text-uppercase">Cliente</p></th>
                                        <td style="background-color:#FFFFFF">
                                            {{$ticket->client() }}
                                        </td>
                                </tr>
                            </thead>
                                 
                        </table>
                    </div>   
                </div> 
            </div>  
        </div>      
    </div>


    <!-- aca arrancan los mensajes -->
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Mensajeria</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>  
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="direct-chat-messages">
                    @foreach ( $message_tickets as $message_ticket )
                    <!--Si el usuario es el que inicio sesión -->
                    @if(Auth::user()->id != $message_ticket->user->id)
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left"> {{ $message_ticket->user->name }}</span>
                                <span class="direct-chat-timestamp pull-right">  {{ $message_ticket->created_at->format('d/m/Y H:i:s') . " (" .$message_ticket->created_at->diffForhumans() . ") "}}</span>
                            </div>
                            @if ($message_ticket->user->photo == NULL)
                                <img
                                    class="direct-chat-img"
                                    src="{{asset('/imagenes/users/default.png')}}"
                                    height="25px"
                                    width="25px"
                                    >
                            @else
                                <img
                                    class="direct-chat-img"
                                    src="{{asset($message_ticket->user->photo)}}"
                                    height="25px"
                                    width="25px"
                                    >
                            @endif                 
                            <div class="direct-chat-text">
                                {{$message_ticket->text}}
                            </div>
                            @if ($message_ticket->image != NULL)
                                <img src="{{asset('imagenes/photo/'.$message_ticket->image)}}"
                                alt="Sin imagen"
                                class="img-thumbnail"
                                height="300px"
                                width="300px"
                                class="img-responsive">
                            @endif
                        </div>
                    @else
                        <!--Si el usuario es el que no inicio sesión -->
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right"> {{ $message_ticket->user->name }}</span>
                                <span class="direct-chat-timestamp pull-left">  {{ $message_ticket->created_at->format('d/m/Y H:i:s') . " (" .$message_ticket->created_at->diffForhumans() . ") "}}</span>
                            </div>
                            @if ($message_ticket->user->photo == NULL)
                                <img
                                    class="direct-chat-img"
                                    src="{{asset('/imagenes/users/default.png')}}"
                                    height="25px"
                                    width="25px"
                                    >
                            @else
                                <img
                                    class="direct-chat-img"
                                    src="{{asset($message_ticket->user->photo)}}"
                                    height="25px"
                                    width="25px"
                                    >
                            @endif                 
                            <div class="direct-chat-text">
                                {{$message_ticket->text}}
                            </div>
                            
                            @if ($message_ticket->image != NULL)
                            <img src="{{asset('imagenes/photo/'.$message_ticket->image)}}"
                                alt="Sin imagen"
                                class="img-thumbnail"
                                height="300px"
                                width="300px"
                                class="img-responsive">                                
                            @endif
              
                                
                        </div>
                    @endif
                    @endforeach    
                </div>
       
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-default pull-right" data-target="#modal-send-{{$ticket->id}}" data-toggle="modal">Escribir <i class="fas fa-pencil-alt"></i></button>
                <div class="modal fade modal-slide-in-right"
                    aria-hidden="true"
                    role="dialog"
                    id="modal-send-{{$ticket->id}}"        
                    >

                
                    <div class="modal-dialog">
                        <!--contenido del modal-->
                        <form 
                            class="modal-content"
                            action="{{route('works.message',['ticket'=>$ticket])}}"
                            method="POST"
                            enctype="multipart/form-data"
                            >
                        
                            {{ csrf_field() }}

                            <!-- cabecera del modal -->
                            <div class="modal-header" style="background-color:#357CA5">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                                    <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                                </button>
                                <h4 class="modal-title" style="color:#FFFFFF">Esribir Mensaje</h4>
                            </div>

                            <!--cuerpo del modal-->
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                     
                                        
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Mensaje</label>
                                            <textarea class="form-control" name="text" id="message-text"></textarea>
                                        </div>
                                       
                                        <div class="form-group"> 
                                            <label for="image" class="col-form-label">Imagen</label>
                                            <input id="file" type="file" name="image" class="img-responsive">
                                            <div id="preview"></div>
                                           
                                       </div>
                                    
                                    </div>                     
                                </div>
                            </div>

                            <!--pie del modal-->
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 ml-auto">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-envelope"> </i> Enviar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        
                    
                        </form>    
                    </div>

                </div>


            
            </div>

        </div>
    </div>        
    <!-- aca terminan los mensajes -->

   
</div>
<div align="center" class="box-body">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- Si el estado es Reasignado o no resuelto, cambia se crea una nueva fecha -->
                @foreach ($tso as $stage)
                    @if ($stage->next->name == 'Reasignado')
                    <button type="button" class="btn btn-warning" data-target="#modal-reprogramming-{{$stage->next->id}}" data-toggle="modal">{{$stage->next->name}}</button>    
                    


                    <div class="modal fade modal-slide-in-right"
                        aria-hidden="true"
                        role="dialog"
                        tabindex="-1"
                        id="modal-reprogramming-{{$stage->next->id}}"        
                        >

                        
                        <div class="modal-dialog">
                            <!--contenido del modal-->
                            <form 
                                class="modal-content"
                                action="{{route('works.reprogramming',['ticket'=>$ticket, 'ticket_stage'=>$stage->next->id])}}"
                                method="POST"
                                >
                                
                                {{ csrf_field() }}

                                <!-- cabecera del modal -->
                                <div class="modal-header" style="background-color:#357CA5">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                                    </button>
                                    <h4 class="modal-title" style="color:#FFFFFF">Seleccione nueva fecha de realización del ticket <b>{{$ticket->id}}</b></h4>
                                </div>

                                <!--cuerpo del modal-->
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <input type="date" name="date" value={{Carbon\Carbon::now()}} required min={{Carbon\Carbon::now()}}>
                                        </div>                     
                                    </div>
                                </div>

                                <!--pie del modal-->
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-md-12 ml-auto">
                                            <button type="submit" class="btn btn-primary btn-responsive">
                                                <i class="fa fa-save"> Guardar</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                
                            
                            </form>    
                        </div>

                        

                    </div>

                    @else
                        @if ($stage->next->name == 'En proceso')
                            <a href="{{asset('/works/updateStage/'.$ticket->id.'/'.$stage->next->id)}}" class="btn btn-primary">{{$stage->next->name}}</a>
                        @endif
                        @if ($stage->next->name == 'Resuelto')
                            <a href="{{asset('/works/updateStage/'.$ticket->id.'/'.$stage->next->id)}}" class="btn btn-success">{{$stage->next->name}}</a> 
                        @endif
   
                        @if ($stage->next->name == 'No Resuelto')
                            <a href="{{asset('/works/updateStage/'.$ticket->id.'/'.$stage->next->id)}}" class="btn btn-danger">{{$stage->next->name}}</a>    
                        @endif


                    @endif
                        
                @endforeach
                




               

                    
        </div>
    </div>



    





@push('scripts')     
<script src="{{asset('js/tablaDetalle.js')}}"></script>

<script language="JavaScript">

document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }

       

    $(document).ready(function(){
        $("#to_id").select2({
            placeholder:'-Seleccione un Destinatario-',
            width: '100%',

        });  
    });
</script>
@endpush


@endsection