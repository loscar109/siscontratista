@if (Session::has('success_message'))
        <div id="success_message" class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('success_message') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_finished'))
        <div id="store_finished" class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_finished') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
@if (Session::has('ticket_not_resolved'))
        <div id="ticket_not_resolved" class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('ticket_not_resolved') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function () 
        {
                $("#success_message").fadeOut(10000);

        });
    </script>
@endpush