 {{--modal - ver--}}
 <div class="modal fade modal-slide-in-right"
 aria-hidden="true"
 role="dialog"
 tabindex="-1"
 id="modal-show-{{$work->id}}">



 {{Form::Open(array(
     'action'=>array('WorkController@show',$work->id),
     'method'=>'get'
     ))}}



     <div class="modal-dialog">
         <!--contenido del modal-->
         <div class="modal-content">
             
             <!-- cabecera del modal -->
             <div class="modal-header" style="background-color:#357CA5">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                     <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                 </button>
                 <h4 class="modal-title" style="color:#FFFFFF">¿Realizar Ticket? <b></b></h4>
             </div>

             <!--cuerpo del modal-->
             <div class="modal-body">
                 <div class="container-fluid">
                     <div class="row">
                     
                         <div class="col-md-8 ml-auto">
                                 <p>Aca van las cosas del ticket</p>

                         </div>
                     </div>                     
                 </div>
             </div>

             <!--pie del modal-->
             <div class="modal-footer">
                 <div class="row">
                     <div class="col-md-12 ml-auto">
                         <button type="button" class="btn btn-danger btn-responsive" data-dismiss="modal">
                             <i class="fa fa-close"> Realizarla mas tarde</i>
                         </button>
                         <button type="button" class="btn btn-success btn-responsive" data-dismiss="modal">
                                 <i class="fa fa-check"> Empezar el Ticket</i>
                         </button>
                         @include('errors.request')
                     </div>
                 </div>
             </div>
         
         
         </div>    
     </div>

 {{Form::Close()}}

</div>