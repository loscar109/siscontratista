@extends('layouts.admin')
@section('titulo')

    <h4 class="box-title">Consumo diario de Material</h4>

@endsection

@section('content')

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        @include('errors.request')
    </div>
</div>

    {!!Form::open(array(
        'url'=>'works/'.$ticket->id.'/finish',
        'method'=>'POST',
        'autocomplete'=>'off',
        'files' => true,
    ))!!}

    {{Form::token()}}
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="col-lg-3 col-sm-5 col-md-5 col-xs-12">
                <div class="form-group">
                    <label>
                        Categoria
                    </label>
                    <select 
                        name="category_id"
                        id="category_id"
                        class="category_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione una categoria"
                            >
                            -Seleccione una categoria-
                        </option>
                        @foreach ($categories as $ca)
                            <option 
                                value="{{$ca->id }}">
                                    {{$ca->description}}
                            </option>
                        @endforeach
                    </select>
                    <br>
                    <label>
                        Materiales
                    </label>
                    <select
                        name="material_id"
                        id="material_id"
                        class="material_id form-control"
                        required
                        >
                        <option 
                            value="0" 
                            disabled="true" 
                            selected="true"
                            title="Seleccione un material"
                            >
                            -Seleccione un material-
                        </option>
                    </select>
                </div>                    
            </div>
        
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                        <input
                            type="number"
                            name="cantidad"
                            id="cantidad"
                            class="cantidad form-control"
                            placeholder="cantidad..."
                            >
                </div>  
            </div>
            
            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                <div class="form-group">
                        <button type="button" id="bt_add" class="btn btn-primary">Agregar</button>     
                </div>  
            </div>

            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color:#A9D0F5">
                        <th>Opciones</th>
                        <th>Categoria</th>
                        <th>Material</th>
                        <th>Cantidad</th>
                    </thead>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
        <div class="form-group">
        <input name="_token" value="{{ csrf_token() }}" type="hidden">
            <button class="btn btn-success" id="btn_add"type="submit"><i class="fa fa-check"> </i>Guardar</button>
            <button class="btn btn-danger" id="" type="reset"> <i class="fa fa-eraser">...</i> Borrar</button>
        </div>
    </div>
</div>
    
    {!!Form::close()!!}



@endsection

@push('scripts')     
    <script type="text/javascript">
    $(document).ready(function(){
        $("#guardar").hide();
        $("#category_id").select2({
            width: '100%',
        });
        $("#material_id").select2({
            width: '100%',
        });

        $(document).on('change','.category_id',function(){
            console.log("hmm its change");
            var category_id=$(this).val();
            console.log(category_id);
            var div=$(this).parent();    
            var op=" ";
            


            $.ajax({
                type:'get',
                url:'{!!URL::to('works/finish/findMaterial')!!}',
                data:{'id':category_id},
                success:function(data){
                    console.log('success');
                    console.log(data);
                    console.log(data.length);
                    op+='<option value="0" selected disabled>-Seleccione un material-</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].description+'</option>';
                    }
                    div.find('.material_id').html(" ");
                    div.find('.material_id').append(op);
                },
                error:function(){    
                }
            });
        });
        $("#bt_add").click(function(){
            agregar();
        });
    });

    var cont=0;
    total=0;

    function agregar()
    {
        material_id=$("#material_id").val();
        category_id=$("#category_id").val();
        categories=$("#category_id option:selected").text();


        materials=$("#material_id option:selected").text();
        cantidad=$("#cantidad").val();
        /*   Aca iría el Ajax para obtener la cantidad por Paquete*/

        if(material_id==null)
            {
                toastr.error("Debe seleccionar un material")   
            }

            else if(cantidad=="" || cantidad<=0)
            {
                toastr.error("Cantidad incorrecta")   

            }
        else
        {

            $.ajax({
                    type:'get',
                    url:'{!!URL::to('works/finish/findMaterialData')!!}',
                    data:{'id':material_id},
                    success:function(data){
                        stock=data['stock'];
                        photo=data['photo'];
                        
                        if (cantidad> stock)
                            {
                            toastr.error("La cantidad ingresada supera el stock en el almacen");
                            
                            }
                        else
                            {
                                var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');")>X</button></td><td><input type="hidden" name="category_id[]" value="'+category_id+'">'+categories+'</td><td><input type="hidden" name="material_id[]" value="'+material_id+'">'+materials+'</td><td><input type="hidden" name="cantidad[]" value="'+cantidad+'">'+cantidad+'</td></tr>';         
                                cont++;
                                $("#detalles").append(fila);
                                eliminarDelSelect2 ();
                                //aca controlamos para que el name se introdusca solo una vez $("#name").hide();
                                $("#guardar").show();
                            }

                    },
                error:function(){    
                    console.log('no anda AJAX');
                }
                    
                });   
        }
    }

    function eliminarDelSelect2 ()
    {
        $("#material_id option:selected").remove();
        $("#cantidad").val("");
        ///argelame esta (se va todo del option) $('option[value="'+ user_id +'"]').remove(); 

    }


    

    function eliminar(index)
    {
        $("#fila" + index).remove();

    }

    </script>
@endpush