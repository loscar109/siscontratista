@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href={{ url()->previous() }}>
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
    <h4 class="box-title"> <b>Mensajes Enviados</b></h4>
</div>
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="box box-solid box-default">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-paper-plane"></i> Mensajes Enviados
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">

                           <!-- elemento de la -->

                           
                                
                        </div>
                        
                    </div>
                    @if($allSend->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th colspan="2" width="20%" style="color:#FFFFFF; border:0" height="25px">
                                        <p class="text-uppercase">Destinatario</p>
                                    </th>

                                    <th width="5%" style="color:#FFFFFF; border:0" height="25px">
                                        <p class="text-uppercase">Adjunto</p>
                                    </th>        

                                    <th width="15%" style="color:#FFFFFF; border:0" height="25px">
                                        <p class="text-uppercase">Asunto</p>
                                    </th>      

                                    <th width="5%" style="color:#FFFFFF; border:0" height="25px">
                                        <p class="text-uppercase">Descripción</p>
                                    </th>   

                                    <th width="73%" style="color:#FFFFFF; border:0; text-align: right" height="25px">
                                        <p class="text-uppercase">Fecha</p>
                                    </th>        
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allSend as $send)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td style="text-align: left; border:0">
                                            @if ($send->user_to->photo == NULL)
                                                <img 
                                                    src="{{asset('/imagenes/users/default.png')}}"
                                                    class="img-circle"
                                                    height="35px"
                                                    width="35px"
                                                > 
                                            @else
                                                <img 
                                                    src="{{asset($send->user_to->photo)}}"
                                                    class="img-circle"
                                                    height="35px"
                                                    width="35px"
                                                >
                                            @endif
                                            
                                           
                                        </td>
                                        <td style="text-align: left; border:0">
                                            <p class="text-lowercase">
                                                {{ $send->user_to->name . " " . $send->user_to->surname }}
                                            </p>
                                        </td>

                                        <td style="text-align: left; border:0">
                                            @if ($send->image == NULL)
                                                <p class="text-uppercase"></p>
                                            @else
                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                       
                                        <td style="text-align: left; border:0">{{ $send->subject }}</td>
                                        <td style="text-align: left; border:0">
                                            <p class="text-lowercase">
                                                {{ str_limit($send->text, 7) }}
                                            </p>
                                        </td>
                                        <td style="text-align: right; border:0"> {{ $send->created_at->format('d/m/Y H:i:s') . " (" .$send->created_at->diffForhumans() . ") "}}</td>

                                    </tr>
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    <br>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay mensajes enviados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>



@push('scripts')     
<script src="{{asset('js/tablaDetalle.js')}}"></script>

    
@endpush
@endsection
