@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href={{ url()->previous() }}>
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
    <h4 class="box-title"> <b>Tickets por Terminar</b></h4>
</div>
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('works.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"></i> Tickets en Proceso
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                   
                    @if($tickets->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Numero de Ticket</p></th>
                                    <th width="15%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Prioridad</p></th>        
                                    <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Fecha de creación</p></th>        
                                    <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Descripción</p></th>        
                                    <th width="15%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Estado actual</p></th>        
                                    <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>           
                                </tr>
                            </thead>
                            <tbody>     
                                @foreach ($tickets as $t)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td>{{$t->number}}</td>

                                        <td>
                                            @if ($t->priority  == 5)
                                                <span class="label label-danger" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                            @if ($t->priority  == 4)
                                                <span class="label label-warning" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                            @if ($t->priority  == 3)
                                                <span class="label label-warning" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                            @if ($t->priority  == 2)
                                                <span class="label label-success" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                            @if ($t->priority  == 1)
                                                <span class="label label-success" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                            @if ($t->priority  != 1 && $t->priority  != 2 && $t->priority  != 3 && $t->priority  != 4 && $t->priority  != 5)
                                                <span class="label label-default" style="font-size:150%">{{$t->priority }}</span>
                                            @endif
                                        </td>
                                        <td>{{$t->created_at->format('d/m/Y')}}</td>

                                        <td><p class="text-uppercase">{{$t->description }}</p></td>
                                        <td>
                                            @if ($t->ticket_stage->name  == "Creado")
                                                <span class="label label-default">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  == "Revisado")
                                                <span class="label label-primary">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  == "En proceso")
                                                <span class="label label-info">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  == "Resuelto")
                                                <span class="label label-success">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  == "No Resuelto")
                                                <span class="label label-danger">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  == "Reasignado")
                                                <span class="label label-warning">{{$t->ticket_stage->name }}</span>
                                            @endif
                                            @if ($t->ticket_stage->name  != "Creado" && $t->ticket_stage->name  != "Revisado" && $t->ticket_stage->name  != "En proceso" && $t->ticket_stage->name  != "Resuelto" && $t->ticket_stage->name  != "No Resuelto" && $t->ticket_stage->name  != "Reasignado")
                                                <span class="label label-default">{{$t->ticket_stage->name }}</span>
                                            @endif
                                        </td>
                    
                                        <td>
                                            @if ($t->ticket_stage->name  != "Resuelto")
                                                <a href="{{URL::action('WorkController@show',$t->id)}}">
                                                    <button title="ver" class="btn btn-primary btn-responsive btn-sm">
                                                        <i class="fa fa-wrench" aria-hidden="true"></i> Realizar Ticket
                    
                                                    </button> 
                                                </a>      
                                            @else
                                            <a href="{{URL::action('WorkController@show',$t->id)}}">
                                                <button title="ver" class="btn btn-success btn-responsive btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>  Ver el Detalle
                    
                                                </button> 
                                            </a>      
                                                
                                            @endif
                                           
                                        </td>
                    
                    
                                    </tr>
                                @endforeach       
                            </tbody>        

                        </table>
                    </div>   
                    <br>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay Tickets registrados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>



@push('scripts')     
<script src="{{asset('js/tablaDetalle.js')}}"></script>

    
@endpush
@endsection




