@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href={{ url()->previous() }}>
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box  box-primary">
         
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="media">
                <div class="media-left">
                    @if ($send->user->photo != NULL)
                    <img src="{{asset($send->user->photo)}}"
                        class="media-object"
                        height="75px"
                        width="75px"
                        >
                    @else
                    <img src="{{asset('/imagenes/users/default.png')}}"
                        alt="Sin imagen"
                        class="media-object"
                        height="75px"
                        width="75px"
                        >
                    @endif 
                </div>
                <div class="mailbox-read-info media-body">
                    <h3>{{ $send->subject }}</h3>
                    <h5>de: {{ $send->user->name . " " . $send->user->surname}} ({{$send->user->nameRoleUser()}}) {{ $send->user->email }}<span class="mailbox-read-time pull-right">Enviado el {{$send->created_at->format('d/m/Y h:i:s A')}}  ({{$send->created_at->diffForHumans()}})</span></h5>
                    <h5>para: {{ $send->user_to->name . " " . $send->user_to->surname}} ({{$send->user_to->nameRoleUser()}}) {{ $send->user_to->email }}</h5>

                </div>
               
            </div>
           
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
              <p>{{$send->text}}</p>           
            </div>
            <!-- /.mailbox-read-message -->
          <!-- /.box-body -->
          @if($send->image != NULL)
          <div class="box-footer">
            <div class="mailbox-controls with-border text-center">
                <img src="{{asset('imagenes/photo/'.$send->image)}}"
                    alt="Sin imagen"
                    class="img-thumbnail"
                    height="700px"
                    width="700px"
                    class="img-responsive">
            </div>
          </div>
          @endif    

          <!-- /.box-footer -->
          <div class="box-footer">
            <div class="pull-right">
                @if ($send->read  == 0)
                <span class="label label-danger">No Leido</span>
            @endif
            @if ($send->read  == 1)
                <span class="mailbox-read-time pull-right"> <span class="label label-success">Leido</span> el {{$send->updated_at->format('d/m/Y h:i:s A')}}  ({{$send->updated_at->diffForHumans()}})</span>
               
            @endif
            </div>
           
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /. box -->
      </div>
   
@endsection




