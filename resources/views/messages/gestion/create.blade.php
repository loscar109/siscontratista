@extends('layouts.admin')
<style>
    #preview {
    
    padding:5px;
    border-radius:2px;
    background:#fff;
    max-width:640px;
    max-height:480px;

    }

    #preview img {width:100%;display:block;   border:1px solid #ddd;}
    </style>
@section('titulo')

<div class="box-header" style="text-align: center">
    <a href="{{asset('messages/gestion')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>   
</div>   
@endsection

@section('content')


{!!Form::open(array(
    'url'=>'messages/gestion',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}
    <div class="box-body">     
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @include('errors.request')
            @include('materials.gestion.mensaje')
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-envelope"></i> Crear Mensaje
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="subject">
                                Asunto
                            </label>
                            <input 
                                type="string"
                                name="subject"
                                maxlength="27"
                                minlength="7"

                                required value="{{old('subject')}}"
                                class="form-control"
                                placeholder="descripcion..."
                                title="Introduzca una descripcion para el ticket a crear"
                                >
                            
                            <div class="form-group">
                                <label for="exampleFormControlTextarea4">Mensaje</label>
                                <textarea maxlength="600" minlength="7" title="Introduzca una descripcion para el ticket a crear"  placeholder="descripcion..."   class="form-control" type="text"  name="text" equired value="{{old('text')}}" class="form-control" id="exampleFormControlTextarea4" rows="3"></textarea>
                              </div>
                            
                                
                              
                            <label>
                                Destinatario
                            </label>
                                <select 
                                name="to_id"
                                id="to_id"
                                class="to_id form-control"
                                required
                                >
                                <option 
                                    value="0" 
                                    disabled="true" 
                                    selected="true"
                                    title="-Seleccione un usuario-"
                                    >
                                    -Seleccione un usuario-
                                </option>
                                @foreach ($users as $user)
                                    <option 
                                        value="{{$user->id }}">
                                            {{$user->superior}}
                                    </option>
                                @endforeach
                            </select>
                           
                            <label for="image">Imagen</label>
                                <input
                                    id="file"
                                    type="file"
                                    name="image"
                                    class="img-responsive"
                                    >
                                <hr>
                                <div id="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
            </div>
        </div>

    {!!Form::close()!!}

    </div>

    @push('scripts')  
    <script>
     document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }
    </script>   
    <script type="text/javascript">
     $(document).ready(function(){
        $ticket = $("#ticket_id").select2({
            placeholder:'-Seleccione un ticket-',
            width: '100%'
        });
        $("#to_id").select2({
            placeholder:'-Seleccione un usuario-',
            width: '100%'
        });

       
 

       


     });
    </script>
    @endpush

@endsection


