@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('messages.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-paper-plane"> </i> Elementos Enviados
                </h4>
                <div class="box-tools">
                    <a href="{{route('messages.gestion.create')}}">
                        <button title="nuevo"class="btn btn-celeste btn-responsive">
                            <i  class="fa fa-envelope"><sup><i class="fa fa-plus"></i></sup></i> Nuevo
                        </button>
                    </a>
                    <a href="{{route('messages.gestion.index')}}">
                        <button title="nuevo"class="btn btn-celeste btn-responsive">
                            <i class="fa fa-envelope" aria-hidden="true"></i> Bandeja de Entrada
                        </button>
                    </a>
                </div>     
            </div>
            <div class="box-body">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    @if($sends->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5; border:0">
                                <tr>
                                    <th colspan="2" width="5%" style="color:#FFFFFF; border:0" height="25px"><p class="text-uppercase">Destinatario</p></th>
                                    <th width="20%" style="color:#FFFFFF; border:0" height="25px"><p class="text-uppercase">Asunto</p></th>
                                    <th width="20%" style="color:#FFFFFF; border:0" height="25px"><p class="text-uppercase">Imagen</p></th>
                                    <th width="20%" style="color:#FFFFFF; border:0" height="25px"><p class="text-uppercase">Fecha</p></th>
                                    <th width="18%" style="color:#FFFFFF; border:0" height="25px"><p class="text-uppercase">Opciones</p></th>
                                </tr>
                            </thead>
                            <tbody>          
                                @foreach ($sends as $send)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td style="text-align: left; border:0">
                                            @if ($send->user_to->photo == NULL)
                                            <img src="{{asset('/imagenes/users/default.png')}}"
                                                class="img-circle"
                                                height="35px"
                                                width="35px">
                                            @else
                                            <img src="{{ asset($send->user_to->photo) }}"
                                                class="img-circle"
                                                height="35px"
                                                width="35px"
                                            >
                                            @endif
                                            
                                        </td>
                                        <td style="text-align: left; border:0">
                                            <p class="text-lowercase">
                                                {{ $send->user_to->name }}
                                            </p>
                                        </td>
    
                                        <td style="text-align: left; border:0">
                                            <p class="text-uppercase">
                                                {{$send->subject }}
                                            </p>
                                        </td>
    
                                        
    
                                        <td style="text-align: left; border:0">
                                            @if ($send->image == NULL)
                                                <p class="text-uppercase"></p>
                                            @else
                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                        <td>{{Carbon\Carbon::parse($send->created_at)->format('d/m/Y H:i:s')}}</td>
                                        <td>
                                                <a href="{{URL::action('MessageController@open',$send)}}">
                                                    <button title="abrir mensaje" class="btn btn-primary">
                                                        <i class="fa fa-envelope"> </i> Abrir Mensaje
                                                    </button>        
                                                </a>
                                
                                        </td>
                                    </tr>
    
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    <br>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay Mensajes en la Bandeja de Entrada
                    </p>
                    @endif            
            </div>
        </div>
    </div>
</div>

@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tablaDetalle').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection




