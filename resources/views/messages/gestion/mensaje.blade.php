@if (Session::has('delete_message_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_message_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_message'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_message') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_message'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_message') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_message'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_message') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif