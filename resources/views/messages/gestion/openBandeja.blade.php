@extends('layouts.admin')
<style>
    #preview {
    
    padding:5px;
    border-radius:2px;
    background:#fff;
    max-width:720px;
    max-height:480px;

    }

    #preview img {width:100%;display:block;   border:1px solid #ddd;;}

</style>
@section('titulo')

<div class="box-header" style="text-align:center">

    <a href="{{asset('messages/gestion')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection




@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box  box-primary">
         
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="media">
                <div class="media-left">
                    @if ($message->user->photo != NULL)
                    <img src="{{asset($message->user->photo)}}"
                        class="media-object"
                        height="75px"
                        width="75px"
                        >
                    @else
                    <img src="{{asset('/imagenes/users/default.png')}}"
                        alt="Sin imagen"
                        class="media-object"
                        height="75px"
                        width="75px"
                        >
                    @endif 
                </div>
                <div class="mailbox-read-info media-body">
                    <h3>{{ $message->subject }}</h3>
                    <h5>de: {{ $message->user->name . " " . $message->user->surname}} ({{$message->user->nameRoleUser()}}) {{ $message->user->email }}<span class="mailbox-read-time pull-right">Enviado el {{$message->created_at->format('d/m/Y h:i:s A')}}  ({{$message->created_at->diffForHumans()}})</span></h5>
                    <h5>para: {{ $message->user_to->name . " " . $message->user_to->surname}} ({{$message->user_to->nameRoleUser()}}) {{ $message->user_to->email }}</h5>

                </div>
               
            </div>
           
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
              <p>{{$message->text}}</p>           
            </div>
            <!-- /.mailbox-read-message -->
          <!-- /.box-body -->
          @if($message->image != NULL)
          <div class="box-footer">
            <div class="mailbox-controls with-border text-center">
                <img src="{{asset('imagenes/photo/'.$message->image)}}"
                    alt="Sin imagen"
                    class="img-thumbnail"
                    height="700px"
                    width="700px"
                    class="img-responsive">
            </div>
          </div>
          @endif    

          <!-- /.box-footer -->
          <div class="box-footer">
            <div class="pull-right">
                @if ($message->read  == 0)
                    <span class="label label-danger">No Leido</span>
                @endif
                @if ($message->read  == 1)
                    <span class="mailbox-read-time pull-right"> <span class="label label-success">Leido</span> el {{$message->updated_at->format('d/m/Y h:i:s A')}}  ({{$message->updated_at->diffForHumans()}})</span>       
                @endif
            </div>
            <div class="pull-left">
                <a href="{{asset('messages/gestion/' . $message->id . '/answer')}}">
                    <button title="atras" class="btn btn-default btn-responsive pull-left">
                        <i class="fa fa-envelope"></i> Responder
                    </button>
                </a>
            </div>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /. box -->
      </div>
                    

 
   



@push('scripts')     
<script language="JavaScript">

document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }

       

    $(document).ready(function(){
        $("#to_id").select2({
            placeholder:'-Seleccione un Destinatario-',
            width: '100%',

        });  
    });
</script>
@endpush


@endsection