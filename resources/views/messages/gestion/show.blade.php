@extends('layouts.admin')
<style>
    #preview {
    
    padding:5px;
    border-radius:2px;
    background:#fff;
    max-width:720px;
    max-height:480px;

    }

    #preview img {width:100%;display:block;   border:1px solid #ddd;;}

</style>
@section('titulo')

<div class="box-header" style="text-align:center">

    <a href="{{asset('messages/gestion')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection




@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('messages.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"></i> {{$message->subject}}
                </h4>
                <div class="box-tools">
                    <button type="button"  class="btn btn-primary" data-target="#modal-send-{{$message->ticket->id}}" data-toggle="modal"><i class="fa fa-envelope" aria-hidden="true"></i> Responder</button>

                </div>
                <div class="modal fade modal-slide-in-right"
                    aria-hidden="true"
                    role="dialog"
                    tabindex="-1"
                    id="modal-send-{{$message->ticket->id}}"        
                    >

                
                    <div class="modal-dialog">
                        <!--contenido del modal-->
                        <form 
                            class="modal-content"
                            action="{{route('messages.message',['message'=>$message])}}"
                            method="POST"
                            enctype="multipart/form-data"
                            >
                        
                            {{ csrf_field() }}

                            <!-- cabecera del modal -->
                            <div class="modal-header" style="background-color:#357CA5">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                                    <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                                </button>
                                <h4 class="modal-title" style="color:#FFFFFF">Enviar Mensaje</h4>
                            </div>

                            <!--cuerpo del modal-->
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Destinatario</label>
                                                <select 
                                                name="to_id"
                                                id="to_id"
                                                class="to_id form-control"
                                                >
                                                <option 
                                                    value="0" 
                                                    disabled="true" 
                                                    selected="true"
                                                    title="Seleccione un Destinatario"
                                                    
                                                    >
                                                    -Seleccione un Destinatario-
                                                </option>
                                                @foreach ($users as $u)
                                                    <option 
                                                        value="{{$u->id }}">
                                                            {{$u->superior}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Asunto</label>
                                            <input type="text" name="subject" class="form-control" id="recipient-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Mensaje</label>
                                            <textarea class="form-control" name="text" id="message-text"></textarea>
                                        </div>
                                       
                                        <div class="form-group"> 
                                            <label for="image" class="col-form-label">Imagen</label>
                                            <input id="file" type="file" name="image" class="img-responsive">
                                            <div id="preview"></div>
                                           
                                       </div>
                                    
                                    </div>                     
                                </div>
                            </div>

                            <!--pie del modal-->
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 ml-auto">
                                        <button type="submit" class="btn btn-primary btn-responsive">
                                            <i class="fa fa-envelope"> Enviar</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        
                    
                        </form>    
                    </div>

                </div>


          
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Detalles del mensaje -->
                    <h4>Asunto: {{ $message->subject }}</h4>
                    <h4>Descripcion: {{ $message->text }}</h4>
                    @if ($message->image != NULL)
                        <h4>Imagen</h4>
                        <img src="{{asset('imagenes/photo/'.$message->image)}}"
                            alt="Sin imagen"
                            class="img-thumbnail"
                            height="100px"
                            width="100px"
                            class="img-responsive">
                    @endif    
                    <h4> Emisor del Mensaje: {{ $message->user->name }} del grupo de trabajo: {{ $message->ticket->workgroup->name }}</h4>
                    <!-- Detalles del mensaje -->
                    <hr>
                    <!-- Detalle del Ticket -->
                    @if ($message->ticket->current_stage->name  == "Creado")
                        <h4>Estado actual: </h4><span class="label label-default">{{$message->ticket->current_stage->name }}</span>
                    @endif
                    @if ($message->ticket->current_stage->name  == "Revisado")
                        <h4>Estado actual: <span class="label label-primary">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    @if ($message->ticket->current_stage->name  == "En proceso")
                        <h4>Estado actual: <span class="label label-info">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    @if ($message->ticket->current_stage->name  == "Resuelto")
                        <h4>Estado actual: <span class="label label-success">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    @if ($message->ticket->current_stage->name  == "No Resuelto")
                        <h4>Estado actual: <span class="label label-danger">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    @if ($message->ticket->current_stage->name  == "Reasignado")
                        <h4>Estado actual: <span class="label label-warning">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    @if ($message->ticket->current_stage->name  != "Creado" && $message->ticket->current_stage->name  != "Revisado" && $message->ticket->current_stage->name  != "En proceso" && $message->ticket->current_stage->name  != "Resuelto" && $message->ticket->current_stage->name  != "No Resuelto" && $message->ticket->current_stage->name  != "Reasignado")
                        <h4>Estado actual: <span class="label label-default">{{$message->ticket->current_stage->name }}</span></h4>
                    @endif
                    <h4>Fecha de Creación del Ticket {{Carbon\Carbon::parse($message->ticket->created_at)->format('d/m/Y')}}</h4>
                
                    <!-- Detalle del Ticket -->
                    <hr>
                    <!-- Detalles del Cliente -->
                    <h4>Cliente {{$message->ticket->client()}}</h4>
                    <h4>Ubicación: {{$message->ticket->address() }}</h4>  
                    <!-- Detalles del Cliente -->

                  

                </div>              
            </div>   
        </div>
    </div>
</div>
                    

 




@push('scripts')     
<script language="JavaScript">

document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }

       

    $(document).ready(function(){
        $("#to_id").select2({
            placeholder:'-Seleccione un Destinatario-',
            width: '100%',

        });  
    });
</script>
@endpush


@endsection