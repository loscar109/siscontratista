
{!! Form::model(Request::only(
    ['ticket_category_id','desde','hasta', 'current_stage_id', "workgroup_id"]),
    ['url' => 'tickets/gestion', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search'] 
    
    )!!}

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="ticket_category_id">Categoría</label>
    <div class="form-group">
        <select 
            name="ticket_category_id"
            id="ticket_category_id"
            class="ticket_category_id form-control"
            >
          
                @foreach ($ticket_categories as $ticket_category)
                    <option 
                        value="{{$ticket_category->id}}"
                        @if($ticket_category_id!=null && $ticket_category_id==$ticket_category->id)
                            selected
                        @endif
                    >
                    {{$ticket_category->name}}
                    </option>
                @endforeach

                <option
                    value="0" 
                    @if($ticket_category_id == null || $ticket_category_id==0)
                        selected
                    @endif
                >
                    -- Todas las categorias --
                </option>
        </select>
    </div>
</div>

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="current_stage_id">Estado</label>
    <div class="form-group">
        <select 
            name="current_stage_id"
            id="current_stage_id"
            class="current_stage_id form-control"
            >
          
                @foreach ($ticket_stages as $ticket_stage)
                    <option 
                        value="{{$ticket_stage->id}}"
                        @if($current_stage_id!=null && $current_stage_id==$ticket_stage->id)
                            selected
                        @endif
                    >
                    {{$ticket_stage->name}}
                    </option>
                @endforeach

                <option
                    value="0" 
                    @if($current_stage_id == null || $current_stage_id==0)
                        selected
                    @endif
                >
                    -- Todos los Estados --
                </option>
        </select>
    </div>
</div>

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="workgroup_id">Grupos de Trabajo</label>
    <div class="form-group">
        <select 
            name="workgroup_id"
            id="workgroup_id"
            class="workgroup_id form-control"
            >
          
                @foreach ($workgroups as $workgroup)
                    <option 
                        value="{{$workgroup->id}}"
                        @if($workgroup_id!=null && $workgroup_id==$workgroup->id)
                            selected
                        @endif
                    >
                    {{$workgroup->name}}
                    </option>
                @endforeach

                <option
                    value="0" 
                    @if($workgroup_id == null || $workgroup_id==0)
                        selected
                    @endif
                >
                    -- Todos los Grupos de Trabajos --
                </option>
        </select>
    </div>
</div>

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="desde">Fecha Desde</label>
    <input
        type="date"
        name="desde"
        id="desde"
        class="fecha form-control"
        value="{{$desde}}"
    >        
</div>    


<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="hasta">Fecha Hasta</label>
    <input
        type="date"
        name="hasta"
        id="hasta"
        class="fecha form-control"
        value="{{$hasta}}"
    >       
</div>
       

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for=""></label>
    <div class="form-group">
        <span class="input-group-btn">
            <button
                title="buscar"
                type="submit"
                id="bt_add"
                name="filtrar"
                class="btn btn-primary btn-responsive">
                    <i class="fa fa-filter"></i> Filtrar
            </button>
            <button 
                class="btn btn-danger"
                name="pdf"
                type="submit"
                >
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                PDF
            </button>
            <a 
            
            href= "{{ route('tickets.gestion.index') }}"
            class="btn btn-default"
            >
            <i class="fas fa-eraser"></i>
                ... Limpiar
        </a>
           
        </span>
    </div>
</div>





{{Form::close()}}

@push('scripts')     
<script type="text/javascript">
$(document).ready(function(){
    $("#ticket_category_id").select2({
        width: '100%', 
        placeholder: '-Seleccione una categoria-'
    });

    $("#current_stage_id").select2({
        width: '100%', 
        placeholder: '-Seleccione un estado-'
    });

    $("#workgroup_id").select2({
        width: '100%', 
        placeholder: '-Seleccione un Grupo de Trabajo-'
    });

    //si existe un cambio en Fecha Desde
    $('#desde').change(function() {
        
        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {
      
        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () { 
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);
       
 
      });
    

});




    


</script>
@endpush
