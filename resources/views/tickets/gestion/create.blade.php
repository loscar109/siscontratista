@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align: center">
    <a href="{{url()->previous()}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>   
</div>
@endsection

@section('content')


{!!Form::open(array(
    'url'=>'tickets/gestion',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}


    <div class="box-body">
        @include('errors.request')
        @include('tickets.gestion.mensaje')
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">   
                        <i class="fa fa-tasks"></i>
                            Crear Ticket
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label for="description">
                                Nombre
                            </label>
                            <input 
                                type="string"
                                name="description"
                                minlength="4"
                                maxlength="70"
                                required value="{{old('description')}}"
                                class="form-control"
                                placeholder="descripcion..."
                                title="Introduzca una descripcion para el ticket a crear"
                                >
                                <br>
                                <label for="detalle">
                                    Detalle
                                </label>
                                <input 
                                    type="text"
                                    name="detail"
                                    minlength="6"
                                    maxlength="70"
                                    required value="{{old('detail')}}"
                                    class="form-control"
                                    placeholder="detalle..."
                                    title="Introduzca una descripcion para el ticket a crear"
                                    >
                                    <br>
                                <label for="priority">
                                        Prioridad
                                </label>
                                <select 
                                    name="priority"
                                    id="priority"
                                    class="priority form-control"
                                    required
                                >
                                    <option value="1" selected="true" title="-prioridad muy baja-">1</option>
                                    <option value="2"  title="-prioridad baja-">2</option>
                                    <option value="3"  title="-prioridad media-">3</option>
                                    <option value="4"  title="-prioridad alta-">4</option>
                                    <option value="5"  title="-prioridad muy alta-">5</option>

                                </select>
                                        <br>
                            <label for="description">
                                Cliente
                            </label>
                            <select 
                                name="client_id"
                                id="client_id"
                                class="client_id form-control"
                                required
                                >
                                <option 
                                    value="0" 
                                    disabled="true" 
                                    selected="true"
                                    title="-Seleccione un cliente-"
                                    >
                                    -Seleccione un cliente-
                                </option>
                                @foreach ($users as $user)
                                    <option 
                                        value="{{$user->id }}">
                                            {{$user->usname . " " . $user->usurname . " " . $user->usemail}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <label>
                            Categorias
                        </label>
                        <select 
                            name="ticket_category_id"
                            id="ticket_category_id"
                            class="ticket_category_id form-control"
                            required
                            >
                            <option 
                                value="0" 
                                disabled="true" 
                                selected="true"
                                title="-Seleccione una categoria-"
                                >
                                -Seleccione una categoria-
                            </option>
                            @foreach ($ticket_category as $ticket_categories)
                                <option 
                                    value="{{$ticket_categories->id }}">
                                        {{$ticket_categories->name}}
                                </option>
                            @endforeach
                        </select>

                        <br><br>
                    
                        @can('configuracion.workgroup.create')
                        <label>
                            Grupo de Trabajo
                        </label>
                        <select
                            name="workgroup_id"
                            id="workgroup_id"
                            class="workgroup_id form-control"
                            required
                            >
                            <option 
                                value="0" 
                                disabled="true" 
                                selected="true"
                                title="-Seleccione un Grupo de Trabajo-"
                                >
                                -Seleccione un Grupo de Trabajo-
                            </option>
                            
                        </select>

                        <br>
                        <br>
                        <label align="center" style="font-size:25px;" class="font-weight-bold; average_time">
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
            <div class="form-group">
                <button title="Limpiar" class="btn btn-danger btn-lg btn-block" type="reset"><i class="fa fa-eraser">...</i> Limpiar</button>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" align="center">    
            <div class="form-group">
                <button title="Guardar" class="btn btn-success btn-lg btn-block" type="submit"> <i class="fa fa-check"></i> Guardar</button>
            </div>
        </div>
    </div>


    

{!!Form::close()!!}


  
@push('scripts')     
    <script type="text/javascript">

    $(document).ready(function(){

        $("#client_id").select2({
            placeholder:'-Seleccione un cliente-',
            width: '100%',
        });
        $("#priority").select2({
            placeholder:'-Seleccione una prioridad-',
            width: '100%',
        });


        $("#ticket_category_id").select2({
            placeholder:'-Seleccione una categoria-',
            width: '100%',
        });


        $("#workgroup_id").select2({
            placeholder:'-Seleccione un Grupo de Trabajo-',
            width: '100%',
        });

        
        $(document).on('change','.ticket_category_id',function(){
            var ticket_category_id=$(this).val();
            console.log("Se selecciono la categoria:"+ticket_category_id);
            var div=$(this).parent();    
            var op=" ";
            


            $.ajax({
                type:'get',
                url:'{!!URL::to('configuracion/ticket_category/create/showWorkGroup')!!}',
                data:{'id':ticket_category_id},
                success:function(data){
                    console.log('se trajo los datos del grupo de trabajo');
                    console.log(data);
                    console.log(data.length);
                    console.log(typeof(data));

                    op+='<option value="0" selected disabled>-Seleccione un Grupo de Trabajo-</option>'+data;
                    
                                          

            
                    div.find('.workgroup_id').html(" ");
                    div.find('.workgroup_id').append(op);
                    op = `<label class="font-weight-bold average_time"></label>`;

                    div.find('.average_time').html(" ");
                    div.find('.average_time').append(op);
                },
                error:function(){    
                }
            });


        });
   


        $(document).on('change','.workgroup_id',function(){
            var workgroup_id=$(this).val();
            console.log("Se selecciono el grupo de trabajo:"+workgroup_id);
            var div=$(this).parent();    
            var op=" ";

            

            $.ajax({
                type:'get',
                url:'{!!URL::to('configuracion/workgroup/create/showAverage')!!}',
                data:{'id':workgroup_id},

                success:function(data){
                    console.log('se trajo el promedio');
                    console.log(data);
                    if(typeof data === "object")
                    {
                        op = `<label class="font-weight-bold average_time">Todavia no hay ticket resueltos</label>`;
                        div.find('.average_time').html(" ");
                        div.find('.average_time').append(op);                    
                    }
                    else
                    {
                        op = `<label class="font-weight-bold average_time"><i style="color:#CCCC00;" class="fa fa-lightbulb-o"></i> ${data}</label>`;
                        div.find('.average_time').html(" ");
                        div.find('.average_time').append(op);
                    
                    }

                  

                        

                        
                        
                    


                },
                error:function(){    
                    console.log('no entra');

                }
            });


        });



     });
    </script>
@endpush

@endsection


