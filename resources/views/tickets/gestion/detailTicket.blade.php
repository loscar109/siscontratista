@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ url()->previous() }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
    <div class="box-body">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-tasks"></i> {{ucwords($ticket->description)}}
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table  style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                                <thead style="background-color:#357CA5">
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Codigo de Ticket</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->number }}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Estado Actual</p></th>
                                            <td style="background-color:#FFFFFF">
                                                @if ($ticket->current_stage->name  == "Creado")
                                                    <span class="label label-default">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  == "Revisado")
                                                    <span class="label label-primary">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  == "En proceso")
                                                    <span class="label label-info">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  == "Resuelto")
                                                    <span class="label label-success">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  == "No Resuelto")
                                                    <span class="label label-danger">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  == "Reasignado")
                                                    <span class="label label-warning">{{$ticket->current_stage->name }}</span>
                                                @endif
                                                @if ($ticket->current_stage->name  != "Creado" && $ticket->current_stage->name  != "Revisado" && $ticket->current_stage->name  != "En proceso" && $ticket->current_stage->name  != "Resuelto" && $ticket->current_stage->name  != "No Resuelto" && $ticket->current_stage->name  != "Reasignado")
                                                    <span class="label label-default">{{$ticket->current_stage->name }}</span>
                                                @endif
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Descripción</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->description }}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Detalle</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->detail }}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Fecha de Creación</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->created_at->format('d/m/Y H:i:s') . " (" .$ticket->created_at->diffForhumans() . ") "}}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Prioridad</p></th>
                                            <td style="background-color:#FFFFFF">
                                                @if ($ticket->priority  == 5)
                                                    <span class="label label-danger" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                                @if ($ticket->priority  == 4)
                                                    <span class="label label-warning" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                                @if ($ticket->priority  == 3)
                                                    <span class="label label-warning" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                                @if ($ticket->priority  == 2)
                                                    <span class="label label-success" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                                @if ($ticket->priority  == 1)
                                                    <span class="label label-success" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                                @if ($ticket->priority  != 1 && $ticket->priority  != 2 && $ticket->priority  != 3 && $ticket->priority  != 4 && $ticket->priority  != 5)
                                                    <span class="label label-default" style="font-size:120%">{{$ticket->priority }}</span>
                                                @endif
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Ubicación</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->address() }}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Cliente</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->client() }}
                                            </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Categoría de Ticket</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->nameTicketCategory() }}
                                            </td>
                                    </tr>                                
                                    <tr>
                                        <th style="background-color:#FFFFFF"><p class="text-uppercase">Nombre del Grupo de Trabajo</p></th>
                                            <td style="background-color:#FFFFFF">
                                                {{ $ticket->nameWorkgroup() }}
                                            </td>
                                    </tr> 
                                </thead>
                            </table>
                        </div>   
                    </div> 
                </div>  
            </div>  
        </div>
        <!-- aca arrancan los mensajes -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="box box-primary direct-chat direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Mensajeria</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>  
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="direct-chat-messages">
                        @foreach ( $ticket->message_tickets as $message_ticket )
                        <!--Si el usuario es el que inicio sesión -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left"> {{ $message_ticket->user->name }}</span>
                                    <span class="direct-chat-timestamp pull-right">  {{ $message_ticket->created_at->format('d/m/Y H:i:s') . " (" .$message_ticket->created_at->diffForhumans() . ") "}}</span>
                                </div>
                                @if ($message_ticket->user->photo == NULL)
                                    <img
                                        class="direct-chat-img"
                                        src="{{asset('/imagenes/users/default.png')}}"
                                        height="25px"
                                        width="25px"
                                        >
                                @else
                                    <img
                                        class="direct-chat-img"
                                        src="{{asset($message_ticket->user->photo)}}"
                                        height="25px"
                                        width="25px"
                                        >
                                @endif                 
                                <div class="direct-chat-text">
                                    {{$message_ticket->text}}
                                </div>
                                @if ($message_ticket->image != NULL)
                                    <img src="{{asset('imagenes/photo/'.$message_ticket->image)}}"
                                    alt="Sin imagen"
                                    class="img-thumbnail"
                                    height="300px"
                                    width="300px"
                                    class="img-responsive">
                                @endif
                            </div>
                        @endforeach    
                    </div>      
                </div>
            </div>
        </div> 
    </div>



    <div class="box-body">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-tasks"> </i> Indice de Tickets
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="divDetalle" class="table-responsive">
                            <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                                <thead style="background-color:#357CA5">
                                    <tr>
                                        <th width="35%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Estado</p></th>
                                        <th width="35%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Usuario Responsable</p></th>
                                        <th width="30%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Fecha</p></th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @foreach ($ticket->stage_histories as $sh)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td><p>{{ $sh->ticket_stage->name }}</p></td>
                                        <td><p>{{ $sh->user->name }}</p></td>
                                        <td><p>{{ $sh->date->format('d/m/Y H:i:s') . " (" .$sh->date->diffForhumans() . ") "}}</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-tasks"> </i> Consumo de Materiales
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if($ticket->consumptions->isNotEmpty())
                        <div id="divDetalle" class="table-responsive">
                            <table id="tablaDetalle2" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                                <thead style="background-color:#357CA5">
                                    <tr>
                                        <th width="35%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Material</p></th>
                                        <th width="35%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Cantidad</p></th>
                                        <th width="30%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Fecha de Consumo</p></th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @foreach ($ticket->consumptions as $consumption)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td><p>{{ $consumption->material->description}}</p></td>
                                        <td><p>{{ $consumption->quantity }}</p></td>
                                        <td>
                                            {{ Carbon\Carbon::parse($consumption->dateOfConsumption)->format('d/m/Y') }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                        @else
                        <p 
                            class="p-3 mb-2 bg-warning text-dark"
                            >
                            No hay Consumos de Materiales registrados
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>




    




@push('scripts')     

<script language="JavaScript">
$(document).ready(function() {
    $('#tablaDetalle').DataTable({
        "aaSorting":[],
        "language":{
            "info":"_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next":"Siguiente",
                "previous":"Anterior"
            },
            "lengthMenu":'Mostrar <select>'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<select> registros',
            "loadingRecords":"Cargando...",
            "processing":"Procesando...",
            "emptyTable":"No hay datos",
            "zeroRecords":"No hay coincidencias",
            "infoEmpty":"",
            "infoFiltered":""

        },
        "pageLength" : 5,
        "lengthMenu": "[[5, 10], [5, 10]]"
    });

    $('#tablaDetalle2').DataTable({
        "aaSorting":[],
        "language":{
            "info":"_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next":"Siguiente",
                "previous":"Anterior"
            },
            "lengthMenu":'Mostrar <select>'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<select> registros',
            "loadingRecords":"Cargando...",
            "processing":"Procesando...",
            "emptyTable":"No hay datos",
            "zeroRecords":"No hay coincidencias",
            "infoEmpty":"",
            "infoFiltered":""

        },
        "pageLength" : 5,
        "lengthMenu": "[[5, 10], [5, 10]]"
    });

} );





</script>
@endpush


@endsection