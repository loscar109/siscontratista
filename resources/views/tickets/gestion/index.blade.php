@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('tickets.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-tasks"> </i> Indice de Tickets
                </h4>
                <div class="box-tools">
                    <a href= "gestion/create">
                        <button class="btn btn-default">
                            <i class="fa fa-tasks"> <sup> <i class="fa fa-plus"></i></sup></i> Nuevo
                        </button>
                    </a>
                                   
                </div>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">

                            @include('tickets.gestion.search') <!-- elemento de la -->

                           
                                
                        </div>
                        
                    </div>

                    @if($tickets->isNotEmpty())
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Codigo</p></th>

                                    <th width="20%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Ticket</p></th>
                                    <th width="20%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Descripción</p></th>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Fecha de Creación</p></th>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Grupo de Trabajo</p></th>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Categoría</p></th>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Estado Actual</p></th>
                                    <th width="10%" style="color:#FFFFFF" height="15px"><p class="text-uppercase">Detalle</p></th>

                      
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($tickets as $ticket)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td><p class="text-uppercase">{{$ticket->number }}</p></td>
                                        <td><p class="text-uppercase">{{$ticket->description }}</p></td>
                                        <td><p class="text-uppercase">{{substr($ticket->detail,0,15)."..." }}</p></td>
                                        <td><p class="text-uppercase">{{Carbon\Carbon::parse($ticket->created_at)->format('d/m/Y')}}</p></td>
                                        <td>{{$ticket->nameWorkGroup()}}</td>
                                        <td><p class="text-uppercase">{{$ticket->nameTicketCategory() }}</p></td>
                                        <td>
                                            @if ($ticket->ticket_stage->name  == "Creado")
                                                <span class="label label-default">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  == "Revisado")
                                                <span class="label label-primary">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  == "En proceso")
                                                <span class="label label-info">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  == "Resuelto")
                                                <span class="label label-success">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  == "No Resuelto")
                                                <span class="label label-danger">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  == "Reasignado")
                                                <span class="label label-warning">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                            @if ($ticket->ticket_stage->name  != "Creado" && $ticket->ticket_stage->name  != "Revisado" && $ticket->ticket_stage->name  != "En proceso" && $ticket->ticket_stage->name  != "Resuelto" && $ticket->ticket_stage->name  != "No Resuelto" && $ticket->ticket_stage->name  != "Reasignado")
                                                <span class="label label-default">{{$ticket->ticket_stage->name }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{URL::action('TicketController@detailTicket',$ticket)}}">
                                                <button title="ver" class="btn btn-celeste btn-responsive ">
                                                    <i class="fa fa-eye"></i>
                                                </button> 
                                            </a> 
                                        </td>
                    
                                        
                                    </tr>
                                    @include('tickets.gestion.modalshow')  
                    
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay Grupos de Trabajos registrados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>




@push('scripts')     
<script src="{{asset('js/tablaDetalle.js')}}"></script>

    
@endpush
@endsection


