<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SisContratista</title>

        <!--Bootsrap 4 CDN-->
        <link rel="stylesheet" href="{{asset('css/allForBootstrap4.css')}}">

        
        <!--Fontawesome CDN-->


        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!--Custom styles-->
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="{{asset('css/botonDefault.css')}}">
        <style>

        html,body{
        background-image: url(imagenes/login/IniciarSesion.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        height: 100%;
        font-family: 'Numans', sans-serif;
        }

        .container{
        height: 100%;
        align-content: center;
        }

        .card{
        height: 350px;
        margin-top: auto;
        margin-bottom: auto;
        width: 400px;
        background-color: rgba(0,0,0,0.5) !important;
        }

        .social_icon span{
        font-size: 60px;
        margin-left: 10px;
        color: #3C8DBC;
        }

        .social_icon span:hover{
        color: white;
        cursor: pointer;
        }

        .card-header h3{
        color: white;
        
        }
        
        #titulo{
        background-color: #3C8DBC;
        }

        #texto{
            color: white;
            margin-left: 100px;


        }

        .social_icon{
        position: absolute;
        right: 20px;
        top: -45px;
        }

        .input-group-prepend span{
        width: 50px;
        background-color: #3C8DBC;
        color: black;
        border:0 !important;
        }
        Carousel with Search
        input:focus{
        outline: 0 0 0 0  !important;
        box-shadow: 0 0 0 0 !important;

        }

        .remember{
        color: white;
        }

        .remember input
        {
        width: 20px;
        height: 20px;
        margin-left: 15px;
        margin-right: 5px;
        }

        .login_btn{
        color: black;
        background-color: #3C8DBC;
        width: 100px;
        }

        .login_btn:hover{
        color: black;
        background-color: white;
        }

        .links{
        color: white;
        }

        .links a{
        margin-left: 4px;
        }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-50">
                <div class="card">
                                <div id="titulo">
                                    <h3 id="texto" >SisContratista</h3>
                                </div>
                                @if (Route::has('login'))
                                @auth
                                <img 
                                    src="{{asset('imagenes/login/open.png')}}"
                                    class="img-thumbnail img-responsive"
                                    height="350px"
                                    width="400px"
                                    alt="No hay imagen"
                                    >


                                    <a href="{{ url('/home') }}">
                                            <button class="btn-celeste btn-lg btn-block">
                                                <i class="fa fa-unlock-alt"></i>  Pagina Principal
                                        </button>
                                    </a>

                                
                                @else
                                <img 
                                    src="{{asset('imagenes/login/lock.png')}}"
                                    class="img-thumbnail img-responsive"
                                    height="350px"
                                    width="400px"
                                    alt="No hay imagen"
                                    >



                                    <a href="{{ route('login') }}">
                                            <button class="btn-celeste btn-lg btn-block">
                                                <i class="fas fa-lock"></i>  Iniciar Sesion
                                        </button>
                                    </a>
                                @endauth
                                @endif
                </div>
              
            </div>
        </div>
    </body>
</html>
            
