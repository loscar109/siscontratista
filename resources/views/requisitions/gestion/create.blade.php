@extends('layouts.admin')
<style>
#toast-container{
    width: 300px;
}</style>
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ url()->previous() }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>   
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        @include('errors.request')
        @include('requisitions.gestion.mensaje')

        {!!Form::open(array(
            'url'=>'requisitions/gestion',
            'method'=>'POST',
            'autocomplete'=>'off',
            'files' => true,
        ))!!}

        {{Form::token()}}
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-box"> </i> Solicitar Nuevo Pedido a la Central
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>
                                        Materiales
                                    </label>
                                    <select 
                                        name="material_id"
                                        id="material_id"
                                        class="material_id form-control"
                                        >
                                        <option 
                                            value="0" 
                                            disabled="true" 
                                            selected="true"
                                            title="Seleccione una categoria"
                                            
                                            >
                                            -Seleccione un Material-
                                        </option>
                                        @foreach ($materials as $m)
                                            <option 
                                                @if($m->is_ordered==false)
                                                value="{{$m->id }}">
                                                    {{$m->bundle_name}}
                                                @endif
                                            </option>
                                        @endforeach
                                    </select>
                                </div>                    
                            </div>
                        
                                
                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="quantity">Cantidad</label>
                                        <input
                                            min="1"
                                            type="number"
                                            pattern="[0-9]+"
                                            name="quantity"
                                            id="quantity"
                                            class="form-control"
                                            >
                                </div>  
                            </div>

                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for=""></label>
                                    <br>
                                    <br>
                                    <button type="button" id="bt_add" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button>     
                                </div>  
                            </div>

                           

                            <div class="box box-default col-lg-12 col-sm-12 col-md-12 col-xs-12"style="width:100%; height:400px; overflow: scroll;">
                                <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                                    <thead style="background-color:#3C8DBC">
                                        <th colspan="2" style="color:#FFFFFF">Material</th>
                                        <th style="color:#FFFFFF">Packs a solicitar</th>
                                        <th style="color:#FFFFFF">Cantidad a solicitar</th>
                                        <th style="color:#FFFFFF">Stock existente</th>
                                        <th style="color:#FFFFFF">Stock futuro</th>


                                    </thead>
                                    <tfoot>
                                        <th></th> <!-- Material-->
                                        <th></th> <!-- Material-->
                                        <th></th> <!-- Packs a solicitar-->
                                        <th></th> <!-- Cantidad a solicitar-->
                                        <th></th> <!-- Stock existente-->
                                        <th></th> <!-- Stock futuro-->

                                    </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="form-group" id="guardar">
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <button class="btn btn-success" id="btn_add"type="submit"><i class="fa fa-check"> </i>Guardar</button>
                        <a href= "{{ route('requisitions.gestion.create') }}" class="btn btn-default">
                            <i class="fas fa-eraser"></i>... Limpiar
                        </a>
                    </div>
            </div>
            
    </div>        
</div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">   
        @if($critico->isNotEmpty())

            <div class="box box-solid box-danger">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-warning" style="color:#yellow"> </i> Stock Crítico
                    </h4>
                </div>
                <div class="box-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="alert alert-danger" role="alert">
                            <p style="font-size:110%"><i class="fa fa-eye" aria-hidden="true"></i> A continuación se muestra los materiales que están por debajo del minimo establecido, se recomienda solicitarlos cuanto antes</p>
                        </div>
                        

                            <div class="panel-body">
                                    <table id="detalle" class="table table-striped table-bordered table-condensed table-hover">
                                        <thead style="background-color:#DD4B39">
                                            <tr>
                                                <th width="30px" style="color:#FFFFFF">Material</th>
                                                <th width="70px" style="color:#FFFFFF">Stock existente</th>
                                                <th width="10px" style="color:#FFFFFF">Stock Minimo</th>
                                                <th width="10px" style="color:#FFFFFF">Unidades por Paquetes</th>


                                            </tr>    
                                        </thead>
                                        <tbody>
                                            @foreach ($critico as $c)
                                                <tr>
                                                    <td>{{ $c->description }}</td>
                                                    <td><p class="text-right">{{ $c->stock }}</p></td>
                                                    <td><p class="text-right">{{ $c->smin }}</p></td>
                                                    <td><p class="text-right">{{ $c->bundle_quantity }}</p></td>
                                                </tr>
                                            @endforeach
                                        </tbody>        
                                    </table>
                            </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Todo en orden</span>
                    <span class="info-box-number">No hay Stock Crítico</span>
                  </div>
                </div>
              </div>
        @endif

    </div>


{!!Form::close()!!}


@push('scripts')     
    <script type="text/javascript">

   
        $(document).ready(function(){

            $('#detalle').DataTable({
                "aaSorting":[],
                "language":{
                "info":"_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next":"Siguiente",
                    "previous":"Anterior"
                },
                "lengthMenu":'Mostrar <select>'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<select> registros',
                "loadingRecords":"Cargando...",
                "processing":"Procesando...",
                "emptyTable":"No hay datos",
                "zeroRecords":"No hay coincidencias",
                "infoEmpty":"",
                "infoFiltered":""
            },
            "pageLength" : 5,
            "lengthMenu": "[[5, 10], [5, 10]]"
            });
            $("#material_id").select2({
                placeholder:'-Seleccione un material-',
                width: '100%',

            });
            $("#bt_add").click(function(){
                agregar();
               
            });
         



            
           
          
            
           
            
        });

        var input=  document.getElementById('quantity');
            input.addEventListener('input',function(){
            if (this.value.length > 5) 
                this.value = this.value.slice(0,5); 
            })


        var cont=0;
        total=0;
        var max=0;
        var bundle_quantity=0;
        var stock=0;
        var suma=0;
        $("#guardar").hide();

        function agregar()
        {
           
            material_id=$("#material_id").val();
            materials=$("#material_id option:selected").text();
            quantity=$("#quantity").val();
  
           

            /*   Aca iría el Ajax para obtener la cantidad por Paquete*/
            $.ajax({
                type:'get',
                url:'{!!URL::to('requisitions/gestion/create/findMaterialData')!!}',
                data:{'id':material_id},
                success:function(data){
                    max=data['smax'];
                    bundle_quantity=data['bundle_quantity'];
                    stock=data['stock'];
                    photo=data['photo'];

                    console.log(stock+bundle_quantity*quantity);
                    console.log(max);
                    suma = bundle_quantity*quantity
                    if (stock+(bundle_quantity*quantity)<= max && quantity>0)
                        {
                            total=1;
                            var fila='<tr><td><img class="img-thumbnail" height="85px" alt="sin imagen" width="85px" src='+photo+'></td><td><input type="hidden" name="material_id[]" value="'+material_id+'"><p style="font-size:110%" class="text-left">'+materials+'</p></td><td><input type="hidden" name="quantity[]" value="'+quantity+'"><p style="font-size:140%" class="text-right">'+quantity+'</p></td><td><input type="hidden" name="suma[]" value="'+suma+'"><p style="font-size:140%;background-color:#F4F590" class="text-right"><b>'+suma+'</b></p></td><td><input type="hidden" name="stock" value="'+stock+'"><p style="font-size:140%" class="text-right">'+stock+'</p></td><td ><input type="hidden"><p style="font-size:140%;background-color:#F4F590" class="text-right"><strong><em>'+(suma+stock)+'</em></strong></td></tr>';         
                            cont++;
                            limpiar();      
                            evaluar();
                            $("#detalles").append(fila);
                            eliminarDelSelect2 ();

                        }
                    else if(quantity<=0)
                        {
                            toastr.error("La cantidad debe ser un numero mayor o igual a 1");
                        }
                    else if(stock+(bundle_quantity*quantity)> max)
                        {
                            toastr.error('La cantidad solicitada ('+(suma+stock)+') excede el stock maximo ('+max+')');
                        }
                  
                        
                    


                },
                error:function(){    
                    console.log('no anda AJAX');
                }
            });   
            /*   Aca iría el Ajax para obtener el stock maximo y realizar el multiplicador*/



         
           


              
             
            /*   Aca cierra el Ajax*/


            
            
        }

        function limpiar()
        {
            $("#quantity").val("");
        }

        function evaluar()
            {
                if(total>0)
                {
                    $("#guardar").show();
                }
                else
                {
                    $("#guardar").hide();
                }
            }

        function eliminarDelSelect2 ()
        {
            $("#material_id option:selected").remove();

        }

       
       

    </script>
@endpush


@endsection