@extends('layouts.admin')

@section('titulo')

<div class="box-header" style="text-align:center">
    <a href="{{ url()->previous() }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>   
@endsection

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        {!!Form::open(array(
            'url'=>'requisitions/manual/'.$requisition->id,
            'method'=>'POST',
            'autocomplete'=>'off',
            'files' => true,
        ))!!}
        
        {{Form::token()}}
    @include('errors.request')
    @include('requisitions.gestion.mensaje')
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h4 class="box-title">
                <i class="fa fa-tasks"> </i> Confirmación de Ingreso del Pedido
            </h4>
        </div>
        <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="divDetalle" class="table-responsive">
                    <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                        <thead style="background-color:#357CA5">
                            <tr>

                                <th width="10%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>
                                <th width="40%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Material</p></th>
                                <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Cantidad que se Pidió</p></th>
                                <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Cantidad que Recibió</p></th>

                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($requisition->requisition_details as $r)
                                <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                    <td align="center"><input id="material_id" name="material[{{$r->material_id}}]"type="checkbox"></td>
                                    <td>{{$r->material->description}}</td>
                                    <td>{{$r->quantity}}</td>
                                    <td><input id="quantity" type="number" name="quantity[{{$r->material_id}}]" value="{{$r->quantity}}"></td>

                                    
                                </tr>

                            @endforeach
                        </tbody>        
                    </table>
                </div>
            </div>
        </div>

        
        <div class="box-footer">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="form-group">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                        <label for="dispatch_number">Numero de Remito</label>
                        <input id="input1" type="number" class="form-control"  name="dispatch_number">
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                        <label for="dispatch_date">Fecha de Entrada de Remito</label>
                        <input type="date" class="form-control" value="{{now()->format('Y-m-d')}}"  name="dispatch_date">                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="container">

            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="form-group">
                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                            <button name="close" class="btn btn-success btn-lg btn-block" id="btn_add"type="submit"><i class="fa fa-check"> </i>Guardar y Cerrar el Pedido</button>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                            <button name="open" class="btn btn-danger btn-lg btn-block" id="abierto" type="submit"> <i class="fa fa-check"></i> Guardar y dejar el Pedido abierto</button>
                        </div>
                </div>
            </div>
        </div>


{!!Form::close()!!}



@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {

  

            $('#tablaDetalle').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);

           

        } );

        var input=  document.getElementById('input1');
            input.addEventListener('input',function(){
            if (this.value.length > 10) 
                this.value = this.value.slice(0,10); 
            })

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"
        } 


        
    </script>
@endpush
@endsection






