@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')

<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('requisitions.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fas fa-box-open"> </i> Indice de Pedidos
                </h4>
                <div class="box-tools">
                    <a href= "{{ asset("requisitions/gestion/entry") }}">
                        <button class="btn btn-celeste">
                            <i class="fa fa-tasks"> <sup> <i class="fa fa-plus"></i></sup></i> Indice de Entradas
                        </button>
                    </a>     
                    <a href= "{{ asset("requisitions/gestion/create") }}">
                        <button class="btn btn-celeste">
                            <i class="fa fa-tasks"> <sup> <i class="fa fa-plus"></i></sup></i> Nuevo
                        </button>
                    </a>   
                              
                </div>        
            </div>
            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">
                            @include('requisitions.gestion.search')<!-- elemento de la -->           
                        </div>                  
                    </div>            
                    @if($requisitions->isNotEmpty())
                        <div id="divDetalle" class="table-responsive">
                            <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                                <thead style="background-color:#357CA5">
                                    <tr>
                                        <th width="10%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Numero de Pedido</p></th>
                                        <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Fecha de Pedido</p></th>
                                        <th width="70%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    @foreach ($requisitions as $r)
                                        <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                            <td>{{ $r->number }}</td>
                                            <td><p class="text-uppercase">{{Carbon\Carbon::parse($r->date_of_entry)->format('d/m/Y') }}</p></td>
                                            <td colspan="2">
                                                @if(!$r->is_completed)
                                                    <!--Cargar Pedido Completo -->
                                                    <a href="" data-target="#modal-automatic-{{$r->id}}" data-toggle="modal">
                                                        <button title="ver" class="btn btn-primary pull-right">
                                                            Pedido Automatico
                                                        </button>
                                                    </a>
                                                
                                                    <!--Cargar Pedido Manuak -->
                                                    <a href="{{ route('requisitions.gestion.manual',$r->id) }}" class="btn btn-success pull-right">
                                                        Pedido Manual 
                                                    </a>
                                                @endif
                                                <a 
                                                    href= "{{URL::action('StockController@exportPdf',$r->id)}}"
                                                    class="btn btn-danger"
                                                    target="_blank">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                    Pedido de Materiales
                            
                                                </a>      
                                            </td>
                                        </tr>
                                        @include('requisitions.gestion.modalshow')  

                                    @endforeach
                                </tbody>        
                            </table>
                        </div>
                    @else
                        <p 
                            class="p-3 mb-2 bg-warning text-dark"
                            >
                            No hay Pedidos registrados
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
    

@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tablaDetalle').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);

         

        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection


