@if (Session::has('delete_requisition_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_requisition_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('store_requisition'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_requisition') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_requisition'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_requisition') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
@if (Session::has('automatic_requisition'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('automatic_requisition') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
@if (Session::has('manual_requisition'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('manual_requisition') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_requisition'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_requisition') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_entry'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_entry') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('search_requisition_fail'))
        <div id="search_requisition" class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('search_requisition_fail') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('search_requisition_success'))
        <div id="search_requisition" class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('search_requisition_success') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function () 
        {
                $( "#search_requisition_success" ).prop('disabled', false);
                $( "#search_requisition" ).prop('disabled', false);


                setTimeout(function(){$("#search_requisition").remove();}, 5000 );
                setTimeout(function(){$("#search_requisition_success").remove();}, 5000 );


                $( "#search_requisition" ).prop('disabled', true);
                $( "#search_requisition_success" ).prop('disabled', true);


        });
    </script>
@endpush


