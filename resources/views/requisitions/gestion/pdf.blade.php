<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">


    <style>
        @page { margin: 50px; }
            
            #footer 
            { 
                position: fixed; 
                left: 0px;
                bottom: -180px; 
                right: 0px; 
                height: 50px; 
                background-color: #357CA5; 
                color: #FFFFFF;

            }
            #footer .page:after
            { 
                content: counter(page, decimal); 
                float: right;
                background-color: #357CA5; 
                color: #FFFFFF;
             
            }
            
           
           
 
  
    </style>

    

    <title>Document</title>
</head>
<body>
    
    <div id="content" class="container">
        
        
        <!--Tabla-->
        <table style="border:3px solid #357CA5 width:100%" class="table table-condensed table-hover">
            
            <!-- Cabecera del reporte -->
            <thead style="background-color:#357CA5">
                <tr>
                    <th class="h2 text-center" colspan="3"style="color:#FFFFFF">Reporte de Pedidos de Materiales</th>
                </tr>
            </thead>
             <!-- Datos del reporte -->
            <thead style="background-color:#FFFFFF; border:0">
                <tr>
                    <th colspan="3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <!-- Logo -->
                                    <img  
                                        src="{{asset('imagenes/logo/logo.png')}}"
                                        alt="logo de la empresa"
                                        width="200px"
                                    > 
                                </td>
                                <td>
                                    <!-- Emision -->
                                    <p>FECHA DE PEDIDO : <strong>{{Carbon\Carbon::parse($requisitions->date_of_entry)->format('d/m/Y') }}</strong></p>
                                    <p>NUMERO DE PEDIDO: <strong>{{$requisitions->number}}</strong></p>
                                   
                                </td>
                            </tr>
                        </table>                       
                    </th>
                </tr>
            </thead>
            <thead>
                <tr>
                    <th colspan="3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <p>USUARIO: <strong> {{ $requisitions->user->surname }}, {{ $requisitions->user->name }}</strong></p>
                                </td>
                            </tr>                        
                        </table>
                    </th>
                </tr>
            </thead>
            <thead style="background-color:#357CA5">
                    <tr colspan="3">
                        <th width="50%" style="color:#FFFFFF" height="25px" ><p class="text-uppercase">Material</p></th>
                        <th width="50%" style="color:#FFFFFF" height="25px" ><p class="text-uppercase">Cantidad</p></th>
                        <th width="50%" style="color:#FFFFFF" height="25px" ><p class="text-uppercase">Total</p></th>

                    </tr>
            </thead>
            <tbody>
                @foreach($requisitions->requisition_details as $rd)
                    <tr>
                        <td class="text-left" style="border:1px solid grey">{{ $rd->material->bundle_name }}</td>
                        <td class="text-right" style="border:1px solid grey">{{ $rd->quantity }}</td>
                        <td class="text-right" style="border:1px solid grey">{{ $rd->quantity * $rd->material->bundle_quantity }}</td>
                    </tr>
                @endforeach
                    
            </tbody>

        </table>
        <div class="page-break"></div>

    </div>

    
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>



  
 <!-- Bootstrap 3.3.5 -->
 <script src="{{ asset('js/bootstrap.min.js') }}"></script>

 
 

   <!-- AdminLTE App -->
 <script src="{{asset('js/app.min.js')}}"></script>
    

 <script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 800, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>

</body>
</html>
