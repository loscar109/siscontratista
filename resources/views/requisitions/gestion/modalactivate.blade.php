{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-activate-{{$r->id}}">


    {{Form::Open(array(
        'action'=>array('StockController@destroy',$r->id),
        'method'=>'delete'
        ))}}



        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header"  style="background-color: green">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Cargar Productos</h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    <p>¿Desea cargar los productos establecidos en el pedido? <b>{{$r->description}}</b></p>
                </div>

                <!--pie del modal-->
                <div class="modal-footer">                  
                    <button type="submit"  class="m-2 btn btn-primary pull-left m-2">Pedido Completo</button>
                    <a href="#">
                        <button  type="button"class="m-2 btn btn-primary pull-left">Pedido Manualmente</button>
                    </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                    @include('errors.request')
                </div>
            

            </div>
        </div>

    {{Form::Close()}}

</div>