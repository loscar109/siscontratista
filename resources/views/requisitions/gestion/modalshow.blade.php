{{--ventanita modal cuando se haga clic en ver--}}
<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-automatic-{{$r->id}}">


     <form action="{{ route('requisitions.gestion.automatic',$r->id) }}" method="post">
        {{csrf_field()}}


        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header" style="background-color:#357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Confirmar Recibo de Pedido Numero: <b>{{$r->number}}</b></h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    @include('errors.request')

                    <div class="container-fluid">
                        <div class="form-group">
                            <label for="dispatch_number">Numero de Remito</label>
                            <input id="quantity" type="number" class="form-control"  name="dispatch_number">

                            <label for="dispatch_date">Fecha de Entrada de Remito</label>
                            <input type="date" class="form-control" value="{{now()->format('Y-m-d')}}"  name="dispatch_date">
                        </div>                     
                    </div>
                </div>
                

                <!--pie del modal-->
                <div class="modal-footer">
                        <div class="col-md-12 ml-auto">
                                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">
                                    <i class="fa fa-close"> Cerrar</i>
                                </button>
                                <button class="btn btn-primary pull-right" type="submit" > Confirmar </button>

                        </div>
                </div>
               
            
            </div>    
        </div>

    </form>

</div>
<script type="text/javascript">
var input=  document.getElementById('quantity');
            input.addEventListener('input',function(){
            if (this.value.length > 10) 
                this.value = this.value.slice(0,10); 
            })
</script>
