{{--ventanita modal cuando se haga clic en ver--}}
      <!-- Bootstrap 3.3.5 -->   
      <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">

      <!-- Font Awesome -->

      <!-- Theme style -->
<script src="{{asset('js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>


<!-- Bootstrap 3.3.5 -->

<!-- AdminLTE App -->


<div class="modal fade bs-example-modal-lg"
     aria-labelledby="classInfo"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     enctype="multipart/form-data"
     id="entry{{$r->id}}">


     <form action="{{ route('requisitions.gestion.entry',$r->id) }}" method="post">
        {{csrf_field()}}


        <div class="modal-dialog modal-lg">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header" style="background-color:#357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Detalle de Remito <b>Nro {{$r->dispatch_number}}</b></h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                        <b><p style="font-size:150%">Numero de Pedido: {{ $r->requisition->number}}</p></b>
                   
                    <div class="table-responsive">                                   
                        <table id="total_records{{$r->id}}" class="table table-condensed table-striped table-hover responsive">
                            <thead>
                                <tr>
                                    <th>Material</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($r->reception_details as $detail)
                                <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                    <td>
                                        <img src="{{asset('imagenes/materiales/'.$detail->material->photo)}}"
                                        class="img-thumbnail"
                                        height="50px"
                                        width="50px"
                                        >
                                    
                                        {{ $detail->material->description }}
                                    </td>
                                    <td style="line-height:50px">
                                        {{ $detail->quantity . " (" . $detail->material->category->medida . ") " }}
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>        
                        </table>
                    </div>

                   
                </div>
                

                <!--pie del modal-->
                <div class="modal-footer">
                        <div class="col-md-12 ml-auto">
                                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">
                                    <i class="fa fa-close"> </i>Cerrar
                                </button>

                        </div>
                </div>
               
            
            </div>    
        </div>

    </form>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#total_records{{$r->id}}").DataTable({

            "language":{
                "info":"_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next":"Siguiente",
                    "previous":"Anterior"
                },
                "lengthMenu":'Mostrar <select>'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<select> registros',
                "loadingRecords":"Cargando...",
                "processing":"Procesando...",
                "emptyTable":"No hay datos",
                "zeroRecords":"No hay coincidencias",
                "infoEmpty":"",
                "infoFiltered":""

            },
            "bDestroy": true,
            "pageLength" : 5,
            "lengthMenu": "[[5, 10], [5, 10]]"                
        });
        cambiar_color_over(celda);

    } );
</script>