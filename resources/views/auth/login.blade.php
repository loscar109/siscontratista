
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="{{asset('css/allForBootstrap4.css')}}">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


    
    <!--Fontawesome CDN-->


	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="stylesheet" href="{{asset('css/botonDefault.css')}}">

    
    <style>
       

        html,body{
        background-image: url(imagenes/login/IniciarSesion.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        height: 100%;
        font-family: 'Numans', sans-serif;
        }

        .container{
        height: 100%;
        align-content: center;
        }

        .card{
        height: 350px;
        margin-top: auto;
        margin-bottom: auto;
        width: 400px;
        background-color: rgba(0,0,0,0.5) !important;
        }

        .social_icon span{
        font-size: 60px;
        margin-left: 10px;
        color: #3C8DBC;
        }

        .social_icon span:hover{
        color: white;
        cursor: pointer;
        }

        .card-header h3{
        color: white;
        }

        strong{
        color: white;
        }

        .social_icon{
        position: absolute;
        right: 20px;
        top: -45px;
        }

        .input-group-prepend span{
        width: 50px;
        background-color: #3C8DBC;
        color: black;
        border:0 !important;
        }
        Carousel with Search
        input:focus{
        outline: 0 0 0 0  !important;
        box-shadow: 0 0 0 0 !important;

        }

        .remember{
        color: white;
        }

        .remember input
        {
        width: 20px;
        height: 20px;
        margin-left: 15px;
        margin-right: 5px;
        }

        .login_btn{
        color: black;
        background-color: #3C8DBC;
        width: 100px;
        }

        .login_btn:hover{
        color: black;
        background-color: white;
        }

        .links{
        color: white;
        }

        .links a{
        margin-left: 4px;
        }

    </style>
</head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <h3 >Iniciar Sesión</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        <div class="card-body">
                            {{ csrf_field() }}
                            @include('auth.message')

                            <div class="input-group form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group-prepend">
                                    <span for="email" class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input
                                    type="email"
                                    minlength="13"
                                    maxlength="30"
                                    class="form-control"
                                    name="email"
                                    title="E-mail"
                                    required
                                    type="{{ old('email') }}"
                                    autofocus>   
                            </div>

                            <div class="input-group form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="input-group-prepend">
                                    <span for="password" class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input 
                                    type="password"
                                    minlength="8"
                                    maxlength="30"
                                    class="form-control"
                                    name="password"
                                    title="Contraseña"
                                    type="{{ old('password') }}"
                                    required
                                    >
                            </div>  

                            <div class="row align-items-center remember">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame
                                    </label>
                                </div>
                            </div>


                        </div>
                        <div  class="card-footer">
                            <input type="submit" value="Ingresar" class="btn float-right btn-celeste">
                                
                        </div>
                    </form>         
                </div>
            </div>
        </div>
    </body>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#email').fadeIn();     
            setTimeout(function() {
            $("#email").fadeOut();           
        },3000);
    });

</script>
    
</html>