    @if ($errors->has('email'))
        <div id="email" class="alert alert-danger"  role="alert">{{ $errors->first('email') }}
                <span aria-hidden="true"></span>  
        </div>
    @endif

    @if ($errors->has('password'))
        <div id="password" class="alert alert-danger" role="alert">{{ $errors->first('password') }}
                <span aria-hidden="true"></span>
        </div>
    @endif
