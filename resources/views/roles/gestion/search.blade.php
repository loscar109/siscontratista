{!!Form::open(array(
    'url'=>'roles/gestion',
    'method' => 'GET',
    'autocomplete' => 'off',
    'role'=>'search')
    )!!}

<div class="form-group">
    <div class="input-group">
        <input type="text" class="form-control" name="searchText" placeholder="Buscar..." value="{{$searchText}}">
        <span class="input-group-btn">
            <button title="buscar" type="submit" class="btn btn-primary btn-responsive"><i class="fa fa-search"></i></button>
        </span>
    </div>
</div>


{{Form::close()}}