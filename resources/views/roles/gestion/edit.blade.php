@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Editar Rol: <strong>{{$roles->name}}</strong></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('roles/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @include('errors.request')

           @can('roles.gestion.edit') 
            {!!Form::model($roles, [
                'method'=>'PATCH',
                'route'=>['roles.gestion.update',$roles->id]
            ])!!}
            @endcan
            

            {{Form::token()}}
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{ $roles->name }}">
            </div>

            <div class="form-group">
                <label for="slug">URL</label>
                <input type="text" name="slug" class="form-control" value="{{ $roles->slug }}">
            </div>

            <div class="form-group">
                <label for="description">Descripcion</label>
                <input type="textarea" name="description" class="form-control" value="{{ $roles->description }}">
            </div>

            <hr>
            <h3>Permiso Especial</h3>
            <div class="form-group">
                <label>{{ Form::radio('special','all-access') }} Acceso Total</label>
                <label>{{ Form::radio('special','no-access') }} Ningun Acceso</label>
            </div>

            <hr>
            <h3>Lista de Permisos</h3>
            <div class="form-group">
                <ul class="list-unstyled">
                    @foreach ($permissions as $permission)
                        <li>
                            <label>
                                {{ Form::checkbox('permissions[]',$permission->id, null) }}
                                {{ $permission->name }}
                                <em>({{ $permission->description ?: 'Sin descripcion' }})</em>
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="form-group">
                <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i></button>
                <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i></button>
            </div>
            {!! Form::close()!!}
           

        </div>
    </div>



@endsection