@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{asset('/home')}}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection


@section('content')
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @include('errors.request')
            @include('roles.gestion.mensaje') 
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-tasks"> </i> Indice de Roles
                    </h4>
                    <div class="box-tools">
                        <a href="gestion/create">
                            <button title="nuevo"class="btn btn-primary btn-responsive">
                                <i class="fa fa-plus">Nuevo</i>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table id="roles" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th width="80%" style="color:#FFFFFF" height="25px">Nombre</th>
                                    <th width="20%" style="color:#FFFFFF" height="25px" >Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td>{{ $role->name }}</td>
                                        <td style="text-align: center" colspan="3">
                                            @can('roles.gestion.show')
                                                <a href="{{URL::action('RoleController@show',$role->id)}}">
                                                    <button title="ver" class="btn btn-celeste btn-responsive ">
                                                        <i class="fa fa-eye"></i>
                                                    </button> 
                                                </a>      
                                            @endcan
                                            @can('roles.gestion.edit')
                                                <a href="{{URL::action('RoleController@edit',$role->id)}}">
                                                    <button title="editar" class="btn btn-celeste btn-responsive">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            $('#roles').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
        cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"
        } 
    </script>
@endpush
@endsection

