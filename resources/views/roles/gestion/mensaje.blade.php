@if (Session::has('delete_role_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_role_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_role'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_role') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_role'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_role') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_role'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_role') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif