@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Crear Rol</h3>
    <div class="box-tools pull-right">
        <a href="{{asset('roles/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    <div class="row">
            {!!Form::open(array(
                'url'=>'roles/gestion',
                'method'=>'POST',
                'autocomplete'=>'off'
            ))!!}

            {{Form::token()}}

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> 
        @include('errors.request')         
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" minlength="13" maxlength="30" name="name" class="form-control" placeholder="Nombre...">
            </div>
            <div class="form-group">
                <label for="slug">URL</label>
                <input type="text" minlength="13" maxlength="30" name="slug" class="form-control" placeholder="Slug...">
            </div>
            <div class="form-group">
                <label for="description">Descripcion</label>
                <input type="textarea" minlength="13" maxlength="30" name="description" class="form-control" placeholder="description...">
            </div>
            <h3>Permiso Especial</h3>
                <div class="form-group">
                    <label>{{ Form::radio('special','all-access') }} Acceso Total</label>
                    <label>{{ Form::radio('special','no-access') }} Ningun Acceso</label>
                </div>
            <hr>
            <div class="form-group">
                <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Limpiar</button>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">       
            <h4>Lista de Permisos</h4>
                <div style="width:700px; height:400px; overflow: scroll;" class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="list-unstyled">
                            @foreach ($permissions as $permission)
                                <li>
                                    <label>
                                        {{ Form::checkbox('permissions[]',$permission->id, null) }}
                                        {{ $permission->name }}
                                        <em>({{ $permission->description ?: 'Sin descripcion' }})</em>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>         
                </div>
        </div>
        {!! Form::close()!!}
    </div>



@endsection