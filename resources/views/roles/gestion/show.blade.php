@extends('layouts.admin')


@section('titulo')

    <h3 class="box-title">Detalles del Rol: <b>{{ucwords($roles->name)}}</b></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('roles/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection



@section('content')

   

    <div class="row">
  
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="name">Nombre</label>
                <p>{{ucwords($roles->name)}}</p>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="slug">URL</label>
                <p>{{ucwords($roles->slug)}}</p>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="description">Descripcion</label>
                <p>{{ucwords($roles->description)}}</p>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="special">Special</label>
                <p>{{ucwords($roles->special  ?: 'Sin Permisos Especiales')}}</p>
            </div>
        </div>


    </div>

    @if($roles->special!="all-access")
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">       
            <div style="width:650px; height:400px; overflow: scroll;" class="panel panel-primary">
                <div class="panel-body">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        
                        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                            <thead style="background-color:#A9D0F5">
                                <th>Permisos</th>
                            </thead>
                            <tfoot>
                                <th></th>
                            </tfoot>
                            <tbody>
                                @foreach ($permission_role as $pr)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td>{{ $pr->permisos}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>             
                </div>
            </div>
        </div>
    </div>
    @else
        <em>{{'El Rol '. ucwords($roles->slug).' posee todos los permisos existenes en el sistema'}}</em>
    @endif
               
    @push('scripts')     
    <script type="text/javascript">

        $(document).ready(function(){
            cambiar_color_over(celda);
        });

        function cambiar_color_over(celda){
            celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
            celda.style.backgroundColor="#FFFFFF"
        } 
    </script>
@endpush


@endsection