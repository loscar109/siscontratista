@extends('layouts.admin')

@section('titulo')

    <h4 class="box-title" >Indice de Flujo de Trabajos</h4>
   
@endsection

@section('content')

@include('configuracion.workflow.mensaje')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <a href="{{asset('/home')}}"><button title="atras" class="btn btn-default btn-responsive">
            <i class="fa fa-arrow-left"> Atras</i> </button>
        </a>     
        <a href="workflow/create">
            <button title="nuevo"class="btn btn-celeste btn-responsive">
                    <i  class="fa fa-tasks"> Nuevo</i> 
            </button>
        </a>
        
    </div>
</div>


<br>
<div id="divDetalle" class="table-responsive">
    <table id="tablaDetalle" style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
        <thead style="background-color:#357CA5">
            <tr>
                <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Nombre</p></th>
                <th width="60%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Descripción</p></th>
                <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>
            </tr>
        </thead>
        <tbody>
            
            @foreach ($workflows as $workflow)
                <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                    <td><p class="text-uppercase">{{$workflow->name }}</p></td>
                    <td><p class="text-uppercase">{{$workflow->description }}</p></td>

                    <td style="text-align: center" colspan="3">

                     
                        <!--boton ver-->
                        <a href="{{URL::action('WorkFlowController@show',$workflow->id)}}">
                            <button title="ver" class="btn btn-primary btn-responsive ">
                                <i class="fa fa-eye"></i>
                            </button> 
                        </a>      

                       
                       
                        
                    </td>
                </tr>



            @endforeach
        </tbody>        
    </table>
</div>


@push('scripts')     
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tablaDetalle').DataTable({
                "language":{
                    "info":"_TOTAL_ registros",
                    "search": "Buscar",
                    "paginate": {
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                    "lengthMenu":'Mostrar <select>'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<select> registros',
                    "loadingRecords":"Cargando...",
                    "processing":"Procesando...",
                    "emptyTable":"No hay datos",
                    "zeroRecords":"No hay coincidencias",
                    "infoEmpty":"",
                    "infoFiltered":""

                }
            });
            cambiar_color_over(celda);
        } );

        function cambiar_color_over(celda){
        celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
        celda.style.backgroundColor="#FFFFFF"

       
        } 
    </script>
@endpush
@endsection


