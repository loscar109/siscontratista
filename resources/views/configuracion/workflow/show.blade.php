@extends('layouts.admin')


@section('titulo')

    <h3 class="box-title">Detalles del WorkFlow: <b>{{ucwords($workflow->name)}}</b></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('configuracion/workflow')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection




@section('content')
    <div class="table-responsive">
        <table style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
            <thead style="background-color:#357CA5">
                <tr>
                    <th width="50%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Estado Actual</p></th>
                    <th width="50%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Estado Siguiente</p></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($workflow->ticket_stage_orders as $tso)
                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                        <td><p class="text-uppercase">{{$tso->current->name }}</p></td>
                        <td><p class="text-uppercase">{{$tso->next->name }}</p></td>
    
                    </tr>
                   
    
    
                @endforeach
            </tbody>        
        </table>
    </div>
@endsection