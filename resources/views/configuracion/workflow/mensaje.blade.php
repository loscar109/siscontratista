@if (Session::has('delete_workflow_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_workflow_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_workflow'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_workflow') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_workflow'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_workflow') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_workflow'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_workflow') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif