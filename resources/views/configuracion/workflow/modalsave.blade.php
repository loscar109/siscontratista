{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-save">



    {{Form::Open(array(
        'action'=>array('WorkFlowController@store',$workflow->id),
        'method'=>'post'
        ))}}



        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header"  style="background-color: #C9303A">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Eliminar Flujo de Trabajo</h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                        <div class="form-group">
                            <label>
                                Estado Origen
                            </label>
                                <select 
                                    name="ticket_stage_id"
                                    id="ticket_stage_current_id"
                                    class="ticket_stage_current_id form-control"
                                    >
                                        @foreach ($ticket_stages as $ticket_stage)
                                            <option 
                                                value="{{$ticket_stage->id}}"
                                                >
                                                {{$ticket_stage->name}}
                                            </option>
                                        @endforeach
                                </select>
                        </div>
                    </div>
            

                <!--pie del modal-->
                <div class="modal-footer">
                    <input id="guardar" name="_token" value="{{ csrf_token() }}" type="hidden">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"> </i>Guardar</button>
                        <button class="btn btn-danger" type="reset"> <i class="fa fa-eraser">...</i> Borrar</button>
                    @include('errors.request')
                </div>
            

            </div>
        </div>

    {{Form::Close()}}

</div>