@extends('layouts.admin')
@section('titulo')

    <h4 class="box-title" >Crear Flujo de Trabajo</h4>
   
@endsection

@section('content')
<div>     
    <a href="{{asset('/configuracion/workflow')}}"><button title="atras" class="btn btn-default btn-responsive">
        <i class="fa fa-arrow-left"> Atras</i> </button>
    </a>     
</div>
<br>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        @include('errors.request')
        @include('configuracion.workflow.mensaje')
    </div>
</div>

{!!Form::open(array(
    'url'=>'configuracion/workflow',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}
    <div class="row">     
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="name">
                    Nombre
                </label>
                <input 
                    id="name"
                    type="string"
                    name="name"
                    minlength="5"
                    maxlength="30"
                    required value="{{old('name')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el estado de ticket a crear"
                    >
            </div>
        </div>
        
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="description">
                    Descripcion
                </label>
                <input 
                    id="description"
                    type="string"
                    name="description"
                    minlength="5"
                    maxlength="30"
                    required value="{{old('description')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el estado de ticket a crear"
                    >
            </div>
        </div>


        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label>
                    Estado Origen
                </label>
                    <select 
                        name="ticket_stage_id"
                        id="ticket_stage_current_id"
                        class="ticket_stage_current_id form-control"
                        >
                            @foreach ($ticket_stages as $ticket_stage)
                                <option 
                                    value="{{$ticket_stage->id}}"
                                    >
                                    {{$ticket_stage->name}}
                                </option>
                            @endforeach
                    </select>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label>
                    Estado Destino
                </label>
                    <select 
                        name="ticket_stage_id"
                        id="ticket_stage_next_id"
                        class="ticket_stage_next_id form-control"
                        >
                            @foreach ($ticket_stages as $ticket_stage)
                                <option 
                                    value="{{$ticket_stage->id}}"
                                    >
                                    {{$ticket_stage->name}}
                                </option>
                            @endforeach
                    </select>
            </div>
        </div>


        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <button type="button" id="bt_add" class="btn btn-primary">Agregar</button>     
            </div>  
        </div>

        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                <thead style="background-color:#A9D0F5">
                    <th>Opciones</th>
                    <th>Estado Origen</th>
                    <th>Estado Destino</th>
                </thead>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
            <div class="form-group">
                    <button type="button" title="eliminar" class="btn btn-primary  btn-responsive"  data-target="#modal-save" data-toggle="modal">
                        <i class="fa fa-check"></i>
                    </button>
                
            </div>
        </div>

    </div>
   

<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-save">

        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header"  style="background-color: #357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Asignar Estado Inicial Al Flujo de Trabajo</h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                        <div class="form-group">
                            <h4>
                                Por favor, seleccione un estado inicial para indicar el punto de partida del flujo de trabajo
                            </h4>
                            <br>
                            <label>
                                Estado Inicial
                            </label>
                                <select 
                                    name="initial_stage_id"
                                    id="initial_stage_id"
                                    class="initial_stage_id form-control"
                                    >
                                        @foreach ($ticket_stages as $ticket_stage)
                                            <option 
                                                value="{{$ticket_stage->id}}"
                                                >
                                                {{$ticket_stage->name}}
                                            </option>
                                        @endforeach
                                </select>
                        </div>
                    </div>
            

                <!--pie del modal-->
                <div class="modal-footer">
                    <input id="guardar" name="_token" value="{{ csrf_token() }}" type="hidden">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"> </i>Guardar</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-close"> Cerrar</i>
                        </button>
                    @include('errors.request')
                </div>
            

            </div>
        </div>

</div>






    {!!Form::close()!!}



@endsection

@push('scripts')     
    <script type="text/javascript">
    $(document).ready(function(){
        $("#guardar").hide();

        $("#ticket_stage_current_id").select2({
            width: '100%',
        });

        $("#initial_stage_id").select2({
            width: '100%',
        });

              $("#ticket_stage_next_id").select2({
            width: '100%',
        });


        $("#bt_add").click(function(){
            agregar();
        });

    });

    var cont=0;
    total=0;

    function agregar()
    {
        //capturo el estado actual del ticket
        current_id=$("#ticket_stage_current_id").val();

        //caputo el estado siguiente del ticket
        next_id=$("#ticket_stage_next_id").val();
        //capturo el nombre del WorkFlow
        name=$("#name").val();
        //capturo la descripcion del WorkFlow
        description=$("#description").val();

        current=$("#ticket_stage_current_id option:selected").text();
        next=$("#ticket_stage_next_id option:selected").text();


        if (name!="" && description!="")
        {

            var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');")>X</button></td><td><input type="hidden" name="current_id[]" value="'+current_id+'">'+current+'</td><td><input type="hidden" name="next_id[]" value="'+next_id+'">'+next+'</td></tr>';         
            cont++;
            if (current != next)
            {
                $("#detalles").append(fila);
                $("#guardar").show();
            }
            else
            {
                toastr.error("No pueden ser iguales Estado Origen y Estado Destino")
            }
                


        }
        else
        {
            if(name=="")
            {
             toastr.error("El nombre del Flujo de Trabajo esta vacío")   
            }

            if(description=="")
            {
                toastr.error("La descripcion del Flujo de Trabajo esta vacío")   



            }

                  
        }
        
    }
    function eliminar(index)
    {
        $("#fila" + index).remove();

    }
</script>
@endpush
