@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Listado de Parametros de Configuracion</h3>
    <div class="box-tools pull-right">
        <a href="{{asset('/home')}}"><button  title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="info-box">
            <span class="info-box-icon bg-green">
                <i class="fa fa-tasks" aria-hidden="true"></i>  
            </span>  
            <div class="info-box-content">
                @can('configuracion.ticket_category.index')
                <a href="{{asset('configuracion/ticket_category') }}">
                    <i class="info-box-text"></i>Categorias de Ticket
                </a>
                @endcan
                @can('configuracion.ticket_stage.index')
                <a href="{{asset('configuracion/ticket_stage') }}">
                    <i class="info-box-text"></i>Estados de Ticket
                </a>
                @endcan
                @can('configuracion.ticket_stage.index')
                <a href="{{asset('configuracion/workflow') }}">
                    <i class="info-box-text"></i>Flujo de Trabajo
                </a>
                @endcan               
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue">
                    <i class="fas fa-box"></i>
            </span>
            <div class="info-box-content">        
                <a href="{{asset('configuracion/category') }}">
                    <i class="info-box-text"></i>Categorias de Materiales
                </a>
            </div>
        </div>
    </div>
            
    


</div>


@endsection