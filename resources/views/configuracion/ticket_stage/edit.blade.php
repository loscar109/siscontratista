@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Editar Estados de Ticket: <strong>{{$ticket_stages->name}}</strong></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('tickets/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @include('errors.request')

            {!!Form::model($ticket_stages, [
                'method'=>'PATCH',
                'route'=>['configuracion.ticket_stage.update',$ticket_stages->id]
            ])!!}
            

            {{Form::token()}}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="control-label">Nombre</label>
                        <input id="name" minlength="13" maxlength="30" type="text" class="form-control" name="name" value="{{ $ticket_stages->name }}" required autofocus>

                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="control-label">Descripción</label>

                        <input id="description" type="description" class="form-control" name="description" value="{{ $ticket_stages->description }}" required>

    
                </div>

                
            <div class="form-group">
                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i></button>
                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i></button>
            </div>
            {!!Form::close()!!}
           

        </div>
    </div>



@endsection