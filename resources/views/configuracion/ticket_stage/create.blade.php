@extends('layouts.admin')
@section('titulo')

    <h4 class="box-title" >Crear Estado Ticket</h4>
   
@endsection

@section('content')
<div>     
    <a href="{{asset('/configuracion/ticket_stage')}}"><button title="atras" class="btn btn-default btn-responsive">
        <i class="fa fa-arrow-left"> Atras</i> </button>
    </a>     
</div>
<br>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        @include('errors.request')
        @include('configuracion.ticket_stage.mensaje')
    </div>
</div>

{!!Form::open(array(
    'url'=>'configuracion/ticket_stage',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}
    <div class="row">     
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="name">
                    Nombre
                </label>
                <input 
                    type="string"
                    name="name"
                    minlength="6"
                    maxlength="30"
                    required value="{{old('name')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el estado de ticket a crear"
                    >
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="description">
                    Descripcion
                </label>
                <input 
                    type="string"
                    name="description"
                    minlength="13"
                    maxlength="30"
                    required value="{{old('description')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el estado de ticket a crear"
                    >
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-left ">
            <div class="form-group">
                <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
            </div>
        </div>

    </div>

    {!!Form::close()!!}



   

@endsection


