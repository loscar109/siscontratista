{{--ventanita modal cuando se haga clic en ver--}}
<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-show-{{$ticket_category->id}}">



    {{Form::Open(array(
        'action'=>array('TicketCategoryController@show',$ticket_category->id),
        'method'=>'get'
        ))}}



        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">
                
                <!-- cabecera del modal -->
                <div class="modal-header" style="background-color:#357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Detalles de la categoria de ticket <b>{{$ticket_category->name}}</b></h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                          
                            <div class="col-md-8 ml-auto">
                                    <p>Descripción de Categoría del Ticket</p>
                                    <ul class="list-group">
                                        <li>Nombre : {{$ticket_category->name }}</li>
                                        <li>Descripción : {{$ticket_category->description }}</li>
                                    </ul>                   
                            </div>
                        </div>                     
                    </div>
                </div>

                <!--pie del modal-->
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12 ml-auto">
                                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">
                                <i class="fa fa-close"> Cerrar</i>
                            </button>
                            @include('errors.request')
                        </div>
                    </div>
                </div>
               
            
            </div>    
        </div>

    {{Form::Close()}}

</div>

