@extends('layouts.admin')
@section('titulo')

    <h4 class="box-title" >Crear Categoria Ticket</h4>
   
@endsection

@section('content')
<div>     
    <a href="{{asset('/configuracion/ticket_category')}}"><button title="atras" class="btn btn-default btn-responsive">
        <i class="fa fa-arrow-left"> Atras</i> </button>
    </a>     
</div>
<br>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        @include('errors.request')
        @include('tickets.gestion.mensaje')
    </div>
</div>

{!!Form::open(array(
    'url'=>'configuracion/ticket_category',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}
    <div class="row">     
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="name">
                    Nombre
                </label>
                <input 
                    type="string"
                    name="name"
                    required value="{{old('name')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el ticket a crear"
                    >
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="description">
                    Descripcion
                </label>
                <input 
                    type="string"
                    name="description"
                    required value="{{old('description')}}"
                    class="form-control"
                    placeholder="descripcion..."
                    title="Introduzca una descripcion para el ticket a crear"
                    >
            </div>
        </div>

        
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label>
                    Flujo de Trabajo
                </label>
                    <select 
                        name="workflow_id"
                        id="workflow_id"
                        class="workflow_id form-control"
                            >
                            <option 
                                value="0" 
                                disabled="true" 
                                selected="true"
                                title="Seleccione un flujo de trabajo"
                                >
                                -Seleccione un flujo de trabajo-
                            </option>
                                @foreach ($workflows as $workflow)
                                    <option 
                                        value="{{$workflow->id}}" >
                                            {{$workflow->name}}
                                    </option>
                                @endforeach
                    </select>
            </div>
        </div>
            
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-left ">
            <div class="form-group">
                <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
            </div>
        </div>

    </div>

    {!!Form::close()!!}



    @push('scripts')     
    <script type="text/javascript">
     $(document).ready(function(){
        $("#workflow_id").select2({
            placeholder:'-Seleccione un flujo de trabajo-',
            width: '100%'
        });
     });
    </script>
    @endpush

@endsection


