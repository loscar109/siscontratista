{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-delete-{{$ticket_category->id}}">



    {{Form::Open(array(
        'action'=>array('TicketCategoryController@destroy',$ticket_category->id),
        'method'=>'delete'
        ))}}



        <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header"  style="background-color: #C9303A">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Eliminar Categoría de Ticket</h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    <p>Confirme si desea Eliminar el Ticket <b>{{$ticket_category->name}}</b></p>
                </div>

                <!--pie del modal-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"> Cerrar</i>
                    </button>
                    <button type="submit" class="btn btn-celeste">
                        <i class="fa fa-check"> Confirmar</i>
                    </button>
                    @include('errors.request')
                </div>
            

            </div>
        </div>

    {{Form::Close()}}

</div>