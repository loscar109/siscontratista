@if (Session::has('delete_ticket_category_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_ticket_category_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_ticket_category'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_ticket_category') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_ticket_category'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_ticket_category') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_ticket_category'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_ticket_category') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif