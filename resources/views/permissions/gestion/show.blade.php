@extends('layouts.admin')


@section('titulo')

    <h3 class="box-title">Detalles del Permiso: <b>{{ucwords($permissions->name)}}</b></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('permissions/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection



@section('content')

   

    <div class="row">
  
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="name">Nombre</label>
                <p>{{ucwords($permissions->name)}}</p>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="slug">URL</label>
                <p>{{ucwords($permissions->slug)}}</p>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <label for="description">Descripcion</label>
                <p>{{ucwords($permissions->description)}}</p>
            </div>
        </div>



        
        
  
@endsection