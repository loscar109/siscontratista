@extends('layouts.admin')

@section('titulo')

    <h3 class="box-title">Editar Permiso: <strong>{{$permissions->name}}</strong></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('permissions/gestion')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @include('errors.request')

           @can('permissions.gestion.edit') 
            {!!Form::model($permissions, [
                'method'=>'PATCH',
                'route'=>['permissions.gestion.update',$permissions->id]
            ])!!}
            @endcan
            

            {{Form::token()}}
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{$permissions->name}}" placeholder="Nombre...">
            </div>

            <div class="form-group">
                <label for="slug">URL</label>
                <input type="text" name="slug" class="form-control" value="{{$permissions->slug}}" placeholder="URL...">
            </div>

            <div class="form-group">
                <label for="description">Descripcion</label>
                <input type="text" name="description" class="form-control" value="{{$permissions->description}}" placeholder="Descripcion...">
            </div>

            <div class="form-group">
                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i></button>
                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i></button>
            </div>
            {!!Form::close()!!}
           

        </div>
    </div>



@endsection