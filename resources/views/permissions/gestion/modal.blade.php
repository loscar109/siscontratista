{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     id="modal-delete-{{$permission->id}}">



    {{Form::Open(array(
        'action'=>array('PermissionController@destroy',$permission->id),
        'method'=>'delete'
        ))}}






        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Eliminar Permiso</h4>
                </div>
                <div class="modal-body">
                    <p>Confirme si desea Eliminar el Permiso {{$permission->name}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    @include('errors.request')


                </div>
            

            </div>
        </div>

    {{Form::Close()}}

</div>