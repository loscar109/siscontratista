@extends('layouts.admin')
@section('titulo')

    <h3 class="box-title">Lista de Permisos</h3>
    <div class="box-tools pull-right">
        <a href="{{asset('/home')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection

@section('content')

    @include('permissions.gestion.mensaje') 
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">          
            @include('permissions.gestion.search')
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">          
            <a href="gestion/create">
                <button title="nuevo"class="btn btn-primary btn-responsive">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table style="border:3px solid #357CA5" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color:#357CA5">
                        <th  width="20%" style="color:#FFFFFF">Nombre</th>
                        <th  width="30%" style="color:#FFFFFF">URL</th>
                        <th  width="30%" style="color:#FFFFFF">Descripcion</th>

                        <th  width="20%" style="color:#FFFFFF" colspan="3">Opciones</th>
                        
                    </thead>
                    @foreach ($permissions as $permission)
                        <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                            <td>{{ $permission->name }}</td>
                            <td>{{$permission->slug}}</td>
                            <td>{{$permission->description}}</td>
                            <td style="text-align: center" colspan="3">
                                @can('permissions.gestion.show')
                                    <a href="{{URL::action('PermissionController@show',$permission->id)}}">
                                        <button title="ver" class="btn btn-primary btn-responsive ">
                                            <i class="fa fa-eye"></i>
                                        </button> 
                                    </a>      
                                @endcan
                                @can('permissions.gestion.edit')
                                    <a href="{{URL::action('PermissionController@edit',$permission->id)}}">
                                        <button title="editar" class="btn btn-warning btn-responsive">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                @endcan
                                @can('permissions.gestion.destroy')
                                    <a href="" data-target="#modal-delete-{{$permission->id}}" data-toggle="modal">
                                        <button title="eliminar" class="btn btn-danger btn-responsive">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                        @include('permissions.gestion.modal')   
                    @endforeach
                </table>
            </div>
            {{$permissions->render()}}
        </div>
    </div>


@push('scripts')     
    <script type="text/javascript">

        $(document).ready(function(){
            cambiar_color_over(celda);
        });

        function cambiar_color_over(celda){
            celda.style.backgroundColor="#A9D1DF"
        }
        function cambiar_color_out(celda){
            celda.style.backgroundColor="#FFFFFF"
        } 
    </script>
@endpush
@endsection
