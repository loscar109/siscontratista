@if (Session::has('delete_permission_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_permission_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_permission'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_permission') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_permission'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_permission') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_permission'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_permission') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif