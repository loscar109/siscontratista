


<!-- Bootstrap 3.3.5 -->

<!-- AdminLTE App -->


<div class="modal fade bs-example-modal-lg"
     aria-labelledby="classInfo"
     aria-hidden="true"
     role="dialog"
     tabindex="-1"
     enctype="multipart/form-data"
     id="modal{{$w->id}}">



        <div class="modal-dialog modal-lg">
            <!--contenido del modal-->
            <div class="modal-content">

                <!-- cabecera del modal -->
                <div class="modal-header" style="background-color:#357CA5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                        <span aria-hidden="true" ><i class="fa fa-close" style="color:#FFFFFF"></i></span>
                    </button>
                    <h4 class="modal-title" style="color:#FFFFFF">Detalle de Grupo: {{$w->name}}</h4>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                        <b><p style="font-size:150%">Categoria: {{ $w->ticket_category->name}} creado el {{ Carbon\Carbon::parse($w->created_at)->format('d/m/Y') }}</p></b>
                    <div class="col-md-12 ml-auto">
                        <div class="table-responsive">                                   
                            <table class="table table-condensed table-striped table-hover responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($w->user_work_groups as $uwg)
                                    <tr> 
                                        <td>
                                            @if ($uwg->user->photo == NULL)
                                                <img src="{{asset('imagenes/users/default.png')}}"
                                                    class="img-thumbnail"
                                                    height="75px"
                                                    width="75px">
                                            @else
                                                <img src="{{asset($uwg->user->photo)}}"
                                                    class="img-thumbnail"
                                                    height="75px"
                                                    width="75px">
                                            @endif       
                                        </td>
                                        <td style="line-height:50px">
                                            {{$uwg->user->name . " " . $uwg->user->surname . " " . $uwg->user->email}}
                                        </td>
                                    

                                    </tr>
                                    @endforeach
                                </tbody>        
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6 ml-auto">
                        
                    </div>
                </div>
                

                <!--pie del modal-->
                <div class="modal-footer">
                        <div class="col-md-12 ml-auto">
                                <button type="button" class="btn btn-default btn-responsive" data-dismiss="modal">
                                    <i class="fa fa-close"> </i>Cerrar
                                </button>

                        </div>
                </div>
               
            
            </div>    
        </div>


</div>


























