@extends('layouts.admin')
@section('titulo')
    <div class="box-header" style="text-align: center">
        <a href ={{ url()->previous() }}>
            <button title="atras" class="btn btn-default btn-responsive pull-left">
                <i class="fa fa-arrow-left"></i> Atras
            </button>
        </a>  
        <h4 class="box-title"><b>Crear Grupo de Trabajo</b></h4>
@endsection

@section('content')
{!!Form::open(array(
    'url'=>'workgroups/gestion',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
))!!}

{{Form::token()}}
<div class="container">
    <div class="box-body">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <label>
                    Nombre
                </label>
                <input
                    id="name"
                    type="text"
                    class="form-control"
                    name="name"
                    value="{{ old('name')}}"
                    required
                    autofocus
                    >
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">  
            <div class="form-group">

                <label>
                    Categoría
                </label>
                <select 
                    name="ticket_category_id" 
                    id="ticket_category_id"
                    class="ticket_category_id form-control"
                    >
                    @foreach ($ticket_categories as $ticket_category)
                        <option 
                            value="{{ $ticket_category->id }}"
                            >
                            {{ $ticket_category->name }}
                        </option>
                        
                    @endforeach
                </select>
            </div>
    </div>
</div>

    <div class="box-body">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">  
            <div class="form group">
                <label for="users">Seleccionar Empleado</label>
                <select name="user_id[]" id="users" class="form control" multiple="multiple">
                </select>
            </div>
        </div>
    
<br>
<br>
<br>
<br>
<br>
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" id="guardar">
            <div class="form-group">
                <input id="guardar" name="_token" value="{{ csrf_token() }}" type="hidden">
                    <button class="btn btn-success btn-lg btn-block" id="btn_add"type="submit"><i class="fa fa-check"> </i>Guardar</button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}









@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $("#ticket_category_id").select2({
            width: '100%'
        });
        $("#users").select2({
            language: 
            {
                inputTooShort: function () 
                {
                    return 'Escriba el nombre del empleado.';
                },
                noResults: function()
                {
                    return "No hay coincidencias";
                },
                searching: function()
                {
                    return "Buscando...";
                }
            },
             width: '100%',
             placeholder: '-- Seleccione uno o mas empleados --',
             ajax:{
                 type:'get',
                 url: '{!!URL::to('workgroups/gestion/create/showEmployeInSelect2')!!}',
                 data: function(params){
                     return{
                         q : params.term,
                         page : params.page
                     }; 
                 },
                 dataType: "json",
                 //tiempo de retraso a proposito
                 delay: 100,
                 cache: true,
                 processResults : function(data, params){
                     params.page = params.page || 1;

                     return{
                         results : data.data,
                         pagination: {
                             more :  (params.page * 10) < data.total
                         }
                     };
                 }
             },
             //cantidad de caracteres para empezar a buscar
             minimumInputLength : 1,
             templateResult : function (user){
                 if(user.loading) return user.name;
                 /* como quiero que se me muestren las opciones a seleccionar
                    foto + dni + nombre
                    foto - 37590136 - Carlos Lisandro Villalba
                 */
                 var dato = "<img class='img-thumbnail'  height='100px' width='100px' src="+user.photo+"></img> &nbsp;"+ user.name +"  "+ user.surname;
                 return dato;
             },
             templateSelection : function(user)
             {
                 /*como quiero que me muestre luego de darle el clic en la opción seleccionada
                    dni + nombre
                    37590136 + Carlos Lisandro
                 */
                 return "<img class='img-thumbnail' height='75px' width='75px' src="+user.photo+"></img> &nbsp;"+user.name+"  "+user.surname;
             },
             escapeMarkup : function(dato)
             { 
                 return dato;
             }
        });


    });
</script>

@endpush