@if (Session::has('delete_workgroup_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_workgroup_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('delete_workgroup'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_workgroup') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_workgroup'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_workgroup') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('store_workgroup'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_workgroup') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif