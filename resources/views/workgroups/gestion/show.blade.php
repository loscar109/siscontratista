@extends('layouts.admin')


@section('titulo')

    <h3 class="box-title">Detalles del Grupo de Trabajo: <b></b></h3>
    <div class="box-tools pull-right">
        <a href="{{asset('configuracion/workflow')}}"><button title="atras" class="btn btn-box-tool btn-responsive">
            <i class="fa fa-arrow-left"></i></button>
        </a>
    </div>
@endsection




@section('content')
    <div class="table-responsive">
        <table style="border:3px solid #357CA5 width:100%" class="table table-bordered table-condensed table-hover">
            <thead style="background-color:#357CA5">
                <tr>
                    <th width="50%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Trabajadores</p></th>
                </tr>
            </thead>
            <tbody>
               @foreach ($user_work_group as $u)
               <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                    <td>{{ $u->user->name}}</td>
                </tr>
               @endforeach
            </tbody>        
        </table>
    </div>
@endsection