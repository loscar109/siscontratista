@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/home') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
    <h4 class="box-title"> <b> Indice de Grupos de Trabajos</b></h4>
   
</div>
@endsection

@section('content')


<div class="box-body">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        @include('workgroups.gestion.mensaje')
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <i class="fa fa-users"></i> Indice de Grupos de Trabajos
                </h4>
                <div class="box-tools">
                    <a href="gestion/create">
                        <button class="btn btn-default">
                            <i class="fa fa-users"><sup> <i class="fa fa-plus"></i></sup> </i> Nuevo
                        </button>    
                    </a>                    
                </div>
            </div>

            <div class="box-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box collapsed-box">
                        <div class="box-header with-border">
                            <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>
                
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                              <i class="fa fa-plus"></i></button>              
                          </div>
                        </div>
                        <div class="box-body" style="display: none;">

                            @include('workgroups.gestion.search') <!-- elemento de la -->

                           
                                
                        </div>
                        
                      </div>




                    @if( $workgroups->isNotEmpty() )
                    <div id="divDetalle" class="table-responsive">
                        <table id="tablaDetalle" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#357CA5">
                                <tr>
                                    <th width="15%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Nombre</p></th>
                                    <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Integrantes</p></th>
                                    <th width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Categoria</p></th>

                                    <th width="25%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Opciones</p></th>
                                </tr>
                            </thead>
                            <tbody>   
                                @foreach ($workgroups as $w)
                                    <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)"> 
                                        <td style="text-align: left">
                                            <p class="text-uppercase">
                                                <i class="fa fa-users"></i> {{ $w->name }}
                                            </p>
                                        </td>
                                       
                                        <td>
                                            @foreach ($w->user_work_groups as $uwg)
                                                <p class="text-uppercase">
                                                    <li> {{ $uwg->user->name . " " . $uwg->user->surname }}</li>
                                                </p>          
                                            @endforeach
                                        </td>
                                        <td>
                                            <p class="text-uppercase">
                                                {{ $w->ticket_category->name }}
                                            </p>
                                        </td>

                                        <td style="text-align: center" >
                                            <a href="" data-target="#modal{{$w->id}}" data-toggle="modal">
                                                <button title="ver" class="btn btn-celeste btn-responsive">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </a>
                                            @include('workgroups.gestion.modalshow')  

                                            <a href="{{URL::action('UserWorkGroupController@graph',$w)}}">
                                                <button title="estadistica" class="btn btn-primary btn-responsive ">
                                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>

                                                </button> 
                                            </a> 

                                        </td>
                                      

                                    </tr>
                                @endforeach
                            </tbody>        
                        </table>
                    </div>
                    @else
                    <p 
                        class="p-3 mb-2 bg-warning text-dark"
                        >
                        No hay Grupos de Trabajos registrados
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')     
    <script src="{{asset('js/tablaDetalle.js')}}"></script>
@endpush
@endsection


