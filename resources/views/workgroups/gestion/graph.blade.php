@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ url()->previous() }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>   
</div>
@endsection

@section('content')
<div class="box-body">
    {!! Form::model(Request::only(
        ['month','year']),
        ['url' => 'workgroups/gestion/' . $id . '/graph', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search'] 
        
        )!!}
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align='center'>
        <div class="box box-solid" align='center'>
            <div class="box-header with-border" align='center'>
                <h4 class="box-title" align='center'>
                    <div class="col-md-6">
                        <select name="month" class="form-control" value={{Carbon\Carbon::now()->format('m')}} type="number">               
                            <option value=0 selected disabled>
                               -- Seleccione un mes --
                            </option>   
                            @if(isset($_GET['month']) && $_GET['month']==1)            
                                <option value=1 selected>Enero</option> 
                            @else
                                <option value=1>Enero</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==2)            
                                <option value=2 selected>Febrero</option> 
                            @else
                                <option value=2>Febrero</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==3)            
                                <option value=3 selected>Marzo</option> 
                            @else
                                <option value=3>Marzo</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==4)            
                                <option value=4 selected>Abril</option> 
                            @else
                                <option value=4>Abril</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==5)            
                                <option value=5 selected>Mayo</option> 
                            @else
                                <option value=5>Mayo</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==6)            
                                <option value=6 selected>Junio</option> 
                            @else
                                <option value=6>Junio</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==7)            
                                <option value=7 selected>Julio</option> 
                            @else
                                <option value=7>Julio</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==8)            
                                <option value=8 selected>Agosto</option> 
                            @else
                                <option value=8>Agosto</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==9)            
                                <option value=9 selected>Septiembre</option> 
                            @else
                                <option value=9>Septiembre</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==10)            
                                <option value=10 selected>Octubre</option> 
                            @else
                                <option value=10>Octubre</option>              
                            @endif
                            @if(isset($_GET['month']) && $_GET['month']==11)            
                                <option value=11 selected>Noviembre</option> 
                            @else
                                <option value=11>Noviembre</option>              
                            @endif       
                            @if(isset($_GET['month']) && $_GET['month']==12)            
                                <option value=12 selected>Diciembre</option> 
                            @else
                                <option value=12>Diciembre</option>              
                            @endif                     
                            

                        </select>


                    </div>
                    <div class="col-md-3">
                        <input name="year" class="form-control" min="2000"  @if(isset($_GET['year'])) value="{{$_GET['year']}}" @else value={{now()->format('Y')}} @endif max={{now()->format('Y')}} type="number" placeholder="año">

                    </div>
                    <div class="col-md-3">

                    <button class="btn btn-default" type="submit"> <i class="fa fa-filter"></i> Filtrar</button>
                </div>


                </h4>
            </div>
            <div class="box-body" align='center'>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div id="pie_today_div" align='right'></div>
                </div> 
                @if($resolved_tickets->isNotEmpty())
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div id="columna"align='left'></div>      
                    </div> 
                @endif
            </div>
        </div>
    </div>
</div>




{{Form::close()}}


@push('scripts')   
<script src="{{asset('js/loader.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    $(document).ready(function() {
    });

</script>    
    <script type="text/javascript">

    google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Dias", "Cantidad de Ticket"],
            @foreach ($days_agruped as $key=>$value)
                [ {{ $key }}, {{ $value }} ],
            @endforeach  
        ]); 

        var view = new google.visualization.DataView(data);
        

        var options = {
            title: "Cantidad de Tickets Resueltos medido en días del último mes",
            width: 800,
            height: 600,
            legend: { position: "none" },
            vAxis: {title : 'Cantidad de Tickets',format: '0'},
            hAxis : {title : 'Dias',format: '0'},
            colors: ["#3C8DBC","#DD4B39"]
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columna"));
        chart.draw(view, options);
    }




</script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Resueltos', {{ $count_resolved_tickets }}],
            ['Cancelados', {{ $count_no_resolved_tickets }}],
        ]);
        

        var options = {
        title: 'Recuento de Tickets (Resueltos y No Resueltos) del último mes',
        width: 800, //400
        height: 600, //600
        legend: false,
        colors: ["#3C8DBC",'#DD4B39']
        };

        var chart = new google.visualization.PieChart(document.getElementById('pie_today_div'));
        chart.draw(data, options);
    }
    </script>
   @endpush
@endsection