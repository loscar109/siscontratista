<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">


    <style>
        @page { margin: 50px; }
            
            #footer 
            { 
                position: fixed; 
                left: 0px;
                bottom: -180px; 
                right: 0px; 
                height: 50px; 
                background-color: #357CA5; 
                color: #FFFFFF;

            }
            #footer .page:after
            { 
                content: counter(page, decimal); 
                float: right;
                background-color: #357CA5; 
                color: #FFFFFF;
             
            }

           
 
  
    </style>

    

    <title>Reporte de Tickets</title>
</head>
<body>
    
    <div id="content" class="container">
        
        
        <!--Tabla-->
        <table style="border:3px solid #357CA5 width:100%" class="table table-condensed table-hover">
            
            <!-- Cabecera del reporte -->
            <thead style="background-color:#357CA5">
                <tr>
                    <th class="h2 text-center" colspan="4"style="color:#FFFFFF">Reporte de Grupos de Trabajo</th>
                </tr>
            
                <!-- Datos del reporte -->
                <tr style="background-color:#FFFFFF; border:0">

                    <td colspan="2">
                        <!-- Logo -->
                        <img  
                            src="{{asset('imagenes/logo/logo.png')}}"
                            alt="logo de la empresa"
                            width="200px"
                        > 
                    </td>
                    <td colspan="2" style="vertical-align:middle">
                        <!-- Emision -->
                        <p>FECHA EMISIÓN: <strong>{{Carbon\Carbon::parse(Carbon\Carbon::now())->format('d/m/Y')}}</strong></p>
                        <p>HORA EMISIÓN: <strong>{{Carbon\Carbon::parse(Carbon\Carbon::now())->toTimeString()}}</strong></p>
                        <p>USUARIO: <strong>{{ Auth::user()->name }}</strong></p>
                    </td>

                </tr>
                @if($desde != NULL || $hasta != NULL || $category !=NULL)
                    <tr>
                        <td colspan="4" style="background-color:#FFFFFF;">
                                
                            @if($category)
                                <p>FILTRADO POR CATEGORIA DE TICKET : <strong>{{$category->name}}</strong></p>
                            @endif
                            @if($desde!=NULL && $hasta!=NULL)
                                <p>FILTRADO POR FECHA DESDE : <strong>{{Carbon\Carbon::parse($desde)->format('d/m/Y')}}</strong></p>
                                <p>FILTRADO POR FECHA HASTA : <strong>{{Carbon\Carbon::parse($hasta)->format('d/m/Y')}}</strong></p>
                            @elseif($desde!=NULL)
                                <p>FILTRADO POR FECHA DESDE : <strong>{{Carbon\Carbon::parse($desde)->format('d/m/Y')}}</strong></p>
                            @elseif($hasta!=NULL)
                                <p>FILTRADO POR FECHA HASTA : <strong>{{Carbon\Carbon::parse($hasta)->format('d/m/Y')}}</strong></p>
                            @endif        
                        </td>
                    </tr>
                @endif
                <tr style="background-color:#357CA5">
                    <th width="30%" style="color:#FFFFFF" height="25px" ><p class="text-uppercase">Grupo de Trabajo</p></th>
                    <th width="30%" style="color:#FFFFFF" height="25px" ><p class="text-uppercase">Fecha Creación</p></th>
                    <th colspan="2" width="20%" style="color:#FFFFFF" height="25px"><p class="text-uppercase">Categoria</p></th>

                </tr>
            </thead>
            <tbody>
                @foreach ($workgroups as $workgroup)
                <tr>
                    <td style="border:1px solid grey"><p class="text-uppercase">{{$workgroup->name }}</p></td>
                    <td style="border:1px solid grey"><p class="text-uppercase">{{$workgroup->created_at->format('d/m/Y') }}</p></td>
                    <td style="border:1px solid grey"><p class="text-uppercase">{{$workgroup->ticket_category->name }}</p></td>

                    
                </tr>         
                @endforeach
            </tbody>

        </table>
        <div class="page-break"></div>

    </div>

    
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>



  
 <!-- Bootstrap 3.3.5 -->
 <script src="{{ asset('js/bootstrap.min.js') }}"></script>

 
 

   <!-- AdminLTE App -->
 <script src="{{asset('js/app.min.js')}}"></script>
    

 <script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 800, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>

</body>
</html>
