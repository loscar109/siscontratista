<?php

Route::get('/', function () {
    return view('welcome');
});

//sistema completo de login servicios recuperar contraseña
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );



//rutas para asignar permisos
Route::middleware(['auth'])->group(function() {
    include 'rutas/Permission.php';
    include 'rutas/Role.php';
    include 'rutas/User.php';
    include 'rutas/TicketCategory.php';
    include 'rutas/TicketStage.php';
    include 'rutas/WorkFlow.php';
    include 'rutas/Ticket.php';
    include 'rutas/UserWorkGroup.php';
    include 'rutas/Configuracion.php';
    include 'rutas/AuditsUser.php';
    include 'rutas/Work.php';
    include 'rutas/Category.php';
    include 'rutas/Material.php';
    include 'rutas/Stock.php';
    include 'rutas/WorkGroup.php';
    include 'rutas/Message.php';


});

