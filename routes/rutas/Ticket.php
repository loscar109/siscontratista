<?php

    Route::post('tickets/gestion', 'TicketController@store')
        ->name('tickets.gestion.store')
        ->middleware('permission:tickets.gestion.create');

    Route::get('tickets/gestion',  'TicketController@index')
        ->name('tickets.gestion.index')
        ->middleware('permission:tickets.gestion.index');

    Route::get('tickets/gestion/create', 'TicketController@create')
        ->name('tickets.gestion.create')
        ->middleware('permission:tickets.gestion.create');

    Route::patch('tickets/gestion/{tickets}', 'TicketController@update')
        ->name('tickets.gestion.update')
        ->middleware('permission:tickets.gestion.edit');

    Route::get('tickets/{tickets}', 'TicketController@show')
        ->name('tickets.gestion.show')
        ->middleware('permission:tickets.gestion.show');

    Route::delete('tickets/gestion/{tickets}', 'TicketController@destroy')
        ->name('tickets.gestion.destroy')
        ->middleware('permission:tickets.gestion.destroy');

    Route::get('tickets/gestion/{tickets}/edit', 'TicketController@edit')
        ->name('tickets.gestion.edit')
        ->middleware('permission:tickets.gestion.edit');

    Route::get('ticket-list-pdf','TicketController@exportPdf')
        ->name('ticket.pdf');


    Route::get('tickets/gestion/{ticket}', 'TicketController@detailTicket')
        ->name('tickets.gestion.detailTicket')

    ?>