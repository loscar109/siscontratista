<?php

    Route::post('workgroups/gestion', 'UserWorkGroupController@store')
        ->name('workgroups.gestion.store')
        ->middleware('permission:workgroups.gestion.create');

    Route::get('workgroups/gestion',  'UserWorkGroupController@index')
        ->name('workgroups.gestion.index')
        ->middleware('permission:workgroups.gestion.index');

    Route::get('workgroups/gestion/create', 'UserWorkGroupController@create')
        ->name('workgroups.gestion.create');

    Route::patch('workgroups/gestion/{workgroup}', 'UserWorkGroupController@update')
        ->name('workgroups.gestion.update')
        ->middleware('permission:workgroups.gestion.edit');

        Route::get('workgroups/gestion/show',  'UserWorkGroupController@show')
        ->name('workgroups.gestion.show')
        ->middleware('permission:workgroups.gestion.show');

        Route::get('workgroups/gestion/{id}/graph',  'UserWorkGroupController@graph')
        ->name('workgroups.gestion.graph');

    Route::delete('workgroups/gestion/{workgroup}', 'UserWorkGroupController@destroy')
        ->name('workgroups.gestion.destroy')
        ->middleware('permission:workgroups.gestion.destroy');

    Route::get('workgroups/gestion/{workgroup}/edit', 'UserWorkGroupController@edit')
        ->name('workgroups.gestion.edit')
        ->middleware('permission:workgroups.gestion.edit');


    Route::get('workgroups/gestion/create/showEmployeInSelect2', 'UserWorkGroupController@showEmployeInSelect2')->name('workgroups.gestion.create');

    Route::get('workgroup-list-pdf','UserWorkGroupController@exportPdf')
    ->name('workgroup.pdf');


    
    ?>