<?php

    Route::post('materials/gestion', 'MaterialController@store')
        ->name('materials.gestion.store')
        ->middleware('permission:materials.gestion.create');

    Route::get('materials/gestion',  'MaterialController@index')
        ->name('materials.gestion.index')
        ->middleware('permission:materials.gestion.index');

        Route::get('materials/gestion/movimientos/{id}',  'MaterialController@movimientos')
        ->name('materials.gestion.movimientos')
        ->middleware('permission:materials.gestion.movimientos');

    Route::get('materials/gestion/create', 'MaterialController@create')
        ->name('materials.gestion.create')
        ->middleware('permission:materials.gestion.create');

    Route::patch('materials/gestion/{materials}', 'MaterialController@update')
        ->name('materials.gestion.update')
        ->middleware('permission:materials.gestion.edit');

    Route::get('materials/{materials}', 'MaterialController@show')
        ->name('materials.gestion.show')
        ->middleware('permission:materials.gestion.show');

    Route::delete('materials/gestion/{materials}', 'MaterialController@destroy')
        ->name('materials.gestion.destroy')
        ->middleware('permission:materials.gestion.destroy');

    Route::get('materials/gestion/{materials}/edit', 'MaterialController@edit')
        ->name('materials.gestion.edit')
        ->middleware('permission:materials.gestion.edit');


    Route::get('material-list-pdf','MaterialController@exportPdf')
        ->name('material.pdf');



    ?>