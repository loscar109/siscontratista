<?php

    Route::post('configuracion/category', 'CategoryController@store')
        ->name('configuracion.category.store')
        ->middleware('permission:configuracion.category.create');
        
    Route::get('configuracion/category', 'CategoryController@index')
        ->name('configuracion.category.index')
        ->middleware('permission:configuracion.category.index');

    Route::get('configuracion/category/create','CategoryController@create')
        ->name('configuracion.category.create')
        ->middleware('permission:configuracion.category.create');

    Route::get('configuracion/category/create/showWorkGroup','CategoryController@showWorkGroup')
        ->name('configuracion.category.create')
        ->middleware('permission:configuracion.category.create');

    Route::patch('configuracion/category/{category}', 'CategoryController@update')
        ->name('configuracion.category.update')
        ->middleware('permission:configuracion.category.edit');

    Route::get('configuracion/category/{category}', 'CategoryController@show')
        ->name('configuracion.category.show')
        ->middleware('permission:configuracion.category.show');

    Route::delete('configuracion/category/{category}', 'CategoryController@destroy')
        ->name('configuracion.category.destroy')
        ->middleware('permission:configuracion.category.destroy');
    
    Route::get('configuracion/category/{category}/edit', 'CategoryController@edit')
        ->name('configuracion.category.edit')
        ->middleware('permission:configuracion.category.edit');
?>