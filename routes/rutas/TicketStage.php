<?php

    Route::post('configuracion/ticket_stage',  'TicketStageController@store')
        ->name('configuracion.ticket_stage.store')
        ->middleware('permission:configuracion.ticket_stage.create');
        
    Route::get('configuracion/ticket_stage',  'TicketStageController@index')
        ->name('configuracion.ticket_stage.index')
        ->middleware('permission:configuracion.ticket_stage.index');

    Route::get('configuracion/ticket_stage/create', 'TicketStageController@create')
        ->name('configuracion.ticket_stage.create')
        ->middleware('permission:configuracion.ticket_stage.create');
    
    Route::patch('configuracion/ticket_stage/{ticket_stage}', 'TicketStageController@update')
        ->name('configuracion.ticket_stage.update')
        ->middleware('permission:configuracion.ticket_stage.edit');

    Route::get('configuracion/ticket_stage/{ticket_stage}', 'TicketStageController@show')
        ->name('configuracion.ticket_stage.show')
        ->middleware('permission:configuracion.ticket_stage.show');

    Route::delete('configuracion/ticket_stage/{ticket_stage}', 'TicketStageController@destroy')
        ->name('configuracion.ticket_stage.destroy')
        ->middleware('permission:configuracion.ticket_stage.destroy');
    
    Route::get('configuracion/ticket_stage/{ticket_stage}/edit', 'TicketStageController@edit')
        ->name('configuracion.ticket_stage.edit')
        ->middleware('permission:configuracion.ticket_stage.edit');
?>