<?php
Route::post('roles/gestion',  'RoleController@store')->name('roles.gestion.store')
        ->middleware('permission:roles.gestion.create');
        
    Route::get('roles/gestion',  'RoleController@index')->name('roles.gestion.index')
        ->middleware('permission:roles.gestion.index');

    Route::get('roles/gestion/create', 'RoleController@create')->name('roles.gestion.create')
        ->middleware('permission:roles.gestion.create');
    
    Route::patch('roles/gestion/{role}', 'RoleController@update')->name('roles.gestion.update')
        ->middleware('permission:roles.gestion.edit');
        
    Route::get('roles/gestion/{role}', 'RoleController@show')->name('roles.gestion.show')
        ->middleware('permission:roles.gestion.show');

    Route::delete('roles/gestion/{role}', 'RoleController@destroy')->name('roles.gestion.destroy')
        ->middleware('permission:roles.gestion.destroy');
    
    Route::get('roles/gestion/{role}/edit', 'RoleController@edit')->name('roles.gestion.edit')
        ->middleware('permission:roles.gestion.edit');
        ?>