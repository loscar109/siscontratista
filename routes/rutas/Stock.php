<?php

    Route::post('requisitions/gestion', 'StockController@store')
        ->name('requisitions.gestion.store')
        ->middleware('permission:requisitions.gestion.create');
    
    Route::get('requisitions/gestion',  'StockController@index')
        ->name('requisitions.gestion.index')
        ->middleware('permission:requisitions.gestion.index');

    Route::get('requisitions/gestion/entry',  'StockController@entry')
        ->name('requisitions.gestion.entry')
        ->middleware('permission:requisitions.gestion.entry');
    
    Route::get('requisitions/gestion/create/findNameMaterial', 'StockController@findNameMaterial')
        ->name('requisitions.gestion.create')
        ->middleware('permission:requisitions.gestion.create');





        Route::get('requisitions/gestion/create/findMaterialData', 'StockController@findMaterialData')
        ->name('requisitions.gestion.create')
        ->middleware('permission:requisitions.gestion.create'); 

    Route::get('requisitions/gestion/create', 'StockController@create')
        ->name('requisitions.gestion.create')
        ->middleware('permission:requisitions.gestion.create');
    
    Route::patch('requisitions/gestion/{requisitions}', 'StockController@update')
        ->name('requisitions.gestion.update')
        ->middleware('permission:requisitions.gestion.edit');
    
    Route::get('requisitions/{requisitions}', 'StockController@show')
        ->name('requisitions.gestion.show')
        ->middleware('permission:requisitions.gestion.show');
    
    Route::delete('requisitions/gestion/{requisitions}', 'StockController@destroy')
        ->name('requisitions.gestion.destroy')
        ->middleware('permission:requisitions.gestion.destroy');
    
    Route::get('requisitions/gestion/{requisitions}/edit', 'StockController@edit')
        ->name('requisitions.gestion.edit')
        ->middleware('permission:requisitions.gestion.edit');


    Route::get('requisition-list-pdf/{requisition}','StockController@exportPdf')->name('requisition.pdf');

    Route::post('requisitions/automatic/{requisition}','StockController@automatic')->name('requisitions.gestion.automatic');
    
    Route::get('requisitions/manual/{requisition}','StockController@manual')->name('requisitions.gestion.manual');
    Route::post('requisitions/manual/{requisition}','StockController@manualSave');




?>