<?php

    Route::post('users/gestion', 'UserController@store')
        ->name('users.gestion.store')
        ->middleware('permission:users.gestion.create');

    Route::get('users/gestion',  'UserController@index')
        ->name('users.gestion.index')
        ->middleware('permission:users.gestion.index');

    Route::get('users/gestion/create', 'UserController@create')
        ->name('users.gestion.create')
        ->middleware('permission:users.gestion.create');

    Route::patch('users/gestion/{user}', 'UserController@update')
        ->name('users.gestion.update')
        ->middleware('permission:users.gestion.edit');

    Route::get('users/{user}', 'UserController@show')
        ->name('users.gestion.show')
        ->middleware('permission:users.gestion.show');


    Route::get('users/gestion/{user}/edit', 'UserController@edit')
        ->name('users.gestion.edit')
        ->middleware('permission:users.gestion.edit');

    Route::get('users/gestion/create/findProvince', 'UserController@findProvince')
        ->name('users.gestion.create')
        ->middleware('permission:users.gestion.create');

    Route::get('users/gestion/create/findCity', 'UserController@findCity')
        ->name('users.gestion.create')
        ->middleware('permission:users.gestion.create');

    Route::get('users/gestion/create/findDistrict', 'UserController@findDistrict')
        ->name('users.gestion.create')
        ->middleware('permission:users.gestion.create');

    Route::delete('users/gestion/{user}', 'UserController@destroy')
        ->name('users.gestion.destroy')
        ->middleware('permission:users.gestion.destroy');




    ?>