<?php

    Route::post('works', 'WorkController@store')
        ->name('works.store')
        ->middleware('permission:works.create');

    Route::get('works',  'WorkController@index')
        ->name('works.index')
        ->middleware('permission:works.index');
        
    Route::get('works/inProccess',  'WorkController@inProccess')
        ->name('works.inProccess')
        ->middleware('permission:works.inProccess');

    Route::get('works/resolved',  'WorkController@resolved')
        ->name('works.resolved')
        ->middleware('permission:works.resolved');

    Route::get('works/created',  'WorkController@created')
        ->name('works.created')
        ->middleware('permission:works.created');

    Route::get('works/create', 'WorkController@create')
        ->name('works.create')
        ->middleware('permission:works.create');

    Route::patch('works/{works}', 'WorkController@update')
        ->name('works.update')
        ->middleware('permission:works.edit');

    Route::get('works/{ticket}', 'WorkController@show')
        ->name('works.show')
        ->middleware('permission:works.show');

    Route::get('works/updateStage/{ticket}/{ticket_stage}', 'WorkController@updateStage')
        ->name('works.updateStage')
        ->middleware('permission:works.updateStage');

    Route::post('works/reprogramming/{ticket}/{ticket_stage}', 'WorkController@reprogramming')
        ->name('works.reprogramming')
        ->middleware('permission:works.reprogramming');

    Route::post('works/message/{ticket}', 'WorkController@message')
        ->name('works.message')
        ->middleware('permission:works.message');


    Route::delete('works/{works}', 'WorkController@destroy')
        ->name('works.destroy')
        ->middleware('permission:works.destroy');

    Route::get('works/{works}/edit', 'WorkController@edit')
        ->name('works.edit')
        ->middleware('permission:works.edit');


    //GET para obtener los datos una vez terminado el ticket
    Route::get('works/{ticket}/finish', 'WorkController@finish')
        ->name('works.finish')
        ->middleware('permission:works.finish');

    //POST para guardar la finalizacion
    Route::post('works/{ticket}/finish', 'WorkController@storeFinished')
        ->name('works.storeFinished')
        ->middleware('permission:works.storeFinished');

    Route::get('works/finish/findMaterial', 'WorkController@findMaterial')
        ->name('works.findMaterial')
        ->middleware('permission:works.findMaterial');

    Route::get('works/{ticket}/send', 'WorkController@send')
        ->name('works.send')
        ->middleware('permission:works.send');


    Route::get('works/finish/findMaterialData', 'WorkController@findMaterialData')
        ->name('works.finish.findMaterialData')
        ->middleware('permission:works.finish.findMaterialData'); 

    ?>