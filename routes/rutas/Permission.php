<?php
Route::post('permissions/gestion', 'PermissionController@store')->name('permissions.gestion.store')
        ->middleware('permission:permissions.gestion.create');
    
    Route::get('permissions/gestion',  'PermissionController@index')->name('permissions.gestion.index')
        ->middleware('permission:permissions.gestion.index');

    Route::get('permissions/gestion/create', 'PermissionController@create')->name('permissions.gestion.create')
        ->middleware('permission:permissions.gestion.create');

    Route::patch('permissions/gestion/{permission}', 'PermissionController@update')->name('permissions.gestion.update')
        ->middleware('permission:permissions.gestion.edit');

    Route::get('permissions/gestion/{permission}', 'PermissionController@show')->name('permissions.gestion.show')
        ->middleware('permission:permissions.gestion.show');

    Route::delete('permissions/gestion/{permission}', 'PermissionController@destroy')->name('permissions.gestion.destroy')
        ->middleware('permission:permissions.gestion.destroy');

    Route::get('permissions/gestion/{permission}/edit', 'PermissionController@edit')->name('permissions.gestion.edit')
        ->middleware('permission:permissions.gestion.edit');
        ?>