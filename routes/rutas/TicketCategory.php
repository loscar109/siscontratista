<?php

    Route::post('configuracion/ticket_category',  'TicketCategoryController@store')
        ->name('configuracion.ticket_category.store')
        ->middleware('permission:configuracion.ticket_category.create');
        
    Route::get('configuracion/ticket_category',  'TicketCategoryController@index')
        ->name('configuracion.ticket_category.index')
        ->middleware('permission:configuracion.ticket_category.index');

    Route::get('configuracion/ticket_category/create', 'TicketCategoryController@create')
        ->name('configuracion.ticket_category.create')
        ->middleware('permission:configuracion.ticket_category.create');

    Route::get('configuracion/ticket_category/create/showWorkGroup', 'TicketCategoryController@showWorkGroup')
        ->name('configuracion.ticket_category.create')
        ->middleware('permission:configuracion.ticket_category.create');

    Route::patch('configuracion/ticket_category/{ticket_category}', 'TicketCategoryController@update')
        ->name('configuracion.ticket_category.update')
        ->middleware('permission:configuracion.ticket_category.edit');

    Route::get('configuracion/ticket_category/{ticket_category}', 'TicketCategoryController@show')
        ->name('configuracion.ticket_category.show')
        ->middleware('permission:configuracion.ticket_category.show');

    Route::delete('configuracion/ticket_category/{ticket_category}', 'TicketCategoryController@destroy')
        ->name('configuracion.ticket_category.destroy')
        ->middleware('permission:configuracion.ticket_category.destroy');
    
    Route::get('configuracion/ticket_category/{ticket_category}/edit', 'TicketCategoryController@edit')
        ->name('configuracion.ticket_category.edit')
        ->middleware('permission:configuracion.ticket_category.edit');
?>