<?php

    Route::post('configuracion/workflow',  'WorkFlowController@store')
        ->name('configuracion.workflow.store')
        ->middleware('permission:configuracion.workflow.create');
        
    Route::get('configuracion/workflow',  'WorkFlowController@index')
        ->name('configuracion.workflow.index')
        ->middleware('permission:configuracion.workflow.index');

    Route::get('configuracion/workflow/create', 'WorkFlowController@create')
        ->name('configuracion.workflow.create')
        ->middleware('permission:configuracion.workflow.create');
    
    Route::patch('configuracion/workflow/{workflow}', 'WorkFlowController@update')
        ->name('configuracion.workflow.update')
        ->middleware('permission:configuracion.workflow.edit');

    Route::get('configuracion/workflow/{workflow}', 'WorkFlowController@show')
        ->name('configuracion.workflow.show')
        ->middleware('permission:configuracion.workflow.show');

    Route::delete('configuracion/workflow/{workflow}', 'WorkFlowController@destroy')
        ->name('configuracion.workflow.destroy')
        ->middleware('permission:configuracion.workflow.destroy');
    
    Route::get('configuracion/workflow/{workflow}/edit', 'WorkFlowController@edit')
        ->name('configuracion.workflow.edit')
        ->middleware('permission:configuracion.workflow.edit');
?>