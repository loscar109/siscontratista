<?php


    Route::get('messages/gestion',  'MessageController@index')
        ->name('messages.gestion.index');

    Route::get('messages/{message}', 'MessageController@show')
        ->name('messages.gestion.show');

    Route::post('messages/gestion/{message}', 'MessageController@message')
        ->name('messages.message');

    Route::get('messages/gestion/create', 'MessageController@create')
        ->name('messages.gestion.create');

    Route::post('messages/gestion', 'MessageController@store')
        ->name('messages.gestion.store');

    Route::get('messages/gestion/allSend', 'MessageController@allSend')
        ->name('messages.gestion.allSend');

    Route::get('messages/gestion/{send}/open', 'MessageController@open')
        ->name('messages.gestion.open');

    Route::get('messages/gestion/{message}/openBandeja', 'MessageController@openBandeja')
        ->name('messages.gestion.openBandeja');




    Route::get('messages/gestion/{message}/answer', 'MessageController@answer')
        ->name('messages.gestion.answer');

    Route::post('gestion/storeAnswer', 'MessageController@storeAnswer')
        ->name('messages.gestion.storeAnswer');

    ?>