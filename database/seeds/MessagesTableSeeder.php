<?php

use Illuminate\Database\Seeder;
use siscontratista\Message;


class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::create([
            'subject'      =>'Google no realiza ninguna busqueda',
            'text'  =>'El navegador web abre pero no encuentra el sitio web que se desea acceder',
            'ticket_id'    =>1               //Internet no funciona
        ]);

        Message::create([
            'subject'      =>'Se realiza un ping',
            'text'  =>'Se realiza un ping a 8.8.8.8 y no se puede enviar paquetes de datos',
            'ticket_id'    =>1               //Internet no funciona
        ]);

        Message::create([
            'subject'      =>'El modem no enciende todas las luces',
            'text'  =>'Se observa que el modem no esta recibiendo datos de la red ',
            'ticket_id'    =>1               //Internet no funciona
        ]);
    }
}
