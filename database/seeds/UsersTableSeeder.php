<?php

use Illuminate\Database\Seeder;
use siscontratista\User;
use Caffeinated\Shinobi\Models\Role;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

   
        //Defino 5 roles que va a tener la empresa (Administrador, Jefe, Empleado, Cliente y Auditor)
        $role_admin = new Role();
        $role_admin->name='ADMIN';
        $role_admin->slug='ADMIN';
        $role_admin->description='ACCESO A TODAS LAS FUNCIONALIDADES Y PARAMETRIZACIÓN DEL SISTEMA';
        $role_admin->special='all-access';
        $role_admin->save();

        $role_jefe = new Role();
        $role_jefe->name='JEFE';
        $role_jefe->slug='JEFE';
        $role_jefe->description='ACCESO A LAS FUNCIONALIDADES DE BÁSICAS DEL SISTEMA';
        $role_jefe->special=NULL;
        $role_jefe->save();

        $role_empleado = new Role();
        $role_empleado->name='EMPLEADO';
        $role_empleado->slug='EMPLEADO';
        $role_empleado->description='ACCESO A LA REALIZACIÓN DE LOS TICKETS';
        $role_empleado->special=NULL;
        $role_empleado->save();

        $role_cliente = new Role();
        $role_cliente->name='CLIENTE';
        $role_cliente->slug='CLIENTE';
        $role_cliente->description='CLIENTE';
        $role_cliente->special=NULL;
        $role_cliente->save();

        $role_auditor = new Role();
        $role_auditor->name='AUDITOR';
        $role_auditor->slug='AUDITOR';
        $role_auditor->description='ACCESO AL MODULO DE AUDITORÍA';
        $role_auditor->special=NULL;
        $role_auditor->save();

        //Defino un Usuario Administrador
        $user_admin=new User();
        $user_admin->name="JORGE FERNANDO";
        $user_admin->surname="RODRIGUEZ";
        $user_admin->email="jorgefernando@mail.com";
        $user_admin->password=bcrypt('JORGEFERNANDO123456');
        $user_admin->photo='/imagenes/empleados/admin.png';
        $user_admin->assigned=false;
        $user_admin->district_id=55;
        $user_admin->address="calle 45 B 8338";
        $user_admin->save();
        $user_admin->assignRole(1); //con esto decimos que va a ser Admin

        //Defino dos usuarios Jefe
        $user_jefe1=new User();
        $user_jefe1->name="ALEJANDRO GABRIEL";
        $user_jefe1->surname="PEZUCK";
        $user_jefe1->email="alejandrogabriel@mail.com";
        $user_jefe1->password=bcrypt('ALEJANDROGABRIEL1123456');
        $user_jefe1->photo='/imagenes/empleados/jefe1.png';
        $user_jefe1->assigned=false;
        $user_jefe1->district_id=1;
        $user_jefe1->address="calle 42 A 8238";
        $user_jefe1->save();
        $user_jefe1->assignRole(2); //con esto decimos que va a ser Jefe

        $user_jefe2=new User();
        $user_jefe2->name="JUAN";
        $user_jefe2->surname="PEREZ";
        $user_jefe2->email="juanperez@mai.com";
        $user_jefe2->password=bcrypt('juanperez2123456');
        $user_jefe2->photo='/imagenes/empleados/jefe2.png';
        $user_jefe2->assigned=false;
        $user_jefe2->district_id=52;
        $user_jefe2->address="calle 32 Altura 4";
        $user_jefe2->save();
        $user_jefe2->assignRole(2); //con esto decimos que va a ser Jefe

       //-----------------------Definimos 2 usuarios Empleado por cada grupo
        $user_empleado1=new User();
        $user_empleado1->name="LUCAS MIGUEL";
        $user_empleado1->surname="MARTINEZ";
        $user_empleado1->email="miguelmartinez@mail.com";
        $user_empleado1->password=bcrypt('12345678');
        $user_empleado1->photo='/imagenes/empleados/emp13.png';
        $user_empleado1->assigned=false;
        $user_empleado1->district_id=39;
        $user_empleado1->address="Calle 22 Altura 3";
        $user_empleado1->save();
        $user_empleado1->assignRole(3); //con esto decimos que va a ser Empleado

        $user_empleado2=new User();
        $user_empleado2->name="MARIO RUBEN";
        $user_empleado2->surname="WALDEMAR";
        $user_empleado2->email="marioruben@mail.com";
        $user_empleado2->password=bcrypt('12345678');
        $user_empleado2->photo='/imagenes/empleados/emp14.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=23;
        $user_empleado2->address="CH 32-33";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        $user_empleado3=new User();
        $user_empleado3->name="CARLOS";
        $user_empleado3->surname="BRISUELA";
        $user_empleado3->email="carlosbrisuel@mail.com";
        $user_empleado3->password=bcrypt('CARLOSBRISUELA123456');
        $user_empleado3->photo='/imagenes/empleados/emp15.png';
        $user_empleado3->assigned=false;
        $user_empleado3->district_id=11;
        $user_empleado3->address="190 3198";
        $user_empleado3->save();
        $user_empleado3->assignRole(3); //con esto decimos que va a ser Empleado

        $user_empleado4=new User();
        $user_empleado4->name="MATIAS";
        $user_empleado4->surname="KURTZ";
        $user_empleado4->email="matiaskurtz@mail.com";
        $user_empleado4->password=bcrypt('MATIASKURTZ123456');
        $user_empleado4->photo='/imagenes/empleados/emp16.png';
        $user_empleado4->assigned=false;
        $user_empleado4->district_id=12;
        $user_empleado4->address="189 88";
        $user_empleado4->save();
        $user_empleado4->assignRole(3); //con esto decimos que va a ser Empleado*/
       //-----------------------Definimos 2 usuarios Empleado por cada grupo

  
        //Definimos 2 usuarios Clientes
        $user_cliente1=new User();
        $user_cliente1->name="MARIA";
        $user_cliente1->surname="SENSUCK";
        $user_cliente1->email="mariasensuck@mail.com";
        $user_cliente1->password=bcrypt('MARIASENSUCK123456');
        $user_cliente1->photo='/imagenes/empleados/cliente1.png';
        $user_cliente1->assigned=false;
        $user_cliente1->district_id=57;
        $user_cliente1->address="calle 28 altura 1500";
        $user_cliente1->save();
        $user_cliente1->assignRole(4); //con esto decimos que va a ser Cliente*/

        $user_cliente2=new User();
        $user_cliente2->name="JOSE ANDREA";
        $user_cliente2->surname="GONZALES";
        $user_cliente2->email="joseandrea@mail.com";
        $user_cliente2->password=bcrypt('JOSEANDREA123456');
        $user_cliente2->photo='/imagenes/empleados/cliente2.png';
        $user_cliente2->assigned=false;
        $user_cliente2->district_id=19;
        $user_cliente2->address="manzana 22 casa 8442";
        $user_cliente2->save();
        $user_cliente2->assignRole(4); //con esto decimos que va a ser Cliente*/

        //Definimos a 1 Auditor
        $user_auditor1=new User();
        $user_auditor1->name="LUIS MARIO";
        $user_auditor1->surname="SOLONEZEN";
        $user_auditor1->email="markzucke@mail.com";
        $user_auditor1->password=bcrypt('SOLONEZEN123456');
        $user_auditor1->photo='/imagenes/empleados/auditor1.png';
        $user_auditor1->assigned=false;
        $user_auditor1->district_id=2;
        $user_auditor1->address="188 3433";
        $user_auditor1->save();
        $user_auditor1->assignRole(5); //con esto decimos que va a ser Auditor*/


       //-----------------------Definimos 2 empleados sin grupo
        $user_empleado5=new User();
        $user_empleado5->name="PABLO";
        $user_empleado5->surname="PEZUCK";
        $user_empleado5->email="pezuck@mail.com";
        $user_empleado5->photo= '/imagenes/empleados/emp2.png';
        $user_empleado5->password=bcrypt('PEZUCK523456');
        $user_empleado5->assigned=false;
        $user_empleado5->district_id=7;
        $user_empleado5->address="calle 32 A";
        $user_empleado5->save();
        $user_empleado5->assignRole(3); //con esto decimos que va a ser Empleado

        $empleado = User::create([
            'name'        =>  'MIGUEL',
            'surname'     =>  'SANCHEZ',
            'email'       =>  'SAAanches@mail.com',
            'password'    =>  bcrypt('123456'),
            'photo'       =>  '/imagenes/empleados/emp1.png',
            'assigned'    =>   false,
            'district_id' =>   1,
            'address'     =>   'calle 32 A',

        ]);

        $empleado->assignRole(3);


        //Definimos 2 EMPLEADOS sin asociar a un grupo
        $user_empleado6=new User();
        $user_empleado6->name="RICARDO";
        $user_empleado6->surname="MANZUR";
        $user_empleado6->email="manzur@mail.com";
        $user_empleado6->photo= '/imagenes/empleados/emp17.png';
        $user_empleado6->password=bcrypt('MANZURO623466');
        $user_empleado6->assigned=false;
        $user_empleado6->district_id=1;
        $user_empleado6->address="calle 44 D";
        $user_empleado6->save();
        $user_empleado6->assignRole(3); //con esto decimos que va a ser Empleado


        //-----------------------Continuamos con los empleados que nos faltan
        /*id = 4
        id = 5
        id = 6
        id = 7*/


        //---seguimos a partir del id 13 
        $user_empleado2=new User();
        $user_empleado2->name="TOMAS";
        $user_empleado2->surname="GOTCHALK";
        $user_empleado2->email="gotchalk@mail.com";
        $user_empleado2->photo= '/imagenes/empleados/emp18.png';
        $user_empleado2->password=bcrypt('gotchalk123456');
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=5;
        $user_empleado2->address="25 1888";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //---seguimos a partir del id 14 
        $user_empleado2=new User();
        $user_empleado2->name="AUGUSTO";
        $user_empleado2->surname="PEREZ";
        $user_empleado2->email="augustoperez@mail.com";
        $user_empleado2->photo= '/imagenes/empleados/emp19.png';
        $user_empleado2->password=bcrypt('augustoperez123456');
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=6;
        $user_empleado2->address="DPTO5 P1 DE 2";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //---seguimos a partir del id 15 
        $user_empleado2=new User();
        $user_empleado2->name="LUCAS";
        $user_empleado2->surname="MATIAUDA";
        $user_empleado2->email="lucasmatiauda@mail.com";
        $user_empleado2->password=bcrypt('LUCASMATIAUDA123456');
        $user_empleado2->photo= '/imagenes/empleados/emp20.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=23;
        $user_empleado2->address="CH 32-33";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado
        
        //---seguimos a partir del id 16 
        $user_empleado2=new User();
        $user_empleado2->name="FRANCISCO JAVIER";
        $user_empleado2->surname="WEBER";
        $user_empleado2->email="francweber@mail.com";
        $user_empleado2->password=bcrypt('francweber123456');
        $user_empleado2->photo= '/imagenes/empleados/emp21.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=21;
        $user_empleado2->address="Altura 22 Casa 5";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

 
        //---seguimos a partir del id 17 
        $user_empleado2=new User();
        $user_empleado2->name="JULIO DAVID";
        $user_empleado2->surname="DA SILVA";
        $user_empleado2->email="juliodavid@mail.com";
        $user_empleado2->password=bcrypt('juliodavid23456');
        $user_empleado2->photo= '/imagenes/empleados/emp22.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=16;
        $user_empleado2->address="Altura 27 Casa 9";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado


         
        //---seguimos a partir del id 18
        $user_empleado2=new User();
        $user_empleado2->name="JORGE RUBEN";
        $user_empleado2->surname="WISPLACK";
        $user_empleado2->email="jorgeruben@mail.com";
        $user_empleado2->password=bcrypt('jorgeruben123456');
        $user_empleado2->photo= '/imagenes/empleados/emp23.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=17;
        $user_empleado2->address="Casa 2050 Altura 3";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

         
        //---seguimos a partir del id 19
        $user_empleado2=new User();
        $user_empleado2->name="SAUL";
        $user_empleado2->surname="BENITEZ";
        $user_empleado2->email="saul@mail.com";
        $user_empleado2->password=bcrypt('saul123456');
        $user_empleado2->photo= '/imagenes/empleados/emp24.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=19;
        $user_empleado2->address="Manzana 5 casa 5";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado


         
        //---seguimos a partir del id 20
        $user_empleado2=new User();
        $user_empleado2->name="NAHUEL";
        $user_empleado2->surname="RIVAS";
        $user_empleado2->email="nahuelricas@mail.com";
        $user_empleado2->password=bcrypt('nahuelrivas123456');
        $user_empleado2->photo= '/imagenes/empleados/emp25.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=24;
        $user_empleado2->address="CH 34 MZ 4 Casa 9";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //---seguimos a partir del id 21
        $user_empleado2=new User();
        $user_empleado2->name="MARIANO";
        $user_empleado2->surname="BENITEZ";
        $user_empleado2->email="marianobenitez@mail.com";
        $user_empleado2->password=bcrypt('marianobenitez123456');
        $user_empleado2->photo= '/imagenes/empleados/emp26.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=15;
        $user_empleado2->address="calle 88 C Altura 4 casa 9";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

         //---seguimos a partir del id 22
         $user_empleado2=new User();
         $user_empleado2->name="ESTEBAN";
         $user_empleado2->surname="ROJAS";
         $user_empleado2->email="estebanrojas@mail.com";
         $user_empleado2->password=bcrypt('estebanrojas123456');
         $user_empleado2->photo= '/imagenes/empleados/emp27.png';
         $user_empleado2->assigned=false;
         $user_empleado2->district_id=18;
         $user_empleado2->address="depto 6 piso 4 de 5";
         $user_empleado2->save();
         $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado


        //---seguimos a partir del id 23
        $user_empleado2=new User();
        $user_empleado2->name="GERMAN";
        $user_empleado2->surname="MUSLACK";
        $user_empleado2->email="germanmuslack@mail.com";
        $user_empleado2->photo= '/imagenes/empleados/emp4.png';
        $user_empleado2->password=bcrypt('germanmuslac123456');
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=7;
        $user_empleado2->address="depto 23 piso 1 de 2";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //------------------agregamos mas clientes
        /*
            id = 8 Maria
            id = 9 Jose Andrea


            seguimos a partir del 24
        */ 


        //---seguimos a partir del id 24
        $user_empleado2=new User();
        $user_empleado2->name="AUGUSTO";
        $user_empleado2->surname="SANTANDER";
        $user_empleado2->email="augustosantander@mail.com";
        $user_empleado2->password=bcrypt('augustosantander123456');
        $user_empleado2->photo= '/imagenes/empleados/emp11.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=19;
        $user_empleado2->address="manzana 5 casa 4";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado
        
        
        //---seguimos a partir del id 25
        $user_empleado2=new User();
        $user_empleado2->name="MATIAS";
        $user_empleado2->surname="KURTZ";
        $user_empleado2->email="matiasoledad@mail.com";
        $user_empleado2->password=bcrypt('matiasoledad123456');
        $user_empleado2->photo= '/imagenes/empleados/emp5.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=19;
        $user_empleado2->address="manzana 7 casa 2";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //---seguimos a partir del id 26
        $user_empleado2=new User();
        $user_empleado2->name="GUILLERMO";
        $user_empleado2->surname="GOMEZ";
        $user_empleado2->email="guillego@mail.com";
        $user_empleado2->password=bcrypt('roxi123456');
        $user_empleado2->photo= '/imagenes/empleados/emp8.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=19;
        $user_empleado2->address="manzana 6 casa 2";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

        //---seguimos a partir del id 27
        $user_empleado2=new User();
        $user_empleado2->name="AGUSTIN";
        $user_empleado2->surname="VILLAREAL";
        $user_empleado2->email="agustinvillareal@mail.com";
        $user_empleado2->password=bcrypt('agustinvillareal23456');
        $user_empleado2->photo= '/imagenes/empleados/emp6.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=55;
        $user_empleado2->address="calle 33 A 8338";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado
            
        //---seguimos a partir del id 28
        $user_empleado2=new User();
        $user_empleado2->name="MARIO";
        $user_empleado2->surname="DO SANTOS";
        $user_empleado2->email="mariodosanto@mail.com";
        $user_empleado2->password=bcrypt('mariodosanto23456');
        $user_empleado2->photo= '/imagenes/empleados/emp7.png';
        $user_empleado2->assigned=false;
        $user_empleado2->district_id=55;
        $user_empleado2->address="calle 31 A 8339";
        $user_empleado2->save();
        $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado

          //---seguimos a partir del id 29
          $user_empleado2=new User();
          $user_empleado2->name="JOSE";
          $user_empleado2->surname="GUTTIERREZ";
          $user_empleado2->email="josegutierres@mail.com";
          $user_empleado2->password=bcrypt('josegutierres23456');
          $user_empleado2->photo= '/imagenes/empleados/emp3.png';
          $user_empleado2->assigned=false;
          $user_empleado2->district_id=55;
          $user_empleado2->address="calle 21 A 7228";
          $user_empleado2->save();
          $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado


            //---seguimos a partir del id 29
            $user_empleado2=new User();
            $user_empleado2->name="JULIO";
            $user_empleado2->surname="BROLLO";
            $user_empleado2->email="juliobrollo@mail.com";
            $user_empleado2->password=bcrypt('juliobrollo23456');
            $user_empleado2->photo= '/imagenes/empleados/emp12.png';
            $user_empleado2->assigned=false;
            $user_empleado2->district_id=55;
            $user_empleado2->address="calle 45 B 8329";
            $user_empleado2->save();
            $user_empleado2->assignRole(3); //con esto decimos que va a ser Empleado
    
    }
}