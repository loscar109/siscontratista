<?php

use Illuminate\Database\Seeder;
use App\Models\TicketAssignamentPrivilege;


class TicketAssignamentPrivilegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        TicketAssignamentPrivilege::create([
            'name'          =>'Asignar',
            'alias'         =>'A',
            'description'   =>'permiso para asignar un tiket a un usuario'
        ]);

        TicketAssignamentPrivilege::create([
            'name'          =>'Leer',
            'alias'         =>'L',
            'description'   =>'permiso para revisar un ticket',
        ]);

        TicketAssignamentPrivilege::create([
            'name'          =>'Leer solo si no finalizo',
            'alias'         =>'LNF',
            'description'   =>'permiso para ver si el ticket no esta finalizado',
        ]);

        TicketAssignamentPrivilege::create([
            'name'          =>'Leer solo si finalizo',
            'alias'         =>'LSF',
            'description'   =>'permiso para ver solamente si el ticket esta terminado',
        ]);

        TicketAssignamentPrivilege::create([
            'name'          =>'Creador',
            'alias'         =>'C',
            'description'   =>'permiso otorgado para quien es el que da de alta el ticket',
        ]);
    }
}
