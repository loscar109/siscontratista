<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $this->call(CountryTableSeeder::class);
    $this->call(ProvinceTableSeeder::class);
    $this->call(CityTableSeeder::class);
    $this->call(DistrictTableSeeder::class);

     //roles y permisos
     $this->call(UsersTableSeeder::class);
     $this->call(PermissionsTableSeeder::class);
     


     //Los Estados de los Tickets
     $this->call(TicketStagesTableSeeder::class);
     
     //Los Flujos de Trabajos
     $this->call(WorkFlowsTableSeeder::class);

     //Las Categorias que pueden tener los Tickets
     $this->call(TicketCategoriesTableSeeder::class);

     //Los Grupos de Trabajo que pueden tener los Tickets
     $this->call(WorkGroupsTableSeeder::class);




     //Asignacion a Usuarios por Grupo de Trabajo
     $this->call(UserWorkGroupsTableSeeder::class);
  
     //El orden que van a tomar los Tickets
     $this->call(TicketStageOrdersTableSeeder::class);

     //Los Tickets
     $this->call(TicketsTableSeeder::class);

     //Las categorias de los materiales
     $this->call(CategoriesTableSeeder::class);

     //Los materiales
     $this->call(MaterialsTableSeeder::class);





       

    }
}
