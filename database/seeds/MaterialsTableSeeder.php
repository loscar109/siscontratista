<?php

use Illuminate\Database\Seeder;
use siscontratista\Material;
use siscontratista\Movement;


use siscontratista\Reception;


class MaterialsTableSeeder extends Seeder
{
    /** 
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ma1=new Material();
        $ma1->description='Adapatador F/G';                                                                                 
        $ma1->photo='ADAPT002.png';
        $ma1->bundle_quantity=150;                      //unidades por bolsa: 150
        $ma1->smin=300;                                 //minimo: 300
        $ma1->stock=750;                                //stock: 750
        $ma1->smax=2250;                                //maximo: 2250
        $ma1->category_id=2;
        $ma1->bundle_name='Bolsa de Adapatador F/G' . " (". $ma1->bundle_quantity . " " . $ma1->category->medida . ")";
        $ma1->real_stock=(($ma1->stock*100)/$ma1->smin);
        $ma1->missing_stock=(100-$ma1->real_stock);       
        $ma1->created_at='2020-01-06 07:22:53';         //creacion:       06/01/2020    
        $ma1->updated_at='2020-01-06 07:22:53';         //actualizacion:  06/01/2020
        $ma1->save();   

        $mov1ma1 = new Movement;
        $mov1ma1->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma1->comprobante='Carga inicial';         //Carga inicial
        $mov1ma1->number='';                   
        $mov1ma1->quantity=750;                        //incremento: +750
        $mov1ma1->stock=750;                           //total     : 750
        $mov1ma1->user_id=2;                           //jefe
        $mov1ma1->material_id=1;                       //Adapatador F/G
        $mov1ma1->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma1->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma1->save();


        $ma2=new Material();
        $ma2->description='Trampas';
        $ma2->photo='ADAPT003.png';
        $ma2->bundle_quantity=250;             //unidades por bolsa: 250
        $ma2->smin=250;                         //minimo: 500
        $ma2->stock=1250;                       //stock: 1250                    
        $ma2->smax=3750;                        //maximo: 3750
        $ma2->category_id=2;
        $ma2->bundle_name='Bolsa de Trampas' . " (". $ma2->bundle_quantity . " " . $ma2->category->medida . ")";
        $ma2->real_stock=(($ma2->stock*100)/$ma2->smin);
        $ma2->missing_stock=(100-$ma2->real_stock);       
        $ma2->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $ma2->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020
        $ma2->save();


        $mov1ma2 = new Movement;
        $mov1ma2->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma2->comprobante='Carga inicial';         //Carga inicial
        $mov1ma2->number='';                   
        $mov1ma2->quantity=1250;                        //incremento: +1250
        $mov1ma2->stock=1250;                           //total     : +1250
        $mov1ma2->user_id=2;                           //jaime(JEFE)
        $mov1ma2->material_id=2;                       //Trampas
        $mov1ma2->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma2->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma2->save();



        $m_3=new Material();
        $m_3->description='CableModem';
        $m_3->photo='EQUIP001.png';
        $m_3->bundle_quantity=12;           //unidades por caja: 12
        $m_3->smin=24;                      //minimo: 24
        $m_3->stock=60;                     //stock: 60     
        $m_3->smax=180;                     //maximo: 180               
        $m_3->category_id=4;
        $m_3->bundle_name='Caja de CableModem' . " (". $m_3->bundle_quantity . " " . $m_3->category->medida . ")";
        $m_3->real_stock=(($m_3->stock*100)/$m_3->smin);
        $m_3->missing_stock=(100-$m_3->real_stock);       
        $m_3->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_3->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020
        $m_3->save();

        $mov1ma3 = new Movement;
        $mov1ma3->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma3->comprobante='Carga inicial';         //Carga inicial
        $mov1ma3->number='';                   
        $mov1ma3->quantity=60;                        //incremento: +1250
        $mov1ma3->stock=60;                           //total     : 1250
        $mov1ma3->user_id=2;                           //jaime(JEFE)
        $mov1ma3->material_id=3;                       //CableModem
        $mov1ma3->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma3->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma3->save();

        $m_4=new Material();
        $m_4->description='Precinto Azul';
        $m_4->photo='PREC003.png';
        $m_4->stock=1250;
        $m_4->smin=500;
        $m_4->smax=3750;
        $m_4->category_id=5;
        $m_4->bundle_quantity=250;
        $m_4->bundle_name='Bolsa de Precinto Azul' . " (". $m_4->bundle_quantity . " " . $m_4->category->medida . ")";
        $m_4->real_stock=(($m_4->stock*100)/$m_4->smin);
        $m_4->missing_stock=(100-$m_4->real_stock);       
        $m_4->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_4->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020
        $m_4->save();


        $mov1ma4 = new Movement;
        $mov1ma4->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma4->comprobante='Carga inicial';         //Carga inicial
        $mov1ma4->number='';                   
        $mov1ma4->quantity=1250;                        //incremento: +1250
        $mov1ma4->stock=1250;                           //total     : 1250
        $mov1ma4->user_id=2;                           //jaime(JEFE)
        $mov1ma4->material_id=4;                       //Precinto Azul
        $mov1ma4->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma4->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma4->save();

        $m_5=new Material();
        $m_5->description='Atenuador Domiciliaro de 8 dB';
        $m_5->photo='ATEN003.png';
        $m_5->stock=250;
        $m_5->smin=100;
        $m_5->smax=750;
        $m_5->category_id=6;
        $m_5->bundle_quantity=50;
        $m_5->bundle_name='Bolsa de Atenuador Domiciliaro de 8 dB' . " (". $m_5->bundle_quantity . " " . $m_5->category->medida . ")";
        $m_5->real_stock=(($m_5->stock*100)/$m_5->smin);
        $m_5->missing_stock=(100-$m_5->real_stock);       
        $m_5->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_5->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020
        $m_5->save();

        $mov1ma5 = new Movement;
        $mov1ma5->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma5->comprobante='Carga inicial';         //Carga inicial
        $mov1ma5->number='';                   
        $mov1ma5->quantity=1250;                        //incremento: +1250
        $mov1ma5->stock=1250;                           //total     : 1250
        $mov1ma5->user_id=2;                           //jaime(JEFE)
        $mov1ma5->material_id=5;                       //Atenuador Domiciliaro de 8 dB
        $mov1ma5->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma5->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma5->save();

        $m_6=new Material();
        $m_6->description='Cable RG 6 C/Portante';
        $m_6->photo='CAB001.png';
        $m_6->stock=1530;
        $m_6->smin=612;
        $m_6->smax=4590;
        $m_6->category_id=1;
        $m_6->bundle_quantity=306;
        $m_6->bundle_name='Rollo de Cable RG 6 C/Portante' . " (". $m_6->bundle_quantity . " " . $m_6->category->medida . ")";
        $m_6->real_stock=(($m_6->stock*100)/$m_6->smin);
        $m_6->missing_stock=(100-$m_6->real_stock);     
        $m_6->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_6->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020  
        $m_6->save();

        $mov1ma6 = new Movement;
        $mov1ma6->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma6->comprobante='Carga inicial';         //Carga inicial
        $mov1ma6->number='';                   
        $mov1ma6->quantity=1530;                        //incremento: +1530
        $mov1ma6->stock=1530;                           //total     : 1530
        $mov1ma6->user_id=2;                           //jaime(JEFE)
        $mov1ma6->material_id=6;                       //Cable RG 6 C/Portante
        $mov1ma6->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma6->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma6->save();

        $m_7=new Material();
        $m_7->description='Cable RG-6 S/Portante';
        $m_7->photo='CAB002.png';
        $m_7->stock=1530;
        $m_7->smin=612;
        $m_7->smax=4590;
        $m_7->category_id=1;
        $m_7->bundle_quantity=306;
        $m_7->bundle_name='Rollo de Cable RG-6 S/Portante' . " (". $m_7->bundle_quantity . " " . $m_7->category->medida . ")";
        $m_7->real_stock=(($m_7->stock*100)/$m_7->smin);
        $m_7->missing_stock=(100-$m_7->real_stock);       
        $m_7->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_7->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020  
        $m_7->save();
  
        $mov1ma7 = new Movement;
        $mov1ma7->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma7->comprobante='Carga inicial';         //Carga inicial
        $mov1ma7->number='';                   
        $mov1ma7->quantity=1530;                        //incremento: +1530
        $mov1ma7->stock=1530;                           //total     : 1530
        $mov1ma7->user_id=2;                           //jaime(JEFE)
        $mov1ma7->material_id=7;                       //Cable RG-6 S/Portante
        $mov1ma7->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma7->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma7->save();

        $m_8=new Material();
        $m_8->description='Cable RG-6 Blanco';
        $m_8->photo='CAB003.png';
        $m_8->stock=1530;
        $m_8->smin=612;
        $m_8->smax=4590;
        $m_8->category_id=1;
        $m_8->bundle_quantity=306;
        $m_8->bundle_name='Rollo de Cable RG-6 Blanco' . " (". $m_8->bundle_quantity . " " . $m_8->category->medida . ")";
        $m_8->real_stock=(($m_8->stock*100)/$m_8->smin);
        $m_8->missing_stock=(100-$m_8->real_stock);       
        $m_8->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_8->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020  
        $m_8->save();

        $mov1ma8 = new Movement;
        $mov1ma8->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma8->comprobante='Carga inicial';         //Carga inicial
        $mov1ma8->number='';                   
        $mov1ma8->quantity=1530;                        //incremento: +1530
        $mov1ma8->stock=1530;                           //total     : 1530
        $mov1ma8->user_id=2;                           //jaime(JEFE)
        $mov1ma8->material_id=8;                       //Cable RG-6 Blanco
        $mov1ma8->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma8->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma8->save();

        $m_9=new Material();
        $m_9->description='Conector F-6';
        $m_9->photo='CONEC001.png';
        $m_9->stock=750;
        $m_9->smin=300;
        $m_9->smax=2250;
        $m_9->category_id=2;
        $m_9->bundle_quantity=150;
        $m_9->bundle_name='Bolsa de Conector F-6' . " (". $m_9->bundle_quantity . " " . $m_9->category->medida . ")";
        $m_9->real_stock=(($m_9->stock*100)/$m_9->smin);
        $m_9->missing_stock=(100-$m_9->real_stock);       
        $m_9->created_at='2020-01-06 07:22:53';      //creacion:06/01/2020    
        $m_9->updated_at='2020-01-06 07:22:53';      //actualizacion:  06/01/2020  
        $m_9->save();

        $mov1ma9 = new Movement;
        $mov1ma9->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma9->comprobante='Carga inicial';         //Carga inicial
        $mov1ma9->number='';                   
        $mov1ma9->quantity=750;                        //incremento: +750
        $mov1ma9->stock=750;                           //total     : 750
        $mov1ma9->user_id=2;                           //jaime(JEFE)
        $mov1ma9->material_id=9;                       //Conector F-6
        $mov1ma9->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma9->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma9->save();

        $M_10=new Material();
        $M_10->description='Adaptador (H-H)';
        $M_10->photo='ADAPT001.png';
        $M_10->stock=750;
        $M_10->smin=300;
        $M_10->smax=2250;
        $M_10->category_id=2;
        $M_10->bundle_quantity=150;
        $M_10->bundle_name='Bolsa de Adaptador (H-H)' . " (". $M_10->bundle_quantity . " " . $M_10->category->medida . ")";
        $M_10->real_stock=(($M_10->stock*100)/$M_10->smin);
        $M_10->missing_stock=(100-$M_10->real_stock);       
        $M_10->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $M_10->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM      
        $M_10->save();

        $mov1ma10 = new Movement;
        $mov1ma10->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma10->comprobante='Carga inicial';         //Carga inicial
        $mov1ma10->number='';                   
        $mov1ma10->quantity=750;                        //incremento: +750
        $mov1ma10->stock=750;                           //total     : 750
        $mov1ma10->user_id=2;                           //jaime(JEFE)
        $mov1ma10->material_id=10;                       //Adaptador (H-H)
        $mov1ma10->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma10->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma10->save();

        $m_11=new Material();
        $m_11->description='Aerosello';
        $m_11->photo='CONEC002.png';
        $m_11->stock=1500;
        $m_11->smin=600;
        $m_11->smax=4500;
        $m_11->category_id=2;
        $m_11->bundle_quantity=300;
        $m_11->bundle_name='Bolsa de Aerosello' . " (". $m_11->bundle_quantity . " " . $m_11->category->medida . ")";
        $m_11->real_stock=(($m_11->stock*100)/$m_11->smin);
        $m_11->missing_stock=(100-$m_11->real_stock);       
        $m_11->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_11->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM  
        $m_11->save();

        $mov1ma11 = new Movement;
        $mov1ma11->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma11->comprobante='Carga inicial';         //Carga inicial
        $mov1ma11->number='';                   
        $mov1ma11->quantity=1500;                        //incremento: +1500
        $mov1ma11->stock=1500;                           //total     : 1500
        $mov1ma11->user_id=2;                           //jaime(JEFE)
        $mov1ma11->material_id=11;                       //Adaptador (H-H)
        $mov1ma11->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma11->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma11->save();

        $m_12=new Material();
        $m_12->description='Conector F 59';
        $m_12->photo='CONEC003.png';
        $m_12->stock=750;
        $m_12->smin=300;
        $m_12->smax=2250;
        $m_12->category_id=2;
        $m_12->bundle_quantity=150;
        $m_12->bundle_name='Bolsa de Conector F 59' . " (". $m_12->bundle_quantity . " " . $m_12->category->medida . ")";
        $m_12->real_stock=(($m_12->stock*100)/$m_12->smin);
        $m_12->missing_stock=(100-$m_12->real_stock);   
        $m_12->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_12->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM      
        $m_12->save();

        $mov1ma12 = new Movement;
        $mov1ma12->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma12->comprobante='Carga inicial';         //Carga inicial
        $mov1ma12->number='';                   
        $mov1ma12->quantity=750;                        //incremento: +750
        $mov1ma12->stock=750;                           //total     : 750
        $mov1ma12->user_id=2;                           //jaime(JEFE)
        $mov1ma12->material_id=12;                       //Adaptador (H-H)
        $mov1ma12->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma12->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma12->save();

        $m_13=new Material();
        $m_13->description='Divisor 2 VIAS';
        $m_13->photo='DIV001.png';
        $m_13->stock=600;
        $m_13->smin=240;
        $m_13->smax=1800;
        $m_13->category_id=3;
        $m_13->bundle_quantity=120;
        $m_13->bundle_name='Caja de Divisor 2 VIAS' . " (". $m_13->bundle_quantity . " " . $m_13->category->medida . ")";
        $m_13->real_stock=(($m_13->stock*100)/$m_13->smin);
        $m_13->missing_stock=(100-$m_13->real_stock);       
        $m_13->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_13->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM      
        $m_13->save();

        $mov1ma13 = new Movement;
        $mov1ma13->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma13->comprobante='Carga inicial';         //Carga inicial
        $mov1ma13->number='';                   
        $mov1ma13->quantity=600;                        //incremento: +600
        $mov1ma13->stock=600;                           //total     : 600
        $mov1ma13->user_id=2;                           //jaime(JEFE)
        $mov1ma13->material_id=13;                       //Adaptador (H-H)
        $mov1ma13->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma13->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma13->save();

        $m_14=new Material();
        $m_14->description='Divisor 3 VIAS';
        $m_14->photo='DIV002.png';
        $m_14->stock=600;
        $m_14->smin=240;
        $m_14->smax=1800;
        $m_14->category_id=3;
        $m_14->bundle_quantity=120;
        $m_14->bundle_name='Caja de Divisor 3 VIAS' . " (". $m_14->bundle_quantity . " " . $m_14->category->medida . ")";
        $m_14->real_stock=(($m_14->stock*100)/$m_14->smin);
        $m_14->missing_stock=(100-$m_14->real_stock);   
        $m_14->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_14->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM       
        $m_14->save();

        $mov1ma14 = new Movement;
        $mov1ma14->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma14->comprobante='Carga inicial';         //Carga inicial
        $mov1ma14->number='';                   
        $mov1ma14->quantity=600;                        //incremento: +600
        $mov1ma14->stock=600;                           //total     : 600
        $mov1ma14->user_id=2;                           //jaime(JEFE)
        $mov1ma14->material_id=14;                       //Adaptador (H-H)
        $mov1ma14->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma14->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma14->save();        

        $m_15=new Material();
        $m_15->description='Divisor 4 VIAS';
        $m_15->photo='DIV003.png';
        $m_15->stock=600;
        $m_15->smin=240;
        $m_15->smax=1800;
        $m_15->category_id=3;
        $m_15->bundle_quantity=120;
        $m_15->bundle_name='Caja de Divisor 4 VIAS' . " (". $m_15->bundle_quantity . " " . $m_15->category->medida . ")";
        $m_15->real_stock=(($m_15->stock*100)/$m_15->smin);
        $m_15->missing_stock=(100-$m_15->real_stock);      
        $m_15->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_15->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM      
        $m_15->save();

        $mov1ma15 = new Movement;
        $mov1ma15->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma15->comprobante='Carga inicial';         //Carga inicial
        $mov1ma15->number='';                   
        $mov1ma15->quantity=600;                        //incremento: +600
        $mov1ma15->stock=600;                           //total     : 600
        $mov1ma15->user_id=2;                           //jaime(JEFE)
        $mov1ma15->material_id=15;                       //Adaptador (H-H)
        $mov1ma15->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma15->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma15->save();    

        $m_16=new Material();
        $m_16->description='Acoplador de 6 dB';
        $m_16->photo='ACOP001.png';
        $m_16->stock=600;
        $m_16->smin=240;
        $m_16->smax=1800;
        $m_16->category_id=3;
        $m_16->bundle_quantity=120;
        $m_16->bundle_name='Caja de Acoplador de 6 dB' . " (". $m_16->bundle_quantity . " " . $m_16->category->medida . ")";
        $m_16->real_stock=(($m_16->stock*100)/$m_16->smin);
        $m_16->missing_stock=(100-$m_16->real_stock);     
        $m_16->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_16->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM     
        $m_16->save();

        $mov1ma16 = new Movement;
        $mov1ma16->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma16->comprobante='Carga inicial';         //Carga inicial
        $mov1ma16->number='';                   
        $mov1ma16->quantity=600;                        //incremento: +600
        $mov1ma16->stock=600;                           //total     : 600
        $mov1ma16->user_id=2;                           //jaime(JEFE)
        $mov1ma16->material_id=16;                       //Adaptador (H-H)
        $mov1ma16->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma16->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma16->save();    

        $m_17=new Material();
        $m_17->description='Acoplador de 9 dB';
        $m_17->photo='ACOP002.png';
        $m_17->stock=600;
        $m_17->smin=240;
        $m_17->smax=1800;
        $m_17->category_id=3;
        $m_17->bundle_quantity=120;
        $m_17->bundle_name='Caja de Acoplador de 9 dB' . " (". $m_17->bundle_quantity . " " . $m_17->category->medida . ")";
        $m_17->real_stock=(($m_17->stock*100)/$m_17->smin);
        $m_17->missing_stock=(100-$m_17->real_stock);  
        $m_17->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_17->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM               
        $m_17->save();

        $mov1ma17 = new Movement;
        $mov1ma17->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma17->comprobante='Carga inicial';         //Carga inicial
        $mov1ma17->number='';                   
        $mov1ma17->quantity=600;                        //incremento: +600
        $mov1ma17->stock=600;                           //total     : 600
        $mov1ma17->user_id=2;                           //jaime(JEFE)
        $mov1ma17->material_id=17;                       //Adaptador (H-H)
        $mov1ma17->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $mov1ma17->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM          
        $mov1ma17->save();   

        $m_18=new Material();
        $m_18->description='Acoplador de 12 dB';
        $m_18->photo='ACOP003.png';
        $m_18->stock=600;
        $m_18->smin=240;
        $m_18->smax=1800;
        $m_18->category_id=3;
        $m_18->bundle_quantity=120;
        $m_18->bundle_name='Caja de Acoplador de 12 dB' . " (". $m_18->bundle_quantity . " " . $m_18->category->medida . ")";
        $m_18->real_stock=(($m_18->stock*100)/$m_18->smin);
        $m_18->missing_stock=(100-$m_18->real_stock);      
        $m_18->created_at='2020-01-06 07:22:53';    //creacion: 01/01/2020 7AM   
        $m_18->updated_at='2020-01-06 07:22:53';    //actualizacion: 01/01/2020 7AM  
        $m_18->save();


        $mov1ma18 = new Movement;
        $mov1ma18->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma18->comprobante='Carga inicial';         //Carga inicial
        $mov1ma18->number='';                   
        $mov1ma18->quantity=600;                        //incremento: +600
        $mov1ma18->stock=600;                           //total     : 600
        $mov1ma18->user_id=2;                           //jaime(JEFE)
        $mov1ma18->material_id=18;                       //Adaptador (H-H)
        $mov1ma18->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1ma18->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1ma18->save();   


        $m_19=new Material();
        $m_19->description='WiFi';
        $m_19->photo='EQUIP002.png';
        $m_19->stock=60;
        $m_19->smin=24;
        $m_19->smax=180;
        $m_19->category_id=4;
        $m_19->bundle_quantity=12;
        $m_19->bundle_name='Caja de WiFi' . " (". $m_19->bundle_quantity . " " . $m_19->category->medida . ")";
        $m_19->real_stock=(($m_19->stock*100)/$m_19->smin);
        $m_19->missing_stock=(100-$m_19->real_stock);      
        $m_19->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_19->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_19->save();

        $mov1ma19 = new Movement;
        $mov1ma19->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma19->comprobante='Carga inicial';         //Carga inicial
        $mov1ma19->number='';                   
        $mov1ma19->quantity=60;                        //incremento: +60
        $mov1ma19->stock=60;                           //total     : 60
        $mov1ma19->user_id=2;                           //jaime(JEFE)
        $mov1ma19->material_id=19;                       //Adaptador (H-H)
        $mov1ma19->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1ma19->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1ma19->save();   

        $m_20=new Material();
        $m_20->description='WiFi Gold';
        $m_20->photo='EQUIP003.png';
        $m_20->stock=60;
        $m_20->smin=24;
        $m_20->smax=180;
        $m_20->category_id=4;
        $m_20->bundle_quantity=12;
        $m_20->bundle_name='Caja de WiFi Gold' . " (". $m_20->bundle_quantity . " " . $m_20->category->medida . ")";
        $m_20->real_stock=(($m_20->stock*100)/$m_20->smin);
        $m_20->missing_stock=(100-$m_20->real_stock);      
        $m_20->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_20->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM 
        $m_20->save();

        $mov1ma20 = new Movement;
        $mov1ma20->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma20->comprobante='Carga inicial';         //Carga inicial
        $mov1ma20->number='';                   
        $mov1ma20->quantity=60;                        //incremento: +60
        $mov1ma20->stock=60;                           //total     : 60
        $mov1ma20->user_id=2;                           //jaime(JEFE)
        $mov1ma20->material_id=20;                       //Adaptador (H-H)
        $mov1ma20->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1ma20->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1ma20->save();   

        $m_21=new Material();
        $m_21->description='WiFi 50M y 100M';
        $m_21->photo='EQUIP004.png';
        $m_21->stock=60;
        $m_21->smin=24;
        $m_21->smax=180;
        $m_21->category_id=4;
        $m_21->bundle_quantity=12;
        $m_21->bundle_name='Caja de WiFi 50M y 100M' . " (". $m_21->bundle_quantity . " " . $m_21->category->medida . ")";
        $m_21->real_stock=(($m_21->stock*100)/$m_21->smin);
        $m_21->missing_stock=(100-$m_21->real_stock);       
        $m_21->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_21->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM 
        $m_21->save();

        $mov1ma21 = new Movement;
        $mov1ma21->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1ma21->comprobante='Carga inicial';         //Carga inicial
        $mov1ma21->number='';                   
        $mov1ma21->quantity=60;                        //incremento: +60
        $mov1ma21->stock=60;                           //total     : 60
        $mov1ma21->user_id=2;                           //jaime(JEFE)
        $mov1ma21->material_id=21;                       //Adaptador (H-H)
        $mov1ma21->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1ma21->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1ma21->save();   

        $m_22=new Material();
        $m_22->description='DCT700';
        $m_22->photo='EQUIP005.png';
        $m_22->stock=60;
        $m_22->smin=24;
        $m_22->smax=180;
        $m_22->category_id=4;
        $m_22->bundle_quantity=12;
        $m_22->bundle_name='Caja de DCT700' . " (". $m_22->bundle_quantity . " " . $m_22->category->medida . ")";
        $m_22->real_stock=(($m_22->stock*100)/$m_22->smin);
        $m_22->missing_stock=(100-$m_22->real_stock);       
        $m_22->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_22->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_22->save();     

        $mov1m_22 = new Movement;
        $mov1m_22->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_22->comprobante='Carga inicial';         //Carga inicial
        $mov1m_22->number='';                   
        $mov1m_22->quantity=60;                        //incremento: +60
        $mov1m_22->stock=60;                           //total     : 60
        $mov1m_22->user_id=2;                           //jaime(JEFE)
        $mov1m_22->material_id=22;                       //Adaptador (H-H)
        $mov1m_22->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_22->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_22->save();   
        
        $m_23=new Material();
        $m_23->description='HD';
        $m_23->photo='EQUIP006.png';
        $m_23->stock=125;
        $m_23->smin=50;
        $m_23->smax=375;
        $m_23->category_id=4;
        $m_23->bundle_quantity=25;
        $m_23->bundle_name='Caja de HD' . " (". $m_23->bundle_quantity . " " . $m_23->category->medida . ")";
        $m_23->real_stock=(($m_23->stock*100)/$m_23->smin);
        $m_23->missing_stock=(100-$m_23->real_stock);       
        $m_23->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_23->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_23->save();    

        $mov1m_23 = new Movement;
        $mov1m_23->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_23->comprobante='Carga inicial';         //Carga inicial
        $mov1m_23->number='';                   
        $mov1m_23->quantity=125;                        //incremento: +60
        $mov1m_23->stock=125;                           //total     : 60
        $mov1m_23->user_id=2;                           //jaime(JEFE)
        $mov1m_23->material_id=23;                       //Adaptador (H-H)
        $mov1m_23->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_23->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_23->save();   

        $m_24=new Material();
        $m_24->description='DVR';
        $m_24->photo='EQUIP007.png';
        $m_24->stock=60;
        $m_24->smin=24;
        $m_24->smax=180;
        $m_24->category_id=4;
        $m_24->bundle_quantity=12;
        $m_24->bundle_name='Caja de DVR' . " (". $m_24->bundle_quantity . " " . $m_24->category->medida . ")";
        $m_24->real_stock=(($m_24->stock*100)/$m_24->smin);
        $m_24->missing_stock=(100-$m_24->real_stock);   
        $m_24->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_24->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM       
        $m_24->save(); 

        $mov1m_24 = new Movement;
        $mov1m_24->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_24->comprobante='Carga inicial';         //Carga inicial
        $mov1m_24->number='';                   
        $mov1m_24->quantity=60;                        //incremento: +60
        $mov1m_24->stock=60;                           //total     : 60
        $mov1m_24->user_id=2;                           //jaime(JEFE)
        $mov1m_24->material_id=24;                       //Adaptador (H-H)
        $mov1m_24->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_24->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_24->save(); 

        $m_25=new Material();
        $m_25->description='Grampa RG-6';
        $m_25->photo='GRAMP001.png';
        $m_25->stock=1500;
        $m_25->smin=600;
        $m_25->smax=4500;
        $m_25->category_id=5;
        $m_25->bundle_quantity=300;
        $m_25->bundle_name='Bolsa de Grampa RG-6' . " (". $m_25->bundle_quantity . " " . $m_25->category->medida . ")";
        $m_25->real_stock=(($m_25->stock*100)/$m_25->smin);
        $m_25->missing_stock=(100-$m_25->real_stock);   
        $m_25->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_25->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_25->save(); 

        $mov1m_25 = new Movement;
        $mov1m_25->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_25->comprobante='Carga inicial';         //Carga inicial
        $mov1m_25->number='';                   
        $mov1m_25->quantity=1500;                        //incremento: +1500
        $mov1m_25->stock=1500;                           //total     : 1500
        $mov1m_25->user_id=2;                           //jaime(JEFE)
        $mov1m_25->material_id=25;                       //Adaptador (H-H)
        $mov1m_25->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_25->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_25->save(); 

        $m_26=new Material();
        $m_26->description='Precinto 150-175';
        $m_26->photo='PREC001.png';
        $m_26->stock=1250;
        $m_26->smin=500;
        $m_26->smax=3750;
        $m_26->category_id=5;
        $m_26->bundle_quantity=250;
        $m_26->bundle_name='Bolsa de Precinto 150-175' . " (". $m_26->bundle_quantity . " " . $m_26->category->medida . ")";
        $m_26->real_stock=(($m_26->stock*100)/$m_26->smin);
        $m_26->missing_stock=(100-$m_26->real_stock);       
        $m_26->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_26->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_26->save(); 

        $mov1m_26 = new Movement;
        $mov1m_26->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_26->comprobante='Carga inicial';         //Carga inicial
        $mov1m_26->number='';                   
        $mov1m_26->quantity=1250;                        //incremento: +1250
        $mov1m_26->stock=1250;                           //total     : 1250
        $mov1m_26->user_id=2;                           //jaime(JEFE)
        $mov1m_26->material_id=26;                       //Adaptador (H-H)
        $mov1m_26->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_26->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_26->save(); 

        $m_27=new Material();
        $m_27->description='Precinto Rojo';
        $m_27->photo='PREC002.png';
        $m_27->stock=1250;
        $m_27->smin=500;
        $m_27->smax=3750;
        $m_27->category_id=5;
        $m_27->bundle_quantity=250;
        $m_27->bundle_name='Bolsa de Precinto Rojo' . " (". $m_27->bundle_quantity . " " . $m_27->category->medida . ")";
        $m_27->real_stock=(($m_27->stock*100)/$m_27->smin);
        $m_27->missing_stock=(100-$m_27->real_stock);       
        $m_27->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_27->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_27->save(); 

        $mov1m_27 = new Movement;
        $mov1m_27->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_27->comprobante='Carga inicial';         //Carga inicial
        $mov1m_27->number='';                   
        $mov1m_27->quantity=1250;                        //incremento: +1250
        $mov1m_27->stock=1250;                           //total     : 1250
        $mov1m_27->user_id=2;                           //jaime(JEFE)
        $mov1m_27->material_id=27;                       //Adaptador (H-H)
        $mov1m_27->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_27->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_27->save(); 


        $m_28=new Material();
        $m_28->description='Filtro de Dato';
        $m_28->photo='FILT001.png';
        $m_28->stock=600;
        $m_28->smin=240;
        $m_28->smax=1800;
        $m_28->category_id=6;
        $m_28->bundle_quantity=120;
        $m_28->bundle_name='Bolsa de Filtro de Dato' . " (". $m_28->bundle_quantity . " " . $m_28->category->medida . ")";
        $m_28->real_stock=(($m_28->stock*100)/$m_28->smin);
        $m_28->missing_stock=(100-$m_28->real_stock);       
        $m_28->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_28->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_28->save(); 

        $mov1m_28 = new Movement;
        $mov1m_28->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_28->comprobante='Carga inicial';         //Carga inicial
        $mov1m_28->number='';                   
        $mov1m_28->quantity=600;                        //incremento: +600
        $mov1m_28->stock=600;                           //total     : 600
        $mov1m_28->user_id=2;                           //jaime(JEFE)
        $mov1m_28->material_id=28;                       //Adaptador (H-H)
        $mov1m_28->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_28->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_28->save(); 

        $m_29=new Material();
        $m_29->description='Filtro de Retorno';
        $m_29->photo='FILT002.png';
        $m_29->stock=600;
        $m_29->smin=240;
        $m_29->smax=1800;
        $m_29->category_id=6;
        $m_29->bundle_quantity=120;
        $m_29->bundle_name='Bolsa de Filtro de Retorno' . " (". $m_29->bundle_quantity . " " . $m_29->category->medida . ")";
        $m_29->real_stock=(($m_29->stock*100)/$m_29->smin);
        $m_29->missing_stock=(100-$m_29->real_stock);  
        $m_29->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_29->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM        
        $m_29->save(); 


        $mov1m_29 = new Movement;
        $mov1m_29->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_29->comprobante='Carga inicial';         //Carga inicial
        $mov1m_29->number='';                   
        $mov1m_29->quantity=600;                        //incremento: +600
        $mov1m_29->stock=600;                           //total     : 600
        $mov1m_29->user_id=2;                           //jaime(JEFE)
        $mov1m_29->material_id=29;                       //Adaptador (H-H)
        $mov1m_29->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_29->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_29->save(); 

        $m_30=new Material();
        $m_30->description='Atenuador Domiciliaro de 3 dB';
        $m_30->photo='ATEN001.png';
        $m_30->stock=250;
        $m_30->smin=100;
        $m_30->smax=750;
        $m_30->category_id=6;
        $m_30->bundle_quantity=50;
        $m_30->bundle_name='Bolsa de Atenuador Domiciliaro de 3 dB' . " (". $m_30->bundle_quantity . " " . $m_30->category->medida . ")";
        $m_30->real_stock=(($m_30->stock*100)/$m_30->smin);
        $m_30->missing_stock=(100-$m_30->real_stock);       
        $m_30->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_30->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_30->save(); 

        $mov1m_30 = new Movement;
        $mov1m_30->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_30->comprobante='Carga inicial';         //Carga inicial
        $mov1m_30->number='';                   
        $mov1m_30->quantity=250;                        //incremento: +250
        $mov1m_30->stock=250;                           //total     : 250
        $mov1m_30->user_id=2;                           //jaime(JEFE)
        $mov1m_30->material_id=30;                       //Adaptador (H-H)
        $mov1m_30->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_30->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_30->save(); 


        $m_31=new Material();
        $m_31->description='Atenuador Domiciliaro de 6 dB';
        $m_31->photo='ATEN002.png';
        $m_31->stock=250;
        $m_31->smin=100;
        $m_31->smax=750;
        $m_31->category_id=6;
        $m_31->bundle_quantity=50;
        $m_31->bundle_name='Bolsa de Atenuador Domiciliaro de 6 dB' . " (". $m_31->bundle_quantity . " " . $m_31->category->medida . ")";
        $m_31->real_stock=(($m_31->stock*100)/$m_31->smin);
        $m_31->missing_stock=(100-$m_31->real_stock);     
        $m_31->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_31->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM     
        $m_31->save(); 
    
        $mov1m_31 = new Movement;
        $mov1m_31->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_31->comprobante='Carga inicial';         //Carga inicial
        $mov1m_31->number='';                   
        $mov1m_31->quantity=250;                        //incremento: +250
        $mov1m_31->stock=250;                           //total     : 250
        $mov1m_31->user_id=2;                           //jaime(JEFE)
        $mov1m_31->material_id=31;                       //Adaptador (H-H)
        $mov1m_31->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_31->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_31->save(); 

        $m_32=new Material();
        $m_32->description='Atenuador Domiciliar de 10 dB';
        $m_32->photo='ATEN004.png';
        $m_32->stock=250;
        $m_32->smin=100;
        $m_32->smax=750;
        $m_32->category_id=6;
        $m_32->bundle_quantity=50;
        $m_32->bundle_name='Bolsa de Atenuador Domiciliar de 10 dB' . " (". $m_32->bundle_quantity . " " . $m_32->category->medida . ")";
        $m_32->real_stock=(($m_32->stock*100)/$m_32->smin);
        $m_32->missing_stock=(100-$m_32->real_stock);       
        $m_32->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_32->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_32->save(); 

        $mov1m_32 = new Movement;
        $mov1m_32->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_32->comprobante='Carga inicial';         //Carga inicial
        $mov1m_32->number='';                   
        $mov1m_32->quantity=250;                        //incremento: +250
        $mov1m_32->stock=250;                           //total     : 250
        $mov1m_32->user_id=2;                           //jaime(JEFE)
        $mov1m_32->material_id=32;                       //Adaptador (H-H)
        $mov1m_32->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_32->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_32->save(); 

        $m_33=new Material();
        $m_33->description='Atenuador Domiciliaro de 12 dB';
        $m_33->photo='ATEN005.png';
        $m_33->stock=250;
        $m_33->smin=100;
        $m_33->smax=750;
        $m_33->category_id=6;
        $m_33->bundle_quantity=50;
        $m_33->bundle_name='Bolsa de Atenuador Domiciliaro de 12 dB' . " (". $m_33->bundle_quantity . " " . $m_33->category->medida . ")";
        $m_33->real_stock=(($m_33->stock*100)/$m_33->smin);
        $m_33->missing_stock=(100-$m_33->real_stock);     
        $m_33->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_33->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM     
        $m_33->save(); 

        $mov1m_33 = new Movement;
        $mov1m_33->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_33->comprobante='Carga inicial';         //Carga inicial
        $mov1m_33->number='';                   
        $mov1m_33->quantity=250;                        //incremento: +250
        $mov1m_33->stock=250;                           //total     : 250
        $mov1m_33->user_id=2;                           //jaime(JEFE)
        $mov1m_33->material_id=33;                       //Adaptador (H-H)
        $mov1m_33->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_33->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_33->save(); 


        $m_34=new Material();
        $m_34->description='Cinta Autovulcanizante';
        $m_34->photo='CINT001.png';
        $m_34->stock=600;
        $m_34->smin=240;
        $m_34->smax=1800;
        $m_34->category_id=7;
        $m_34->bundle_quantity=120;
        $m_34->bundle_name='Caja de Cinta Autovulcanizante' . " (". $m_34->bundle_quantity . " " . $m_34->category->medida . ")";
        $m_34->real_stock=(($m_34->stock*100)/$m_34->smin);
        $m_34->missing_stock=(100-$m_34->real_stock);  
        $m_34->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_34->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $m_34->save(); 

        $mov1m_34 = new Movement;
        $mov1m_34->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_34->comprobante='Carga inicial';         //Carga inicial
        $mov1m_34->number='';                   
        $mov1m_34->quantity=600;                        //incremento: +600
        $mov1m_34->stock=600;                           //total     : 600
        $mov1m_34->user_id=2;                           //jaime(JEFE)
        $mov1m_34->material_id=34;                       //Adaptador (H-H)
        $mov1m_34->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_34->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_34->save(); 

        $m_35=new Material();
        $m_35->description='Sellador de 100 ml';
        $m_35->photo='SELL001.png';
        $m_35->stock=500;
        $m_35->smin=200;
        $m_35->smax=1500;
        $m_35->category_id=7;
        $m_35->bundle_quantity=100;
        $m_35->bundle_name='Caja de Sellador de 100 ml' . " (". $m_35->bundle_quantity . " " . $m_35->category->medida . ")";
        $m_35->real_stock=(($m_35->stock*100)/$m_35->smin);
        $m_35->missing_stock=(100-$m_35->real_stock);  
        $m_35->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_35->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM            
        $m_35->save(); 

        $mov1m_35 = new Movement;
        $mov1m_35->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_35->comprobante='Carga inicial';         //Carga inicial
        $mov1m_35->number='';                   
        $mov1m_35->quantity=500;                        //incremento: +500
        $mov1m_35->stock=500;                           //total     : 500
        $mov1m_35->user_id=2;                           //jaime(JEFE)
        $mov1m_35->material_id=35;                       //Adaptador (H-H)
        $mov1m_35->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_35->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_35->save(); 

        $m_36=new Material();
        $m_36->description='Tubo pasa pared blanco';
        $m_36->photo='TAR001.png';
        $m_36->stock=1250;
        $m_36->smin=500;
        $m_36->smax=3750;
        $m_36->category_id=8;
        $m_36->bundle_quantity=250;
        $m_36->bundle_name='Bolsa de Tubo pasa pared blanco' . " (". $m_36->bundle_quantity . " " . $m_36->category->medida . ")";
        $m_36->real_stock=(($m_36->stock*100)/$m_36->smin);
        $m_36->missing_stock=(100-$m_36->real_stock);     
        $m_36->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_36->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_36->save(); 

        $mov1m_36 = new Movement;
        $mov1m_36->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_36->comprobante='Carga inicial';         //Carga inicial
        $mov1m_36->number='';                   
        $mov1m_36->quantity=1250;                        //incremento: +1250
        $mov1m_36->stock=1250;                           //total     : 1250
        $mov1m_36->user_id=2;                           //jaime(JEFE)
        $mov1m_36->material_id=36;                       //Adaptador (H-H)
        $mov1m_36->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_36->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_36->save(); 

        $m_37=new Material();
        $m_37->description='Pitón 6mm con Tope';
        $m_37->photo='PIT001.png';
        $m_37->stock=1250;
        $m_37->smin=500;
        $m_37->smax=3750;
        $m_37->category_id=8;
        $m_37->bundle_quantity=250;
        $m_37->bundle_name='Bolsa de Pitón 6mm con Tope' . " (". $m_37->bundle_quantity . " " . $m_37->category->medida . ")";
        $m_37->real_stock=(($m_37->stock*100)/$m_37->smin);
        $m_37->missing_stock=(100-$m_37->real_stock);       
        $m_37->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_37->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM     
        $m_37->save();

        $mov1m_37 = new Movement;
        $mov1m_37->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_37->comprobante='Carga inicial';         //Carga inicial
        $mov1m_37->number='';                   
        $mov1m_37->quantity=1250;                        //incremento: +1250
        $mov1m_37->stock=1250;                           //total     : 1250
        $mov1m_37->user_id=2;                           //jaime(JEFE)
        $mov1m_37->material_id=37;                       //Adaptador (H-H)
        $mov1m_37->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_37->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_37->save(); 


        $m_38=new Material();
        $m_38->description='Pitón 8mm con Tope';
        $m_38->photo='PIT002.png';
        $m_38->stock=1250;
        $m_38->smin=500;
        $m_38->smax=3750;
        $m_38->category_id=8;
        $m_38->bundle_quantity=250;
        $m_38->bundle_name='Bolsa de Pitón 8mm con Tope' . " (". $m_38->bundle_quantity . " " . $m_38->category->medida . ")";
        $m_38->real_stock=(($m_38->stock*100)/$m_38->smin);
        $m_38->missing_stock=(100-$m_38->real_stock);       
        $m_38->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_38->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_38->save();

        $mov1m_38 = new Movement;
        $mov1m_38->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_38->comprobante='Carga inicial';         //Carga inicial
        $mov1m_38->number='';                   
        $mov1m_38->quantity=1250;                        //incremento: +1250
        $mov1m_38->stock=1250;                           //total     : 1250
        $mov1m_38->user_id=2;                           //jaime(JEFE)
        $mov1m_38->material_id=38;                       //Adaptador (H-H)
        $mov1m_38->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_38->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_38->save(); 


        $m_39=new Material();
        $m_39->description='Tarugo 6mm';
        $m_39->photo='TARPP002.png';
        $m_39->stock=500;
        $m_39->smin=200;
        $m_39->smax=1500;
        $m_39->category_id=8;
        $m_39->bundle_quantity=100;
        $m_39->bundle_name='Bolsa de Tarugo 6mm' . " (". $m_39->bundle_quantity . " " . $m_39->category->medida . ")";
        $m_39->real_stock=(($m_39->stock*100)/$m_39->smin);
        $m_39->missing_stock=(100-$m_39->real_stock);   
        $m_39->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_39->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_39->save();

        $mov1m_39 = new Movement;
        $mov1m_39->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_39->comprobante='Carga inicial';         //Carga inicial
        $mov1m_39->number='';                   
        $mov1m_39->quantity=500;                        //incremento: +500
        $mov1m_39->stock=500;                           //total     : 500
        $mov1m_39->user_id=2;                           //jaime(JEFE)
        $mov1m_39->material_id=39;                       //Adaptador (H-H)
        $mov1m_39->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_39->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_39->save(); 

        $m_40=new Material();
        $m_40->description='Tarugo 8mm';
        $m_40->photo='TARPP003.png';
        $m_40->stock=1250;
        $m_40->smin=500;
        $m_40->smax=3750;
        $m_40->category_id=8;
        $m_40->bundle_quantity=250;
        $m_40->bundle_name='Bolsa de Tarugo 8mm' . " (". $m_40->bundle_quantity . " " . $m_40->category->medida . ")";
        $m_40->real_stock=(($m_40->stock*100)/$m_40->smin);
        $m_40->missing_stock=(100-$m_40->real_stock);       
        $m_40->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_40->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_40->save();

        $mov1m_40 = new Movement;
        $mov1m_40->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_40->comprobante='Carga inicial';         //Carga inicial
        $mov1m_40->number='';                   
        $mov1m_40->quantity=1250;                        //incremento: +1250
        $mov1m_40->stock=1250;                           //total     : 1250
        $mov1m_40->user_id=2;                           //jaime(JEFE)
        $mov1m_40->material_id=40;                       //Adaptador (H-H)
        $mov1m_40->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_40->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_40->save(); 

        $m_41=new Material();
        $m_41->description='Nomeclador numero 1';
        $m_41->photo='NOMEC002.png';
        $m_41->stock=600;
        $m_41->smin=240;
        $m_41->smax=1800;
        $m_41->category_id=9;
        $m_41->bundle_quantity=120;
        $m_41->bundle_name='Bolsa de Nomeclador numero 1' . " (". $m_41->bundle_quantity . " " . $m_41->category->medida . ")";
        $m_41->real_stock=(($m_41->stock*100)/$m_41->smin);
        $m_41->missing_stock=(100-$m_41->real_stock);       
        $m_41->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_41->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_41->save();

        $mov1m_41 = new Movement;
        $mov1m_41->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_41->comprobante='Carga inicial';         //Carga inicial
        $mov1m_41->number='';                   
        $mov1m_41->quantity=600;                        //incremento: +600
        $mov1m_41->stock=600;                           //total     : 600
        $mov1m_41->user_id=2;                           //jaime(JEFE)
        $mov1m_41->material_id=41;                       //Adaptador (H-H)
        $mov1m_41->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_41->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_41->save(); 

        $m_42=new Material();
        $m_42->description='Nomeclador numero 2';
        $m_42->photo='NOMEC003.png';
        $m_42->stock=600;
        $m_42->smin=240;
        $m_42->smax=1800;
        $m_42->category_id=9;
        $m_42->bundle_quantity=120;
        $m_42->bundle_name='Bolsa de Nomeclador numero 2' . " (". $m_42->bundle_quantity . " " . $m_42->category->medida . ")";
        $m_42->real_stock=(($m_42->stock*100)/$m_42->smin);
        $m_42->missing_stock=(100-$m_42->real_stock);       
        $m_42->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_42->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_42->save();

        $mov1m_42 = new Movement;
        $mov1m_42->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_42->comprobante='Carga inicial';         //Carga inicial
        $mov1m_42->number='';                   
        $mov1m_42->quantity=600;                        //incremento: +600
        $mov1m_42->stock=600;                           //total     : 600
        $mov1m_42->user_id=2;                           //jaime(JEFE)
        $mov1m_42->material_id=42;                       //Adaptador (H-H)
        $mov1m_42->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_42->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_42->save(); 

        $m_43=new Material();
        $m_43->description='Nomeclador numero 3';
        $m_43->photo='NOMEC004.png';
        $m_43->stock=600;
        $m_43->smin=240;
        $m_43->smax=1800;
        $m_43->category_id=9;
        $m_43->bundle_quantity=120;
        $m_43->bundle_name='Bolsa de Nomeclador numero 3' . " (". $m_43->bundle_quantity . " " . $m_43->category->medida . ")";
        $m_43->real_stock=(($m_43->stock*100)/$m_43->smin);
        $m_43->missing_stock=(100-$m_43->real_stock);       
        $m_43->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_43->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_43->save();

        $mov1m_43 = new Movement;
        $mov1m_43->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_43->comprobante='Carga inicial';         //Carga inicial
        $mov1m_43->number='';                   
        $mov1m_43->quantity=600;                        //incremento: +600
        $mov1m_43->stock=600;                           //total     : 600
        $mov1m_43->user_id=2;                           //jaime(JEFE)
        $mov1m_43->material_id=43;                       //Adaptador (H-H)
        $mov1m_43->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_43->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_43->save(); 


        $m_44=new Material();
        $m_44->description='Nomeclador numero 4';
        $m_44->photo='NOMEC005.png';
        $m_44->stock=600;
        $m_44->smin=240;
        $m_44->smax=1800;
        $m_44->category_id=9;
        $m_44->bundle_quantity=120;
        $m_44->bundle_name='Bolsa de Nomeclador numero 4' . " (". $m_44->bundle_quantity . " " . $m_44->category->medida . ")";
        $m_44->real_stock=(($m_44->stock*100)/$m_44->smin);
        $m_44->missing_stock=(100-$m_44->real_stock);  
        $m_44->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_44->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM         
        $m_44->save();

        $mov1m_44 = new Movement;
        $mov1m_44->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_44->comprobante='Carga inicial';         //Carga inicial
        $mov1m_44->number='';                   
        $mov1m_44->quantity=600;                        //incremento: +600
        $mov1m_44->stock=600;                           //total     : 600
        $mov1m_44->user_id=2;                           //jaime(JEFE)
        $mov1m_44->material_id=44;                       //Adaptador (H-H)
        $mov1m_44->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_44->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_44->save(); 

        $m_45=new Material();
        $m_45->description='Nomeclador numero 5';
        $m_45->photo='NOMEC006.png';
        $m_45->stock=600;
        $m_45->smin=240;
        $m_45->smax=1800;
        $m_45->category_id=9;
        $m_45->bundle_quantity=120;
        $m_45->bundle_name='Bolsa de Nomeclador numero 5' . " (". $m_45->bundle_quantity . " " . $m_45->category->medida . ")";
        $m_45->real_stock=(($m_45->stock*100)/$m_45->smin);
        $m_45->missing_stock=(100-$m_45->real_stock);       
        $m_45->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_45->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_45->save();

        $mov1m_45 = new Movement;
        $mov1m_45->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_45->comprobante='Carga inicial';         //Carga inicial
        $mov1m_45->number='';                   
        $mov1m_45->quantity=600;                        //incremento: +600
        $mov1m_45->stock=600;                           //total     : 600
        $mov1m_45->user_id=2;                           //jaime(JEFE)
        $mov1m_45->material_id=45;                       //Adaptador (H-H)
        $mov1m_45->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_45->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_45->save(); 

        $m_46=new Material();
        $m_46->description='Nomeclador numero 6';
        $m_46->photo='NOMEC007.png';
        $m_46->stock=600;
        $m_46->smin=240;
        $m_46->smax=1800;
        $m_46->category_id=9;
        $m_46->bundle_quantity=120;
        $m_46->bundle_name='Bolsa de Nomeclador numero 6' . " (". $m_46->bundle_quantity . " " . $m_46->category->medida . ")";
        $m_46->real_stock=(($m_46->stock*100)/$m_46->smin);
        $m_46->missing_stock=(100-$m_46->real_stock);       
        $m_46->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_46->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_46->save();

        $mov1m_46 = new Movement;
        $mov1m_46->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_46->comprobante='Carga inicial';         //Carga inicial
        $mov1m_46->number='';                   
        $mov1m_46->quantity=600;                        //incremento: +600
        $mov1m_46->stock=600;                           //total     : 600
        $mov1m_46->user_id=2;                           //jaime(JEFE)
        $mov1m_46->material_id=46;                       //Adaptador (H-H)
        $mov1m_46->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_46->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_46->save(); 

        $m_47=new Material();
        $m_47->description='Nomeclador numero 7';
        $m_47->photo='NOMEC008.png';
        $m_47->stock=600;
        $m_47->smin=240;
        $m_47->smax=1800;
        $m_47->category_id=9;
        $m_47->bundle_quantity=120;
        $m_47->bundle_name='Bolsa de Nomecladores numero 7' . " (". $m_47->bundle_quantity . " " . $m_47->category->medida . ")";
        $m_47->real_stock=(($m_47->stock*100)/$m_47->smin);
        $m_47->missing_stock=(100-$m_47->real_stock);       
        $m_47->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_47->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_47->save();

        $mov1m_47 = new Movement;
        $mov1m_47->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_47->comprobante='Carga inicial';         //Carga inicial
        $mov1m_47->number='';                   
        $mov1m_47->quantity=600;                        //incremento: +600
        $mov1m_47->stock=600;                           //total     : 600
        $mov1m_47->user_id=2;                           //jaime(JEFE)
        $mov1m_47->material_id=47;                       //Adaptador (H-H)
        $mov1m_47->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_47->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_47->save(); 

        $m_48=new Material();
        $m_48->description='Nomeclador numero 8';
        $m_48->photo='NOMEC009.png';
        $m_48->stock=600;
        $m_48->smin=240;
        $m_48->smax=1800;
        $m_48->category_id=9;
        $m_48->bundle_quantity=120;
        $m_48->bundle_name='Bolsa de Nomeclador numero 8' . " (". $m_48->bundle_quantity . " " . $m_48->category->medida . ")";
        $m_48->real_stock=(($m_48->stock*100)/$m_48->smin);
        $m_48->missing_stock=(100-$m_48->real_stock);       
        $m_48->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_48->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_48->save();

        $mov1m_48 = new Movement;
        $mov1m_48->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_48->comprobante='Carga inicial';         //Carga inicial
        $mov1m_48->number='';                   
        $mov1m_48->quantity=600;                        //incremento: +600
        $mov1m_48->stock=600;                           //total     : 600
        $mov1m_48->user_id=2;                           //jaime(JEFE)
        $mov1m_48->material_id=48;                       //Adaptador (H-H)
        $mov1m_48->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_48->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_48->save(); 

        $m_49=new Material();
        $m_49->description='Nomeclador numero 9';
        $m_49->photo='NOMEC010.png';
        $m_49->stock=600;
        $m_49->smin=240;
        $m_49->smax=1800;
        $m_49->category_id=9;
        $m_49->bundle_quantity=120;
        $m_49->bundle_name='Bolsa de Nomeclador numero 9' . " (". $m_49->bundle_quantity . " " . $m_49->category->medida . ")";
        $m_49->real_stock=(($m_49->stock*100)/$m_49->smin);
        $m_49->missing_stock=(100-$m_49->real_stock);       
        $m_49->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_49->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM   
        $m_49->save();

        $mov1m_49 = new Movement;
        $mov1m_49->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_49->comprobante='Carga inicial';         //Carga inicial
        $mov1m_49->number='';                   
        $mov1m_49->quantity=600;                        //incremento: +600
        $mov1m_49->stock=600;                           //total     : 600
        $mov1m_49->user_id=2;                           //jaime(JEFE)
        $mov1m_49->material_id=49;                       //Adaptador (H-H)
        $mov1m_49->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_49->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_49->save(); 


        $m_50=new Material();
        $m_50->description='Nomeclador numero 0';
        $m_50->photo='NOMEC001.png';
        $m_50->stock=600;
        $m_50->smin=240;
        $m_50->smax=1800;
        $m_50->category_id=9;
        $m_50->bundle_quantity=120;
        $m_50->bundle_name='Bolsa de Nomeclador numero 0' . " (". $m_50->bundle_quantity . " " . $m_50->category->medida . ")";
        $m_50->real_stock=(($m_50->stock*100)/$m_50->smin);
        $m_50->missing_stock=(100-$m_50->real_stock);      
        $m_50->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_50->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_50->save();

        $mov1m_50 = new Movement;
        $mov1m_50->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_50->comprobante='Carga inicial';         //Carga inicial
        $mov1m_50->number='';                   
        $mov1m_50->quantity=600;                        //incremento: +600
        $mov1m_50->stock=600;                           //total     : 600
        $mov1m_50->user_id=2;                           //jaime(JEFE)
        $mov1m_50->material_id=50;                       //Adaptador (H-H)
        $mov1m_50->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_50->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_50->save(); 

        $m_51=new Material();
        $m_51->description='Letra A';
        $m_51->photo='NOMEC011.png';
        $m_51->stock=600;
        $m_51->smin=240;
        $m_51->smax=1800;
        $m_51->category_id=9;
        $m_51->bundle_quantity=120;
        $m_51->bundle_name='Bolsa de Letra A' . " (". $m_51->bundle_quantity . " " . $m_51->category->medida . ")";
        $m_51->real_stock=(($m_51->stock*100)/$m_51->smin);
        $m_51->missing_stock=(100-$m_51->real_stock);       
        $m_51->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_51->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM      
        $m_51->save();

        $mov1m_51 = new Movement;
        $mov1m_51->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_51->comprobante='Carga inicial';         //Carga inicial
        $mov1m_51->number='';                   
        $mov1m_51->quantity=600;                        //incremento: +600
        $mov1m_51->stock=600;                           //total     : 600
        $mov1m_51->user_id=2;                           //jaime(JEFE)
        $mov1m_51->material_id=51;                       //Adaptador (H-H)
        $mov1m_51->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_51->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_51->save(); 

        $m_52=new Material();
        $m_52->description='Letra C';
        $m_52->photo='NOMEC012.png';
        $m_52->stock=600;
        $m_52->smin=240;
        $m_52->smax=1800;
        $m_52->category_id=9;
        $m_52->bundle_quantity=120;
        $m_52->bundle_name='Bolsa de Letras C' . " (". $m_52->bundle_quantity . " " . $m_52->category->medida . ")";
        $m_52->real_stock=(($m_52->stock*100)/$m_52->smin);
        $m_52->missing_stock=(100-$m_52->real_stock);       
        $m_52->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_52->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_52->save();

        $mov1m_52 = new Movement;
        $mov1m_52->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_52->comprobante='Carga inicial';         //Carga inicial
        $mov1m_52->number='';                   
        $mov1m_52->quantity=600;                        //incremento: +600
        $mov1m_52->stock=600;                           //total     : 600
        $mov1m_52->user_id=2;                           //jaime(JEFE)
        $mov1m_52->material_id=52;                       //Adaptador (H-H)
        $mov1m_52->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_52->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_52->save(); 

        $m_53=new Material();
        $m_53->description='Letra D';
        $m_53->photo='NOMEC013.png';
        $m_53->stock=600;
        $m_53->smin=240;
        $m_53->smax=1800;
        $m_53->category_id=9;
        $m_53->bundle_quantity=120;
        $m_53->bundle_name='Bolsa de Letras D' . " (". $m_53->bundle_quantity . " " . $m_53->category->medida . ")";
        $m_53->real_stock=(($m_53->stock*100)/$m_53->smin);
        $m_53->missing_stock=(100-$m_53->real_stock);      
        $m_53->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_53->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM  
        $m_53->save();

        $mov1m_53 = new Movement;
        $mov1m_53->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_53->comprobante='Carga inicial';         //Carga inicial
        $mov1m_53->number='';                   
        $mov1m_53->quantity=600;                        //incremento: +600
        $mov1m_53->stock=600;                           //total     : 600
        $mov1m_53->user_id=2;                           //jaime(JEFE)
        $mov1m_53->material_id=53;                       //Adaptador (H-H)
        $mov1m_53->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_53->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_53->save(); 


        $m_54=new Material();
        $m_54->description='Letra E';
        $m_54->photo='NOMEC014.png';
        $m_54->stock=600;
        $m_54->smin=240;
        $m_54->smax=1800;
        $m_54->category_id=9;
        $m_54->bundle_quantity=120;
        $m_54->bundle_name='Bolsa de Letras E' . " (". $m_54->bundle_quantity . " " . $m_54->category->medida . ")";
        $m_54->real_stock=(($m_54->stock*100)/$m_54->smin);
        $m_54->missing_stock=(100-$m_54->real_stock);     
        $m_54->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $m_54->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM    
        $m_54->save();

        $mov1m_54 = new Movement;
        $mov1m_54->date='2020-01-06';                   //Fecha: 06/01/2020
        $mov1m_54->comprobante='Carga inicial';         //Carga inicial
        $mov1m_54->number='';                   
        $mov1m_54->quantity=600;                        //incremento: +600
        $mov1m_54->stock=600;                           //total     : 600
        $mov1m_54->user_id=2;                           //jaime(JEFE)
        $mov1m_54->material_id=54;                       //Adaptador (H-H)
        $mov1m_54->created_at='2020-01-06 08:22:53';    //creacion: 01/01/2020 8AM   
        $mov1m_54->updated_at='2020-01-06 08:22:53';    //actualizacion: 01/01/2020 8AM          
        $mov1m_54->save(); 
    
    }

    
}
