<?php

use Illuminate\Database\Seeder;
use siscontratista\Workflow;


class WorkFlowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $workflow = new Workflow();
        $workflow->name='Ordenes de Trabajo';
        $workflow->description='Recorrido para Ordenes de Trabajo';
        $workflow->initial_stage_id=1;

        $workflow->save();
    }
}
