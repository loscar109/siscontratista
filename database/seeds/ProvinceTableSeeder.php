<?php

use Illuminate\Database\Seeder;
use siscontratista\Province;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
        {
            $prov_Arg = array(
                "Misiones",
                "Corrientes",
                "Entre Rios",
                "Chaco",
                "Formosa",
                "Santa Fe",
                "Jujuy",
                "Salta",
                "Tucuman",
                "Santiago del Estero",
                "Catamarca",
                "La Rioja",
                "Cordoba",
                "San Juan",
                "San Luis",
                "Mendoza",
                "La Pampa",
                "Buenos Aires",
                "Neuquen",
                "Rio Negro",
                "Chubut",
                "Santa Cruz",
                "Tierra del Fuego"
            );
            for ($i=0; $i < 23; $i++) { 
                Province::create([
                    'name'=>$prov_Arg[$i],
                    'country_id'=>1]);
            }
        }
}

