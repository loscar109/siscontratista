<?php

use Illuminate\Database\Seeder;
use siscontratista\MessagePhase;


class MessagePhasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*     messages                                                 message_states
        message_id| subject                             message_state_id    |   description
            1     | Google no realiza ninguna busqueda               1      |   Creado
            2     | Se realiza un ping                               2      |   Enviado
            3     | El modem no enciende todas las luces             3      |   Revisado
                                                                     4      |   Leido
                                                                     5      |   Fallido
                                                                    ...     |   ...
        */

        MessagePhase::create([
            'date'               => '2019-06-01',
            'message_id'         => 1, //Google no realiza ninguna busqueda
            'message_state_id'   => 1,  //Enviado
            'user_id'            => 2
        ]);

        MessagePhase::create([
            'date'               => '2019-06-01',
            'message_id'         => 1, //Google no realiza ninguna busqueda
            'message_state_id'   => 2,  //Leido
            'user_id'            => 3
        ]);


    }
}
