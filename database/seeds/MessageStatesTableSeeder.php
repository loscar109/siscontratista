<?php

use Illuminate\Database\Seeder;
use siscontratista\MessageState;


class MessageStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       MessageState::create([
            'name'         =>'Enviado',
            'description'  =>'Estado que confirma el envio de un mensaje'
        ]);

        MessageState::create([
            'name'         =>'Leido',
            'description'  =>'Estado que confirma la lectura de un mensaje por parte del receptor'
        ]);


    }
}
