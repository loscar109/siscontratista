<?php

use Illuminate\Database\Seeder;
use siscontratista\TicketStage;

class TicketStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = array(
            "Creado",
            "Revisado",
            "En proceso",
            "Resuelto",
            "No Resuelto",
            "Reasignado"
        );


        $description = array(
            "Creacion de un Ticket",
            "Cuando se 've' el Ticket por primera vez",
            "Se está atendiendo (realizando) el Ticket",
            "Resultado Exitoso de un Ticket",
            "Resultado Fallido de un Ticket",
            "Si no se resuelve el Ticket, esta pasa al estado reasignado"
        );

 


        for ($i=0; $i < count($names); $i++) {
            TicketStage::create([
                'name'=>$names[$i],
                'description'=>$description[$i]
            ]);
        }

    }
}
