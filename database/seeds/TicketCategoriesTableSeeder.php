<?php

use Illuminate\Database\Seeder;
use siscontratista\TicketCategory;

class TicketCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        TicketCategory::create([
            'name'          =>'Service',
            'description'   =>'Tipo de ticket cuya finalidad es intentar reparar, arreglar el desperfecto',
            'workflow_id'   =>1
        ]);

        TicketCategory::Create([
            'name'          =>'Desconexión',
            'description'   =>'Tipo de ticket que consiste en eliminar, dar de baja el servicio',
            'workflow_id'   =>1

        ]);

        TicketCategory::create([
            'name'          =>'Re-conexión',
            'description'   =>'Tipo de ticket que consiste en realizar la re-conexión del servicio solicitado',
            'workflow_id'   =>1

        ]);

        TicketCategory::create([
            'name'          =>'Instalacion',
            'description'   =>'Tipo de ticket que consiste en instalar, dar de alta el servicio',
            'workflow_id'   =>1

        ]);



    }
}
