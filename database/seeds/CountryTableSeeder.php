<?php

use Illuminate\Database\Seeder;
use siscontratista\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create(['name'=>'Argentina']);
    
    }
}
