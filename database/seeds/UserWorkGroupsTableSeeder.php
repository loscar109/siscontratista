<?php

use Illuminate\Database\Seeder;
use siscontratista\UserWorkGroup;
use siscontratista\User;


class UserWorkGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
            Workgroup                           User
            id  name                            id  name

            1   Equipo SERV0001                 4   LUCAS MIGUEL
                                                5   MARIO RUBEN
           
            2   Equipo SERV0002                 6   CARLOS
                                                7   MATIAS

            3   Equipo DESC0001                 13  TOMAS
                                                14  AUGUSTO
                                                15  LUCAS

            4   Equipo DESC0002                 16  FRANCISCO JAVIER
                                                17  JULIO DAVID

            5   Equipo RXIN0001                 18  JORGE RUBEN

            6   Equipo RXIN0002                 19  SAUL

            7   Equipo INST0001                 20  NAHUEL
                                                21  MARIANO
                                                22  ESTEBAN

            8   Equipo INST0002                 23  GERMAN
                                                
        */

        $user_work_group_1_1=new UserWorkGroup();
        $user_work_group_1_1->user_id=4;          // 4 LUCAS MIGUEL
        $user_work_group_1_1->workgroup_id=1;     // Equipo SERV0001
        $user_work_group_1_1->save();
        User::find(4)->update(['assigned'=>true]);
             
        
        $user_work_group_1_2=new UserWorkGroup();
        $user_work_group_1_2->user_id=5;           // 5 MARIO RUBEN
        $user_work_group_1_2->workgroup_id=1;      // Equipo SERV0001
        $user_work_group_1_2->save();
        User::find(5)->update(['assigned'=>true]);

        $user_work_group_2_1=new UserWorkGroup();
        $user_work_group_2_1->user_id=6;            // 6 CARLOS
        $user_work_group_2_1->workgroup_id=2;       // Equipo SERV0002
        $user_work_group_2_1->save();
        User::find(6)->update(['assigned'=>true]);

        $user_work_group_3=new UserWorkGroup();
        $user_work_group_3->user_id=7;              // 7 MATIAS
        $user_work_group_3->workgroup_id=2;         // Equipo SERV0002
        $user_work_group_3->save();
        User::find(7)->update(['assigned'=>true]);

        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=13;             // 13 TOMAS
        $user_work_group_4->workgroup_id=3;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(13)->update(['assigned'=>true]);

        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=14;             // 14 AUGUSTO
        $user_work_group_4->workgroup_id=3;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(14)->update(['assigned'=>true]);

        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=15;             // 15 LUCAS
        $user_work_group_4->workgroup_id=3;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(15)->update(['assigned'=>true]);


        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=16;             // 16 FRANCISCO JAVIER
        $user_work_group_4->workgroup_id=4;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(16)->update(['assigned'=>true]);


        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=17;             // 17 JULIO DAVID
        $user_work_group_4->workgroup_id=4;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(17)->update(['assigned'=>true]);
        

        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=18;             // 18  JORGE RUBEN
        $user_work_group_4->workgroup_id=5;         // Equipo RXIN0001
        $user_work_group_4->save();
        User::find(18)->update(['assigned'=>true]);
        
        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=19;             // 19  SAUL
        $user_work_group_4->workgroup_id=6;         // Equipo RXIN0002
        $user_work_group_4->save();
        User::find(19)->update(['assigned'=>true]);
        
        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=20;             // 20  NAHUEL
        $user_work_group_4->workgroup_id=7;         // Equipo INST0001
        $user_work_group_4->save();
        User::find(20)->update(['assigned'=>true]);
        
        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=21;             // 21  MARIANO
        $user_work_group_4->workgroup_id=7;         // Equipo INST0001
        $user_work_group_4->save();
        User::find(21)->update(['assigned'=>true]);
        
        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=22;             // 22  ESTEBAN
        $user_work_group_4->workgroup_id=7;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(22)->update(['assigned'=>true]);
        
        $user_work_group_4=new UserWorkGroup();
        $user_work_group_4->user_id=23;             // 23  GERMAN
        $user_work_group_4->workgroup_id=8;         // Equipo DESC0001
        $user_work_group_4->save();
        User::find(23)->update(['assigned'=>true]);
      
        
    }
}
