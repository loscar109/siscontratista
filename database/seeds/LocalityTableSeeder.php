<?php

use Illuminate\Database\Seeder;
use siscontratista\Locality;

class LocalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dep_Arg = array(
            "Posadas",
            "Iguazú",
            "Gral. Manuel Belgrano",
            "Eldorado",
            "San Pedro",
            "Montecarlo",
            "Guaraní",
            "San Martin",
            "Cainguás",
            "25 de Mayo",
            "San Ignacio",
            "Oberá",
            "Candelaria",
            "Leandro N. Alem",
            "San Javier",
            "Apóstoles",
            "Concepción"
        );
        for ($i=0; $i < 17; $i++) { 
            City::create([
                'name'=>$dep_Arg[$i],
                'province_id'=>1]);
        }
    }
}
