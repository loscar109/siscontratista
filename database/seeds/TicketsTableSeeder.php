<?php

use Illuminate\Database\Seeder;
use siscontratista\Ticket;
use siscontratista\StageHistory;
use siscontratista\Consumption;
use siscontratista\Movement;


class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          /*
            Workgroup                           Empleado                         Cliente
            id  name                            id  name                        id  name

            1   Equipo SERV0001                 4   LUCAS MIGUEL                8   MARIA
                                                5   MARIO RUBEN                 9   JOSE ANDREA"                 
                                                                                24  SOLANG NUÑEZ
            2   Equipo SERV0002                 6   CARLOS                      25  MATIAS
                                                7   MATIAS                      26  ROXXANA
                                                                                27  AGUSTIN
            3   Equipo DESC0001                 13  TOMAS                       28  MARIA
                                                14  AUGUSTO                     29  JOSE
                                                15  LUCAS                       30  JULIO
                                                                      
            4   Equipo DESC0002                 16  FRANCISCO JAVIER
                                                17  JULIO DAVID

            5   Equipo RXIN0001                 18  JORGE RUBEN

            6   Equipo RXIN0002                 19  SAUL

            7   Equipo INST0001                 20  NAHUEL
                                                21  MARIANO
                                                22  ESTEBAN

            8   Equipo INST0002                 23  GERMAN
                                                
        */


        //--------------------------Equipo SERV0001 TICKET 1------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000001',
            'description'           =>  'Nodo con falla eléctrica',
            'created_at'            =>  '2020-02-01 07:20:18',  // Creado el 01 de Feb a las 7 AM
            'updated_at'            =>  '2020-02-01 17:40:32',  // Ultima actualización el 01 de Feb a las 5 PM
            'time_resolved'         =>  1,                      // 1 dia en terminar el ticket
            'ticket_category_id'    =>  1,                      // Service,
            'current_stage_id'      =>  4,                      // Resuelto
            'date'                  =>  '2020-02-01',           // 01/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                      // Equipo SERV0001 	
            'priority'              =>  5,                      // Prioridad alta
            'client_id'             =>  8,                      // Cliente MARIA
            'detail'                =>  'Cayo una antena sobre la ruta y dependiendo del viento
                                         a veces corta la luz y con ello el servicio, verificar 
                                         si el problema es del servicio',
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-01 07:20:18',  // Creado el 01 de Feb a las 7 AM
            'ticket_id'             =>  1,                      // Nodo con falla eléctrica
            'ticket_stage_id'       =>  1,                      // Creado
            'user_id'               =>  1                       // Administrador Carlos Villalba
        ]);


        StageHistory::create([
            'date'                  =>  '2020-02-01 09:15:50',    // Revisado el 01 de Feb a las 9 AM
            'ticket_id'             =>  1,                        // Nodo con falla eléctrica
            'ticket_stage_id'       =>  2,                        // Revisado
            'user_id'               =>  4                         // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-01 09:50:55',   // En Proceso el 01 de Feb a las 9 AM
            'ticket_id'             =>  1,                       // Nodo con falla eléctrica
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-01 17:40:32',   // REsuelto el 01 de Feb a las 4 PM
            'ticket_id'             =>  1,                       // Nodo con falla eléctrica
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);

      






        //--------------------------Equipo SERV0001 TICKET 2------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000002',
            'description'           =>  'No se ve YouTube',
            'created_at'            =>  '2020-02-03 07:16:44',   // Creado el 03 de Feb a las 7 AM
            'updated_at'            =>  '2020-02-06 18:40:32',   // Ultima actualización 06 de Feb a las 6 PM
            'time_resolved'         =>  4,                       // 4 dias en terminar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  4,                       // Resuelto
            'date'                  =>  '2020-02-03',   // 03/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  1,                       // prioridad baja
            'client_id'             =>  9,                       // Cliente JOSE ANDREA
            'detail'                =>  'Verificar si el problema es la conexión
                                         o el equipo del cliente',
        ]);


        StageHistory::create([
            'date'                  =>  '2020-02-03 07:16:44',   // Creado el 03 de Feb a las 7 AM
            'ticket_id'             =>  2,                       // No se ve YouTube
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-03 07:28:18',   // Revisado el 03 de Feb a las 7 AM
            'ticket_id'             =>  2,                       // No se ve YouTube
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-05 08:32:43',   // En Proceso el 05 de Feb a las 8 AM
            'ticket_id'             =>  2,                       // No se ve YouTube
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        
        StageHistory::create([
            'date'                  =>  '2020-02-06 18:40:32',   // Resuelto el 06 de Feb a las 6 PM
            'ticket_id'             =>  2,                       // No se ve YouTube
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);  

        //--------------------------Equipo SERV0001 TICKET 3------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000003',
            'description'           =>  'Problemas en visualización de imagen',
            'created_at'            =>  '2020-02-04 07:08:44',   // Creado el 04 de Feb a las 7 AM
            'updated_at'            =>  '2020-02-07 18:40:32',   // Ultima actualización el 10/11/2019 a las 6 PM
            'time_resolved'         =>  4,                       // 3 dias en terminar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  4,                       // Resuelto
            'date'                  =>  '2020-02-04',   // 03/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  2,                       // prioridad baja
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Verificar si el decodificador esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-04 07:08:44',   // Creado el 03 de Feb a las 7 AM
            'ticket_id'             =>  3,                       // Problemas en visualización de imagen
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-04 08:21:15',   // Revisado el 04 de Feb a las 8 AM
            'ticket_id'             =>  3,                       // Problemas en visualización de imagen
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-05 08:21:15',   // En Proceso el 04 de Feb a las 8 AM
            'ticket_id'             =>  3,                       // Problemas en visualización de imagen
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);

        StageHistory::create([
            'date'                  =>  '2020-02-07 18:40:32',   // Resuelto el 06 de Feb a las 6 PM
            'ticket_id'             =>  3,                       // Problemas en visualización de imagen
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);
        //--------------------------Equipo SERV0001 TICKET 4------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000004',
            'description'           =>  'No enciende las luces del modem',
            'created_at'            =>  '2020-02-04 07:08:44',   // Creado el 04 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-07 18:40:37',   // Ultima actualización el 06 de Feb las 6 PM
            'time_resolved'         =>  4,                       // 7 dias en terminar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  4,                       // Resuelto
            'date'                  =>  '2020-02-04',   // 06/11/2019 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  3,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Comprobar si el equipo esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-04 07:08:44',   // Creado el 04 de Feb a las 7 AM
            'ticket_id'             =>  4,                       // No enciende las luces del modem
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-04 08:21:41',   // Revisado el 04 de Feb a las 8 AM
            'ticket_id'             =>  4,                       // No enciende las luces del modem
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  4                        // Empleado: LUCAS MIGUEL del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-06 07:08:44',   // En Proceso el 06 de Feb a las 7 AM
            'ticket_id'             =>  4,                       // No enciende las luces del modem
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 18:40:37',   // Resuelto el 06 de Feb a las 6 PM
            'ticket_id'             =>  4,                       // Problemas en visualización de imagen
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        //--------------------------Equipo SERV0001 TICKET 5------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000005',
            'description'           =>  'Enciende solo una luz del modem',
            'created_at'            =>  '2020-02-06 07:12:21',   // Creado el 06 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-08 17:22:18',   // Ultima actualización el 08 de Feb a las 5 PM
            'time_resolved'         =>  3,                       // 3 diaS en terminar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  4,                       // Resuelto
            'date'                  =>  '2020-02-06',   // 06/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  3,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Comprobar si el equipo esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-06 07:12:21',   // Creado el 06 de Feb a las 7 AM
            'ticket_id'             =>  5,                       // Enciende solo una luz del modem
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 08:55:18',   // Revisado el 07 de Feb a las 8 AM
            'ticket_id'             =>  5,                       // Enciende solo una luz del modem
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-08 14:51:11',   // En Proceso el 08 de Feb las 2 PM
            'ticket_id'             =>  5,                       // Enciende solo una luz del modem
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-08 17:22:18',   // Resuelto el 08 de Feb a las 5 PM
            'ticket_id'             =>  5,                       // Enciende solo una luz del modem
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        //--------------------------Equipo SERV0001 TICKET 6------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000006',
            'description'           =>  'Fallas en el DecoFlow',
            'created_at'            =>  '2020-02-07 07:12:21',   // Creado el 07 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-07 18:22:18',   // Ultima actualización el 07 de Feb a las 6 PM
            'time_not_resolved'     =>  1,                       // 1 dia en no finalizar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  5,                       // No Resuelto
            'date'                  =>  '2020-02-07',   // 06/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  3,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Comprobar si el equipo esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 07:12:21',   // Creado el 07 de Feb a las 7 AM
            'ticket_id'             =>  6,                       // Fallas en el DecoFlow
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 08:55:18',   // Revisado el 07 de Feb a las 8 AM
            'ticket_id'             =>  6,                       // Fallas en el DecoFlow
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 09:51:11',   // En Proceso el 08 de Feb las 9 AM
            'ticket_id'             =>  6,                       // Fallas en el DecoFlow
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 18:22:18',   // No Resuelto el 07 de Feb a las 6 PM
            'ticket_id'             =>  6,                       // Fallas en el DecoFlow
            'ticket_stage_id'       =>  5,                       // No Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);

        //--------------------------Equipo SERV0001 TICKET 7------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000007',
            'description'           =>  'Imagen distorcionada en la TV',
            'created_at'            =>  '2020-02-07 07:12:21',   // Creado el 07 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-08 18:22:18',   // Ultima actualización el 08 de Feb a las 6 PM
            'time_not_resolved'     =>  2,                       // 2 dias en no finalizar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  5,                       // No Resuelto
            'date'                  =>  '2020-02-07',   // 07/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  3,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Cambiar el equipo si esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 07:12:21',   // Creado el 07 de Feb a las 7 AM
            'ticket_id'             =>  7,                       // Imagen distorcionada en la TV
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-07 08:55:18',   // Revisado el 07 de Feb a las 8 AM
            'ticket_id'             =>  7,                       // Imagen distorcionada en la TV
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-08 09:51:11',   // En Proceso el 08 de Feb las 9 AM
            'ticket_id'             =>  7,                       // Imagen distorcionada en la TV
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-08 18:22:18',   // No Resuelto el 08 de Feb a las 6 PM
            'ticket_id'             =>  7,                       // Imagen distorcionada en la TV
            'ticket_stage_id'       =>  5,                       // No Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        

     
      


//--------------------------Equipo SERV0001 TICKET 8------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000008',
            'description'           =>  'Problema en la configuracion del Router',
            'created_at'            =>  '2020-02-10 07:12:21',   // Creado el 10 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-10 18:22:18',   // Ultima actualización el 08 de Feb a las 6 PM
            'time_resolved'         =>  1,                       // 2 dias en no finalizar el ticket
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  4,                       // Resuelto
            'date'                  =>  '2020-02-10',   // 07/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  3,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Comprobar ultima actualizacion del modem (para saber si es de 50Mb o 100Mb)',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-10 07:12:21',   // Creado el 10 de Feb a las 7 AM
            'ticket_id'             =>  8,                       // Problema en la configuracion del Router
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-10 08:55:18',   // Revisado el 07 de Feb a las 8 AM
            'ticket_id'             =>  8,                       // Problema en la configuracion del Router
            'ticket_stage_id'       =>  2,                       // Revisado
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-10 09:51:11',   // En Proceso el 08 de Feb las 9 AM
            'ticket_id'             =>  8,                       // Problema en la configuracion del Router
            'ticket_stage_id'       =>  3,                       // En Proceso
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-10 18:22:18',   // Resuelto el 08 de Feb a las 6 PM
            'ticket_id'             =>  8,                       // Problema en la configuracion del Router
            'ticket_stage_id'       =>  4,                       // Resuelto
            'user_id'               =>  5                        // Empleado: MARIO RUBEN del grupo SERV0001
        ]);

   //--------------------------Equipo SERV0001 TICKET 9------------------------------------------------   
        Ticket::create([
            'number'                =>  '0000000009',
            'description'           =>  'Problema en el numero de MAC(verificar)',
            'created_at'            =>  '2020-02-03 07:12:21',   // Creado el 10 de Feb a las 7 AM',
            'updated_at'            =>  '2020-02-03 07:12:21',   // Ultima actualización el 08 de Feb a las 6 PM
            'ticket_category_id'    =>  1,                       // Service
            'current_stage_id'      =>  1,                       // Resuelto
            'date'                  =>  '2020-02-03',   // 07/02/2020 a las 7 AM
            'workgroup_id'          =>  1,                       // Equipo SERV0001 
            'priority'              =>  1,                       // prioridad media
            'client_id'             =>  9,                       // Cliente Jose Andrea
            'detail'                =>  'Comprobar ultima actualizacion del modem (para saber si es de 50Mb o 100Mb)',
        ]);
        StageHistory::create([
            'date'                  =>  '2020-02-03 07:12:21',   // Creado el 10 de Feb a las 7 AM
            'ticket_id'             =>  9,                       // Problema en la configuracion del Router
            'ticket_stage_id'       =>  1,                       // Creado
            'user_id'               =>  1                        // Administrador Carlos Villalba
        ]);
 



         

        //--------------------------Equipo SERV0002 TICKET 1------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000010',
            'description'           =>  'Nodo con falla eléctrica',
            'created_at'            =>  '2019-12-06',
            'updated_at'            =>  '2019-12-06',

            'time_resolved'         =>  2, //3 dia en terminar el ticket
            'ticket_category_id'    =>  1,  //Service,
            'current_stage_id'      =>  4,  //Resuelto
            'date'                  =>  '2019-12-06',
            'workgroup_id'          =>  2,  //Equipo SERV0002 	
            'priority'              =>  5,  //prioridad alta
            'client_id'             =>  24,  //SOLANG NUÑEZ
            'detail'                =>  'Cada cierta hora los cables entrelazados no paran de lanzar chispas',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  4,
            'user_id'               =>  6
        ]);

        //--------------------------Equipo SERV0002 TICKET 2------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000011',
            'description'           =>  'No se visualiza ninguna pagina en internet',
            'created_at'            =>  '2019-12-06',
            'updated_at'            =>  '2019-12-06',

            'time_resolved'         =>  2, //3 dia en terminar el ticket
            'ticket_category_id'    =>  1, //Service
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-12-06',
            'workgroup_id'          =>  2,  //Equipo SERV0002 
            'priority'              =>  1, //prioridad baja
            'client_id'             =>  25, //Cliente MATIAS
            'detail'                =>  'Verificar si es problema del equipo o de la red',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  4,
            'user_id'               =>  7  //Empleado MATIAS

        ]);
        //--------------------------Equipo SERV0003 TICKET 3------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000012',
            'description'           =>  'Imagen distorcionada en televisor PHilip 42',
            'created_at'            =>  '2019-11-04',
            'time_resolved'         =>  3, //3 dia en terminar el ticket
            'ticket_category_id'    =>  1, //Service
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-04',
            'workgroup_id'          =>  2,  //Equipo SERV0002 
            'priority'              =>  2, //prioridad baja
            'client_id'             =>  24,  //SOLANG NUÑEZ
            'detail'                =>  'Verificar si el problema es el equipo o de la red',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  6  //Empleado CARLOS

        ]);
        //--------------------------Equipo SERV0004 TICKET 4------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000013',
            'description'           =>  'Las luces del decodificar se apagan y no se ve los canales premium',
            'created_at'            =>  '2019-11-03',
            'time_resolved'         =>  4, //3 dia en terminar el ticket
            'ticket_category_id'    =>  1, //Service
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-03',
            'workgroup_id'          =>  2,  //Equipo SERV0002 
            'priority'              =>  3, //prioridad media
            'client_id'             =>  25, //Cliente MATIAS
            'detail'                =>  'Comprobar si el equipo esta en corto',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  7  //Empleado MATIAS
        ]);
        //--------------------------Equipo SERV0002 TICKET 5------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000014',
            'description'           =>  'El modem cayó al piso y no funciona bien',
            'created_at'            =>  '2019-11-03',
            'time_resolved'         =>  1, //3 dia en terminar el ticket
            'ticket_category_id'    =>  1, //Service
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-03',
            'workgroup_id'          =>  2,  //Equipo SERV0002 
            'priority'              =>  3, //prioridad media
            'client_id'             =>  24,  //SOLANG NUÑEZ
            'detail'                =>  'Comprobar si el equipo esta quemado',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  6  //Empleado CARLOS

        ]);
      








        //--------------------------Equipo DESC0001 TICKET 1------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000015',
            'description'           =>  'Dar de baja el servicio por falta de pago',
            'created_at'            =>  '2019-11-04',
            'time_resolved'         =>  8, //2 dia en terminar el ticket
            'ticket_category_id'    =>  2,  //Desconexión,
            'current_stage_id'      =>  4,  //Resuelto
            'date'                  =>  '2019-11-04',
            'workgroup_id'          =>  3,  //Equipo DESC0001 	
            'priority'              =>  5,  //prioridad alta
            'client_id'             =>  26, //Cliente  ROXXANA
            'detail'                =>  'Cuota atrasada de hace 3 meses, dar de baja la conexión establecida',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  13  // Empleado TOMAS
        ]);

        //--------------------------Equipo DESC0001 TICKET 2------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000016',
            'description'           =>  'Dar de baja el servicio por falta de pago',
            'created_at'            =>  '2019-11-06',
            'time_resolved'         =>  4, //3 dia en terminar el ticket
            'ticket_category_id'    =>  2,  //Desconexión,
            'current_stage_id'      =>  4,  //Resuelto
            'date'                  =>  '2019-11-06',
            'workgroup_id'          =>  3,  //Equipo DESC0001 	
            'priority'              =>  5,  //prioridad alta
            'client_id'             =>  27, //CLiente  AGUSTIN
            'detail'                =>  'Cuota atrasada de hace 6 meses, dar de baja la conexión establecida',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  14  // Empleado AUGUSTO
        ]);
        //--------------------------Equipo DESC0001 TICKET 3------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000017',
            'description'           =>  'Dar de baja la linea',
            'created_at'            =>  '2019-11-03',
            'time_resolved'         =>  3, //5 dia en terminar el ticket
            'ticket_category_id'    =>  2, //Desconexion
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-03',
            'workgroup_id'          =>  3, //Equipo DESC0001 	
            'priority'              =>  2, //prioridad baja
            'client_id'             =>  27, //CLiente  AGUSTIN
            'detail'                =>  'Desconectar de forma inmediata el servicio de internet',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  14  // Empleado AUGUSTO

        ]); 
        //--------------------------Equipo DESC0001 TICKET 4------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000018',
            'description'           =>  'Por mudanza del Cliente',
            'created_at'            =>  '2019-11-06',
            'time_resolved'         =>  5, //3 dia en terminar el ticket
            'ticket_category_id'    =>  2, //Desconexion
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-06',
            'workgroup_id'          =>  3, //Equipo DESC0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  27, //CLiente  AGUSTIN
            'detail'                =>  'Dar de baja el servicio',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  13  // Empleado TOMAS
        ]);        
        //--------------------------Equipo DESC0001 TICKET 5------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000019',
            'description'           =>  'Por solicitud del Cliente',
            'created_at'            =>  '2019-11-02',
            'time_resolved'         =>  6, //3 dia en terminar el ticket
            'ticket_category_id'    =>  2, //Desconexion
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-02',
            'workgroup_id'          =>  3, //Equipo DESC0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  27, //CLiente  AGUSTIN
            'detail'                =>  'Cliente inconforme con el servicio, solicitó dar de baja inmediatamente',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  13  // Empleado TOMAS
        ]);        
        




        //--------------------------Equipo DESC0002 TICKET 1------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000020',
            'description'           =>  'Por falta de pago',
            'created_at'            =>  '2019-11-02',
            'time_resolved'         =>  2, //3 dia en terminar el ticket
            'ticket_category_id'    =>  2, //Desconexion
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-02',
            'workgroup_id'          =>  4, //Equipo DESC0002 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  27, //CLiente  AGUSTIN
            'detail'                =>  'Dar de baja el servicio',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  16  //Empleado 16  FRANCISCO JAVIER
        ]);   
        //--------------------------Equipo DESC0002 TICKET 2------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000021',
           'description'           =>  'Por falta de pago',
           'created_at'            =>  '2019-11-02',
           'time_resolved'         =>  2, //3 dia en terminar el ticket
           'ticket_category_id'    =>  2, //Desconexion
           'current_stage_id'      =>  4, //Resuelto
           'date'                  =>  '2019-11-02',
           'workgroup_id'          =>  4, //Equipo DESC0002 	
           'priority'              =>  3, //prioridad media
           'client_id'             =>  27, //CLiente  AGUSTIN
           'detail'                =>  'Dar de baja el servicio',
       ]);
       StageHistory::create([
           'date'                  =>  now(),
           'ticket_id'             =>  $t->id,
           'ticket_stage_id'       =>  $t->current_stage_id,
           'user_id'               =>  17  //Empleado 17  JULIO DAVID
           ]);   
       //--------------------------Equipo DESC0002 TICKET 3------------------------------------------------   
       $t = Ticket::create([
        'number'                =>  '0000000022',
          'description'           =>  'Por falta de pago',
          'created_at'            =>  '2019-11-12',
          'time_resolved'         =>  9, //3 dia en terminar el ticket
          'ticket_category_id'    =>  2, //Desconexion
          'current_stage_id'      =>  4, //Resuelto
          'date'                  =>  '2019-11-12',
          'workgroup_id'          =>  4, //Equipo DESC0002 	
          'priority'              =>  3, //prioridad media
          'client_id'             =>  27, //CLiente  AGUSTIN
          'detail'                =>  'Dar de baja el servicio',
      ]);
      StageHistory::create([
          'date'                  =>  now(),
          'ticket_id'             =>  $t->id,
          'ticket_stage_id'       =>  $t->current_stage_id,
          'user_id'               =>  17  // Empleado 17  JULIO DAVID
          ]);   
      //--------------------------Equipo DESC0002 TICKET 4------------------------------------------------   
      $t = Ticket::create([
        'number'                =>  '0000000023',
         'description'           =>  'Por falta de pago',
         'created_at'            =>  '2019-11-06',
         'time_resolved'         =>  2, //3 dia en terminar el ticket
         'ticket_category_id'    =>  2, //Desconexion
         'current_stage_id'      =>  4, //Resuelto
         'date'                  =>  '2019-11-06',
         'workgroup_id'          =>  4, //Equipo DESC0002 	
         'priority'              =>  3, //prioridad media
         'client_id'             =>  27, //CLiente  AGUSTIN
         'detail'                =>  'Dar de baja el servicio',
     ]);
     StageHistory::create([
         'date'                  =>  now(),
         'ticket_id'             =>  $t->id,
         'ticket_stage_id'       =>  $t->current_stage_id,
         'user_id'               =>  16  //Empleado 16  FRANCISCO JAVIER
         ]);   
     //--------------------------Equipo DESC0002 TICKET 5------------------------------------------------   
     $t = Ticket::create([
        'number'                =>  '0000000024',
        'description'           =>  'Por falta de pago',
        'created_at'            =>  '2019-11-12',
        'time_resolved'         =>  2, //3 dia en terminar el ticket
        'ticket_category_id'    =>  2, //Desconexion
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-12',
        'workgroup_id'          =>  4, //Equipo DESC0002 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  27, //CLiente  AGUSTIN
        'detail'                =>  'Dar de baja el servicio',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  16  //Empleado 16  FRANCISCO JAVIER
        ]);
 














    //--------------------------Equipo RXIN0001 TICKET 1------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000025',
        'description'           =>  'Pago abonado, reconectar la linea',
        'created_at'            =>  '2019-11-06',
        'time_resolved'         =>  6, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-06',
        'workgroup_id'          =>  5, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconectar la linea previamente dada de baja el dia 22/10/2019',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  18  //18  JORGE RUBEN
    ]);   
    //--------------------------Equipo RXIN0001 TICKET 2------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000026',
        'description'           =>  'Reconectar el Servicio Domiliciario',
        'created_at'            =>  '2019-11-03',
        'time_resolved'         =>  8, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-03',
        'workgroup_id'          =>  5, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconectar la linea después de 3 meses',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  18  //18  JORGE RUBEN
    ]);   
     //--------------------------Equipo RXIN0001 TICKET 3------------------------------------------------   
     $t = Ticket::create([
        'number'                =>  '0000000027',
        'description'           =>  'Reconectar servicio dada de baja',
        'created_at'            =>  '2019-11-09',
        'time_resolved'         =>  7, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-09',
        'workgroup_id'          =>  5, //Equipo RXIN0001 	
        'priority'              =>  5, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconexión solicitada hace 1 año',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  18  //18  JORGE RUBEN
    ]);        //--------------------------Equipo RXIN0001 TICKET 4------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000028',
       'description'           =>  'Reconecxión al Intendente',
       'created_at'            =>  '2019-11-30',
       'time_resolved'         =>  3, //3 dia en terminar el ticket
        'ticket_category_id'   => 3, //RECONEXION
       'current_stage_id'      =>  4, //Resuelto
       'date'                  =>  '2019-11-30',
       'workgroup_id'          =>  5, //Equipo RXIN0001 	
       'priority'              =>  5, //prioridad media
       'client_id'             =>  28, //CLiente  Maria
       'detail'                =>  'Reconectar servicio inmediatamente',
   ]);
   StageHistory::create([
       'date'                  =>  now(),
       'ticket_id'             =>  $t->id,
       'ticket_stage_id'       =>  $t->current_stage_id,
       'user_id'               =>  18  //18  JORGE RUBEN
   ]);   
    //--------------------------Equipo RXIN0001 TICKET 5------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000029',
        'description'           =>  'Reconexión domiciliaria',
        'created_at'            =>  '2019-11-28',
        'time_resolved'         =>  2, //2 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-28',
        'workgroup_id'          =>  5, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconexión domiciliaria',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  18  //18  JORGE RUBEN
    ]);
        
        




    //--------------------------Equipo RXIN0002 TICKET 1------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000030',
        'description'           =>  'Reconexión Domiciliaria',
        'created_at'            =>  '2019-12-02',
        'time_resolved'         =>  2, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-12-02',
        'workgroup_id'          =>  6, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconexión Domiciliaria',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  19  //19  Saul
    ]);   
    //--------------------------Equipo RXIN0002 TICKET 2------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000031',
        'description'           =>  'Reconexión de Servicio',
        'created_at'            =>  '2019-12-03',
        'time_resolved'         =>  2, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-12-03',
        'workgroup_id'          =>  6, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconexión de Servicio dado de baja el dia 02/10',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  19  //19  Saul
    ]);   
     //--------------------------Equipo RXIN0002 TICKET 3------------------------------------------------   
     $t = Ticket::create([
        'number'                =>  '0000000032',
        'description'           =>  'Reconexión asistida por central',
        'created_at'            =>  '2019-11-27',
        'time_resolved'         =>  3, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-27',
        'workgroup_id'          =>  6, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconectar servicio solicitado a travéz del servicio de comunicación',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  19  //19  Saul
    ]);        //--------------------------Equipo RXIN0002 TICKET 4------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000033',
       'description'           =>  'Reconexión asistida por central',
       'created_at'            =>  '2019-11-24',
       'time_resolved'         =>  3, //3 dia en terminar el ticket
       'ticket_category_id'    =>  3, //RECONEXION
       'current_stage_id'      =>  4, //Resuelto
       'date'                  =>  '2019-11-24',
       'workgroup_id'          =>  6, //Equipo RXIN0001 	
       'priority'              =>  3, //prioridad media
       'client_id'             =>  28, //CLiente  Maria
       'detail'                =>  'Reconexión asistida por central',
   ]);
   StageHistory::create([
       'date'                  =>  now(),
       'ticket_id'             =>  $t->id,
       'ticket_stage_id'       =>  $t->current_stage_id,
       'user_id'               =>  19  //19  Saul
       ]);   
    //--------------------------Equipo RXIN0002 TICKET 5------------------------------------------------   
    $t = Ticket::create([
        'number'                =>  '0000000034',
        'description'           =>  'Reconexión domiciliaria',
        'created_at'            =>  '2019-11-30',
        'time_resolved'         =>  1, //3 dia en terminar el ticket
        'ticket_category_id'    =>  3, //RECONEXION
        'current_stage_id'      =>  4, //Resuelto
        'date'                  =>  '2019-11-30',
        'workgroup_id'          =>  6, //Equipo RXIN0001 	
        'priority'              =>  3, //prioridad media
        'client_id'             =>  28, //CLiente  Maria
        'detail'                =>  'Reconexión domiciliaria',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  19  //19  Saul
    ]);
    



        //--------------------------Equipo INST0001 TICKET 1------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000035',
            'description'           =>  'Instalación de decodificador',
            'created_at'            =>  '2019-11-20',
            'time_resolved'         =>  5, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-20',
            'workgroup_id'          =>  7, //Equipo INST0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'DECO Plus HD 28 canales',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  20 // 20  NAHUEL
        ]);   
        //--------------------------Equipo INST0001 TICKET 2------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000036',
            'description'           =>  'Instalación de decodificador y HD',
            'created_at'            =>  '2019-11-22',
            'time_resolved'         =>  3, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-22',
            'workgroup_id'          =>  7, //Equipo INST0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'HD GOLD y DECO Plus 28 canales',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  21 // 21 Mariano
        ]);   
         //--------------------------Equipo INST0001 TICKET 3------------------------------------------------   
         $t = Ticket::create([
            'number'                =>  '0000000037',
            'description'           =>  'Instalación de DCT',
            'created_at'            =>  '2019-11-22',
            'time_resolved'         =>  4, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-22',
            'workgroup_id'          =>  7, //Equipo INST0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'DCT, con cables VGA',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  22 //  22  ESTEBAN
        ]);        //--------------------------Equipo INST0001 TICKET 4------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000038',
           'description'           =>  'Instalación de Cable-Modem',
           'created_at'            =>  '2019-11-29',
           'time_resolved'         =>  2, //3 dia en terminar el ticket
           'ticket_category_id'    =>  4, //INSTALACION
           'current_stage_id'      =>  4, //Resuelto
           'date'                  =>  '2019-11-29',
           'workgroup_id'          =>  7, //Equipo INST0001 	
           'priority'              =>  3, //prioridad media
           'client_id'             =>  28, //CLiente  Maria
           'detail'                =>  'CM de 10Mbps',
       ]);
       StageHistory::create([
           'date'                  =>  now(),
           'ticket_id'             =>  $t->id,
           'ticket_stage_id'       =>  $t->current_stage_id,
           'user_id'               =>  22 //  22  ESTEBAN
           ]);   
        //--------------------------Equipo INST0001 TICKET 5------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000039',
            'description'           =>  'Instalación de Cable-Modem y HD',
            'created_at'            =>  '2019-11-02',
            'time_resolved'         =>  8, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-02',
            'workgroup_id'          =>  7, //Equipo INST0001 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'HD Plus de 12 canales y CM de 6Mbps',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  22 //  22  ESTEBAN
        ]);


        









        //--------------------------Equipo INST0002 TICKET 1------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000040',
            'description'           =>  'Instalación de Antena',
            'created_at'            =>  '2019-11-06',
            'time_resolved'         =>  1, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-06',
            'workgroup_id'          =>  8, //Equipo INST0002 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'Antena Parabolica',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  23 // 23  GERMAN
        ]);   
        //--------------------------Equipo INST0002 TICKET 2------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000041',
            'description'           =>  'Instalación de Antena y DECO',
            'created_at'            =>  '2019-11-06',
            'time_resolved'         =>  2, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-06',
            'workgroup_id'          =>  8, //Equipo INST0002 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'Decodificador de señal',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  23 // 23  GERMAN
        ]);   
         //--------------------------Equipo INST0002 TICKET 3------------------------------------------------   
         $t = Ticket::create([
            'number'                =>  '0000000042',
            'description'           =>  'Instalación de HD y DECO',
            'created_at'            =>  '2019-11-09',
            'time_resolved'         =>  2, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-09',
            'workgroup_id'          =>  8, //Equipo INST0002 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'HD max 1080i ',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  23 // 23  GERMAN
        ]);        //--------------------------Equipo INST0002 TICKET 4------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000043',
           'description'           =>  'Instalación de HD estándar',
           'created_at'            =>  '2019-11-19',
           'time_resolved'         =>  8, //3 dia en terminar el ticket
           'ticket_category_id'    =>  4, //INSTALACION
           'current_stage_id'      =>  4, //Resuelto
           'date'                  =>  '2019-11-19',
           'workgroup_id'          =>  8, //Equipo INST0002 	
           'priority'              =>  3, //prioridad media
           'client_id'             =>  28, //CLiente  Maria
           'detail'                =>  '720 HD',
       ]);
       StageHistory::create([
           'date'                  =>  now(),
           'ticket_id'             =>  $t->id,
           'ticket_stage_id'       =>  $t->current_stage_id,
           'user_id'               =>  23 // 23  GERMAN
           ]);   
        //--------------------------Equipo INST0002 TICKET 5------------------------------------------------   
        $t = Ticket::create([
            'number'                =>  '0000000044',
            'description'           =>  'instalación domiciliaria',
            'created_at'            =>  '2019-11-02',
            'time_resolved'         =>  8, //3 dia en terminar el ticket
            'ticket_category_id'    =>  4, //INSTALACION
            'current_stage_id'      =>  4, //Resuelto
            'date'                  =>  '2019-11-02',
            'workgroup_id'          =>  8, //Equipo INST0002 	
            'priority'              =>  3, //prioridad media
            'client_id'             =>  28, //CLiente  Maria
            'detail'                =>  'DECO, HD, y CM paquete Flow',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  23 // 23  GERMAN
        ]);
            













        //Ahora traigo los tickets de prueba


        //-------------------Para el workgroup con id = 1-------------------------
       /* $t = Ticket::create([
            'description'           =>  'Problema con la velocidad de Internet',
            'created_at'            =>  '2019-12-02',
            'ticket_category_id'    =>  1,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-02',
            'workgroup_id'          =>  1,
            'priority'              =>  4,
            'client_id'             =>  30,
            'detail'                =>  'Verificar si el modem funciona correctamente',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  5 
        ]); 

        $t = Ticket::create([
            'description'           =>  'Problema con nodo de comunicación',
            'created_at'            =>  '2019-12-02',
            'ticket_category_id'    =>  1,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-02',
            'workgroup_id'          =>  1,
            'priority'              =>  1,
            'client_id'             =>  30,
            'detail'                =>  'Verificar si la cobertura en el área esta activa',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  5 
        ]); */
        
        //-------------------Para el workgroup con id = 2-------------------------


        $t = Ticket::create([
            'number'                =>  '0000000045',
            'description'           =>  'Comprobar el estado del decodificador HD',
            'created_at'            =>  '2019-12-03',
            'ticket_category_id'    =>  1,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-03',
            'workgroup_id'          =>  2,
            'priority'              =>  1,
            'client_id'             =>  29,
            'detail'                =>  'Verificar los puertos HDMI',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  7 
        ]);

        $t = Ticket::create([
            'number'                =>  '0000000046',
            'description'           =>  'Comprobar el estado del modem',
            'created_at'            =>  '2019-12-02',
            'ticket_category_id'    =>  1,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-03',
            'workgroup_id'          =>  2,
            'priority'              =>  1,
            'client_id'             =>  29,
            'detail'                =>  'Verificar los puertos HDMI',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  7 
        ]); 


        //-------------------Para el workgroup con id = 3-------------------------


        $t = Ticket::create([
            'number'                =>  '0000000047',
            'description'           =>  'Comprobar el estado del decodificador HD',
            'created_at'            =>  '2019-12-02',
            'ticket_category_id'    =>  2,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-02',
            'workgroup_id'          =>  3,
            'priority'              =>  2,
            'client_id'             =>  30,
            'detail'                =>  'Verificar los puertos HDMI',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  15
        ]);

     

        //-------------------Para el workgroup con id = 4-------------------------


        $t = Ticket::create([
            'number'                =>  '0000000048',
            'description'           =>  'Desconexión de Linea por falta de Pago',
            'created_at'            =>  '2019-12-03',
            'ticket_category_id'    =>  2,
            'current_stage_id'      =>  1,
            'date'                  =>  '2019-12-03',
            'workgroup_id'          =>  4,
            'priority'              =>  2,
            'client_id'             =>  30,
            'detail'                =>  'Dar de baja el servicio',
        ]);
        StageHistory::create([
            'date'                  =>  now(),
            'ticket_id'             =>  $t->id,
            'ticket_stage_id'       =>  $t->current_stage_id,
            'user_id'               =>  15
        ]);




       //-------------------Para el workgroup con id = 5-------------------------
       $t = Ticket::create([
        'number'                =>  '0000000049',
        'description'           =>  'Reanudación del servicio, cortado con anterioridad',
        'created_at'            =>  '2019-12-04',
        'ticket_category_id'    =>  3,
        'current_stage_id'      =>  1,
        'date'                  =>  '2019-12-04',
        'workgroup_id'          =>  5,
        'priority'              =>  1,
        'client_id'             =>  18,
        'detail'                =>  'Reconectar los componentes deshabilitados',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  15
    ]);


       //-------------------Para el workgroup con id = 6-------------------------
       $t = Ticket::create([
        'number'                =>  '0000000050',
        'description'           =>  'Rehabilitación de Linea Caducada',
        'created_at'            =>  '2019-12-04',
        'ticket_category_id'    =>  3,
        'current_stage_id'      =>  1,
        'date'                  =>  '2019-12-04',
        'workgroup_id'          =>  6,
        'priority'              =>  1,
        'client_id'             =>  25,
        'detail'                =>  'Reconectar la linea',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  19 //Saul
    ]);


    //-------------------Para el workgroup con id = 7-------------------------
    $t = Ticket::create([
    'number'                =>  '0000000051',
    'description'           =>  'Instalación domiciliaria de equipo HD',
    'created_at'            =>  '2019-12-02',
    'ticket_category_id'    =>  4,
    'current_stage_id'      =>  1,
    'date'                  =>  '2019-12-02',
    'workgroup_id'          =>  7,
    'priority'              =>  2,
    'client_id'             =>  25,
    'detail'                =>  'Instalar en el domicilio del cliente el decodificador',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  21 //Mariano
    ]);
    
    //-------------------Para el workgroup con id = 8-------------------------
    $t = Ticket::create([
    'number'                =>  '0000000052',
    'description'           =>  'Instalación domiciliaria de cable modem',
    'created_at'            =>  '2019-12-03',
    'ticket_category_id'    =>  4,
    'current_stage_id'      =>  1,
    'date'                  =>  '2019-12-03',
    'workgroup_id'          =>  8,
    'priority'              =>  1,
    'client_id'             =>  25,
    'detail'                =>  'Instalar en el domicilio del cliente el CM de 10Mbps',
    ]);
    StageHistory::create([
        'date'                  =>  now(),
        'ticket_id'             =>  $t->id,
        'ticket_stage_id'       =>  $t->current_stage_id,
        'user_id'               =>  23 //German
    ]);

}
}
