<?php

use Illuminate\Database\Seeder;
use siscontratista\TicketStageOrder;


class TicketStageOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Claves primarias de TicketStages

        id  name        description
        1   "Creado"    "Creacion de un Ticket"
        2   "Revisado"  "Cuando se 've' el Ticket por primera vez"
        3   "En proceso""Se está atendiendo (realizando) el Ticket"
        4   "Resuelto"  "Resultado Exitoso de un Ticket",
        5   "No Resuelto""Resultado Fallido de un Ticket",
        6   "Reasignado""Si no se resuelve el Ticket, esta pasa al estado reasignado",


        */

        TicketStageOrder::create([
            'current_id'    =>  1,  //Creado
            'next_id'       =>  2,  //Revisado
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'        =>  2,  //Revisado
            'next_id'           =>  6,  //Re-asignado
            'workflow_id'       =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  2,  //Revisado
            'next_id'       =>  3,  //En Proceso
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  3,  //En Proceso
            'next_id'       =>  4,  //Resuelto
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  3,  //En Proceso
            'next_id'       =>  5,  //No Resuelto
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  3,  //En Proceso
            'next_id'       =>  6,  //Re-asignado
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);   

        TicketStageOrder::create([
            'current_id'    =>  6,  //Re-asignado
            'next_id'       =>  4,  //Resuelto
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  6,  //Re-asignado
            'next_id'       =>  5,  //No Resuelto
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);

        TicketStageOrder::create([
            'current_id'    =>  6,  //Re-asignado
            'next_id'       =>  6,  //Re-asignado
            'workflow_id'   =>  1,  //Ordenes de Trabajo
        ]);
    }
}
