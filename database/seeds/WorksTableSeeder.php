<?php

use Illuminate\Database\Seeder;
use App\Models\Work;

class WorksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Work::create([
            'ticket_category_id'=>1,    //Service
            'work_group_id'     =>1,    //Movil 940
            ]);

        Work::create([
            'ticket_category_id'=>4,    //Instalacion
            'work_group_id'     =>1,    //Movil 940
            ]);

        Work::create([
            'ticket_category_id'=>2,    //Desconexión
            'work_group_id'     =>2,    //Movil 941
            ]);

        Work::create([
            'ticket_category_id'=>3,    //Re-Conexion
            'work_group_id'     =>3,    //Movil 942
            ]);

        Work::create([
            'ticket_category_id'=>4,    //Instalacion
            'work_group_id'     =>4,    //Movil 943
            ]);

    }
}
