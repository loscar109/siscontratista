<?php

use Illuminate\Database\Seeder;
use App\Models\TicketAssignament;

class TicketAssignamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketAssignament::create([
            'user_id'                           =>1, //Villalba Carlos
            'ticket_id'                         =>1, //Internet No Funciona
            'ticket_assignament_privilege_id'   =>1  //Asignar
        ]);

        TicketAssignament::create([
            'user_id'                           =>1, //Villalba Carlos
            'ticket_id'                         =>2, //No se ve YouTube
            'ticket_assignament_privilege_id'   =>1  //Asignar
        ]);
    }
}
