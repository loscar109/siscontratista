<?php

use Illuminate\Database\Seeder;
use siscontratista\District;

class DistrictTableSeeder extends Seeder
{
    public function run()
    {
        District::create([
            'name'=>'25 de Mayo',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'A-32',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Aguas Corrientes',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Alta Gracia',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Andresito Gucurarí',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Bajada Vieja',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Baradero',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Belgrano',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Belgrano',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Centenario',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Cocomarola',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Cristo Rey',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Cruz del Sur',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'El Brete',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'El Chaquito - Heller',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'El Palomar',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Hipólito Irigoyen',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Islas Malvinas',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Itaembé Mini',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Kennedy',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Las Orquideas',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'La Picada',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Lavalle',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Los Aguacates',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Los Álamos',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Los Kiris',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Los Pinos',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Lucas Braulio Areco',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Luis Piedrabuena',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Nueva Esperanza A - 4',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Parque Adam',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Papini',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Patoti',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Regimiento',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Residencial Sur',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Rocamora',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'San Alberto',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'San Gerardo',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'San Jorge',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'San Marcos',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'San Miguel',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Santa Rita',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Santa Rosa',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Sesquicentenario',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Sur Argentino',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Tajamar',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Teniente 1° Roberto Estévez',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Tiro Federal',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Blosset',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Cabello',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Coz',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Dolores',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Lanús',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Longa',
            'zone'=>'sur',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Poujade',
            'zone'=>'este',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Sarita',
            'zone'=>'oeste',
            'city_id'=>1]);

        District::create([
            'name'=>'Villa Urquiza',
            'zone'=>'norte',
            'city_id'=>1]);

        District::create([
            'name'=>'Yacyretá',
            'zone'=>'sur',
            'city_id'=>1]);

       

    }
}
