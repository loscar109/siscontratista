<?php

use Caffeinated\Shinobi\Models\Permission;

    $work_INDEX_REmpleado = new Permission();
    $work_INDEX_REmpleado->name='Indice de Trabajos';
    $work_INDEX_REmpleado->slug='works.index';
    $work_INDEX_REmpleado->description='Lista y navega todos los trabajos del sistema';
    $work_INDEX_REmpleado->save();
    $work_INDEX_REmpleado->assignRole(3);//Empleado

    $work_INDEX_REmpleado = new Permission();
    $work_INDEX_REmpleado->name='Ver Trabajos';
    $work_INDEX_REmpleado->slug='works.show';
    $work_INDEX_REmpleado->description='Ve los trabajos';
    $work_INDEX_REmpleado->save();
    $work_INDEX_REmpleado->assignRole(3);//Empleado

    $work_INDEX_REmpleado = new Permission();
    $work_INDEX_REmpleado->name='Processing';
    $work_INDEX_REmpleado->slug='works.updateStage';
    $work_INDEX_REmpleado->description='Procesando..';
    $work_INDEX_REmpleado->save();
    $work_INDEX_REmpleado->assignRole(3);//Empleado

   
    $work_INDEX_REmpleado = new Permission();
    $work_INDEX_REmpleado->name='ReProgramming';
    $work_INDEX_REmpleado->slug='works.reprogramming';
    $work_INDEX_REmpleado->description='Reprogramando..';
    $work_INDEX_REmpleado->save();
    $work_INDEX_REmpleado->assignRole(3);//Empleado

    $work_INDEX_REmpleado = new Permission();
    $work_INDEX_REmpleado->name='SendMessagge';
    $work_INDEX_REmpleado->slug='works.message';
    $work_INDEX_REmpleado->description='Enviar Mensaje..';
    $work_INDEX_REmpleado->save();
    $work_INDEX_REmpleado->assignRole(3);//Empleado


    $work_FINISH_REmpleado = new Permission();
    $work_FINISH_REmpleado->name='Finish';
    $work_FINISH_REmpleado->slug='works.finish';
    $work_FINISH_REmpleado->description='Finalizando..';
    $work_FINISH_REmpleado->save();
    $work_FINISH_REmpleado->assignRole(3);//Empleado

    $work_STOREFINISH_REmpleado = new Permission();
    $work_STOREFINISH_REmpleado->name='StoreFinish';
    $work_STOREFINISH_REmpleado->slug='works.storeFinished';
    $work_STOREFINISH_REmpleado->description='Finalziado';
    $work_STOREFINISH_REmpleado->save();
    $work_STOREFINISH_REmpleado->assignRole(3);//Empleado

    $work_findMaterial_REmpleado = new Permission();
    $work_findMaterial_REmpleado->name='findMaterial';
    $work_findMaterial_REmpleado->slug='works.findMaterial';
    $work_findMaterial_REmpleado->description='Encontrar Material';
    $work_findMaterial_REmpleado->save();
    $work_findMaterial_REmpleado->assignRole(3);//Empleado


    $p = new Permission();
    $p->name='inProccess';
    $p->slug='works.inProccess';
    $p->description='Trabajos en Procesos';
    $p->save();
    $p->assignRole(3);//Empleado

    $p = new Permission();
    $p->name='resolved';
    $p->slug='works.resolved';
    $p->description='Trabajos Terminados';
    $p->save();
    $p->assignRole(3);//Empleado

    $p = new Permission();
    $p->name='created';
    $p->slug='works.created';
    $p->description='Trabajo Creado';
    $p->save();
    $p->assignRole(3);//Empleado

    $p = new Permission();
    $p->name='seeAllMessage';
    $p->slug='works.send';
    $p->description='Ver Mensajes Enviados';
    $p->save();
    $p->assignRole(3);//Empleado

    $p = new Permission();
    $p->name='Crear Materiales';
    $p->slug='works.finish.findMaterialData';
    $p->description='Crea Materiales';
    $p->save();
    $p->assignRole(3);//Empleado
?>