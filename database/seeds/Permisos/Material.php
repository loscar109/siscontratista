<?php

use Caffeinated\Shinobi\Models\Permission;

    $p = new Permission();
    $p->name='Indice de Materiales';
    $p->slug='materials.gestion.index';
    $p->description='Lista y navega todos los materiales del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Crear Materiales';
    $p->slug='materials.gestion.create';
    $p->description='Crea Materiales';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Acceder a los movimientos';
    $p->slug='materials.gestion.movimientos';
    $p->description='Ver Movimientos';
    $p->save();
    $p->assignRole(2);


    $p = new Permission();
    $p->name='Cargar una entrada';
    $p->slug='requisitions.gestion.entry';
    $p->description='Cargar entrada';
    $p->save();
    $p->assignRole(2);

 
    
?>