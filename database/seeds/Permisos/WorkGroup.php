<?php

use Caffeinated\Shinobi\Models\Permission;

    $p = new Permission();
    $p->name='Indice de Grupos de Trabajo';
    $p->slug='workgroups.gestion.index';
    $p->description='Lista y navega todos los grupos del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Crear Grupo de Trabajo';
    $p->slug='workgroups.gestion.create';
    $p->description='Crea Tickets';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Ver Detalle de Grupo de Trabajo';
    $p->slug='workgroups.gestion.show';
    $p->description='Crea Tickets';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Mostrar promedio';
    $p->slug='configuracion.workgroup.create';
    $p->description='Mostrar promedio';
    $p->save();
    $p->assignRole(2);

    


    $p = new Permission();
    $p->name='Mostrar select2';
    $p->slug='workgroups.gestion.showEmployeInSelect2';
    $p->description='Mostrar select2';
    $p->save();
    $p->assignRole(2);
?>