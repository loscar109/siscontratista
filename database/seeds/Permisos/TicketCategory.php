<?php

use Caffeinated\Shinobi\Models\Permission;

    $p = new Permission();
    $p->name='Crear Categorias de Ticket';
    $p->slug='configuracion.ticket_category.create';
    $p->description='Crea Categgorias';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Indice de Categorias de Ticket';
    $p->slug='configuracion.ticket_category.index';
    $p->description='Lista y navega todos las categorias del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Actualiza Categorias de Ticket';
    $p->slug='configuracion.ticket_category.update';
    $p->description='Lista y navega todos las categorias del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Ver Categorias de Ticket';
    $p->slug='configuracion.ticket_category.show';
    $p->description='Lista y navega todos las categorias del sistema';
    $p->save();
    $p->assignRole(2);


    $p = new Permission();
    $p->name='Eliminar Categorias de Ticket';
    $p->slug='configuracion.ticket_category.destroy';
    $p->description='Lista y navega todos las categorias del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Eliminar Categorias de Ticket';
    $p->slug='configuracion.ticket_category.edit';
    $p->description='Lista y navega todos las categorias del sistema';
    $p->save();
    $p->assignRole(2);
?>