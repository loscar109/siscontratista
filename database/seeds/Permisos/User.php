<?php

use Caffeinated\Shinobi\Models\Permission;


    //Permisos para VER INDICE
    $user_INDEX_RJefe = new Permission();
    $user_INDEX_RJefe->name='Indice de Usuarios';
    $user_INDEX_RJefe->slug='users.gestion.index';
    $user_INDEX_RJefe->description='Permite listar los usuarios existentes en el sistema';
    $user_INDEX_RJefe->save();
    $user_INDEX_RJefe->assignRole(2);//Jefe
    
    //Permisos para CREAR
    $user_CREATE_RJefe = new Permission();
    $user_CREATE_RJefe->name='Crear un Usuario';
    $user_CREATE_RJefe->slug='users.gestion.create';
    $user_CREATE_RJefe->description='Permite crear un determinado usuario en el sistema';
    $user_CREATE_RJefe->save();
    $user_CREATE_RJefe->assignRole(2);//Jefe
    
    //Permisos para VER
    $user_SHOW_RJefe = new Permission();
    $user_SHOW_RJefe->name='Ver un Usuario';
    $user_SHOW_RJefe->slug='users.gestion.show';
    $user_SHOW_RJefe->description='Permite ver detalle de una determinado usuario en el sistema';
    $user_SHOW_RJefe->save();
    $user_SHOW_RJefe->assignRole(2);//Jefe
    
    //Permisos para EDITAR
    $user_EDIT_RJefe = new Permission();
    $user_EDIT_RJefe->name='Editar un Usuario';
    $user_EDIT_RJefe->slug='users.gestion.edit';
    $user_EDIT_RJefe->description='Permite modificar un determinado usuario exitente en el sistema';
    $user_EDIT_RJefe->save();
    $user_EDIT_RJefe->assignRole(2);//Jefe
    
    //Permisos para ELIMINAR
    $user_DESTROY_RJefe = new Permission();
    $user_DESTROY_RJefe->name='Eliminar un Usuario';
    $user_DESTROY_RJefe->slug='users.gestion.destroy';
    $user_DESTROY_RJefe->description='Permite eliminar un determinado usuario exitente en el sistema';
    $user_DESTROY_RJefe->save();
    $user_DESTROY_RJefe->assignRole(2);//Jefe *///


    
?>