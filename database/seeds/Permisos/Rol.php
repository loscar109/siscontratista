<?php

use Caffeinated\Shinobi\Models\Permission;
    
    Permission::create([
            'name'          =>  'Indice de roles',
            'slug'          =>  'roles.gestion.index',
            'description'   =>  'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
            'name'          =>  'Ver detalle de rol',
            'slug'          =>  'roles.gestion.show',
            'description'   =>  'Ver en detalle cada rol del sistema',
        ]);

        Permission::create([
            'name'          =>  'Edición de rol',
            'slug'          =>  'roles.gestion.edit',
            'description'   =>  'Editar cualquier dato de un rol del sistema',
        ]);

        Permission::create([
            'name'          =>  'Eliminar rol',
            'slug'          =>  'roles.gestion.destroy',
            'description'   =>  'Eliminar cualquier rol del sistema',
        ]);

        Permission::create([
            'name'          =>  'Creacion de rol',
            'slug'          =>  'roles.gestion.create',
            'description'   =>  'Crear un determinado role',
        ]);

?>