<?php

use Caffeinated\Shinobi\Models\Permission;




    //Ver detalle de Auditoria de Usuarios
    $audits_USER_SHOW = new Permission();
    $audits_USER_SHOW->name='Indice de detalle de Auditoria de Usuarios';
    $audits_USER_SHOW->slug='audits.user.home';
    $audits_USER_SHOW->description='Ver detalle de auditoria de Usuario';
    $audits_USER_SHOW->save();
    $audits_USER_SHOW->assignRole(5);//Auditor
?>