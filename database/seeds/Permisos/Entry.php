<?php

use Caffeinated\Shinobi\Models\Permission;

    $p = new Permission();
    $p->name='Indice de Entradas';
    $p->slug='requisitions.gestion.index';
    $p->description='Lista y navega todas las entradas del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Crear Entradas';
    $p->slug='requisitions.gestion.create';
    $p->description='Crea Entradas';
    $p->save();
    $p->assignRole(2);

?>