<?php

use Caffeinated\Shinobi\Models\Permission;

    $p = new Permission();
    $p->name='Indice de Tickets';
    $p->slug='tickets.gestion.index';
    $p->description='Lista y navega todos los tickets del sistema';
    $p->save();
    $p->assignRole(2);

    $p = new Permission();
    $p->name='Crear Tickets';
    $p->slug='tickets.gestion.create';
    $p->description='Crea Tickets';
    $p->save();
    $p->assignRole(2);

?>