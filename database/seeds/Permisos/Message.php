<?php

use Caffeinated\Shinobi\Models\Permission;

    $m = new Permission();
    $m->name='Indice de Mensajes';
    $m->slug='messages.gestion.index';
    $m->description='Lista y navega todos los mensajes';
    $m->save();
    $m->assignRole(3);//Empleado
    $m->assignRole(2);

    $m = new Permission();
    $m->name='Crear Mensaje';
    $m->slug='messages.gestion.create';
    $m->description='crea los mensajes';
    $m->save();
    $m->assignRole(3);//Empleado
    $m->assignRole(2);

    $m = new Permission();
    $m->name='Crear Mensaje';
    $m->slug='messages.gestion.allSend';
    $m->description='ve todos los mensajes enviados';
    $m->save();
    $m->assignRole(3);

?>

