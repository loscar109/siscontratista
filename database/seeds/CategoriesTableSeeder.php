<?php

use Illuminate\Database\Seeder;
use siscontratista\Category;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['description'=>'Cables','medida'=>'metros']);
        Category::create(['description'=>'Conectores y Adaptadores','medida'=>'unidades']);
        Category::create(['description'=>'Divisores y Acopladores','medida'=>'unidades']);
        Category::create(['description'=>'Equipos','medida'=>'unidades']);
        Category::create(['description'=>'Grampas y Precintos','medida'=>'unidades']);
        Category::create(['description'=>'Filtros y Atenuadores Domiciliarios','medida'=>'unidades']);
        Category::create(['description'=>'Cintas y Selladores','medida'=>'unidades']);
        Category::create(['description'=>'Pitones, Tarugos y Tubos Pasapared','medida'=>'unidades']);
        Category::create(['description'=>'Nomencladores','medida'=>'unidades']);


 

    }
}