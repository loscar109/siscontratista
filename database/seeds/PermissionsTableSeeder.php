<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permisos para el Modulo Usuarios (INDICE, CREAR, VER, EDITAR, ELIMINAR) solo -JEFE-
        include 'Permisos/User.php';

        include 'Permisos/Rol.php';

      
        include 'Permisos/Work.php';
        include 'Permisos/AuditsUser.php';
        include 'Permisos/Message.php';
        include 'Permisos/Ticket.php';
        include 'Permisos/TicketCategory.php';

        include 'Permisos/WorkGroup.php';

        include 'Permisos/Material.php';
        include 'Permisos/Entry.php';







     
       
    }
}