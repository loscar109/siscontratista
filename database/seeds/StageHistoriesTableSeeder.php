<?php

use Illuminate\Database\Seeder;
use siscontratista\StageHistory;


class StageHistoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*     tickets                              ticket_stages
        ticket_id | description                 ticket_stage_id     |   description
            1     | Internet no funciona               1            |   Creado
            2     | No se ve YouTube                   2            |   Asignado
            3     | Internet anda muy lento            3            |   Revisado
            4     | Por falta de pago                  4            |   En Proceso
            5     | Nodo con falla eléctrica           5            |   Resuelto
           ...    | ....                              ...           |   ...
        */

        StageHistory::create([
            'date'              =>'2019-05-01',
            'ticket_id'         =>1, //
            'ticket_stage_id'   =>1,  //Creado
            'user_id'           =>4  //


        ]);

        StageHistory::create([
            'date'              =>'2019-05-01',
            'ticket_id'         =>2, //
            'ticket_stage_id'   =>1, //
            'user_id'           =>5  //
        ]);

    }
}
