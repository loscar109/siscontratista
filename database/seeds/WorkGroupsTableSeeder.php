<?php

use Illuminate\Database\Seeder;
use siscontratista\WorkGroup;


class WorkGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        //-------------------------2 Grupos de Trabajo para SERVICE----------------------------------
        WorkGroup::create([                                                                       
            'name'                  =>  'Equipo SERV0001',  // Equipo SERV0001'                    
            'ticket_category_id'    =>  1,                  // Service
            'average_time'          =>  4,                  // (4 + 4 + 4 + 3 + 1)/5 = 3.2-> 4
            'created_at'            =>  '2019-11-04'
            ]);     
        WorkGroup::create([
            'name'                  =>  'Equipo SERV0002',  //Equipo SERV0002'
            'ticket_category_id'    =>  1,                 //Service
            'average_time'          =>  3,
            'created_at'            =>  '2019-11-07'

            ]);    
        //-------------------------2 Grupos de Trabajo para DESCONEXION------------------------------
        WorkGroup::create([
            'name'                  =>  'Equipo DESC0001',  //Equipo DESC0001
            'ticket_category_id'    =>  2,                  //Desconexión
            'average_time'          =>  2,
            'created_at'            =>  '2019-11-07'

 
            ]);
        WorkGroup::create([
            'name'                  =>  'Equipo DESC0002',  //Equipo DESC0001
            'ticket_category_id'    =>  2,                   //Desconexión
            'average_time'          =>  4,
            'created_at'            =>  '2018-11-07'


            ]);
        //-------------------------2 Grupos de Trabajo para RE-CONEXION------------------------------
        WorkGroup::create([
            'name'                  =>  'Equipo RXIN0001',  //Equipo RXIN0001
            'ticket_category_id'    =>  3,                   //Reconexión
            'average_time'          =>  6,
            'created_at'            =>  '2018-07-11'


            ]);
        WorkGroup::create([
            'name'                  =>  'Equipo RXIN0002',  //Equipo RXIN0002
            'ticket_category_id'    =>  3,                   //Reconexión
            'average_time'          =>  3,
            'created_at'            =>  '2020-01-05'


            ]);
        //-------------------------2 Grupos de Trabajo para INSTALACION------------------------------
        WorkGroup::create([
            'name'                  =>  'Equipo INST0001',  //Equipo INST0001
            'ticket_category_id'    =>  4,                   //Instalación
            'average_time'          =>  5,
            'created_at'            =>  '2020-01-02'


            ]); 
        WorkGroup::create([
            'name'                  =>  'Equipo INST0002',  //Equipo INST0002
            'ticket_category_id'    =>  4,                   //Instalación
            'average_time'          =>  5,
            'created_at'            =>  '2019-12-11'


            ]); 


    }
}
