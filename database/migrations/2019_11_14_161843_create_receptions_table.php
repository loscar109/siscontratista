<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dispatch_number')->unique();   //Numero de Remito
            $table->dateTime('dispatch_date');  //Fecha y Hora de Entrada de Remito
            $table->unsignedBigInteger('user_id'); //Usuario (de la empresa) que confirma la entrada
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');    
            $table->unsignedBigInteger('requisition_id');  //Numero de Pedido de la Entrada
            $table->foreign('requisition_id')->references('id')->on('requisitions')->onDelete('restrict');
            $table->dateTime('date_of_entry');      //Fecha y Hora de la Entrada
            DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receptions');
    }
}
