<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*despues de crear un detalle de entrada se incrementa el stock
        DB::unprepared("CREATE TRIGGER `tr_updStock` AFTER INSERT ON `detail_entries`
            FOR EACH ROW
                BEGIN
                    UPDATE materials SET stock = stock + NEW.quantity
                    WHERE materials.id = NEW.material_id;
                END
        "); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*DB::unprepared("DROP TRIGGER IF EXISTS `tr_updStock`");   */
    }
}
