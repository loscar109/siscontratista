<?php

namespace siscontratista;


use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TicketAssignamentPrivilege extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'alias',
        'description'

    ];

    public $timestamps=false;

    //Un Ticket de privilegio de Asignacion puede tener muchos Ticket asignados asociados
    public function ticketAssignaments()
    {
        return $this->hasMany('App\Models\TicketAssignament');
    }
}
