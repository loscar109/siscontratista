<?php

namespace siscontratista;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;


use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements AuditableContract, UserResolver
{
    use Notifiable, ShinobiTrait, Auditable;

    protected $table='users';

    protected $primaryKey='id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'photo',
        'assigned',
        'district_id',
        'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    //Un Usuario tiene muchos Roles asociados
    public function roleUsers()
    {
        return $this->belongsToMany('Caffeinated\Shinobi\Models\Role');
    }

    //Obtener el nombre del Rol del Usuario
    public function nameRoleUser()
    {
        return $this->roleUsers->first()->name;
    }
    
    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    public function district()
    {
        return $this->belongsTo('siscontratista\District');
    }

    public function city()
    {
        return $this->district->city;
    }

    public function province()
    {
        return $this->district->city->province;
    }

    public function country()
    {
        return $this->district->city->province->country;
    }


    //Un usuario tiene muchos grupos de trabajos asociados (esto lo hice asi para que la seleccion sea mas facil)
    public function user_work_groups()
    {
        return $this->hasMany('siscontratista\UserWorkGroup')->first();
        
    }

    //Un usuario puede tener muchos mensajes asociados
    public function messages()
    {
        return $this->hasMany('siscontratista\Message');
    }

    public function message_to()
    {
        return $this->hasMany('siscontratista\Message','to_id','id');
    }


    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

            
   public function nameComplete()
   {
       return $this->name ." ". $this->surname;
   }

      
    

}
