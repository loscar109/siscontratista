<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class District extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'city_id',
        'zone'
        
    ];

    public $timestamps=true;

    protected $table='districts';

    protected $primaryKey='id';

     /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    public function city()
    {
        return $this->belongsTo('siscontratista\City');
    }

    public function province()
    {
        return $this->city->province;
    }

    public function country()
    {
        return $this->city->province->country;
    }



    public function users()
    {
        return $this->hasMany('siscontratista\User');
    }
}
