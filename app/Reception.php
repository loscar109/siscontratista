<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Reception extends Model implements AuditableContract

{
    use Auditable;

    protected $fillable = [
        'dispatch_number',
        'dispatch_date',
        'date_of_entry',
        'user_id',
        'requisition_id'
        
    ];


    public $dates = ['date_of_entry'];

    public $timestamps=true;

    protected $table='receptions';

    protected $primaryKey='id';

      /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/
        
    //Un recibimiento de material corresponde a un pedido del mismo
    public function requisition()
    {
        return $this->belongsTo('siscontratista\Requisition');
    }


    public function user()
    {
        return $this->belongsTo('siscontratista\User');
    }

    public function reception_details()
    {
        return $this->hasMany('siscontratista\ReceptionDetail');
    }
    
}
