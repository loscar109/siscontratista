<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class TicketCategory extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'description',
        'workflow_id'


    ];

    protected $table='ticket_categories';

    protected $primaryKey='id';

    public $timestamps=false;


    /*
    
        HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo
        
        */

    //Una Categoria de Ticket puede tener muchos Ticket asociados
    public function tickets()
    {
        return $this->hasMany('siscontratista\Ticket');
    }

     //Una Categoria de Ticket puede tener muchos Grupos de Trabajos asociados
     public function work_groups()
     {
         return $this->hasMany('siscontratista\WorkGroup');
     }

     public function workflow()
     {
         return $this->belongsTo('siscontratista\Workflow');
     }


}
