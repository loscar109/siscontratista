<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Movement extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'date',
        'comprobante',
        'number',
        'quantity',
        'stock',
        'user_id',
        'material_id',

    ];

    public $timestamps=true;


    public function user()
    {
        return $this->belongsTo('siscontratista\User');
    }

    public function material()
    {
        return $this->belongsTo('siscontratista\Material');
    }
}
