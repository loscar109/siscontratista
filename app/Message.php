<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Message extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'subject',
        'text',
        'user_id',
        'to_id',
        'image',
        'read'
    ];

    public $timestamps=true;

   

    //Un mensasje puede tener un usuario al que se dirige
    public function user_to()
    {
        return $this->belongsTo('siscontratista\User','to_id','id');
    }

    //Un usuario es el que crea el mensaje
    public function user()
    {
        return $this->belongsTo('siscontratista\User');
    }

        /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/

   

}
