<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Material extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'description',
        'photo',
        'stock',
        'smin',
        'smax',
        'category_id',
        'is_ordered',
        'bundle_name',
        'bundle_quantity',
        'real_stock',
        'missing_stock'
        
    ];

    public $timestamps=true;

    protected $table='materials';

    protected $primaryKey='id';

    //Un Material puede pertenecer a una Categoria
    public function category()
    {
        return $this->belongsTo('siscontratista\Category');
    }


    public function movements()
    {
        return $this->hasMany('siscontratista\Movement')->orderBy('created_at','desc');
    }


    public function medida()
    {
        return $this->category->medida;
    }



    //Obtener el nombre de la Categoria
    public function nameMaterialCategory()
    {
        return $this->category->description;
    }


    //Obtener la fecha casteada 

    public function onlyDate()
    {
        return $this->created_at->format('d/m/Y');
    }
}
