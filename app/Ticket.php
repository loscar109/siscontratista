<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Ticket extends Model implements AuditableContract
{
    use Auditable;
    protected $fillable = [
        'number',
        'description',
        'date',
        'time_resolved',
        'time_not_resolved',
        'ticket_category_id',
        'current_stage_id',
        'workgroup_id',
        'priority',
        'client_id',
        'detail',

    ];

    protected $table='tickets';

    protected $primaryKey='id';


    public $timestamps=true;

    //Un Ticket puede pertenecer a una categoría de ticket
    public function ticket_category()
    {
        return $this->belongsTo('siscontratista\TicketCategory');
    }



    public function objectWorkflow()
    {
        return $this->ticket_category->workflow->initial_stage_id;
    }

    //Obtener el grupo de trabajo al que se le asigno la categoria del ticket
    public function workgroup()
    {
        return $this->belongsTo('siscontratista\WorkGroup');
    }

        //Obtener el grupo de trabajo al que se le asigno la categoria del ticket
        public function nameWorkgroup()
        {
            return $this->workgroup()->first()->name;
        }
    

    //Obtener el nombre del Rol del Usuario
    public function nameTicketCategory()
    {
        return $this->ticket_category->name;
    }

    //Obtener el Nombre del Grupo de Trabajo para una categoria de un ticket dado

    public function onlyDate()
    {
        return $this->created_at->format('d/m/Y');
    }

    //Un Ticket tiene muchas Fases de Ticket asociadas
    public function stage_histories()
    {
        return $this->hasMany('siscontratista\StageHistory');
    } 

    public function user()
    {
        return $this->belongsTo('siscontratista\User','client_id','id');
    }

    public function district()
    {
        return $this->user->district;
    }

    public function city()
    {
        return $this->user->district->city;
    }

    public function province()
    {
        return $this->user->district->city->province;
    }

    public function country()
    {
        return $this->user->district->city->province->country;
    }

    public function address()
    {
        return $this->user->address .", ". $this->user->district->name .", ". $this->user->city()->name .", ". $this->user->province()->name .", ". $this->user->country()->name;

    }

    public function client()
    {
        return $this->user->name .", ". $this->user->surname;
    }

    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    //Los mensajes de un Ticket
    public function message_tickets()
    {
        return $this->hasMany('siscontratista\MessageTicket');
    }

    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/
    public function consumptions()
    {
        return $this->hasMany('siscontratista\Consumption');
    }

    //Los mensajes de un Ticket
    public function messages()
    {
        return $this->hasMany('siscontratista\Message');
    }

    //cuenta la cantidad de mensajes que tiene un determinado Ticket
    public function countMessage()
    {
        return $this->messages->count();
    }

    //Obtener el ultimo estado de mi Ticket
    public function ticketStage()
    {
        return $this->stage_histories->ticket_stage->first();
    } 

    //Devuelve el ultimo estado de mi Ticket
    public function ticket_stage()
    {
        return $this->belongsTo('siscontratista\TicketStage','current_stage_id','id');
    }

    public function current_stage()
    {
        return $this->belongsTo('siscontratista\TicketStage');
    }

}
