<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'       =>  'required',
            'category_id'       =>  'required',
            'stock'             =>  'required',
            'smin'              =>  'required',
            'smax'              =>  'required',
            'photo'             =>  'required',
            'bundle_name'       =>  'required',
            'bundle_quantity'   =>  'required',
        ];
    }
}
