<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'desde' => 'nullable|before_or_equal:hasta',
            'hasta' => 'nullable|after_or_equal:desde',
            'ticket_category_id' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'desde.before_or_equal'           =>  'La Fecha Desde debe ser una fecha anterior o igual a la Fecha Hasta',
            'hasta.after_or_equal'            =>  'La Fecha Hasta debe ser una fecha posterior o igual a Fecha Desde',



        ];
    } 



}
