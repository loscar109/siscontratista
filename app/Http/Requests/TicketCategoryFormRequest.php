<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketCategoryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|string|max:255|unique:ticket_categories',
            'description' => 'required',
            'workflow_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'                  =>  'El nombre de la Categoría es requerida',
            'name.unique'                    =>  'El nombre de la Categoría :attribute ya existe',
            'description.required'           =>  'La descripción de la Categoría es requerida',
            'workflow_id.required'           =>  'Seleccione un Flujo de Trabajo'


        ];
    } 



    public function attributes()
    {
    
        return [
        'name' => "<b>$this->name</b>",
        ];
    }
}
