<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserWorkGroupFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'workgroup_id' =>'nullable',
            'user_id'      =>'required',
            'ticket_category_id'=>'required'
        ];
    }
}
