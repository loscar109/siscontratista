<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        return [
            'description'=>'required|min:6|max:70',
            'ticket_category_id'=>'required',
            'current_stage_id',
            'workgroup_id',
            'priority'=>'integer|min:1|max:5',
            'client_id' => 'required',
            'detail'

            ];
    }

    public function messages()
    {
        return [
            'description.required'           =>  'La description del Ticket :attribute es requerida',
            'description.max'                =>  'El Ticket :attribute no debe superar los :max caracteres',
            'description.min'                =>  'El Ticket :attribute debe tener al menos :min caracteres',
            'description.unique'             =>  'El Ticket :attribute ya existe ',
            'ticket_category_id.required'    =>  'Debe seleccionar una Categoría' 


        ];
    } 



    public function attributes()
    {
    
        return [
        'description' => "<b>$this->description</b>",
        ];
    }
}
