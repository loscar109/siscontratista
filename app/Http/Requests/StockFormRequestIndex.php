<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StockFormRequestIndex extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_to'       =>  'nullable|date|after_or_equal:date_of_entry',
            'date_of_entry' =>  'nullable|date'
        ];
    }

    public function messages()
    {
        return [
            'date_to.after_or_equal:date_of_entry'           =>  'La fecha debe ser igual o posterior a la fecha de entrada',
        ];
    } 

    public function attributes()
    {
    
        return [
        'date_to' => "<b>$this->date_to</b>",
        ];
    }

}
