<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequisitionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity'      => 'required',
            'material_id'   => 'required'

        ];
    }

    public function messages()
    {
        return [
            'quantity.required'     =>  'Debe ingresar una cantidad',
            'material_id.required'  =>  'Debe seleccionar un material',


        ];
    } 
}
