<?php

namespace siscontratista\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkFlowFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:workflows',
            'description' => 'required',
            'initial_stage_id'=>'required'

        ];
    
    }

    public function messages()
    {
        return [
            'name.required'                  =>  'El nombre del Flujo de Trabajo es requerido',
            'name.unique'                    =>  'El Flujo de Trabajo :attribute ya existe',
            'description.required'           =>  'La descripcion del Flujo de Trabajo es requerida'
        ];
    } 

    public function attributes()
    {
    
        return [
        'name' => "<b>$this->name</b>",
        ];
    }

}
