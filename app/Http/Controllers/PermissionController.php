<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Permission;
use siscontratista\Http\Requests\PermissionFormRequest;
use Illuminate\Support\Facades\Redirect;
use DB;

class PermissionController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));
            $permissions=DB::table('permissions')
                ->where('name','LIKE','%'.$query.'%')
                ->orderBy('name','asc')
                ->paginate(3);
            return view('permissions.gestion.index',["permissions"=>$permissions,"searchText"=>$query]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("permissions.gestion.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionFormRequest $request)
    {
        $permissions=new Permission;
        $permissions->name=$request->get('name');
        $permissions->slug=$request->get('slug');
        $permissions->description=$request->get('description');
        $permissions->save();
        return Redirect::to('permissions/gestion');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return view("permissions.gestion.show",["permissions"=>Permission::findOrFail($id)]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("permissions.gestion.edit",["permissions"=>Permission::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionFormRequest $request, $id)
    {
        $permissions=Permission::findOrFail($id);        
        $permissions->name=$request->get('name');
        $permissions->slug=$request->get('slug');
        $permissions->description=$request->get('description');
        $permissions->save();
        return Redirect::to('permissions/gestion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permissions=Permission::findOrFail($id);
        try{
            Permission::destroy($id);
            $permissions->update();
            return Redirect::to('permissions/gestion');
        }catch(\Illuminate\Database\QueryException $e){
            return Redirect::to('permissions/gestion');
        }
       
    }
}

