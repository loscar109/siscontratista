<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\TicketCategory;
use siscontratista\WorkGroup;
use siscontratista\Workflow;




use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\TicketCategoryFormRequest;

use DB;
use Session;


class WorkGroupController extends Controller
{

    public function showAverage(Request $request)
	{	
        
        $wg=WorkGroup::find($request->id);
        if( $wg->minExperience() )
        {
            return response()->json('Promedio de resolución de tickets del grupo en dias: ' . $wg->average_time);

        }
        else 
        {
            return response()->json("Grupo relativamente nuevo con pocos o ningún ticket terminado");

        }



	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
