<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;




use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;

use siscontratista\User;
use siscontratista\Ticket;
use siscontratista\TicketStage;
use siscontratista\TicketCategory;
use siscontratista\WorkGroup;
use siscontratista\StageHistory;
use siscontratista\Workflow;
use siscontratista\Material;

use siscontratista\UserWorkGroup;
use siscontratista\TicketStageOrder;



use Illuminate\Support\Facades\Input;

use Response;
use Illuminate\Support\Collection;
use DB;
use DateTime;
use Session;






class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if( auth()->user()->nameRoleUser()=="ADMIN" || auth()->user()->nameRoleUser()=="JEFE" || auth()->user()->nameRoleUser()=="CLIENTE")
            {
                $resolved_tickets=0;
                $count_resolved_tickets=0;
                $no_resolved_tickets=0;
                $count_no_resolved_tickets=0;
              

                $days_agruped=array();
                $tickets=Ticket::select('tickets.*')
                ->join('ticket_categories as tc','tickets.ticket_category_id','tc.id')
                ->join('work_groups as wg','wg.ticket_category_id','tc.id')
                ->join('user_work_groups as uwg','uwg.workgroup_id','wg.id')
                ->where('uwg.user_id',auth()->id())
                ->get();
    
            }
            elseif (auth()->user()->nameRoleUser()=="AUDITOR")
            {
                return redirect()->route('audits.user.home');
            }
            
        if(auth()->user()->nameRoleUser()=="EMPLEADO") 
            {
                if(auth()->user()->user_work_groups() == null)
                {
                    return view('pertenecer');
                }
                $workgroup = auth()->user()->user_work_groups()->workgroup_id;

                $month = now()->format('m');
                $year = now()->format('Y');
                $tickets = Ticket::whereWorkgroup_id($workgroup)->get();
                                //                $resolved_tickets = Ticket::whereWorkgroup_id($workgroup)->where('current_stage_id','=',4)->where('updated_at',">=",now()->subDays(30))->get();

                $resolved_tickets = Ticket::whereWorkgroup_id($workgroup)->where('current_stage_id','=',4)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->orderBy('updated_at')->get();
                $no_resolved_tickets = Ticket::whereWorkgroup_id($workgroup)->where('current_stage_id','=',5)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->orderBy('updated_at')->get();
                $count_resolved_tickets = $resolved_tickets->count();
                $count_no_resolved_tickets =  $no_resolved_tickets->count();

                //creo un array donde voy a almacenar la cadena de valores (dias)
                $days=array();
                foreach ($resolved_tickets as $resolved_ticket) //recorro los tickets resueltos del grupo
                {

                    $days[]=$resolved_ticket->time_resolved;    //almaceno en la cadena el tiempo en (dias) que tardo en resolver el ticket
                    

                }
                //array_count_values($data) --> Retorna un array asociativo de valores a partir de array como keys y su respectivo recuento como valores. 
                $days_agruped=array_count_values($days);
                /*     dd($days);
                    [0]     =>  1
                    [1]     =>  1
                    [2]     =>  2
                    [3]     =>  2
                    [4]     =>  3
                    [5]     =>  3
                    [6]     =>  3
                    [7]     =>  1
                    [8]     =>  1
                    [9]     =>  1
                    [10]    =>  1
                    [11]    =>  1
                
                


                $verdad = true;
                if($days[2]==3)
                {
                    dd($verdad);
                }
                else {
                    $verdad = false;
                    dd($verdad);
                }
                */ 
                
         

            }

        else 
        {
            $resolved_tickets=0;
            $count_resolved_tickets=0;
            $no_resolved_tickets=0;
            $count_no_resolved_tickets=0;
          

            $days_agruped=array();
            $tickets=Ticket::select('tickets.*')
            ->join('ticket_categories as tc','tickets.ticket_category_id','tc.id')
            ->join('work_groups as wg','wg.ticket_category_id','tc.id')
            ->join('user_work_groups as uwg','uwg.workgroup_id','wg.id')
            ->where('uwg.user_id',auth()->id())
            ->get();
        }
        

    
        
        $category = TicketCategory::all();
        $allTicket = array();
        foreach($tickets as $t)
            $allTicket[$t->current_stage_id][] = $t;
        /*foreach($allTicket as $id=>$value)    
            $allTicket[$id]['category'] = TicketCategory::find($id);*/

        //dd($allTicket);


        $tabla = DB::table('materials')
            ->select('materials.*')
            ->whereRaw('materials.stock < materials.smin')
            ->orderBy('materials.real_stock','asc')
            ->get();

        $grafico =$tabla->take(5);

       
        

        return view('home',[
            'allTicket'                 =>  $allTicket,
            'tabla'                     =>  $tabla,
            'grafico'                   =>  $grafico,
            "resolved_tickets"          =>  $resolved_tickets,
            "no_resolved_tickets"       =>  $no_resolved_tickets,
            "count_resolved_tickets"    =>  $count_resolved_tickets,
            "count_no_resolved_tickets" =>  $count_no_resolved_tickets,
            "days_agruped"              =>  $days_agruped,

            ]);
        
    }

  
 
}