<?php

namespace siscontratista\Http\Controllers;


use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Ticket;
use siscontratista\TicketStage;
use siscontratista\TicketCategory;
use siscontratista\WorkGroup;
use siscontratista\StageHistory;
use siscontratista\Workflow;
use siscontratista\User;
use siscontratista\UserWorkGroup;
use siscontratista\TicketStageOrder;
use siscontratista\Material;
use siscontratista\Message;
use siscontratista\MessageTicket;
use siscontratista\Movement;


use siscontratista\Category;
use siscontratista\Consumption;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;


use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\WorkFormRequest;
use siscontratista\Http\Requests\MessageFormRequest;

use Response;
use Illuminate\Support\Collection;
use DB;
use Session;

class WorkController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        //obtengo el id del usuario loggeado
        //auth()->user() = obtengo el objeto usuario
        //->user_work_groups()->workgroup_id relacion del metodo y el atributo

        $workgroup = auth()->user()->user_work_groups()->workgroup_id;
        $tickets = Ticket::whereWorkgroup_id($workgroup)->orderBy('priority','desc')->get();
        //dd($tickets);
        return view('works.index',["tickets"=>$tickets]);
        
    }

   
    public function inProccess(Request $request)
    {   
  

        $workgroup = auth()->user()->user_work_groups()->workgroup_id;
        $tickets = Ticket::whereWorkgroup_id($workgroup)
            ->whereIn('current_stage_id',[2, 3, 6]) //Donde el estado sea "Revisado" o/y "En Proceso"
            ->orderBy('priority','desc')->get();
            
        return view('works.inProccess',["tickets"=>$tickets]);
        
    }
    public function resolved(Request $request)
    {   
        $workgroup = auth()->user()->user_work_groups()->workgroup_id;
        $tickets = Ticket::whereWorkgroup_id($workgroup)
            ->where('current_stage_id','=',4)
            ->orderBy('priority','desc')->get();
        return view('works.inProccess',["tickets"=>$tickets]);
        
    }

    public function created(Request $request)
    {   
        $workgroup = auth()->user()->user_work_groups()->workgroup_id;
        $tickets = Ticket::whereWorkgroup_id($workgroup)
            ->where('current_stage_id','=',1)//Donde el estado sea "Creado"
            ->orderBy('priority','desc')->get();
        return view('works.created',["tickets"=>$tickets]);
        
    }



    public function show(Ticket $ticket)
    {        
        

        //necesito para el modal de enviar mensaje cuando seleciono todos los usuarios
        $users=DB::table('users as us')
            ->join('role_user as ru','ru.user_id','=','us.id')
            ->join('roles as ro','ru.role_id','=','ro.id')
            ->join('user_work_groups as uwg', 'uwg.user_id','us.id')
            ->join('work_groups as wg', 'uwg.workgroup_id','wg.id')

            ->select('us.id', DB::raw('CONCAT(us.name," ",us.surname," - ","(",ro.name,")") AS superior'))
            ->where('us.id','!=',auth()->id())  //que no sea yo mismo
            //->where('ro.id','!=',1)             //que no sea un administrador
            ->where('ro.id','!=',4)             //que no sea un cliente
            ->where('ro.id','!=',5)             //que no sea un auditor


            ->get();

        //Busco el workflow
        $workflow = $ticket->ticket_category->workflow_id;

        //Voy a buscar todos los mensajes  asociados a un ticket
        $message_tickets = MessageTicket::whereTicket_id($ticket->id)->orderBy('created_at','asc')->get(); //obtengo los mensajes (bandeja de entrada:solo los que recivo)  de ese Ticket
        //dd($message_tickets);


        $tso = TicketStageOrder::whereWorkflow_id($workflow)
            ->whereCurrent_id($ticket->current_stage_id)->get();
        foreach($tso as $stage)
        {   
            if($stage->next->name == 'Revisado')
            {
                $stage_history = new StageHistory;
                $stage_history->ticket_stage_id = $stage->next_id;
                $stage_history->date = now()->format('Y-m-d H:i:s');
                $stage_history->ticket_id = $ticket->id;
                $stage_history->user_id = auth()->id();

                $stage_history->save();

                $ticket->update(['current_stage_id'=>$stage->next_id]);

                $tso = TicketStageOrder::whereWorkflow_id($workflow)
                    ->whereCurrent_id($ticket->current_stage_id)->get();

                    return view ('works.show',compact('ticket','tso','users','message_tickets'));

            }

        }

        return view ('works.show',compact('ticket','tso','users','message_tickets'));

    }

    public function updateStage(Ticket $ticket, TicketStage $ticket_stage)
    {
        //Busco el workflow
        $workflow = $ticket->ticket_category->workflow_id;

        //Busco si la transicion es valida
        $transition = TicketStageOrder::whereWorkflow_id($workflow)
            ->whereCurrent_id($ticket->current_stage_id)
            ->whereNext_id($ticket_stage->id)
            ->first();
        
        //Si no es valida redirecciona y corta el flujo
        if(!$transition)
        {
            return Redirect::to('works/inProccess')->withErrors("Cambio de Estado no permitido");

        }

        //si es terminado me pido que me diga los materiales usados
        if($ticket_stage->id == 4)
        {
            
            return Redirect::to('works/'.$ticket->id.'/finish');
        }


        //Si es valida crea un historial de estado, se actualiza el estado del ticket y redirecciona al indice
        $stage_history = new StageHistory;
        $stage_history->ticket_stage_id = $ticket_stage->id;
        $stage_history->date = now()->format('Y-m-d H:i:s');
        $stage_history->ticket_id = $ticket->id;


        $stage_history->user_id = auth()->id();

        $stage_history->save();
        $ticket->update(['current_stage_id'=>$ticket_stage->id]);
        if($ticket->current_stage_id == 5)
        {
            $days = $ticket->created_at->diffInDays(Carbon::now());
          
            $days++;

            //Si me da cero incremento ese dia a 1
            if ($days == 0)
            {
                $days = 1;
            }

            $ticket->time_not_resolved = $days;
            $ticket->save();
            Session::flash('ticket_not_resolved','El ticket '.$ticket->description. ' no pudo ser resuelto');

            return Redirect::to('home');    

        }
        return Redirect::to('works/'.$ticket->id);    

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reprogramming(Request $request, Ticket $ticket, TicketStage $ticket_stage)
    {

        //Busco el workflow
        $workflow = $ticket->ticket_category->workflow_id;

        //Busco si la transicion es valida
        $transition = TicketStageOrder::whereWorkflow_id($workflow)
            ->whereCurrent_id($ticket->current_stage_id)
            ->whereNext_id($ticket_stage->id)
            ->first();
        
        //Si no es valida redirecciona y corta el flujo
        if(!$transition)
        {
            return Redirect::to('works')->withErrors("Cambio de Estado no permitido");

        }

        

        //Si es valida crea un historial de estado, se actualiza el estado del ticket y redirecciona al indice
        $stage_history = new StageHistory;
        $stage_history->ticket_stage_id = $ticket_stage->id;
        $stage_history->date = now()->format('Y-m-d H:i:s');
        $stage_history->ticket_id = $ticket->id;
        $stage_history->user_id = auth()->id();

        $stage_history->save();
        $ticket->update([
            'current_stage_id'=>$ticket_stage->id,
            'date'=>$request->date
            ]);
        return Redirect::to('works/inProccess');    
    }

    public function findMaterial(Request $request)
	{	
	    $materials=Material::select('description','id')
			->where('category_id',$request->id)
			->take(10)
            ->get();
        return response()->json($materials);
	}

  
    public function finish(Ticket $ticket)
    {
        //busco los materiales para completar los desplegables
        $categories = Category::all();
        $materials = Material::all();


        return view ('works.finish',compact('categories','materials','ticket'));
    }

    public function storeFinished(Request $request, Ticket $ticket)
    {
        foreach($request->material_id as $key=>$value)
        {
            $consumptions = new Consumption;
            $consumptions->quantity = $request->cantidad[$key];
            $consumptions->dateOfConsumption = now()->format('Y-m-d H:i:s');
            $consumptions->ticket_id = $ticket->id;
            $consumptions->material_id = $value;
            $material =Material::find($value);
            $material->decrement('stock',$request->cantidad[$key]);       
            $material->update(['real_stock'=>(($material->stock*100)/$material->smin)]);
            $material->update(['missing_stock'=>(100-($material->real_stock))]);

            $consumptions->save();


            //carga de un movimiento cuando se crea un material
            $movimiento = new Movement;
            $movimiento->date=$consumptions->dateOfConsumption;
            $movimiento->comprobante='Ticket';
            $movimiento->number=$ticket->number;
            $movimiento->quantity=-$consumptions->quantity;
            $movimiento->stock= $material->stock;
            $movimiento->user_id=auth()->id();
            $movimiento->material_id=$material->id;
            $movimiento->save();



        }
  
        $stage_history = new StageHistory;
        $stage_history->ticket_stage_id = 4; //Resuelto
        $stage_history->date = now()->format('Y-m-d H:i:s');
        $stage_history->ticket_id = $ticket->id;
        $stage_history->user_id = auth()->id();
        $stage_history->save();

        //Dias en que tardo en resolver el tickeIF
        $days = $ticket->created_at->diffInDays(Carbon::now());
        /*
            el problema radica cuando 
             - si creo el ticket el 09/01 y termino ese dia 09/01 me 
            diria que finalice en 1 dia (por la funcion de abajo)

            - si creo el ticket el 09/01 y temrino el 10/01 me diria que
            finalice en 1 dia (porque su valor no es 0, se omite la funcion de abajo)

            SOLUCION: sumar +1 al dia inmediatatamente cuando se finaliza el ticket

        */ 
        $days++;

        //Si me da cero incremento ese dia a 1
        if ($days == 0)
            {
                $days = 1;
            }


        $ticket->update([
            'current_stage_id'=>4,
            'time_resolved'=>$days
            ]);


        $wg = $ticket->workgroup;

        $wg->update([
            'average_time'=> $wg->estimatedTime()
        ]);

           







        Session::flash('store_finished','El ticket '.$ticket->description. ' a sido terminado exitosamente');
        return Redirect::to('home');  
      
    }

    


    public function message(MessageFormRequest $request, Ticket $ticket)
    {

        //dd($request->all());

        

        $message_tickets = new MessageTicket;
        $message_tickets->text=$request->get('text');
        $message_tickets->ticket_id=$ticket->id;
        $message_tickets->user_id = auth()->id();
        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/imagenes/photo/',$file->getClientOriginalName());
            $message_tickets->image=$file->getClientOriginalName();
        }
        $message_tickets->save();

        Session::flash('success_message','El mensaje '.str_limit($message_tickets->text, 7). ' ha sido enviado correctamente');
        return Redirect::to('works/'.$ticket->id);
    }

    public function findMaterialData(Request $request)
    {
        $material=Material::find($request->id);
        $retorno = [
            'stock' =>  $material->stock,
            'photo' =>  asset('imagenes/materiales/'.$material->photo),


        ];
        return response()->json($retorno);

    }
    public function send($id)
    {
        $allSend = Message::whereTicket_id($id)->whereUser_id(auth()->id())->get();
        return view('works.send',["allSend"=>$allSend]);


    }

}
