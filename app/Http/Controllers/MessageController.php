<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Message;
use siscontratista\Ticket;
use siscontratista\UserWorkGroup;

use siscontratista\User;
use Illuminate\Support\Facades\Input;



use Illuminate\Support\Facades\Redirect;

use DB;
use Session;
use Auth;

class MessageController extends Controller
{
    public function index(Request $request)
    {

       //$messages = Message::all();
        //dd($messages);
        $user = auth()->id();
        $messages = Message::whereTo_id($user)->get();

        return view('messages.gestion.index',["messages"=> $messages]);
    }

    public function show($id)
    {        
           //necesito para el modal de enviar mensaje cuando seleciono todos los usuarios
           $users=DB::table('users as us')
           ->join('role_user as ru','ru.user_id','=','us.id')
           ->join('roles as ro','ru.role_id','=','ro.id')
           ->select('us.id', DB::raw('CONCAT(us.name," ",us.surname," - ","(",ro.name,")") AS superior'))
           ->where('us.id','!=',auth()->id())  //que no sea yo mismo
           ->where('ro.id','!=',1)             //que no sea un administrador
           ->where('ro.id','!=',4)             //que no sea un cliente
           ->where('ro.id','!=',5)             //que no sea un auditor


           ->get();


        $message=Message::findOrFail($id);

        if($message->read == false)
        {
            $message->update(['read'=>true]);
        }

        //dd($message);
        return view ('messages.gestion.show',compact('message','users'));

    }



    public function create()
    {
        $users=DB::table('users as us')
        ->join('role_user as ru','ru.user_id','=','us.id')
        ->join('roles as ro','ru.role_id','=','ro.id')
        ->select('us.id', DB::raw('CONCAT(us.name," ",us.surname," - ","(",ro.name,")") AS superior'))
        ->where('us.id','!=',auth()->id())  //que no sea yo mismo
        ->where('ro.id','!=',4)             //que no sea un cliente
        ->where('ro.id','!=',5)             //que no sea un auditor
        ->whereIn('ro.id',[1, 2, 3])             //que sea un empleado
        ->get();

        return view('messages.gestion.create',["users"=>$users]);
    }

    //abrir los mensjaes enviados
    public function open(Message $send)
    {

        return view('messages.gestion.open',['send'=>$send]);
    }


    //abrir los mensajes de bandeja
    public function openBandeja(Message $message)
    {
        

        if($message->read == false)
        {
            $message->update(['read'=>true]);
        }




        return view('messages.gestion.openBandeja',[
            'message'       =>  $message,
            
           ]);
        

    }

    public function answer(Message $message)
    {
        $asunto = "Re: " . $message->subject; //asunto
        $destinatario = User::whereId($message->user_id)->first(); //destinatario: Pablo
        $id_destinatario = $destinatario->id;
        $emisor = User::whereId($message->to_id)->first(); //emisor del mensaje
        return view('messages.gestion.answer',[
            "message"           =>  $message,
            "asunto"            =>  $asunto,
            "destinatario"      =>  $destinatario,
            "emisor"            =>  $emisor,
            "id_destinatario"   =>  $id_destinatario
            ]);
    }

    public function storeanswer(Request $request, Message $message)
    {
        $m = new Message;
        $m->subject=$request->get('subject');
        $m->text=$request->get('text');
        $m->to_id=$request->get('to_id');
        $m->user_id=auth()->id();
        $m->read=false;
        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/imagenes/photo/',$file->getClientOriginalName());
            $m->image=$file->getClientOriginalName();
        }
        $m->save();

        Session::flash('message_ticket_category','El mensaje '.$m->subject. ' se creó con éxito');
        return Redirect::to('messages/gestion');    
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    


        $message = new Message;
        $message->subject=$request->get('subject');
        $message->text=$request->get('text');
        $message->to_id=$request->get('to_id');
        $message->user_id=auth()->id();
        $message->read=false;
        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/imagenes/photo/',$file->getClientOriginalName());
            $message->image=$file->getClientOriginalName();
        }
        $message->save();

        Session::flash('store_message','El mensaje '.$message->subject. ' se envió con éxito');
        return Redirect::to('messages/gestion');    
    }

    public function message(Request $request, Ticket $ticket, $id)
    {
        
        $m=Message::findOrFail($id);
      


        $message = new Message;
        $message->to_id=$request->get('to_id');
        $message->subject=$request->get('subject');
        $message->text=$request->get('text');
        $message->ticket_id=$m->ticket_id;
        $message->user_id = auth()->id();
        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/imagenes/photo/',$file->getClientOriginalName());
            $message->image=$file->getClientOriginalName();
        }
        $message->save();

        Session::flash('success_message','El mensaje '.$message->subject. ' ha sido enviado correctamente');
        
        return Redirect::to('messages/gestion');
    }


    public function allSend()
    {
        //usuario autenticado
        $user = auth()->id();
        $sends = Message::whereUser_id($user)->get();

        return view('messages.gestion.allSend',['sends'=>$sends]);

    }
}
