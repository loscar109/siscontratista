<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Requisition;
use siscontratista\RequisitionDetail;

use siscontratista\Reception;
use siscontratista\ReceptionDetail;

use siscontratista\Material;
use siscontratista\User;
use siscontratista\Category;

use siscontratista\Movement;

use Barryvdh\DomPDF\Facade as PDF;




use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use siscontratista\Http\Requests\EntryFormRequest;
use siscontratista\Http\Requests\StockFormRequestIndex;

use siscontratista\Http\Requests\RequisitionFormRequest;

use DB;
use Response;
use Illuminate\Support\Collection;
use Session;




class StockController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth');
    }

    public function findNameMaterial(Request $request)
	{	
	    $materials=Material::select('description','id')
			->where('category_id',$request->id)
			->take(10)
            ->get();
        return response()->json($materials);
    }
    


    public function findMaterialData(Request $request)
    {
        $material=Material::find($request->id);
        $retorno = [
            'bundle_quantity'   =>  $material->bundle_quantity,
            'smax'              =>  $material->smax,
            'stock'             =>  $material->stock,
            'photo'             =>  asset('imagenes/materiales/'.$material->photo),
            
            

        ];
        return response()->json($retorno);

    }




    public function exportPdf($id)
    {
        $requisitions=Requisition::findOrFail($id);     
        $pdf = PDF::loadView('requisitions.gestion.pdf',["requisitions"=> $requisitions]);
        $pdf->setPaper('a4','letter');
        return $pdf->stream('requisition.pdf');
        /*$requisitions = Requisition::all();
 
        
        $pdf = PDF::loadView('requisitions.gestion.pdf',["requisitions"=> $requisitions]);
        $pdf->setPaper('a4','letter');

        return $pdf->stream('requisition-list-pdf');*/
    }

   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $desde=$request->desde;
        $hasta=$request->hasta;

        if(count($request->all())>0)       
        {
            $sql = Requisition::select('requisitions.*');
            if($desde)
            {
                $sql = $sql->whereDate('created_at','>=',$desde);

            }
            if($hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$hasta);

            }
            $requisitions=$sql->orderBy('created_at','desc')->get();



        }
        else
        {
            $requisitions=Requisition::orderBy('created_at','desc')->get();

        }
        


        return view('requisitions.gestion.index',[
            'requisitions' => $requisitions,
            "desde"        =>  $desde,
            "hasta"        =>  $hasta,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $critico=Material::whereRaw('stock < smin')->whereIsOrdered(false)->get();
        //dd($materials);
        
        /*TODO:
            - deberia pasar los materiales que esten por encima del smax?*/
        $materials=Material::whereRaw('stock <= smax')->whereIsOrdered(false)->get();
       

        return view("requisitions.gestion.create",[
            "materials"     =>  $materials,
            "critico"       =>  $critico
            ]);    
    }
            

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequisitionFormRequest $request)
    {

            $n=Requisition::count() + 1;

            $requisitions=new Requisition;
            $requisitions->date_of_entry=now()->format('Y-m-d H:i:s');
            $requisitions->number=str_pad($n, 10, '0', STR_PAD_LEFT);
            $requisitions->user_id=auth()->user()->id;
            $requisitions->save();

            $material_id=$request->get('material_id');
            $quantity=$request->get('quantity');

            $cont = 0;

            while ($cont < count($material_id)) 
            {
                $requisition_details                        =       new RequisitionDetail();
                $requisition_details->requisition_id      =       $requisitions->id;
                $requisition_details->material_id           =       $material_id[$cont];
                $requisition_details->quantity              =       $quantity[$cont];
                Material::find($material_id[$cont])->update(['is_ordered'=>true]);

                $requisition_details->save();
                $cont=$cont+1;
            }

        Session::flash('store_requisition','El pedido numero '.$requisitions->number. ' se creó con éxito');
        return Redirect::to('requisitions/gestion');

    }

 


    //carga de producto (automatizada)
    public function automatic(Request $request, Requisition $requisition)
    {
        $request->validate([
            'dispatch_number' => 'required|digits:10|unique:receptions',
            'dispatch_date'   => 'required|date|before_or_equal:today'

        ],[
            'dispatch_number.unique' => 'el numero de remito ya fue ingresado',
            'dispatch_number.required' => 'el numero de remito es requerido',
            'dispatch_date.required' => 'la fecha es requerida',
            'dispatch_date.before_or_equal' => 'la fecha debe ser igual o anterior a hoy',

        ]);


        if($request->dispatch_date < $requisition->date_of_entry->format('Y-m-d'))
        {
            return Redirect::to('requisitions/gestion')->withErrors("La fecha de Remito no puede ser anterior a la fecha de pedido");
        }

        //creo una nueva recepcion tomano los valores de mi requisition
        $reception = new Reception;
        $reception->dispatch_number = $request->dispatch_number;
        $reception->dispatch_date = $request->dispatch_date;
        $reception->date_of_entry=now()->format('Y-m-d');
        $reception->user_id=auth()->id();
        $reception->requisition_id=$requisition->id;
        $reception->save();
        
        foreach($requisition->requisition_details as $r)
        {
            $m = Material::find($r->material_id);

            $reception_detail = new ReceptionDetail;
            $reception_detail->quantity=$r->quantity*$m->bundle_quantity;
            $reception_detail->material_id=$r->material_id;
            $reception_detail->reception_id=$reception->id;
            $reception_detail->save();
            $m->update([
                //Se actualiza el stock pero no me lo incrementa
                'stock'=>$m->stock + $reception_detail->quantity,
                'is_ordered'=>false
                ]
            );
            $m->update(['real_stock'=>(($m->stock*100)/$m->smin)]);
            $m->update(['missing_stock'=>(100-($m->real_stock))]);

            //carga de un movimiento cuando se crea un material
            $movimiento = new Movement;
            $movimiento->date=$reception->date_of_entry->format('Y-m-d');
            $movimiento->comprobante='Remito';
            $movimiento->number=$reception->dispatch_number;
            $movimiento->quantity= $reception_detail->quantity;
            $movimiento->stock= $m->stock;
            $movimiento->user_id=auth()->id();
            $movimiento->material_id=$m->id;
            $movimiento->save();




        }
        $requisition->update(['is_completed'=>true]);

        Session::flash('automatic_requisition','El pedido numero '.$requisition->id. ' incrementó su stock');

        return Redirect::to('requisitions/gestion');  

      
    }



    public function entry(Request $request)
    {
        $requisitions=Requisition::all();
        $desde=$request->desde;
        $hasta=$request->hasta;

        if(count($request->all())>1)
        {
            $sql = Reception::select('receptions.*');
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->requisition_id)
            {
                $sql =$sql->whereRequisition_id($request->requisition_id);

            }
            $receptions=$sql->orderBy('created_at','desc')->get();
            $requisition_id=$request->requisition_id;


        }
        else
        {
            $requisition_id=null;
            $receptions=Reception::orderBy('created_at','desc')->get();

        }


        return view("requisitions.gestion.entry",[
            'receptions'      =>  $receptions,
            "requisition_id"  =>  $requisition_id,
            "requisitions"    =>  $requisitions,
            "desde"           =>  $desde,
            "hasta"           =>  $hasta,

            ]);     
    
    }

















    //carga de producto manual (automatizada)
    public function manual(Requisition $requisition)
    {
       //dd($requisition->requisition_details[0]->quantity);
        return view('requisitions.gestion.manual',compact('requisition'));
    }
    public function manualSave(Request $request, Requisition $requisition)
    {
        $request->validate([
            'dispatch_number' => 'required|digits:10|unique:receptions',
            'dispatch_date'   => 'required|date|before_or_equal:today'

        ],[
            'dispatch_number.unique' => 'el numero de remito ya fue ingresado',
            'dispatch_number.required' => 'el numero de remito es requerido',
            'dispatch_date.required' => 'la fecha es requerida',
            'dispatch_date.before_or_equal' => 'la fecha debe ser igual o anterior a hoy',

        ]);
        if(!is_array($request->material))
            return Redirect::to('requisitions/gestion');  

       //dd($request->all());
       $reception = new Reception;
       $reception->dispatch_number = $request->dispatch_number;
       $reception->dispatch_date = $request->dispatch_date;
       $reception->date_of_entry=now()->format('Y-m-d');
       $reception->user_id=auth()->id();
       $reception->requisition_id=$requisition->id;
       $reception->save();

       foreach($request->material as $key=>$value)
        {
            $m = Material::find($key);

            $reception_detail = new ReceptionDetail;
            $reception_detail->quantity=$request->quantity[$key]*$m->bundle_quantity;
            $reception_detail->material_id=$key;
            $reception_detail->reception_id=$reception->id;
            $reception_detail->save();
            $m->update([
                'stock'=>$m->stock + $reception_detail->quantity,
                'is_ordered'=>false
                ]
            );
            $m->update(['real_stock'=>(($m->stock*100)/$m->smin)]);
            $m->update(['missing_stock'=>(100-($m->real_stock))]);

            //carga de un movimiento cuando se crea un material
            $movimiento = new Movement;
            $movimiento->date=$reception->date_of_entry->format('Y-m-d');
            $movimiento->comprobante='Remito';
            $movimiento->number=$reception->dispatch_number;
            $movimiento->quantity= $reception_detail->quantity;
            $movimiento->stock= $m->stock;
            $movimiento->user_id=auth()->id();
            $movimiento->material_id=$m->id;
            $movimiento->save();






        }

        if($request->exists('close'))
            $requisition->update(['is_completed'=>true]);

            Session::flash('manual_requisition','El pedido numero '.$requisition->id. ' incrementó su stock');

        return Redirect::to('requisitions/gestion');  
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
