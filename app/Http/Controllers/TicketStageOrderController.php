<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\TicketStageOrder;
use siscontratista\TicketStage;



use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\TicketStageOrderFormRequest;

use DB;
use Session;
class TicketStageOrderController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $order=TicketStageOrder::all();
        
        return view('orders.gestion.index',["order"=> $order]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stage=TicketStage::all();


        return view("orders.gestion.create",["stage"=>$stage]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketStageOrderFormRequest $request)
    {

        //aca esta el problema
        $cont = 0;
        $stage_initial_id = $request->get('stage_initial_id');
        $stage_final_id = $request->get('stage_final_id');

        while ($cont < count($stage_initial_id)) 
        {
            $initial=new TicketStageOrder();
            $initial->stage_initial_id=$stage_initial_id->id;
            $userworkgroup->ticket_stage_id =$ticket_stage_id[$cont];

            $userworkgroup->save();
            $cont=$cont+1;
        }


        Session::flash('store_workgroup','El Grupo de Trabajo '.$workgroup->name. ' se creó con éxito');
        return Redirect::to('workgroups/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $workgroups=WorkGroup::findOrFail($id);


        return view("workgroups.gestion.show",["workgroups"=>$workgroups]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workgroups=WorkGroup::findOrFail($id);
        $users=User::all();
        return view("workgroups.gestion.edit",["workgroups"=>$workgroups,"users"=>$users]);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkGroupFormRequest $request, $id)
    {
   

        $workgroups=WorkGroup::findOrFail($id);
        $workgroups->name=$request->get('name');
        $workgroups->update();
        Session::flash('update_workgroup','El Grupo de Trabajo '.$workgroups->name. ' ha sido actualizado con éxito');
        return Redirect::to('workgroups/gestion'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workgroups=WorkGroup::findOrFail($id);
        try{
            WorkGroup::destroy($id);
            $workgroups->update();
            Session::flash('delete_workgroup','El Grupo de Trabajo '.$workgroups->name. ' ha sido eliminado correctamente');
            return Redirect::to('workgroups/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_workgroup_error','El Grupo de Trabajo '.$workgroups->name. ' no puede ser eliminado');
            return Redirect::to('workgroups/gestion');

        }
    }
}
