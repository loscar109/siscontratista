<?php



class TicketController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }

    public function pdf($menus, $desde, $hasta, $receta)
    {
       
        $pdf = PDF::loadView('recetas.pdf',[
            "menus"     =>  $menus,
            "receta"    =>  $receta,
            "desde"     =>  $desde,
            "hasta"     =>  $hasta,

            ]);

        $pdf->setPaper('a4','letter');


        return $pdf->stream('menus/pdf');
    }

    /*
    */ 
  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historial_menus(Request $request)
    {
        $recetas=Recetas::all();
        
        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = MenuDiario::select('menu_diarios.*');

            if($request->receta_id)
            {
                $sql = $sql->whereReceta_id($request->receta_id);
            }
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
           
            
            $menus=$sql->orderBy('created_at','desc')->get();
            $receta_id=$request->receta_id;
        

        }
        else
        {
            $receta_id=null;
           

            $menus=MenuDiario::orderBy('created_at','desc')->get();


        }
        if($request->exists('pdf'))
        {
            $colspan = 0;
            if($receta_id != NULL)
            {
                $receta=Receta::find($receta_id);
                $colspan++;
            }
            else 
            {
                $receta=NULL;
            }

           

            return $this->pdf($menus, $desde, $hasta, $receta);
        }

        return view('recetas.historialmenus',[
            "menus"                 =>  $menus,
            "receta_id"             =>  $receta_id,
            "recetas"               =>  $recetas,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta

            ]);
    }

}
