<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\User;
use siscontratista\Country;
use siscontratista\Province;
use siscontratista\City;
use siscontratista\District;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\UserFormRequest;
use siscontratista\Http\Requests\UserFormRequestEdit;
use Illuminate\Support\Facades\Input;

use DB;
use Session;
use Response;
use Illuminate\Support\Collection;

class UserController extends Controller
{

    public function __construct()
    {
       
        $this->middleware('auth');
       
    }

    
    public function findProvince(Request $request)
	{	
	    $provinces=Province::select('name','id')
			->where('country_id',$request->id)
            ->get();
        return response()->json($provinces);
	}

    public function findCity(Request $request)
	{	

		$cities=City::select('name','id')
			->where('province_id',$request->id)
			->get();
		return response()->json($cities);

    }
    
    public function findDistrict(Request $request)
	{	

		$districts=District::select('name','id')
			->where('city_id',$request->id)
			->get();
		return response()->json($districts);

	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*Resumen*
         * TODO:
         * - Muestra todos los usuarios EMPLEADOS que NO ESTEN ASIGNADOS a algun grupo de trabajo
         * FIXME:
         * - ¿Debería mostrarme solo los EMPLEADOS?
         */

        $users=User::select(
            'users.*',
            'role_user.*')
                ->join('role_user','role_user.user_id','=','users.id')
                ->where('role_user.role_id',3)
                ->get();

        return view('users.gestion.index',["users"=>$users]);
        
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=DB::table('roles')->get();
        $countries=Country::all();
        $provinces=Province::all();
        $cities=City::all();
        $districts=District::all();
        return view("users.gestion.create",[
            "roles"     =>  $roles,
            "countries" =>  $countries,
            "provinces" =>  $provinces,
            "cities"    =>  $cities,
            "districts" =>  $districts,
            ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        if(Input::hasFile('imagen')){
            $file=Input::file('imagen');
            $directory='/imagenes/empleados/';
            $archivo=$file->getClientOriginalName();
            $file->move(public_path() . $directory,$archivo);
           
        }
    
        $request->merge([
            'name'=>strtoupper($request->name),
            'surname'=>strtoupper($request->surname),
            'password'=>bcrypt($request->password),
            'email'=>strtolower($request->email),
            'photo'=>$directory . $archivo,
        ]);
            //dd($request->all());
        $users = User::create($request->all());
        $users->roles()->sync($request->get('roles'));
        

       

        
       // $users->save();

        Session::flash('store_user','El usuario '.$users->name. ' se creó con éxito');
        return Redirect::to('users/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $users=User::findOrFail($id);


        return view("users.gestion.show",["users"=>$users]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users=User::findOrFail($id);
        $roles=DB::table('roles')->get();
        return view("users.gestion.edit",["users"=>$users,"roles"=>$roles]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequestEdit $request, $id)
    {
        $users=User::findOrFail($id);
        $users->update($request->all());
        $users->roles()->sync($request->get('roles'));
        $users->name=$request->get('name');
        $users->email=$request->get('email');
        $users->password=bcrypt($request->get('password'));
        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/imagenes/users/',$file->getClientOriginalName());
            $users->image=$file->getClientOriginalName();
        }
        $users->save();
        Session::flash('update_user','El usuario '.$users->name. ' ha sido actualizado con éxito');
        return Redirect::to('users/gestion');    
    }


    /**
     * TODO:
     * - Elimina de forma lógica al usuario de tipo EMPLEADO seleccionado
     * FIXME:
     * - ¿Debería eliminarme lógicamente solo los EMPLEADOS?
     */   
    public function destroy($id)
    {

        $user=User::findOrFail($id);
        try{
            User::destroy($id);
            $user->update();
            Session::flash('delete_user','El Usuario '.$user->name. ' ha sido dado de baja correctamente');
            return Redirect::to('users/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_user_error','El Usuario '.$user->name. ' no puede ser eliminado');
            return Redirect::to('users/gestion');

        }
           
    }

}
