<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\TicketCategory;
use siscontratista\WorkGroup;
use siscontratista\Workflow;




use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\TicketCategoryFormRequest;

use DB;
use Session;

class TicketCategoryController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showWorkGroup(Request $request)
	{
        //debo obtener la cantidad de tickets asignados por grupos de trabajo
        //dd($request);
	
	    $query=WorkGroup::select('name','average_time','id')
            ->where('ticket_category_id',$request->id)
            ->get();

        //creo un array
        $carga=array();

        //workgroup
        $workgroups = array();

        //recorro el grupo de trabajo pasandole el id y abriendo los tickets que no esten terminados, a estos los multiplico por el promedio 
        foreach($query as $q)
        {
            $carga[$q->id]=$q->open_tickets()*$q->average_time;
            $workgroups[$q->id] = $q;
        }
        //Declaro a min
        $min = min($carga);



        foreach($carga as $id=>$c)
        {
            if($c == $min)
            {
                $workgroups[$id]['recomendado']=true;
            }
            else
            {
                $workgroups[$id]['recomendado']=false;
            }    
        }

        $retorno = "";
        foreach($workgroups as $id=>$w)
        {
            $retorno=$retorno.'<option value="'.$id.'">'.$w->name.' ';
            if($w->recomendado)
            {
                $retorno=$retorno.'(recomendado)</option>';
            }
            else
            {
                $retorno=$retorno.'</option>';
            }


                                

        };


        return response()->json($retorno);
    }
    
   

    /*public function showWorkflow(Request $request)
	{	

        $ticket_category = TicketCategory::find($request->id);

        return response()->json($ticket_category->workflows);
	}
*/
    public function index(Request $request)
    {

        $ticket_categories = TicketCategory::all();
        
        return view('configuracion.ticket_category.index',["ticket_categories"=>$ticket_categories]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workflows = Workflow::all();
        return view('configuracion.ticket_category.create',["workflows"=>$workflows]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketCategoryFormRequest $request)
    {
    


        $ticket_category = TicketCategory::create($request->all());
        $ticket_category->name=$request->get('name');
        $ticket_category->description=$request->get('description');
        $ticket_category->workflow_id=$request->get('workflow_id');

        $ticket_category->save();

        Session::flash('store_ticket_category','La categoria '.$ticket_category->name. ' se creó con éxito');
        return Redirect::to('configuracion/ticket_category');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $ticket_category=TicketCategory::findOrFail($id);


        return view("configuracion.ticket_category.show",["ticket_category"=>$ticket_category]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket_category=TicketCategory::findOrFail($id);
        return view("configuracion.ticket_category.edit",["ticket_category"=>$ticket_category]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketCategoryFormRequest $request, $id)
    {
  

        $ticket_category=TicketCategory::findOrFail($id);
        $ticket_category->name=$request->get('name');
        $ticket_category->description=$request->get('description');
        $ticket_category->update();
        Session::flash('update_ticket_category','La categoría '.$ticket_category->name. ' ha sido actualizado con éxito');
        return Redirect::to('configuracion/ticket_category');    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket_category=TicketCategory::findOrFail($id);
        try{
            TicketCategory::destroy($id);
            $ticket_category->update();
            Session::flash('delete_ticket_category','La categoría '.$ticket_category->name. ' ha sido eliminado correctamente');
            return Redirect::to('configuracion/ticket_category');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_ticket_category_error','La categoría '.$ticket_category->name. ' no puede ser eliminado');
            return Redirect::to('configuracion/ticket_category');

        }
    }
}
