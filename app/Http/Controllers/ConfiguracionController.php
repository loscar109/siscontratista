<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;
use siscontratista\Configuracion;

use Illuminate\Support\Facades\Redirect;
use DB;
use Session;

class ConfiguracionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('configuracion.index');
    }
    

}