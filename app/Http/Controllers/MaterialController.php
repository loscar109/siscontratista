<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Material;
use siscontratista\Category;

use siscontratista\Movement;

use Barryvdh\DomPDF\Facade as PDF;




use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\MaterialFormRequest;
use Illuminate\Support\Facades\Input;

use DB;
use Session;
use Response;
use Illuminate\Support\Collection;


class MaterialController extends Controller
{

    public function __construct()
    {
       
        $this->middleware('auth');
       
    }

    public function exportPdf()
    {
        //dd($_GET['ticket_category_id']);

        if (isset($_GET['category_id']))
            $materials=Material::whereCategory_id($_GET['category_id'])->get();
        else {
            $materials = Material::all();
        }
        

        $pdf = PDF::loadView('materials.gestion.pdf',["materials"=> $materials]);
        $pdf->setPaper('a4','letter');

        return $pdf->stream('material-list-pdf');
    }

  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories=Category::all();

        if($request->category_id)
        {
            $materials=Material::whereCategory_id($request->category_id)->get();
            $category_id=$request->category_id;

        }
        else
        {
            $materials=Material::all();
            $category_id=null;
        }

        
        return view('materials.gestion.index',["materials"=> $materials, "category_id"=>$category_id,"categories"=> $categories]);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();


        return view("materials.gestion.create",["categories"=>$categories]);


    }

   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialFormRequest $request)
    {

        $material = new Material;
        $material->description=$request->get('description');
        $material->category_id=$request->get('category_id');
        $material->stock=$request->get('stock');
        $material->smin=$request->get('smin');
        $material->smax=$request->get('smax');
        $material->bundle_quantity=$request->get('bundle_quantity');
        $material->bundle_name=$request->get('bundle_name') . " (". $material->bundle_quantity . " " . $material->category->medida . ")";

        //$material->bundle_name=$request->get('bundle_name');
        if(Input::hasFile('photo')){
            $file=Input::file('photo');
            //$file->move(public_path().'/imagenes/materiales/',$file->getClientOriginalName());
            //$file->move(public_path().'/imagenes/materiales/',auth()->id().'.pdf');
            //$file->move(public_path().'/imagenes/materiales/',auth()->id().'pdf');
            $file->move(public_path().'/imagenes/materiales/',$file->getClientOriginalName());
            $material->photo=$file->getClientOriginalName();

            //$material->photo=auth()->id().'.pdf';
        }
       
        $material->save();

        //carga de un movimiento cuando se crea un material
        $movimiento = new Movement;
        $movimiento->date=$material->created_at->format('Y-m-d');
        $movimiento->comprobante='Carga inicial';
        $movimiento->number='';
        $movimiento->quantity= $material->stock;
        $movimiento->stock= $material->stock;
        $movimiento->user_id=auth()->id();
        $movimiento->material_id=$material->id;
        $movimiento->save();





        Session::flash('store_material','El material '.$material->description. ' se creó con éxito');
        return Redirect::to('materials/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $tickets=Ticket::findOrFail($id);


        return view("tickets.gestion.show",["tickets"=>$tickets]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tickets=Ticket::findOrFail($id);
        $ticket_categories=TicketCategory::all();
        return view("tickets.gestion.edit",["tickets"=>$tickets,"ticket_categories"=>$ticket_categories]);

    }

    public function movimientos(Request $request, $id)
    {
        //obtengo el material
        $material=Material::findOrFail($id);

        $comprobante =$request->comprobante;
        $desde=$request->desde;
        $hasta=$request->hasta;
        $sql = Movement::whereMaterial_id($id);

        if(count($request->all())>0)
        {
            if($comprobante)
            {
                $sql = $sql->whereComprobante($comprobante);
            }
            if($desde)
            {
                $sql = $sql->whereDate('created_at','>=',$desde);
            }            
            if($hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$hasta);
            }
        }
       
        $movimientos=$sql->orderBy('created_at','desc')->get();
     
        return view("materials.gestion.movimientos",[
            "material"     =>  $material,
            "movimientos"  =>  $movimientos,
            "comprobante"  =>   $comprobante,
            "desde"        =>  $desde,
            "hasta"        =>  $hasta,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketFormRequest $request, $id)
    {
   

        $tickets=Ticket::findOrFail($id);
        $tickets->description=$request->get('description');
        $tickets->ticket_category_id=$request->get('ticket_category_id');
        $tickets->update();
        Session::flash('update_ticket','El ticket '.$tickets->description. ' ha sido actualizado con éxito');
        return Redirect::to('tickets/gestion'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tickets=Ticket::findOrFail($id);
        try{
            Ticket::destroy($id);
            $tickets->update();
            Session::flash('delete_ticket','El ticket '.$tickets->description. ' ha sido eliminado correctamente');
            return Redirect::to('tickets/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_ticket_error','El ticket '.$tickets->description. ' no puede ser eliminado');
            return Redirect::to('tickets/gestion');

        }
    }
}
