<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;


use siscontratista\Workflow;
use siscontratista\TicketStage;
use siscontratista\TicketStageOrder;





use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\WorkFlowFormRequest;
use Illuminate\Support\Facades\Input;

use DB;
use Session;

class WorkFlowController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $workflows=Workflow::all();
        $ticket_stages=TicketStage::all();
        return view('configuracion.workflow.index',["workflows"=>$workflows,"ticket_stages"=>$ticket_stages]);


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Necesitamos los Estados de los Tickets
        $ticket_stages=TicketStage::all();

        return view("configuracion.workflow.create",["ticket_stages"=>$ticket_stages]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkFlowFormRequest $request)
    {
        //dd($request->all());
        //creo el nombre y descripcion del flujo de trabajo y lo guardo
        $workflow=new Workflow;
        $workflow->name=$request->get('name');
        $workflow->description=$request->get('description');
        $workflow->initial_stage_id=$request->get('initial_stage_id');

        $workflow->save();


        $current_id = $request->get('current_id');
        $next_id = $request->get('next_id');
        $cont = 0;
        while ($cont < count($current_id)) 
        {
            $order=new TicketStageOrder;
            $order->workflow_id=$workflow->id;
            $order->current_id=$current_id[$cont];
            $order->next_id=$next_id[$cont];

            $order->save();
            $cont=$cont+1;
        }
        /*
            Cada vez que agrego un dato de la columno origen, este dato enviar al select2 del modal
        */ 

        Session::flash('store_workflow','El Flujo de Trabajo '.$workflow->name. ' se creó con éxito');
        return Redirect::to('configuracion/workflow');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $workflow=Workflow::findOrFail($id);
        return view("configuracion.workflow.show",["workflow"=>$workflow]);

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workgroups=WorkGroup::findOrFail($id);
        $users=User::all();
        return view("workgroups.gestion.edit",["workgroups"=>$workgroups,"users"=>$users]);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkGroupFormRequest $request, $id)
    {
   

        $workgroups=WorkGroup::findOrFail($id);
        $workgroups->name=$request->get('name');
        $workgroups->update();
        Session::flash('update_workgroup','El Grupo de Trabajo '.$workgroups->name. ' ha sido actualizado con éxito');
        return Redirect::to('workgroups/gestion'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workflows=WorkFlow::findOrFail($id);
        try{
            WorkFlow::destroy($id);
            //$workflows->update();
            Session::flash('delete_workflow','El Flujo de Trabajo '.$workflows->name. ' ha sido eliminado correctamente');
            return Redirect::to('configuracion/workflow');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_workflow_error','El Flujo de Trabajo '.$workflows->name. ' no puede ser eliminado');
            return Redirect::to('configuracion/workflow');

        }
    }
}
