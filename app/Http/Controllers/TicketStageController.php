<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\TicketStage;




use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\TicketStageFormRequest;

use DB;
use Session;

class TicketStageController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $ticket_stages = TicketStage::all();
        
        return view('configuracion.ticket_stage.index',["ticket_stages"=>$ticket_stages]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.ticket_stage.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketStageFormRequest $request)
    {
    


        $ticket_stages = TicketStage::create($request->all());
        $ticket_stages->name=$request->get('name');
        $ticket_stages->description=$request->get('description');
        $ticket_stages->save();


        Session::flash('store_ticket_stage','El estado de Ticket '.$ticket_stages->name. ' se creó con éxito');
        return Redirect::to('configuracion/ticket_stage');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $ticket_stages=TicketStage::findOrFail($id);


        return view("configuracion.ticket_stage.show",["ticket_stages"=>$ticket_stages]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket_stages=TicketStage::findOrFail($id);
        return view("configuracion.ticket_stage.edit",["ticket_stages"=>$ticket_stages]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketStageFormRequest $request, $id)
    {
  

        $ticket_stages=TicketStage::findOrFail($id);
        $ticket_stages->name=$request->get('name');
        $ticket_stages->description=$request->get('description');
        $ticket_stages->update();
        Session::flash('update_ticket_stage','El estado de ticket '.$ticket_stages->name. ' ha sido actualizado con éxito');
        return Redirect::to('configuracion/ticket_stage');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket_stage=TicketStage::findOrFail($id);
        try{
            TicketStage::destroy($id);
            $ticket_stage->update();
            Session::flash('delete_ticket_stage','El estado de Ticket '.$ticket_stage->name. ' ha sido eliminado correctamente');
            return Redirect::to('configuracion/ticket_stage');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_ticket_stage_error','El estado de Ticket '.$ticket_stage->name. ' no puede ser eliminado');
            return Redirect::to('configuracion/ticket_stage');

        }
    }
}
