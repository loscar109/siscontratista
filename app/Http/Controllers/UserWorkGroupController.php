<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Ticket;
use siscontratista\User;
use siscontratista\WorkGroup;
use siscontratista\UserWorkGroup;
use siscontratista\TicketCategory;
use Barryvdh\DomPDF\Facade as PDF;





use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\UserWorkGroupFormRequest;
use siscontratista\Http\Requests\IndexFormRequest;

use Illuminate\Support\Facades\Input;

use DB;
use Session;

class UserWorkGroupController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }

    public function exportPdf($workgroups, $desde, $hasta, $category)
    {
       
        $pdf = PDF::loadView('workgroups.gestion.pdf',[
            "workgroups"    =>  $workgroups,
            "category"      =>  $category,
            "desde"         =>  $desde,
            "hasta"         =>  $hasta
            ]);

        $pdf->setPaper('a4','letter');


        return $pdf->download('workgroup-list-pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexFormRequest $request)
    {
        
        $ticket_categories=TicketCategory::all(); //obteneme todas las categorias
       
        $desde=$request->desde; //almacenamos la fecha desde (si hay request)
        $hasta=$request->hasta; //almacenamos la fecha hasta (si hay request)

 
        
        if(count($request->all())>1) //si existe algun request(es decir, si uso el "Filtrar")
        {
            //dd($request->all());
            $sql = WorkGroup::select('work_groups.*'); //inicio la consulta sobre una determinada tabla

            if($request->ticket_category_id) //si el request proviene de la categoria del ticket
            {
                
                $sql = $sql->whereTicket_category_id($request->ticket_category_id); //creo la consulta y almaceno en "sql"
            }
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=', $request->desde); //creo la consulta y almaceno en "sql"
            }
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta); //creo la consulta y almaceno en "sql"
            }
            
            
            $workgroups=$sql->orderBy('created_at','desc')->get(); //ejecuto la consulta
            $ticket_category_id=$request->ticket_category_id; //mantengo el id de la categoria del tiquet


        }
        else //si nunca filtre, (si no existió request)
        {
            
            $ticket_category_id=null; //en el select2 que me aparesca " -- Todas las Categorias --"
            $workgroups=WorkGroup::orderBy('created_at','desc')->get(); //que me obtenga directamente todos los grupos


        }
        if($request->exists('pdf'))
        {
            if($ticket_category_id != NULL)
            {
                $category=TicketCategory::find($ticket_category_id);

            }
            else 
            {
                $category=NULL;
            }
            return $this->exportPdf($workgroups, $desde, $hasta, $category);
        }

        return view('workgroups.gestion.index',[
            "workgroups"            =>  $workgroups,         //los grupos de trabajo
            "ticket_category_id"    =>  $ticket_category_id, //si los id son identicos que me mantenga el valor
            "ticket_categories"     =>  $ticket_categories,  //las categorias asociadas al grupo de trabajo
            "desde"                 =>  $desde,              //fecha desde utilizada en la vista
            "hasta"                 =>  $hasta               //fecha hasta utilizada en la vista

            ]);


            /*
        $categories=TicketCategory::all();
        $workgroups=WorkGroup::orderBy('created_at','desc')->get();

        if($request->ticket_category_id)
        {

            $workgroups->where('ticket_category_id','=',$request->categories)->get();
            
        };
        

        
        return view('workgroups.gestion.index',[
            "workgroups"            =>  $workgroups,
            "categories"            =>  $categories,
            ]);
        */
    }


    public function graph(Request $request, $id)
    {
        $workgroup = WorkGroup::findOrFail($id);
        $tickets = Ticket::whereWorkgroup_id($workgroup->id)->get(); 
        $month = $request->month;
        $year = $request->year;

        $resolved_tickets = Ticket::whereWorkgroup_id($workgroup->id)
            ->where('current_stage_id','=',4)
            ->whereMonth('updated_at',$month)
            ->whereYear('updated_at',$year)
            ->orderBy('updated_at')
            ->get();

        $no_resolved_tickets = Ticket::whereWorkgroup_id($workgroup->id)
            ->where('current_stage_id','=',5)
            ->whereMonth('updated_at',$month)
            ->whereYear('updated_at',$year)
            ->orderBy('updated_at')
            ->get();

        $count_resolved_tickets = $resolved_tickets->count();
        $count_no_resolved_tickets =  $no_resolved_tickets->count();
        $days=array();
        $days_agruped=array();

        foreach ($resolved_tickets as $resolved_ticket) 
        {
            $days[]=$resolved_ticket->time_resolved;    
        }
        $days_agruped=array_count_values($days);


    



        return view('workgroups.gestion.graph',[
            'resolved_tickets'          =>  $resolved_tickets,
            'no_resolved_tickets'       =>  $no_resolved_tickets,
            "days_agruped"              =>  $days_agruped,
            "count_resolved_tickets"    =>  $count_resolved_tickets,
            "count_no_resolved_tickets" =>  $count_no_resolved_tickets,
            "month"                     =>  $month,
            "year"                      =>  $year,
            "id"                        =>  $id


            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
    
        /*$users=User::select('users.*','ru.id as rid')
        ->join('role_user as ru','ru.user_id','users.id')
        ->whereAssigned(false)
        ->where('ru.role_id',3)
        ->get();*/
        
        
        $ticket_categories=TicketCategory::all();

        $users=User::all();

        return view("workgroups.gestion.create",["users"=>$users, "ticket_categories"=>$ticket_categories]);

    }

    public function showEmployeInSelect2()
    {
        $users=User::select(
            'users.id',
            'users.name',
            'users.surname',
            'users.assigned',
            'users.photo',
            'ru.id as rid'
                )
                ->join('role_user as ru','ru.user_id','users.id')
                ->where('ru.role_id',3)
                ->whereAssigned(false)
                ->where('name','LIKE','%' . request('q') . '%')
                ->orderBy('name','asc')
                ->paginate(10);
                    
        return response()->json($users);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserWorkGroupFormRequest $request)
    {

        //creo el nombre del grupo de trabajo y busco la categoria del ticket
        $workgroup=new WorkGroup;
        $workgroup->name=$request->get('name');
        $workgroup->ticket_category_id=$request->get('ticket_category_id');
        $workgroup->save();

        $cont = 0;
        $user_id = $request->get('user_id');

        while ($cont < count($user_id)) 
        {
            $userworkgroup=new UserWorkGroup();

            $userworkgroup->workgroup_id=$workgroup->id;

            $userworkgroup->user_id =$user_id[$cont];

            
            $userworkgroup->save();

            //update recibe un array py ahi le agregamos el valor true
            User::find($user_id[$cont])->update(['assigned'=>true]);

            $cont=$cont+1;
        }

        
        /*$cont = 0;
        $user_id = $request->get('user_id');
        while ($cont < count($user_id)) 
        {
            $userworkgroup=new UserWorkGroup();
            $userworkgroup->workgroup_id=$workgroup->id;
            $userworkgroup->user_id =$user_id[$cont];

            
            $userworkgroup->save();
            //update recibe un array py ahi le agregamos el valor true
            User::find($user_id[$cont])->update(['assigned'=>true]);

            $cont=$cont+1;
        }
*/

        Session::flash('store_workgroup','El Grupo de Trabajo '.$workgroup->name. ' se creó con éxito');
        return Redirect::to('workgroups/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workgroups=WorkGroup::findOrFail($id);
        $users=User::all();
        return view("workgroups.gestion.edit",["workgroups"=>$workgroups,"users"=>$users]);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkGroupFormRequest $request, $id)
    {
   

        $workgroups=WorkGroup::findOrFail($id);
        $workgroups->name=$request->get('name');
        $workgroups->update();
        Session::flash('update_workgroup','El Grupo de Trabajo '.$workgroups->name. ' ha sido actualizado con éxito');
        return Redirect::to('workgroups/gestion'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workgroups=WorkGroup::findOrFail($id);
        try{
            WorkGroup::destroy($id);
            $workgroups->update();
            Session::flash('delete_workgroup','El Grupo de Trabajo '.$workgroups->name. ' ha sido eliminado correctamente');
            return Redirect::to('workgroups/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_workgroup_error','El Grupo de Trabajo '.$workgroups->name. ' no puede ser eliminado');
            return Redirect::to('workgroups/gestion');

        }
    }
}
