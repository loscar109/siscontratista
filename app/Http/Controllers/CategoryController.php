<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;

use siscontratista\Http\Requests;

use siscontratista\Category;





use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\CategoryFormRequest;

use DB;
use Session;

class CategoryController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $categories = Category::all();
        
        return view('configuracion.category.index',["categories"=>$categories]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.category.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFormRequest $request)
    {
    


        $category = new Category;
        
        $category->description=$request->get('description');
        $category->save();

        Session::flash('store_category','La categoria '.$category->description. ' se creó con éxito');
        return Redirect::to('configuracion/category');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $category=Category::findOrFail($id);


        return view("configuracion.category.show",["category"=>$category]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::findOrFail($id);
        return view("configuracion.category.edit",["category"=>$category]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, $id)
    {
  

        $category=Category::findOrFail($id);
        $category->description=$request->get('description');
        $ticket_category->update();
        Session::flash('update_category','La categoría '.$category->description. ' ha sido actualizado con éxito');
        return Redirect::to('configuracion/category');    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        try{
            Category::destroy($id);
            $category->update();
            Session::flash('delete_category','La categoría '.$category->description. ' ha sido eliminado correctamente');
            return Redirect::to('configuracion/category');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_category_error','La categoría '.$category->description. ' no puede ser eliminado');
            return Redirect::to('configuracion/category');

        }
    }
}
