<?php

namespace siscontratista\Http\Controllers;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Response;
use Illuminate\Support\Collection;
use Session;

class RoleController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles=DB::table('roles')->get();
        return view('roles.gestion.index',["roles"=>$roles]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $permissions=DB::table('permissions')
        ->get();
        return view("roles.gestion.create",["permissions"=>$permissions]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roles = Role::create($request->all());
        $roles->permissions()->sync($request->get('permissions'));
        Session::flash('store_role','El role '.$roles->name. ' se creó con éxito');
        return Redirect::to('roles/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

      
        $roles=DB::table('roles as ro')
            ->select('ro.id','ro.name','ro.slug','ro.description','ro.special')            
            ->where('ro.id','=',$id)
            ->groupBy('ro.id','ro.name')
            ->first();
        


        $permission_role=DB::table('permission_role as pr')
            ->join('permissions as pe','pr.permission_id','=','pe.id')
            ->join('roles as ro','pr.role_id','=','ro.id')
            ->select(
                DB::raw('CONCAT(pe.name," - ",pe.description) AS permisos')
                )
            ->where('pr.role_id','=',$id)
            ->get();

       
    return view("roles.gestion.show",["roles"=>$roles,"permission_role"=>$permission_role]);
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles=Role::findOrFail($id);
        $permissions = Permission::get();
        return view("roles.gestion.edit",["roles"=>$roles, "permissions"=>$permissions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roles=Role::findOrFail($id);
        $roles->update($request->all());
        $roles->permission()->sync($request->get('permissions'));
        return Redirect::to('roles/gestion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles=Role::findOrFail($id);
        try{
            Role::destroy($id);
            $roles->update();
            Session::flash('delete_role','El rol '.$roles->name. ' ha sido eliminado correctamente');
            return Redirect::to('roles/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_role_error','El rol '.$roles->name. ' no puede ser eliminado');
            return Redirect::to('roles/gestion');

        }
    }
}
