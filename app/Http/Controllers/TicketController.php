<?php

namespace siscontratista\Http\Controllers;

use Illuminate\Http\Request;
use siscontratista\Http\Requests;
use siscontratista\Ticket;
use siscontratista\User;
use siscontratista\TicketStage;
use siscontratista\TicketCategory;
use siscontratista\StageHistory;
use siscontratista\WorkGroup;
use siscontratista\Workflow;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Redirect;
use siscontratista\Http\Requests\TicketFormRequest;
use DB;
use Session;

class TicketController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('auth');
       
    }

    public function exportPdf($tickets, $desde, $hasta, $category, $stage, $workgroup)
    {
       
        $pdf = PDF::loadView('tickets.gestion.pdf',[
            "tickets"   =>  $tickets,
            "category"  =>  $category,
            "desde"     =>  $desde,
            "hasta"     =>  $hasta,
            "stage"     =>  $stage,
            "workgroup" =>  $workgroup

            ]);

        $pdf->setPaper('a4','letter');


        return $pdf->download('ticket-list-pdf');
    }

    /*
    */ 
  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ticket_categories=TicketCategory::all();
        $ticket_stages=TicketStage::all();
        $workgroups=WorkGroup::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Ticket::select('tickets.*');

            if($request->ticket_category_id)
            {
                $sql = $sql->whereTicket_category_id($request->ticket_category_id);
            }
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->current_stage_id)
            {
                $sql = $sql->whereCurrent_stage_id($request->current_stage_id);
            }
            if($request->workgroup_id)
            {
                $sql =$sql->whereWorkgroup_id($request->workgroup_id);
            }
            
            $tickets=$sql->orderBy('created_at','desc')->get();
            $ticket_category_id=$request->ticket_category_id;
            $current_stage_id=$request->current_stage_id;
            $workgroup_id=$request->workgroup_id;


        }
        else
        {
            $current_stage_id=null;
            $ticket_category_id=null;
            $workgroup_id=null;

            $tickets=Ticket::orderBy('created_at','desc')->get();


        }
        if($request->exists('pdf'))
        {
            $colspan = 0;
            if($ticket_category_id != NULL)
            {
                $category=TicketCategory::find($ticket_category_id);
                $colspan++;
            }
            else 
            {
                $category=NULL;
            }

            if($current_stage_id != NULL)
            {
                $stage=TicketStage::find($current_stage_id);

            }
            else 
            {
                $stage=NULL;
            }           
            if($workgroup_id != NULL)
            {
                $workgroup=WorkGroup::find($workgroup_id);

            }
            else 
            {
                $workgroup=NULL;
            }        

            return $this->exportPdf($tickets, $desde, $hasta, $category, $stage, $workgroup);
        }

        return view('tickets.gestion.index',[
            "tickets"               =>  $tickets,
            "ticket_category_id"    =>  $ticket_category_id,
            "ticket_categories"     =>  $ticket_categories,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "ticket_stages"         =>  $ticket_stages,
            "current_stage_id"      =>  $current_stage_id,
            "workgroups"            =>  $workgroups,
            "workgroup_id"          =>  $workgroup_id,

            ]);
    }

    public function detailTicket(Ticket $ticket)
    {
        //dd($ticket);
        return view("tickets.gestion.detailTicket",["ticket"=>$ticket]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ticket_category=TicketCategory::all();
        $workgroup=WorkGroup::all();

        $users=DB::table('users as us')
            ->join('role_user as ru','ru.user_id','=','us.id')
            ->join('roles as ro','ru.role_id','=','ro.id')
            ->select('us.id','us.name as usname','us.surname as usurname','us.email as usemail')
            ->where('ro.name','=','CLIENTE')
            ->get();

        


        return view("tickets.gestion.create",[
            "ticket_category"   =>  $ticket_category,
            "workgroup"         =>  $workgroup,
            "users"             =>  $users
            ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketFormRequest $request)
    {
        $n=Ticket::count() + 1;

    
        //Creo y guardo el Ticket
        $tickets = new Ticket;
        $tickets->number=str_pad($n, 10, '0', STR_PAD_LEFT);
        $tickets->description=$request->get('description');
        $tickets->ticket_category_id=$request->get('ticket_category_id');
        $tickets->priority=$request->get('priority');
        $tickets->workgroup_id=$request->get('workgroup_id');
        $tickets->client_id=$request->get('client_id');
        $tickets->detail=$request->get('detail');

        $tickets->date =now()->format('Y-m-d');


        $tickets->current_stage_id=TicketCategory::find($request->ticket_category_id)->workflow->initial_stage_id;

        $tickets->save();

        //Cuando creo un ticket voy guardando su historial
        $stage_histories=new StageHistory();
        //cargo la fecha de hoy
        $stage_histories->date =now()->format('Y-m-d H:i:s');
        //Lo asocio con el Ticket que recien cree
        $stage_histories->ticket_id=$tickets->id;
        $stage_histories->user_id = auth()->id();

        $stage_histories->ticket_stage_id=$tickets->current_stage_id;
        //Guardo el historial 
        $stage_histories->save();






        Session::flash('store_ticket','El ticket '.$tickets->description. ' se creó con éxito');
        return Redirect::to('tickets/gestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $tickets=Ticket::findOrFail($id);


        return view("tickets.gestion.show",["tickets"=>$tickets]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tickets=Ticket::findOrFail($id);
        $ticket_categories=TicketCategory::all();
        return view("tickets.gestion.edit",["tickets"=>$tickets,"ticket_categories"=>$ticket_categories]);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketFormRequest $request, $id)
    {
   

        $tickets=Ticket::findOrFail($id);
        $tickets->description=$request->get('description');
        $tickets->ticket_category_id=$request->get('ticket_category_id');
        $tickets->update();
        Session::flash('update_ticket','El ticket '.$tickets->description. ' ha sido actualizado con éxito');
        return Redirect::to('tickets/gestion'); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tickets=Ticket::findOrFail($id);
        try{
            Ticket::destroy($id);
            $tickets->update();
            Session::flash('delete_ticket','El ticket '.$tickets->description. ' ha sido eliminado correctamente');
            return Redirect::to('tickets/gestion');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_ticket_error','El ticket '.$tickets->description. ' no puede ser eliminado');
            return Redirect::to('tickets/gestion');

        }
    }
}
