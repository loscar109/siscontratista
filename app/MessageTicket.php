<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class MessageTicket extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'text',
        'image',
        'ticket_id',
        'user_id',
     
    ];

    public $timestamps=true;
        /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/
        //Un Mensaje esta asociado a un Ticket
        public function ticket()
        {
            return $this->belongsTo('siscontratista\Ticket');
        }

        public function user()
        {
            return $this->belongsTo('siscontratista\User');
        }
       
}
