<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Province extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'country_id'
        
    ];

    public $timestamps=true;

    protected $table='provinces';

    protected $primaryKey='id';

        /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/

        public function country()
        {
            return $this->belongsTo('siscontratista\Country');
        }

        public function cities()
        {
            return $this->hasMany('siscontratista\City');
        }
}
