<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TicketStageOrder extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'workflow_id',
        'current_id',
        'next_id'

    ];

    protected $table='ticket_stage_orders';

    protected $primaryKey='id';

    public $timestamps=false;

    //Existe un orden de Estado para cada Estado de Ticket
    public function current()
    {
        return $this->belongsTo('siscontratista\TicketStage');
    }
    
    public function next()
    {
        return $this->belongsTo('siscontratista\TicketStage');
    }
}
