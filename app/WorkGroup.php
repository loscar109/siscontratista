<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class WorkGroup extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'ticket_category_id',
        'average_time'
        
    ];

    protected $table='work_groups';

    protected $primaryKey='id';


    public $timestamps=true;

        /*
    
        HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo

        pelotudo acordate del nombre de la funcion "ticket_category" funciona pero si
        pones "ticket_categories" no va a funcionar

        pelotudo2 acordate si inculaste salame
        
        */

        //Un grupo de Trabajo pertenece a una cetegoria de ticket
        public function ticket_category()
        {
            return $this->belongsTo('siscontratista\TicketCategory');
        }

   
        public function user_work_groups()
        {
            return $this->hasMany('siscontratista\UserWorkGroup','workgroup_id');
        }
        
       
        public function tickets()
        {
            return $this->hasMany('siscontratista\Ticket','workgroup_id');
        }


        public function open_tickets()
        {
            return $this->tickets->where('current_stage_id','!=','4')->count();
        }
        //Cantidad de Tickets que finalziaron por Grupo de Trabajo
        public function finishedTickets()
        {
            return $this->tickets->where('current_stage_id','=','4')->count();
        }  
        //Evalua si la cantidad obtenida por finishedTickets() 
        //es de al menos 5 para que sea tomado para el tiempo promedio
        public function minExperience()
        {
            if( $this->finishedTickets() > 4 )
            {
                return true;

            }
            else
            {
                return false;
            }
        }
        //Función que calcula el tiempo estimado de realización de un ticket en (dias)
        //toma los ultimos 5 (por eso es que minExperience tiene que ser mayor a 4)
        public function estimatedTime()
        {
            

           return ceil(Ticket::whereWorkgroup_id($this->id)
                ->whereCurrent_stage_id(4)
                ->latest('updated_at')
                ->take(5)
                ->get()
                ->avg('time_resolved'));             
        }

}

