<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class RequisitionDetail extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'quantity',
        'requisition_id',
        'material_id'

    ];




    public $timestamps=false;

    protected $table='requisition_details';

    protected $primaryKey='id';


    public function material()
    {
        return $this->belongsTo('siscontratista\Material');
    }


    public function requisition()
    {
        return $this->belongsTo('siscontratista\Requisition');
    }
}
