<?php

namespace siscontratista;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\Model;

class StageHistory extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'date',
        'ticket_id',
        'ticket_stage_id',
        'user_id'

    ];

    public $dates = ['date'];
    public $timestamps=false;

    //Una Fase de Ticket tiene un Ticket asociado
    public function ticket()
    {
        return $this->belongsTo('siscontratista\Ticket');
    }
    //Una Fase de Ticket tiene un Estado de Ticket asociado
    public function ticket_stage()
    {
        return $this->belongsTo('siscontratista\TicketStage');
    }

     //Una Fase de Ticket tiene un Estado de Ticket asociado
     public function user()
     {
         return $this->belongsTo('siscontratista\User');
     }
}
