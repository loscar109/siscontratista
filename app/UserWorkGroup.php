<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class UserWorkGroup extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'user_id',
        'workgroup_id'
    ];

    protected $table='user_work_groups';

    protected $primaryKey='id';

    public $timestamps=false;

    //Un Grupo de Trabajo de Usuario puede tener un Usuario asignado
    public function user()
    {
        return $this->belongsTo('siscontratista\User');
    }

    //Un Grupo de Trabajo de Usuario puede tener un Usuario asignado
    public function workgroup()
    {
        return $this->belongsTo('siscontratista\WorkGroup');
    }

    //Un Grupo de Trabajo de Usuario puede tener un Usuario asignado
    public function ticket_category()
    {
        return $this->belongsTo('siscontratista\TicketCategory');
    }

    
}
