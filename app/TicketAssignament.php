<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TicketAssignament extends Model implements AuditableContract

{
    use Auditable;

    protected $fillable = [
        'user_id',
        'ticket_id',
        'ticket_assignament_privilege_id'

    ];

    public $timestamps=false;

    //Una Asignación de Ticket pertenece a un Privilegio de Asignación de Ticket asociado
    public function ticketAssignamentPrivilege()
    {
        return $this->belongsTo('App\Models\TicketAssignamentPrivilege');
    }

    //Una Asignación de Ticket pertenece a un Ticket asociado
    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket');
    }

    //Una Asignación de Ticket pertenece a un Usuario asociado
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
