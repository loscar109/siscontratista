<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Consumption extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'quantity',
        'dateOfConsumption',
        'ticket_id',
        'material_id'
    ];




    public $timestamps=false;

    protected $table='consumptions';

    protected $primaryKey='id';

 /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/


    public function ticket()
    {
        return $this->belongsTo('siscontratista\Ticket');
    }

    public function material()
    {
        return $this->belongsTo('siscontratista\Material');
    }
}
