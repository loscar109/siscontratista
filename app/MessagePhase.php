<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class MessagePhase extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'date',
        'message_id',
        'message_state_id',
        'user_id'

    ];

    public $dates = ['date'];
    public $timestamps=false;

    //Una Fase de Mensaje tiene un Mensaje asociado
    public function message()
    {
        return $this->belongsTo('App\Models\Message');
    }
    //Una Fase de Mensaje tiene una Etapa de Mensaje asociado
    public function messageState()
    {
        return $this->belongsTo('App\Models\MessageState');
    }
}
