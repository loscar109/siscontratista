<?php

namespace siscontratista\Console\Commands;

use Illuminate\Console\Command;
use siscontratista\Ticket;
use Carbon\Carbon;

class RegisteredTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica que el Ticket no se haya realizado en determinado tiempo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

            //toma la fecha de hoy
            $now = now();
            //trae todos los ticket que no hayan sido Resueltos
            $tickets = Ticket::where('current_stage_id','!=',4)->get();
            //Recorro los tickets que no hayan sido resueltos
            foreach($tickets as $t)
            {
                //obrengo su promedio
                $at = $t->workgroup->average_time;
                //si el promedio es diferente a nulo
                if($at != NULL)
                {
                    //se parsea la fecha 
                    $creado = Carbon::createFromFormat('Y-m-d H:i:s',$t->created_at);
                    //si la fecha de hoy es mayor a la fecha de creación del ticket; si es asi agrego los dias a la fecha de creación
                    if($now > $creado->addDays($at))
                    {
                        //si la prioridad es menor a 5 incremento
                        if($t->priority < 5)
                        {
                            $t->increment('priority',1);

                        }
                    }
                }
                    

            }
       
    }
}
