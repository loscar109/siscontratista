<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TicketStage extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'description'
    ];
    
    protected $table='ticket_stages';

    protected $primaryKey='id';

    public $timestamps=false;

    /*
        HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo
    */ 

    //Un Estado de Ticket tiene muchos estados iniciales
    public function currentStage()
    {
        return $this->hasMany('siscontratista\TicketStageOrder', 'current_id','id');
    
    }

    //Un Estado de Ticket tiene muchos estados finales
    public function nextStage()
    {
        return $this->hasMany('siscontratista\TicketStageOrder', 'next_id','id');
    }

    //

 




    //Un Estado de Ticket tiene muchas Fases de Ticket asociadas
    public function stage_histories()
    {
        return $this->hasMany('siscontratista\StageHistory');
    } 
    
    
    //Estados de los Tickets

    //Estado Creado 
    public static function getCreated()
    {
        return TicketStage::find(1);
    }

    public static function getReviewed()
    {
        return TicketStage::find(2);
    }

    public static function getInProcess()
    {
        return TicketStage::find(3);
    }

    public static function getResolved()
    {
        return TicketStage::find(4);
    }

    public static function getUnSolved()
    {
        return TicketStage::find(5);
    }

    public static function getReassigned()
    {
        return TicketStage::find(6);
    }

   

    public static function getOnlyName(){
        return static::all()->pluck('name');}

    public static function findByName($name){
        return static::where(compact('name'))->first();}

}