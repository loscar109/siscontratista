<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Category extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'description',
        'medida'
    ];




    public $timestamps=false;

    protected $table='categories';

    protected $primaryKey='id';


    //Una Categoria  puede tener muchos Materiales asociados
    public function materials()
    {
        return $this->hasMany('siscontratista\Material');
    }
}
