<?php

namespace siscontratista;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\Model;

class MessageState extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps=false;

    //Un Estado de Mensaje tiene muchos estados iniciales
    public function initialState(){
        return $this->hasMany('App\Models\MessageStateOrder', 'state_initial_id','id');}
    //Un Estado de Mensaje tiene muchos estados finales
    public function finalState(){
        return $this->hasMany('App\Models\MessageStateOrder', 'state_final_id','id');}
   //Una Estado de Mensaje puede tener muchas Fases de Ticket
        public function messagePhases()  {
        return $this->hasMany('App\Models\MessagePhase');}

}
