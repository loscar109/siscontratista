<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Workflow extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'description',
        'initial_stage_id'
    ];
    
    protected $table='workflows';

    protected $primaryKey='id';

    public $timestamps=false;

            /*
    
        HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo

        pelotudo acordate del nombre de la funcion "ticket_category" funciona pero si
        pones "ticket_categories" no va a funcionar

        pelotudo2 acordate si inculaste salame
        
        */

    //un flujo de trabajo tiene muchos ordenes de estado de ticket
    public function ticket_stage_orders()
    {
        return $this->hasMany('siscontratista\TicketStageOrder');
    }
}
