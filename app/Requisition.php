<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Requisition extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'date_of_entry',
        'user_id',
        'is_completed',
        'number'

    ];


    public $dates = ['date_of_entry'];


    public $timestamps=true;

    protected $table='requisitions';

    protected $primaryKey='id';

     /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/
    
    //Un pedido de stock puede tener mas de un recibimiento del mismo
    public function receptions()
    {
        return $this->hasMany('siscontratista\Reception');
    } 

    public function user()
    {
        return $this->belongsTo('siscontratista\User');
    }

    public function requisition_details()
    {
        return $this->hasMany('siscontratista\RequisitionDetail');
    } 
}
