<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Country extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name'
        
    ];

    public $timestamps=true;

    protected $table='countries';

    protected $primaryKey='id';


    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    public function provinces()
    {
        return $this->hasMany('siscontratista\Province');
    }
}
