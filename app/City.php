<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class City extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name',
        'province_id'
        
    ];

    public $timestamps=true;

    protected $table='cities';

    protected $primaryKey='id';

        /*HAS es si tiene el id el otro
        BELONG es si el id lo tengo yo*/

        public function province()
        {
            return $this->belongsTo('siscontratista\Province');
        }

        public function country()
        {
            return $this->province->country;
        }

        public function districts()
        {
            return $this->hasMany('siscontratista\District');
        }
}
