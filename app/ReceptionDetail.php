<?php

namespace siscontratista;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class ReceptionDetail extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'quantity',
        'material_id',
        'reception_id'
        
    ];

    public $timestamps=true;

    protected $table='reception_details';

    protected $primaryKey='id';

    public function reception()
    {
        return $this->belongsTo('siscontratista\Reception');
    }

    public function material()
    {
        return $this->belongsTo('siscontratista\Material');
    }

    public function requisition()
    {
        return $this->reception->requisition->number;
    }
}
